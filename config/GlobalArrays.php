<?php
return [

    'bread_crumps' => [
        'index' => [
            'url' => '/',
            'title_bg' => 'Начало',
            'parent_keys' => [],
        ],
        'destinations' => [
            'url' => 'destinations',
            'title_bg' => 'Дестинации',
            'parent_keys' => ['index'],
        ],
        'cruize-types' => [
            'url' => 'cruize-types',
            'title_bg' => 'Типове Круизи',
            'parent_keys' => ['index'],
        ],
        'promotions' => [
            'url' => 'promotions',
            'title_bg' => 'Промоции',
            'parent_keys' => ['index'],
        ],
        'top-cruises' => [
            'url' => 'top-cruises',
            'title_bg' => 'Топ круизи',
            'parent_keys' => ['index'],
        ],
        'company-list' => [
            'url' => 'company-list',
            'title_bg' => 'Компании',
            'parent_keys' => ['index'],
        ],
        'ship-list' => [
            'url' => 'ship-list',
            'title_bg' => 'Кораби',
            'parent_keys' => ['index'],
        ],
        'faq' => [
            'url' => 'faq',
            'title_bg' => ' Въпроси',
            'parent_keys' => ['index'],
        ],
        'about-us' => [
            'url' => 'about-us',
            'title_bg' => ' За нас',
            'parent_keys' => ['index'],
        ],
        'contacts' => [
            'url' => 'contacts',
            'title_bg' => ' Контакти',
            'parent_keys' => ['index'],
        ],
        'news' => [
            'url' => 'news',
            'title_bg' => '  Блог',
            'parent_keys' => ['index'],
        ],
        'reservation' => [
            'url' => '#',
            'title_bg' => 'Резервация',
            'parent_keys' => ['index'],
        ],

        'offer' => [
            'url' => '#',
            'title_bg' => 'Избор на каюта',
            'parent_keys' => ['index', 'reservation'],
        ],
        'offer-details' => [
            'url' => '#',
            'title_bg' => 'Деатайли за каюта',
            'parent_keys' => ['index', 'reservation'],
        ],
        'personal-data' => [
            'url' => '#',
            'title_bg' => 'Лични данни',
            'parent_keys' => ['index', 'reservation'],
        ],
        'success-reservation' => [
            'url' => '#',
            'title_bg' => 'Успешна резервация',
            'parent_keys' => ['index', 'reservation'],
        ],


    ],
    'transLiterate' => [
        'cyr' => array('а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у',
            'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',
            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У',
            'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'),
        'lat' => array('a', 'b', 'w', 'g', 'd', 'e', 'e', 'zh', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u',
            'f', 'h', 'ts', 'ch', 'sh', 'sht', 'i', 'y', 'y', 'e', 'yu', 'ya', 'A', 'B', 'V', 'G', 'D', 'E', 'E', 'Zh',
            'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U',
            'F', 'H', 'Ts', 'Ch', 'Sh', 'Sht', 'I', 'Y', 'Y', 'E', 'Yu', 'Ya')
    ],
    'news_preferred_count' => '10',
    'news_more_count' => '4',

    'ships_preferred_count' => '10',
    'ships_more_count' => '4',

    'companies_preferred_count' => '10',
    'companies_more_count' => '4',

    'destinations_preferred_count' => '10',
    'destinations_more_count' => '4',
//MORE ROUTES IN DESTINATION INNER VIEW
    'destination_routes_preferred_count' => '10',
    'destination_routes_more_count' => '4',
//MORE ROUTES IN DESTINATION INNER VIEW

    'cruise_types_preferred_count' => '10',
    'cruise_types_more_count' => '4',

    'cruise_list_on_type_preferred_count' => '10',
    'cruise_list_on_type_more_count' => '4',

    'promo_routes_preferred_count' => '10',
    'promo_routes_more_count' => '4',


    'index_page_top_routes' => '10',
    'top_routes_preferred_count' => '10',
    'top_routes_more_count' => '4',

    'monthNamed_bg' => [
        '01' => 'януари',
        '02' => 'февруари',
        '03' => 'март',
        '04' => 'април',
        '05' => 'май',
        '06' => 'юни',
        '07' => 'юли',
        '08' => 'август',
        '09' => 'септември',
        '10' => 'октомври',
        '11' => 'ноември',
        '12' => 'декември',
    ],
    'monthNamedCapitalise_bg' => [
        '01' => 'Януари',
        '02' => 'Февруари',
        '03' => 'Март',
        '04' => 'Април',
        '05' => 'Май',
        '06' => 'Юни',
        '07' => 'Юли',
        '08' => 'Август',
        '09' => 'Септември',
        '10' => 'Октомври',
        '11' => 'Ноември',
        '12' => 'Декември',
    ],
    'day_of_week_bg' => [
        '1' => 'понеделник',
        '2' => 'вторник',
        '3' => 'сряда',
        '4' => 'четвъртък',
        '5' => 'петък',
        '6' => 'събота',
        '7' => 'неделя',

    ],
    'years_dropdown_down_value' => 1970,
    'years_dropdown_up_value' => 2000,
    'price_per_cabin_multiplied_count' => 2,

    'firstDropDownSearch_bg' => [
        'route_types' => [
            0 => 'Тип круиз'
        ],
        'ports' => [
            0 => 'Пристанище'
        ],
        'durations' => [
            0 => 'Продължителност'
        ],
        'companies' => [
            0 => 'Компания'
        ],
        'destinations' => [
            0 => 'Дестинация'
        ],
        'ships' => [
            0 => 'Кораб'
        ],
        'dateToStart' => [
            0 => 'Месец на тръгване'
        ],
    ]
];