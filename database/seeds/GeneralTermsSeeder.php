<?php

use Illuminate\Database\Seeder;

class GeneralTermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('general_terms')->insert([
            'id' => '1'
        ]);
    }
}
