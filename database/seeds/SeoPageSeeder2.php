<?php

use Illuminate\Database\Seeder;

class SeoPageSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //INDEX
        DB::table('seo_pages')->insert([
            'id' => '1',
            'model_page_name' => 'Начална страница',
        ]);
//DESTINATIONS
        DB::table('seo_pages')->insert([
            'id' => '2',
            'model_page_name' => 'Дестинации',
        ]);

//        ROUTE TYPES
        DB::table('seo_pages')->insert([
            'id' => '3',
            'model_page_name' => 'Типове круизи',
        ]);
//        PROMOTIONS
        DB::table('seo_pages')->insert([
            'id' => '4',
            'model_page_name' => 'Промоционални круизи',
        ]);
//        COMPANIES
        DB::table('seo_pages')->insert([
            'id' => '5',
            'model_page_name' => 'Компании',
        ]);
//        SHIPS
        DB::table('seo_pages')->insert([
            'id' => '6',
            'model_page_name' => 'Кораби',
        ]);
//        FAQ
        DB::table('seo_pages')->insert([
            'id' => '7',
            'model_page_name' => 'Чести въпроси',
        ]);
        //        ABOUT
        DB::table('seo_pages')->insert([
            'id' => '8',
            'model_page_name' => 'За нас',
        ]);
//CONTACTS
        DB::table('seo_pages')->insert([
            'id' => '9',
            'model_page_name' => 'Конаткти',
        ]);
//NEWS
        DB::table('seo_pages')->insert([
            'id' => '10',
            'model_page_name' => 'Блог',
        ]);

//        PORTS
        DB::table('seo_pages')->insert([
            'id' => '11',
            'model_page_name' => 'Пристанища',
        ]);
        //        top-routes
        DB::table('seo_pages')->insert([
            'id' => '12',
            'model_page_name' => 'Топ круизи',
        ]);
    }
}
