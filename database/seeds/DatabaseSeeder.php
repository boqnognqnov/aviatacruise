<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AboutDataSeeder::class);
        $this->call(PointTypesSeeder::class);
        $this->call(ReservationSubscriptionSeeder::class);
        $this->call(PageContactsSeeder::class);
        $this->call(GeneralTermsSeeder::class);
        $this->call(SeoPageSeeder2::class);
    }
}
