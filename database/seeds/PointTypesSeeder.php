<?php

use Illuminate\Database\Seeder;

class PointTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('point_types')->insert([
            'id' => '1',
            'name_bg' => 'Пристанище',
            'picture' => 'port.png',
        ]);
    }
}
