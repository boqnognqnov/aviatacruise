<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => '1',
            'name' => 'aviata@admin',
            'email' => 'aviata@abv.bg',
            'password' => Hash::make("@vi@t@1!2-3+")
        ]);
    }
}
