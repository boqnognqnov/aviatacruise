<?php

use Illuminate\Database\Seeder;

class ReservationSubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('additional_info')->insert([
            'id' => '1',
            'name_bg' => 'Желая да получа информация за настаняване на деца в тази каюта',
        ]);
        
        DB::table('additional_info')->insert([
            'id' => '2',
            'name_bg' => 'Желая да получа информация за настаняване на един човек в каюта',
        ]);

        DB::table('additional_info')->insert([
            'id' => '3',
            'name_bg' => 'Желая да получа информация за настаняване на допълнителни  легла в тази каюта',
        ]);
    }
}
