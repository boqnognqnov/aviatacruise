<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoutesGallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes_gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('route_id');
            $table->string('picture');
            $table->integer('position');

            $table->string('pic_title_bg');
            $table->string('pic_alt_bg');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('routes_gallery');
    }
}
