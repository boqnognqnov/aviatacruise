<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DateRoomOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('date_room_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id');
            $table->integer('date_id');

            $table->double('price');
            $table->integer('room_discount');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('date_room_offers');
    }
}
