<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_id');

            $table->string('fname');
            $table->string('sname');
            $table->string('lname');

            $table->string('email');
            $table->string('phone');
            $table->dateTime('date_birth');
            $table->string('id_number');
            $table->boolean('is_titular')->default(false);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
    }
}
