<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_bg');
            $table->integer('company_id');
            $table->string('picture');

            $table->integer('people');
            $table->integer('tonnage');
            $table->integer('equipage_count');
            $table->integer('width');
            $table->integer('length');
            $table->integer('speed');
            $table->integer('voltage');
            $table->string('first_start');
            $table->string('flag');
            $table->string('description_title_bg');
            $table->string('description_text_bg');
            $table->string('food_title_bg');
            $table->string('food_text_bg');
            $table->string('life_title_bg');
            $table->string('life_left_text_bg');
            $table->string('life_right_text_bg');

            $table->string('slug_bg');

            $table->string('seo_title_bg');
            $table->text('seo_description_bg');

            $table->string('pic_title_bg');
            $table->string('pic_alt_bg');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ships');
    }
}
