<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class News extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->string('title_bg');
            $table->text('text_bg');
            $table->string('picture');
            $table->integer('position');
            $table->string('slug_bg');

            $table->string('seo_title_bg');
            $table->text('seo_description_bg');

            $table->string('pic_title_bg');
            $table->string('pic_alt_bg');

            $table->boolean('is_front')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
