<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShipGallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ship_gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ship_id');
            $table->string('picture');

            $table->string('pic_title_bg');
            $table->string('pic_alt_bg');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ship_gallery');
    }
}
