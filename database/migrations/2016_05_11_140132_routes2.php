<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Routes2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ship_id');
            $table->integer('route_type_id');
            $table->integer('destination_id');

            $table->string('is_front');
            $table->string('is_special');
            $table->string('route_title_bg');
            $table->string('route_slug_bg');
            $table->text('route_description_bg');
            $table->string('picture');

            $table->string('seo_title_bg');
            $table->text('seo_description_bg');

            $table->string('pic_title_bg');
            $table->string('pic_alt_bg');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('routes');
    }
}
