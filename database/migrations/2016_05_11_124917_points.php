<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Points extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_bg');
            $table->string('country_bg');
            $table->text('description_bg');
            $table->text('description_inner_bg');

            $table->string('picture');
            $table->string('coordinates');
            $table->integer('point_type_id');
            $table->string('slug_bg');

            $table->string('pic_title_bg');
            $table->string('pic_alt_bg');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('points');
    }
}
