<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageIndexSliderData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_index_slider', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title_bg');
            $table->text('text_bg');

            $table->string('button1_text_bg');
            $table->string('button2_text_bg');

            $table->string('button1_url');

            $table->string('picture');
            $table->integer('position');
            $table->boolean('is_active')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_index_slider');
    }
}
