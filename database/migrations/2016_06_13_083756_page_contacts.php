<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('coordinates');
            $table->text('address_bg');
            $table->text('mails');
            $table->text('phones');
            $table->text('fax');
            $table->text('work_time_bg');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_contacts');
    }
}
