<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelationPrompPacketToRoute2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_packet_to_route', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('packet_id');
            $table->integer('route_id');

        });

        Schema::table('promo_packet_to_route', function (Blueprint $table) {
            $table->integer('packet_id')->unsigned()->change();
            $table->foreign('packet_id')->references('id')->on('promo_packets');

            $table->integer('route_id')->unsigned()->change();
            $table->foreign('route_id')->references('id')->on('routes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promo_packet_to_route');
    }
}
