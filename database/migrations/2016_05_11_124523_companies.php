<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Companies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_bg');
            $table->string('slug_bg');
            $table->text('description_bg');
            $table->text('additional_info_bg');
            $table->string('picture');
            $table->string('logo');

            $table->string('seo_title_bg');
            $table->text('seo_description_bg');

            $table->string('pic_title_bg');
            $table->string('pic_description_bg');

            $table->string('logo_title_bg');
            $table->string('logo_description_bg');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
