<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Relations3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routes', function (Blueprint $table) {
            $table->integer('destination_id')->unsigned()->change();
            $table->foreign('destination_id')->references('id')->on('destinations');
        });

        Schema::table('routes', function (Blueprint $table) {
            $table->integer('route_type_id')->unsigned()->change();
            $table->foreign('route_type_id')->references('id')->on('routes_types');
        });

        Schema::table('routes', function (Blueprint $table) {
            $table->integer('ship_id')->unsigned()->change();
            $table->foreign('ship_id')->references('id')->on('ships');
        });


        Schema::table('ships', function (Blueprint $table) {
            $table->integer('company_id')->unsigned()->change();
            $table->foreign('company_id')->references('id')->on('companies');
        });


        Schema::table('ship_gallery', function (Blueprint $table) {
            $table->integer('ship_id')->unsigned()->change();
            $table->foreign('ship_id')->references('id')->on('ships');
        });

//        Schema::table('ship_room_types', function (Blueprint $table) {
//            $table->integer('ship_id')->unsigned()->change();
//            $table->foreign('ship_id')->references('id')->on('ships');
//        });

//        Schema::table('ship_room_types', function (Blueprint $table) {
//            $table->integer('room_id')->unsigned()->change();
//            $table->foreign('room_id')->references('id')->on('room_types');
//        });

//        Schema::table('routes_ships', function (Blueprint $table) {
//            $table->integer('ship_id')->unsigned()->change();
//            $table->foreign('ship_id')->references('id')->on('ships');
//        });
//
//        Schema::table('routes_ships', function (Blueprint $table) {
//            $table->integer('route_id')->unsigned()->change();
//            $table->foreign('route_id')->references('id')->on('routes');
//        });

//        Schema::table('deck_room_types', function (Blueprint $table) {
//            $table->integer('room_id')->unsigned()->change();
//            $table->foreign('room_id')->references('id')->on('room_types');
//        });
//
//        Schema::table('deck_room_types', function (Blueprint $table) {
//            $table->integer('deck_id')->unsigned()->change();
//            $table->foreign('deck_id')->references('id')->on('decks');
//        });

        Schema::table('room_types', function (Blueprint $table) {
            $table->integer('deck_id')->unsigned()->change();
            $table->foreign('deck_id')->references('id')->on('decks');
        });

        Schema::table('reservations', function (Blueprint $table) {
            $table->integer('room_type_id')->unsigned()->change();
            $table->foreign('room_type_id')->references('id')->on('room_types');
        });

        Schema::table('reservations', function (Blueprint $table) {
            $table->integer('route_date_id')->unsigned()->change();
            $table->foreign('route_date_id')->references('id')->on('route_dates');
        });

        Schema::table('decks', function (Blueprint $table) {
            $table->integer('ship_id')->unsigned()->change();
            $table->foreign('ship_id')->references('id')->on('ships');
        });


        Schema::table('clients', function (Blueprint $table) {
            $table->integer('reservation_id')->unsigned()->change();
            $table->foreign('reservation_id')->references('id')->on('reservations');
        });


        Schema::table('reservation_packets', function (Blueprint $table) {
            $table->integer('reservation_id')->unsigned()->change();
            $table->foreign('reservation_id')->references('id')->on('reservations');
        });

//        Schema::table('reservation_packets', function (Blueprint $table) {
//            $table->integer('company_id')->unsigned()->change();
//            $table->foreign('company_id')->references('id')->on('companies');
//        });

        Schema::table('reservation_packets', function (Blueprint $table) {
            $table->integer('packet_id')->unsigned()->change();
            $table->foreign('packet_id')->references('id')->on('promo_packets');
        });


        Schema::table('reservation_additional_info', function (Blueprint $table) {
            $table->integer('reservation_id')->unsigned()->change();
            $table->foreign('reservation_id')->references('id')->on('reservations');
        });

        Schema::table('reservation_additional_info', function (Blueprint $table) {
            $table->integer('info_id')->unsigned()->change();
            $table->foreign('info_id')->references('id')->on('additional_info');
        });


        Schema::table('date_room_offers', function (Blueprint $table) {
            $table->integer('date_id')->unsigned()->change();
            $table->foreign('date_id')->references('id')->on('route_dates');
        });


        Schema::table('date_room_offers', function (Blueprint $table) {
            $table->integer('room_id')->unsigned()->change();
            $table->foreign('room_id')->references('id')->on('room_types');
        });


        Schema::table('route_dates', function (Blueprint $table) {
            $table->integer('route_id')->unsigned()->change();
            $table->foreign('route_id')->references('id')->on('routes');
        });

        Schema::table('route_points', function (Blueprint $table) {
            $table->integer('route_id')->unsigned()->change();
            $table->foreign('route_id')->references('id')->on('routes');
        });


        Schema::table('route_points', function (Blueprint $table) {
            $table->integer('point_id')->unsigned()->change();
            $table->foreign('point_id')->references('id')->on('points');
        });


        Schema::table('routes_gallery', function (Blueprint $table) {
            $table->integer('route_id')->unsigned()->change();
            $table->foreign('route_id')->references('id')->on('routes');
        });


        Schema::table('news', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->change();
            $table->foreign('category_id')->references('id')->on('news_category');
        });


        Schema::table('news_gallery', function (Blueprint $table) {
            $table->integer('news_id')->unsigned()->change();
            $table->foreign('news_id')->references('id')->on('news');
        });

        Schema::table('points', function (Blueprint $table) {
            $table->integer('point_type_id')->unsigned()->change();
            $table->foreign('point_type_id')->references('id')->on('point_types');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
