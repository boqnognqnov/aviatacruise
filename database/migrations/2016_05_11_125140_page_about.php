<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageAbout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_about', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text_first_left_bg');
            $table->text('text_deduction_bg');
            $table->string('title1_bg');
            $table->string('title2_bg');
            $table->string('title3_bg');
            $table->string('slide_img');
            $table->text('slide_text_bg');
            $table->string('content_img');
            $table->string('content_img_front');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_about');
    }
}
