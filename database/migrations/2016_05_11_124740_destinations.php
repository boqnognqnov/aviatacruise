<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Destinations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destinations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_bg');
            $table->string('slug_bg');
            $table->string('description_bg');
            $table->string('picture');

            $table->string('seo_title_bg');
            $table->text('seo_description_bg');

            $table->string('pic_title_bg');
            $table->string('pic_alt_bg');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('destinations');
    }
}
