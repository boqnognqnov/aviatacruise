<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RouteDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('route_id');
            $table->dateTime('date');
            $table->dateTime('end_date');
            $table->integer('discount');
            $table->boolean('is_disabled')->default(false);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('route_dates');
    }
}
