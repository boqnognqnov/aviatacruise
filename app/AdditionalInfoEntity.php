<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalInfoEntity extends Model
{
    protected $table = 'additional_info';

    public static $originalImagePath = 'uploads/images/reservations/original';
    public static $rules = array(
        'name_bg' => 'required',
        'description_bg' => 'required',
//        'picture' => 'image',
    );

    public static function createUpdate($data)
    {
        try {

            $rules = self::$rules;

            $validator = \Validator::make($data, $rules);

            if ($validator->fails()) {

                return array('validationError', $validator);
            }

            $record = AdditionalInfoEntity::findOrNew($data['id']);

            $record->name_bg = $data['name_bg'];
            $record->description_bg = $data['description_bg'];

            if (isset($data['picture'])) {

                if (!empty($record->picture)) {

                    File::delete(self::$originalImagePath . $record->picture);
                }

                $fileName = time() . str_random(10) . '.' . $data['picture']->getClientOriginalExtension();
                $image = Image::make($data['picture']->getRealPath());

                $image->save(self::$originalImagePath . $fileName);
                $record->picture = $fileName;
            }

            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }

    public static function destroyInfoType($typeId)
    {
        try {
            ReservationAdditionalInfoEntity::where('info_id', '=', $typeId)->delete();
            AdditionalInfoEntity::destroy($typeId);
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', 'success');
    }


}
