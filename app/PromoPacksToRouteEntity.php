<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class PromoPacksToRouteEntity extends Model
{
    protected $table = 'promo_packet_to_route';

    public $timestamps = false;


    public static function setThisPacket($routeId, $packetId)
    {
        try {
            $packetToRouteRecord = PromoPacksToRouteEntity::where('packet_id', '=', $packetId)
                ->where('route_id', '=', $routeId)
                ->first();

            if (!$packetToRouteRecord) {
                $packetToRouteRecord = new PromoPacksToRouteEntity();
                $packetToRouteRecord->route_id = $routeId;
                $packetToRouteRecord->packet_id = $packetId;
                $packetToRouteRecord->save();

            } else {
                $packetToRouteRecord->delete();
            }


        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', 'success');
    }
}
