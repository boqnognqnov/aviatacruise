<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class SeoStaticPagesEntity extends Model
{

    protected $table = 'seo_pages';
    public $timestamps = false;

    public static function newsRules()
    {
        return array(
//            'picture' => 'image',
            'seo_title_bg' => 'required',
            'seo_description_bg' => 'required',
        );
    }


    public static function updatePageData($data)
    {
        try {
            $rules = self::newsRules();


            $validator = \Validator::make($data, $rules);
            if ($validator->fails()) {
                return array('validationError', $validator);
            }


            $record = SeoStaticPagesEntity::find($data['page_id']);

            $record->seo_title_bg = $data['seo_title_bg'];
            $record->seo_description_bg = $data['seo_description_bg'];

            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }


}
