<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class PageContactsEntity extends Model
{
    protected $table = 'page_contacts';

    public $timestamps = false;


    public static $rules = array(

        'coordinates' => 'required',
        'address_bg' => 'required',
        'mails' => 'required',
        'phones' => 'required',
//        'fax' => 'required',
        'work_time_bg' => 'required',
    );


    public static function updatePage($data)
    {
        try {

            $record = PageContactsEntity::first();
            $rules = self::$rules;

            $validator = \Validator::make($data, $rules);
            if ($validator->fails()) {
                return array('validationError', $validator);
            }


            $record->coordinates = $data['coordinates'];
            $record->address_bg = $data['address_bg'];
            $record->mails = $data['mails'];
            $record->phones = $data['phones'];
            if (isset($data['fax'])) {
                $record->fax = $data['fax'];
            }
            $record->work_time_bg = $data['work_time_bg'];

            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }
}
