<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryQuestionsEntity extends Model
{
    protected $table = 'question_categories';



    public
    static function rules()
    {
        return array(
            'name_bg' => 'required'
        );
    }

    public
    static function createUpdate($data)
    {
//        try {

            $validator = \Validator::make($data, self::rules());
            if ($validator->fails()) {
                return array('validationError', $validator);
            }

            $record = CategoryQuestionsEntity::findOrNew($data['id']);
            $record->name_bg = $data['name_bg'];
            $record->slug_bg = str_slug($data['name_bg']);

            $record->save();

//        } catch (\Exception $ex) {
//            \Log::error($ex);
//            return array('runtimeError', $ex);
//        }
        return array('success', $record);

    }


    public
    static function destroyCategory($categoryId)
    {
        try {

            QuestionsEntity::where('category_id', '=', $categoryId)->delete();

            CategoryQuestionsEntity::destroy($categoryId);

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');
    }
}
