<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryNewsEntity extends Model
{
    protected $table = 'news_category';


    public static function getCategory($categoryId)
    {
        return CategoryNewsEntity::find($categoryId)->name_bg;
    }

    public
    static function rules()
    {
        return array(
            'name_bg' => 'required'
        );
    }

    public
    static function createUpdate($data)
    {
        try {

            $validator = \Validator::make($data, self::rules());
            if ($validator->fails()) {
                return array('validationError', $validator);
            }

            $record = CategoryNewsEntity::findOrNew($data['category_id']);
            $record->name_bg = $data['name_bg'];
            $record->slug_bg = str_slug($data['name_bg']);

            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);

    }


    public
    static function destroyCategory($categoryId)
    {
        try {

            $newsOnThisCategory = NewsEntity::where('category_id', '=', $categoryId)->get();
            foreach ($newsOnThisCategory as $oneNews) {

                $gallery = NewsGalleryEntity::where('news_id', '=', $oneNews->id)->get();
                foreach ($gallery as $oneImgGallery) {
                    NewsGalleryEntity::destroyImg($oneImgGallery->id);
                }

                NewsEntity::destroyNews($oneNews->id);
            }

            CategoryNewsEntity::destroy($categoryId);

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');
    }
}
