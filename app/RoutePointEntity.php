<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoutePointEntity extends Model
{
    protected $table = 'route_points';

    public static function setRouteConnections($data)
    {

        RoutePointEntity::where('route_id', '=', $data['route_id'])->delete();

        $sequenceCounter = 1;
        foreach ($data['pointsData'] as $onePointData) {
            $record = new RoutePointEntity();

            $record->route_id = $data['route_id'];

            $record->point_id = $onePointData['point_id'];
            $record->day = $onePointData['day'];
            $record->sequence = $sequenceCounter;
            $record->time_arrival = $onePointData['time_arrival'];
            $record->time_departure = $onePointData['time_departure'];

            $record->save();

            $sequenceCounter++;
        }

    }


}
