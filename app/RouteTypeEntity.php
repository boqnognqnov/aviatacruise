<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class RouteTypeEntity extends Model
{
    protected $table = 'routes_types';
    public static $path = 'uploads/images/routesTypes/';

    public static function rules()
    {
        return array(
            'name_bg' => 'required',
            'text_bg' => 'required',
            'picture' => 'image',
        );
    }

    public static function createUpdate($data)
    {
        try {

            $rules = self::rules();

            if ($data['route_type_id'] == 0) {

                $rules['picture'] = 'image|required';
            }

            $validator = \Validator::make($data, $rules);

            if ($validator->fails()) {

                return array('validationError', $validator);
            }

            $record = RouteTypeEntity::findOrNew($data['route_type_id']);

            $record->name_bg = $data['name_bg'];
            $record->slug_bg = str_slug($data['name_bg']);

            $record->text_bg = $data['text_bg'];

            if (isset($data['picture'])) {

                if (!empty($record->picture)) {

                    File::delete(self::$path . $record->picture);
                }

                $fileName = time() . str_random(10) . '.' . $data['picture']->getClientOriginalExtension();
                $image = Image::make($data['picture']->getRealPath());
                $image->resize(720, 450);
                $image->save(self::$path . $fileName);
                $record->picture = $fileName;
            }

            if (isset($data['seo_title_bg'])) {
                $record->seo_title_bg = $data['seo_title_bg'];
            }

            if (isset($data['seo_description_bg'])) {
                $record->seo_description_bg = $data['seo_description_bg'];
            }

            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }


    public static function destroyRouteТype($routeTypeId)
    {
        try {
//            return dump($routeTypeId);
            $record = RouteTypeEntity::find($routeTypeId);

            $routes = RouteEntity::where('route_type_id', $routeTypeId)->get();

            foreach ($routes as $route) {

                RouteEntity::destroyRoute($route->id);
            }

            $record->delete();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', 'success');
    }

    public static function setPictureSeoData($data)
    {
        try {
            $record = RouteTypeEntity::find($data['route_type_id']);
            $record->pic_title_bg = $data['pic_title_bg'];
            $record->pic_alt_bg = $data['pic_alt_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }
}
