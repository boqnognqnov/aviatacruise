<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class ShipEntity extends Model
{

    protected $table = 'ships';

    public static $path = 'uploads/images/ships/';
    public static $rules = array(
        'name_bg' => 'required',
        'company_id' => 'required',
        'picture' => 'image',
    );

    public static function createUpdate($data)
    {
        try {

            $rules = self::$rules;

            if ($data['ship_id'] == 0) {

                $rules['picture'] = 'image|required';
            }

            $validator = \Validator::make($data, $rules);

            if ($validator->fails()) {

                return array('validationError', $validator);
            }

            $record = ShipEntity::findOrNew($data['ship_id']);

            $record->name_bg = $data['name_bg'];
            $record->slug_bg = str_slug($data['name_bg']);


            $record->company_id = $data['company_id'];

            if (isset($data['picture'])) {

                if (!empty($record->picture)) {

                    File::delete(self::$path . $record->picture);
                }

                $fileName = time() . str_random(10) . '.' . $data['picture']->getClientOriginalExtension();
                $image = Image::make($data['picture']->getRealPath());
                $image->resize(720, 450);

                $image->save(self::$path . $fileName);
                $record->picture = $fileName;
            }


            $record->people = $data['people'];
            $record->tonnage = $data['tonnage'];
            $record->equipage_count = $data['equipage_count'];
            $record->width = $data['width'];
            $record->length = $data['length'];
            $record->speed = $data['speed'];
            $record->voltage = $data['voltage'];
            $record->first_start = $data['first_start'];
            $record->flag = $data['flag'];
            $record->description_title_bg = $data['description_title_bg'];
            $record->description_text_bg = $data['description_text_bg'];
            $record->food_title_bg = $data['food_title_bg'];
            $record->food_text_bg = $data['food_text_bg'];
            $record->life_title_bg = $data['life_title_bg'];
            $record->life_left_text_bg = $data['life_left_text_bg'];
            $record->life_right_text_bg = $data['life_right_text_bg'];

            if (isset($data['seo_title_bg'])) {
                $record->seo_title_bg = $data['seo_title_bg'];
            }

            if (isset($data['seo_description_bg'])) {
                $record->seo_description_bg = $data['seo_description_bg'];
            }

            $record->save();

        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }


    public static function destroyShip($shipId)
    {
        try {

            $record = ShipEntity::find($shipId);

            File::delete(self::$path . $record->picture);

            $images = ShipGalleryEntity::where('ship_id', $shipId)->get();

            foreach ($images as $image) {

                ShipGalleryEntity::destroyImg($image->id);
            }

            $routesWithThisShip = RouteEntity::where('ship_id', '=', $shipId)->get();
            foreach ($routesWithThisShip as $oneRoute) {
                RouteEntity::destroyRoute($oneRoute->id);
            }

            $decsOnThisShip = DeckEntity::where('ship_id', '=', $shipId)->get();
            foreach ($decsOnThisShip as $oneDeck) {
                $room = RoomTypeEntity::where('deck_id', '=', $oneDeck->id)->first();
                if ($room) {
                    RoomTypeEntity::destroyRoomType($room->id);
                }

                DeckEntity::destroyDeck($oneDeck->id);
            }


            $record->delete();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', 'success');
    }

    public static function setPictureSeoData($data)
    {
        try {
            $record = ShipEntity::find($data['ship_id']);
            $record->pic_title_bg = $data['pic_title_bg'];
            $record->pic_alt_bg = $data['pic_alt_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }
}
