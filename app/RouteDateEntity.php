<?php

namespace App;

use App\Classes\GlobalFunctions;
use Illuminate\Database\Eloquent\Model;

class RouteDateEntity extends Model
{
    protected $table = 'route_dates';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public static function setDate($data)
    {
        try {
            $record = RouteDateEntity::findOrNew($data['id']);

            $record->route_id = $data['route_id'];

            $record->date = GlobalFunctions::convertStringToTime($data['date']);
            $record->end_date = GlobalFunctions::convertStringToTime($data['end_date']);

            $record->discount = $data['discount'];


            $record->save();

            $dateId = 0;
            if (isset($data['shipRooms'])) {
                $dateData = DateRoomOfferEntity::createUpdate($data['shipRooms'], $record->id);
                $dateId = $dateData[1]->date_id;
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        $record->date_id = $dateId;
        return array('success', $record);
    }

    public static function changeStatus($routeDateId)
    {
        try {

            $record = RouteDateEntity::find($routeDateId);

            if ($record->is_disabled == false) {
                $record->is_disabled = true;
            } else {
                $record->is_disabled = false;
            }


            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }


    public static function destroyRouteEvent($routeDateId)
    {
        try {

            DateRoomOfferEntity::where('date_id', '=', $routeDateId)->delete();

            $reservations = ReservationsEntity::where('route_date_id', '=', $routeDateId)->get();

            foreach ($reservations as $oneReservation) {

                ReservationsEntity::destroyReservation($oneReservation->id);
            }

            RouteDateEntity::destroy($routeDateId);

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');
    }
}
