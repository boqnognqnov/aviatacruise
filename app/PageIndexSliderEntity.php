<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class PageIndexSliderEntity extends Model
{
    protected $table = 'page_index_slider';

    public static $path = 'uploads/images/sliderIndexPage/';


    public static $rules = array(
        'title_bg' => 'required',
        'text_bg' => 'required',
        'button1_text_bg' => 'required',
        'button1_url' => 'required|url',
        'picture' => 'image',
    );


    public static function createUpdate($data)
    {

        $rules = self::$rules;
        if ($data['id'] == 0) {
            $rules['picture'] = 'required|image';
        }

        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        try {

            $record = PageIndexSliderEntity::findOrNew($data['id']);

            $record->title_bg = $data['title_bg'];
            $record->text_bg = $data['text_bg'];
            $record->button1_text_bg = $data['button1_text_bg'];
            $record->button1_url = $data['button1_url'];


            if (isset($data['picture'])) {
                if (!empty($record->picture)) {

                    File::delete(self::$path . $record->picture);
                }

                $fileName = time() . str_random(10) . '.' . $data['picture']->getClientOriginalExtension();
                $image = Image::make($data['picture']->getRealPath());
                $image->resize(735, 490);
                $image->save(self::$path . $fileName);
                $record->picture = $fileName;
            }


            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }

    public static function destroySlider($sliderId)
    {
        try {

            $record = PageIndexSliderEntity::find($sliderId);

            if (!empty($record->picture)) {
                File::delete(self::$path . $record->picture);
            }

            $record->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');
    }

}
