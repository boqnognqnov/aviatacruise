<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class ShipGalleryEntity extends Model
{
    protected $table = 'ship_gallery';

    public static $path = 'uploads/images/shipsGallery/';


    public static function uploadImgs($data)
    {
        try {

            foreach ($data['pictures'] as $onePicture) {
                $record = new ShipGalleryEntity();

                $record->ship_id = $data['ship_id'];

                $fileName = time() . str_random(10) . '.' . $onePicture->getClientOriginalExtension();
                $image = Image::make($onePicture->getRealPath());

                $image->resize(1900, 500);

                $image->save(self::$path . $fileName);
                $record->picture = $fileName;


                $record->save();
            }

        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', 'success');
    }


    public static function destroyImg($imgId)
    {
        try {

            $record = ShipGalleryEntity::find($imgId);

            File::delete(self::$path . $record->picture);

            $record->delete();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', 'success');
    }

    public static function setPictureSeoData($data)
    {
        try {
            $record = ShipGalleryEntity::find($data['pic_id']);
            $record->pic_title_bg = $data['pic_title_bg'];
            $record->pic_alt_bg = $data['pic_alt_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }
}
