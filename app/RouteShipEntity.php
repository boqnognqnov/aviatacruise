<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RouteShipEntity extends Model
{
    protected $table = 'routes_ships';

    public static $rules = array(
        'ship_id' => 'required',
        'route_id' => 'required',
    );

    public static function createUpdate($data)
    {
        try {

            $rules = self::$rules;

            $validator = \Validator::make($data, $rules);

            if ($validator->fails()) {

                return array('validationError', $validator);
            }

            $record = RouteShipEntity::findOrNew($data['id']);

            $record->ship_id = $data['ship_bg'];
            $record->route_id = $data['route_id'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }


}
