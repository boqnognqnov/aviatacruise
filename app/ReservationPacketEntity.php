<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class ReservationPacketEntity extends Model
{
    protected $table = 'reservation_packets';

    public static $rules = array(
        'company_id' => 'required',
        'reservation_id' => 'required',
        'packet_id' => 'required',
    );

    public static function createUpdate($data)
    {
        try {

            $rules = self::$rules;

            $validator = \Validator::make($data, $rules);

            if ($validator->fails()) {

                return array('validationError', $validator);
            }

            $record = ReservationPacketEntity::findOrNew($data['id']);

//            $record->company_id = $data['company_id'];
            $record->reservation_id = $data['reservation_id'];
            $record->packet_id = $data['packet_id'];

            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }


}
