<?php
//
//namespace App;
//
//use Illuminate\Database\Eloquent\Model;
//
//class ShipInfoEntity extends Model
//{
//    protected $table = 'ship_info';
//
//    public static $rules = array(
//        'ship_id' => 'required',
//        'people' => 'required',
//        'tonnage' => 'required',
//        'equipage_count' => 'required',
//        'width' => 'required',
//        'length' => 'required',
//        'speed' => 'required',
//        'voltage' => 'required',
//        'first_start' => 'required',
//        'flag' => 'required',
//        'description_title_bg' => 'required',
//        'description_text_bg' => 'required',
//        'food_title_bg' => 'required',
//        'food_text_bg' => 'required',
//        'life_title_bg' => 'required',
//        'life_left_text_bg' => 'required',
//        'life_right_text_bg' => 'required',
//    );
//
//    public static function getShipInfo($shipId)
//    {
//        $info = [];
//        $tempRec = ShipInfoEntity::where('ship_id', '=', $shipId)->first();
//        if ($tempRec) {
//            $info = $tempRec->toArray();
//        }
//        return $info;
//    }
//
//
//    public static function createUpdate($data)
//    {
//        try {
//
//            $rules = self::$rules;
//
//            $validator = \Validator::make($data, $rules);
//
//            if ($validator->fails()) {
//
//                return array('validationError', $validator);
//            }
//
//
//            $record = ShipInfoEntity::where('ship_id', '=', $data['ship_id'])->first();
//            if (!$record) {
//                $record = new ShipInfoEntity();
//                $record->ship_id = $data['ship_id'];
//            }
//
//            $record->name_bg = $data['name_bg'];
////            $record->ship_id = $data['ship_id'];
//            $record->people = $data['people'];
//            $record->tonnage = $data['tonnage'];
//            $record->equipage_count = $data['equipage_count'];
//            $record->width = $data['width'];
//            $record->length = $data['length'];
//            $record->speed = $data['speed'];
//            $record->voltage = $data['voltage'];
//            $record->first_start = $data['first_start'];
//            $record->flag = $data['flag'];
//            $record->description_title_bg = $data['description_title_bg'];
//            $record->description_text_bg = $data['description_text_bg'];
//            $record->food_title_bg = $data['food_title_bg'];
//            $record->food_text_bg = $data['food_text_bg'];
//            $record->life_title_bg = $data['life_title_bg'];
//            $record->life_left_text_bg = $data['life_left_text_bg'];
//            $record->life_right_text_bg = $data['life_right_text_bg'];
//
//            $record->save();
//        } catch (\Exception $ex) {
//
//            \Log::error($ex);
//
//            return array('runtimeError', $ex);
//        }
//
//        return array('success', $record);
//    }
//
//
//}
