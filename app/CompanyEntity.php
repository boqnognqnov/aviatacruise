<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class CompanyEntity extends Model
{

    protected $table = 'companies';

    public static $pathImg = 'uploads/images/companies/pictures/';
    public static $pathLogo = 'uploads/images/companies/logos/';
    public static $rules = array(
        'name_bg' => 'required',
        'description_bg' => 'required',
        'picture' => 'image',
        'logo' => 'image',
    );

    public static function createUpdate($data)
    {
        try {

            $rules = self::$rules;

            if ($data['company_id'] == 0) {

                $rules['picture'] = 'image|required';
                $rules['logo'] = 'image|required';
            }

            $validator = \Validator::make($data, $rules);

            if ($validator->fails()) {

                return array('validationError', $validator);
            }

            $record = CompanyEntity::findOrNew($data['company_id']);

            $record->name_bg = $data['name_bg'];
            $record->slug_bg = str_slug($data['name_bg']);
            $record->description_bg = $data['description_bg'];
            $record->additional_info_bg = $data['additional_info_bg'];

            if (isset($data['picture'])) {

                if (!empty($record->picture)) {

                    File::delete(self::$pathImg . $record->picture);
                }

                $fileName = time() . str_random(10) . '.' . $data['picture']->getClientOriginalExtension();
                $image = Image::make($data['picture']->getRealPath());
                $image->save(self::$pathImg . $fileName);
                $record->picture = $fileName;
            }

            if (isset($data['logo'])) {

                if (!empty($record->logo)) {

                    File::delete(self::$pathLogo . $record->logo);
                }

                $fileName = time() . str_random(10) . '.' . $data['logo']->getClientOriginalExtension();
                $image = Image::make($data['logo']->getRealPath());
                $image->resize(720, 450);
                $image->save(self::$pathLogo . $fileName);
                $record->logo = $fileName;
            }

            if (isset($data['seo_title_bg'])) {
                $record->seo_title_bg = $data['seo_title_bg'];
            }

            if (isset($data['seo_description_bg'])) {
                $record->seo_description_bg = $data['seo_description_bg'];
            }

            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }

    public static function getCompanyNameByCompanyId($companyId)
    {
        return CompanyEntity::find($companyId)->name_bg;
    }

    public static function setPictureSeoData($data)
    {
        try {
            $record = CompanyEntity::find($data['company_id']);
            $record->pic_title_bg = $data['pic_title_bg'];
            $record->pic_description_bg = $data['pic_description_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }

    public
    static function setLogoSeoData($data)
    {
//        try {
            $record = CompanyEntity::find($data['company_id']);
            $record->logo_title_bg = $data['logo_title_bg'];
            $record->logo_description_bg = $data['logo_description_bg'];
            $record->save();
//        } catch (\Exception $ex) {
//            \Log::error($ex);
//            return array('runtimeError', $ex);
//        }
        return array('success', $record);
    }

    public
    static function destroyCompany($companyId)
    {
        try {

            $record = CompanyEntity::find($companyId);

            File::delete(self::$pathImg . $record->picture);
            File::delete(self::$pathLogo . $record->logo);
            ReservationPacketEntity::where('company_id', $companyId);

            $ships = ShipEntity::where('company_id', $companyId);

            foreach ($ships as $ship) {

                ShipEntity::destroyShip($ship->id);
            }

            $record->delete();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', 'success');
    }
}
