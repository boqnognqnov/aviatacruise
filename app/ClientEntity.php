<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientEntity extends Model
{
    protected $table = 'clients';

    public static $rulesTitular = array(
        'fname' => 'required',
        'sname' => 'required',
        'lname' => 'required',
        'email' => 'required|email',
        'phone' => 'required|numeric|min:5',
        'is_titular' => 'required',
    );

    public static $rulesPassenger = array(
        'fname' => 'required',
        'lname' => 'required',
        'is_titular' => 'required',
    );

    public static $messagesTitular = array(
        'fname.required' => 'Първото име на титуляра не е попълнено',
        'sname.required' => 'Бащиното име на титуляра не е попълнено',
        'lname.required' => 'Фамилията на титуляра не е попълнена',
        'email.required' => 'Емейла на титуляра не е попълнен',
        'email.email' => 'Емейла на титуляра не е в правилен формат',
        'phone.required' => 'Телефона на титуляра не е попълнен',
        'phone.numeric' => 'Телефона на титуляра не е в правилен формат',
//        'phone.min' => 'Телефона на титуляра трябва да съдържа поне 5 цифри',
        'is_titular' => 'required',
    );

    public static $messagesPassenger = array(
        'fname.required' => 'Първото име на някое от придружаващите лица не е попълнено',
        'lname.required' => 'Фамилията на някое от придружаващите лица не е попълнена',
        'is_titular' => 'required',
    );

    public static function validateClients($data)
    {
        $validator = null;

        if ($data['is_titular'] == true) {
            $validator = \Validator::make($data, self::$rulesTitular, self::$messagesTitular);
        } else {
            $validator = \Validator::make($data, self::$rulesPassenger, self::$messagesPassenger);
        }


        if ($validator->fails()) {
            return array('validationError', $validator);
        }
        return array('success', 'success');
    }

    public static function createUpdate($data)
    {
        try {


            $record = ClientEntity::findOrNew($data['id']);

            $record->reservation_id = $data['reservation_id'];
            $record->fname = $data['fname'];
            if (isset($data['sname'])) {
                $record->sname = $data['sname'];
            }

            $record->lname = $data['lname'];

            if (isset($data['email'])) {
                $record->email = $data['email'];
            }
            if (isset($data['phone'])) {
                $record->phone = $data['phone'];
            }

            $record->date_birth = $data['date_birth'];

            if (isset($data['id_number'])) {
                $record->id_number = $data['id_number'];
            }

            $record->is_titular = false;
            if (isset($data['is_titular'])) {
                $record->is_titular = $data['is_titular'];
            }


            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }


}
