<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class RouteEntity extends Model
{
    protected $table = 'routes';
    public static $path = 'uploads/images/routes/';

    public static $rules = array(
        'picture' => 'image',
        'route_title_bg' => 'required',
        'route_type_id' => 'required',
        'route_description_bg' => 'required',
    );

    public static function createUpdate($data)
    {
        try {
            $rules = self::$rules;

            if ($data['route_id'] == 0) {

                $rules['picture'] = 'image|required';
            }

            $validator = \Validator::make($data, $rules);

            if ($validator->fails()) {

                return array('validationError', $validator);
            }

            $record = RouteEntity::findOrNew($data['route_id']);

            $record->route_type_id = $data['route_type_id'];

            $record->duration_count = $data['duration_count'];

            $record->destination_id = $data['destination_id'];
//            $record->discount = $data['discount'];
            $record->is_front = false;

            if (isset($data['is_front'])) {

                $record->is_front = true;
            }

            $record->route_title_bg = $data['route_title_bg'];
            $record->route_slug_bg = str_slug($data['route_title_bg']);
            $record->route_description_bg = $data['route_description_bg'];
            $record->ship_id = $data['ship_id'];

            if (isset($data['picture'])) {
                if (!empty($record->picture)) {
                    File::delete(self::$path . $record->picture);

                }
                $fileName = time() . str_random(10) . '.' . $data['picture']->getClientOriginalExtension();
                $image = Image::make($data['picture']->getRealPath());
                $image->resize(720, 475);
                $image->save(self::$path . $fileName);
                $record->picture = $fileName;
            }

            if (isset($data['seo_title_bg'])) {
                $record->seo_title_bg = $data['seo_title_bg'];
            }

            if (isset($data['seo_description_bg'])) {
                $record->seo_description_bg = $data['seo_description_bg'];
            }


            $record->save();

        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }

    public static function destroyRoute($routeId)
    {
        try {

            $record = RouteEntity::find($routeId);

            File::delete(self::$path . $record->picture);
//            RouteShipEntity::where('route_id', $routeId)->delete();
            RoutePointEntity::where('route_id', $routeId)->delete();

            $images = RouteGalleryEntity::where('route_id', $routeId)->get();

            foreach ($images as $image) {

                RouteGalleryEntity::destroyImg($image->id);
            }

            $routeDates = RouteDateEntity::where('route_id', $routeId)->get();

            foreach ($routeDates as $routeDate) {

                RouteDateEntity::destroyRouteEvent($routeDate->id);
            }

            $record->delete();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', 'success');
    }

    public static function setPictureSeoData($data)
    {
        try {
            $record = RouteEntity::find($data['route_id']);
            $record->pic_title_bg = $data['pic_title_bg'];
            $record->pic_alt_bg = $data['pic_alt_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }
}
