<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class PageAboutEntity extends Model
{
    protected $table = 'page_about';

    public static $pathSlideImg = 'uploads/images/aboutPageData/slideImg/';
    public static $pathContentImg = 'uploads/images/aboutPageData/contentImg/';

    public static $rules = array(
        'slide_img' => 'image',
        'content_img' => 'image',
        'content_img_front' => 'image',
        'slide_text_bg' => 'required',
        'text_first_left_bg' => 'required',
        'text_deduction_bg' => 'required',
        'title1_bg' => 'required',
        'title2_bg' => 'required',
        'title3_bg' => 'required',
    );


    public static function updatePage($data)
    {
        try {

            $record = PageAboutEntity::first();
            $rules = self::$rules;

            if (empty($record->slide_img)) {
                $rules['slide_img'] = 'image|required';
            }

            if (empty($record->content_img)) {
                $rules['content_img'] = 'image|required';
            }

            if (empty($record->content_img_front)) {
                $rules['content_img_front'] = 'image|required';
            }

            $validator = \Validator::make($data, $rules);
            if ($validator->fails()) {
                return array('validationError', $validator);
            }


            $record->slide_text_bg = $data['slide_text_bg'];
            $record->text_first_left_bg = $data['text_first_left_bg'];
            $record->text_deduction_bg = $data['text_deduction_bg'];
            $record->title1_bg = $data['title1_bg'];
            $record->title2_bg = $data['title2_bg'];
            $record->title3_bg = $data['title3_bg'];

            if (isset($data['slide_img'])) {
                if (!empty($record->slide_img)) {

                    File::delete(self::$pathSlideImg . $record->slide_img);
                }

                $fileName = time() . str_random(10) . '.' . $data['slide_img']->getClientOriginalExtension();
                $image = Image::make($data['slide_img']->getRealPath());
                $image->save(self::$pathSlideImg . $fileName);
                $record->slide_img = $fileName;
            }

            if (isset($data['content_img'])) {
                if (!empty($record->content_img)) {

                    File::delete(self::$pathContentImg . $record->content_img);
                }

                $fileName = time() . str_random(10) . '.' . $data['content_img']->getClientOriginalExtension();
                $image = Image::make($data['content_img']->getRealPath());
                $image->save(self::$pathContentImg . $fileName);
                $record->content_img = $fileName;
            }

            if (isset($data['content_img_front'])) {
                if (!empty($record->content_img_front)) {

                    File::delete(self::$pathContentImg . $record->content_img_front);
                }

                $fileName = time() . str_random(10) . '.' . $data['content_img_front']->getClientOriginalExtension();
                $image = Image::make($data['content_img_front']->getRealPath());
                $image->save(self::$pathContentImg . $fileName);
                $record->content_img_front = $fileName;
            }
            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }

    public static function setSeoSliderPictureData($data)
    {
        try {
            $record = PageAboutEntity::first();
            $record->pic_slide_title_bg = $data['pic_slide_title_bg'];
            $record->pic_slide_alt_bg = $data['pic_slide_alt_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }

    public static function setAboutSeoPictureData($data)
    {
        try {
            $record = PageAboutEntity::first();
            $record->pic_content_title_bg = $data['pic_content_title_bg'];
            $record->pic_content_alt_bg = $data['pic_content_alt_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }

    public static function setIndexSeoPictureData($data)
    {
        try {
            $record = PageAboutEntity::first();
            $record->pic_content_front_title_bg = $data['pic_content_front_title_bg'];
            $record->pic_content_front_alt_bg = $data['pic_content_front_alt_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }
}
