<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class RoomTypeEntity extends Model
{
    protected $table = 'room_types';

    public static $path = 'uploads/images/cabins/';


    public static $rules = array(
        'name_bg' => 'required',
        'description_bg' => 'required',
        'picture' => 'image',
//        'price' => 'numeric|required',
    );

    public static function getRoomsOnThisShip($shipId)
    {
        return RoomTypeEntity::join('decks', 'room_types.deck_id', '=', 'decks.id')
            ->join('ships', 'decks.ship_id', '=', 'ships.id')->where('ships.id', '=', $shipId)->select('room_types.*')->get();
    }

    public static function getRoomPrice($roomId, $dateId)
    {
        return DateRoomOfferEntity::where('room_id', '=', $roomId)->where('date_id', '=', $dateId)->first();
    }

    public static function createUpdate($data)
    {
        try {
            $rules = self::$rules;

            if ($data['room_id'] == 0) {
                $rules['picture'] = 'required|image';
            }

            $validator = \Validator::make($data, $rules);

            if ($validator->fails()) {

                return array('validationError', $validator);
            }

            $record = RoomTypeEntity::findOrNew($data['room_id']);

            $record->deck_id = $data['deck_id'];
            $record->name_bg = $data['name_bg'];
            $record->description_bg = $data['description_bg'];

            $record->room_count = $data['room_count'];

            if (isset($data['picture'])) {

                if (!empty($record->picture)) {

                    File::delete(self::$path . $record->picture);
                }

                $fileName = time() . str_random(10) . '.' . $data['picture']->getClientOriginalExtension();
                $image = Image::make($data['picture']->getRealPath());
                $image->resize(400, 235);
                $image->save(self::$path . $fileName);
                $record->picture = $fileName;
            }

            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }

    public static function destroyRoomType($routeTypeId)
    {
        try {

            $record = RoomTypeEntity::find($routeTypeId);

            DateRoomOfferEntity::where('room_id', $routeTypeId)->delete();

            $reservations = ReservationsEntity::where('route_date_id', '=', $routeTypeId)->get();

            foreach ($reservations as $oneReservation) {

                ReservationsEntity::destroyReservation($oneReservation->id);
            }


            File::delete(self::$path . $record->picture);

            $record->delete();

        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', 'success');
    }
}
