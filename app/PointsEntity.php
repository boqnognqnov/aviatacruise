<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class PointsEntity extends Model
{
    protected $table = 'points';

    public static $path = 'uploads/images/stopPoints/';


    public static $rules = array(
        'picture' => 'image',
        'name_bg' => 'required',
        'description_bg' => 'required',
        'country_bg' => 'required',
        'coordinates' => 'required',
    );


    public static function createUpdate($data)
    {
        try {
            $rules = self::$rules;
            if ($data['point_id'] == 0) {
                $rules['picture'] = 'image|required';
            }

            $validator = \Validator::make($data, $rules);
            if ($validator->fails()) {
                return array('validationError', $validator);
            }


            $record = PointsEntity::findOrNew($data['point_id']);

            $record->name_bg = $data['name_bg'];
            $record->slug_bg = str_slug($data['name_bg']);
            $record->description_bg = $data['description_bg'];

            if (isset($data['description_inner_bg'])) {
                $record->description_inner_bg = $data['description_inner_bg'];
            }

            $record->country_bg = $data['country_bg'];
            $record->coordinates = $data['coordinates'];
            $record->point_type_id = $data['point_type_id'];


            if (isset($data['picture'])) {
                if (!empty($record->picture)) {
                    File::delete(self::$path . $record->picture);

                }
                $fileName = time() . str_random(10) . '.' . $data['picture']->getClientOriginalExtension();
                $image = Image::make($data['picture']->getRealPath());
                $image->save(self::$path . $fileName);
                $record->picture = $fileName;
            }

            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }

    public static function destroyPoint($pointId)
    {
        try {
//            DELETE ALL ROUTES WITH THIS POINT
            RoutePointEntity::where('point_id', '=', $pointId)->delete();

            $stopPoint = PointsEntity::find($pointId);
            File::delete(self::$path . $stopPoint->picture);
            $stopPoint->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');
    }

    public static function setPictureSeoData($data)
    {
        try {
            $record = PointsEntity::find($data['point_id']);
            $record->pic_title_bg = $data['pic_title_bg'];
            $record->pic_alt_bg = $data['pic_alt_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }
}
