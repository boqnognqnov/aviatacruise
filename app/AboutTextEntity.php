<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class AboutTextEntity extends Model
{
    protected $table = 'about_texts';

    public static $pathPreDefinedRows = 'uploads/images/aboutPageData/fourPreDefinedRows/';

    public static $rulesPreDefinedRows = array(
        'picture' => 'image',
        'picture_front' => 'image',
        'text1_bg' => 'required',
        'text2_bg' => 'required',
    );

    public static $rulesOtherRows = array(
        'text1_bg' => 'required'
    );


    public static function setPreDefinedRows($dataByRows)
    {
        try {

            foreach ($dataByRows['rows'] as $id => $oneRow) {
//                return dump($oneRow);
                $status = self::checkValidation($id, $oneRow);

                if ($status[0] == 'validationError') {
                    return array('validationError', $status[1]);
                }

                self::setOnePreDefinedRow($id, $oneRow);
            }

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');

    }

    private static function checkValidation($id = 0, $oneRow)
    {
        $rules = self::$rulesPreDefinedRows;

        $record = AboutTextEntity::find($id);

        if (empty($record->picture)) {
            $rules['picture'] = 'required|image';
        }

        if (empty($record->picture_front)) {
            $rules['picture_front'] = 'required|image';
        }

        $validator = \Validator::make($oneRow, $rules);
        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        return array('success', $record);


    }

    private static function setOnePreDefinedRow($id, $data)
    {

        $record = AboutTextEntity::find($id);
        $record->text1_bg = $data['text1_bg'];
        $record->text2_bg = $data['text2_bg'];

        if (isset($data['picture'])) {
            if (!empty($record->picture)) {

                File::delete(self::$pathPreDefinedRows . $record->picture);
            }
            $fileName = time() . str_random(10) . '.' . $data['picture']->getClientOriginalExtension();
            $image = Image::make($data['picture']->getRealPath());
            $image->save(self::$pathPreDefinedRows . $fileName);
            $record->picture = $fileName;
        }

        if (isset($data['picture_front'])) {
            if (!empty($record->picture_front)) {

                File::delete(self::$pathPreDefinedRows . $record->picture_front);
            }
            $fileName = time() . str_random(10) . '.' . $data['picture_front']->getClientOriginalExtension();
            $image = Image::make($data['picture_front']->getRealPath());
            $image->save(self::$pathPreDefinedRows . $fileName);
            $record->picture_front = $fileName;
        }

        $record->save();
    }

    public static function createUpdate($data)
    {
        try {
            $rules = self::$rulesOtherRows;

            foreach ($data['otherRows'] as $id => $text) {
//            return dump($text);
//                $validator = \Validator::make($text, $rules);
//
//                if ($validator->fails()) {
//
//                    return array('validationError', $validator);
//                }
                $text = trim($text);
                if (!empty($text)) {
                    $record = AboutTextEntity::findOrNew($id);
                    $record->text1_bg = $text;
                    $record->save();
                }
            }
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }

    public static function destroyRow($rowId)
    {
        try {

            $record = AboutTextEntity::find($rowId);

            $record->delete();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }
}
