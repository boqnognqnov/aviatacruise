<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationsEntity extends Model
{
    protected $table = 'reservations';

    public static $rules = array(
        'passengers' => 'required',
        'route_date_id' => 'required',
        'room_type_id' => 'required',
        'comment' => 'required',
    );

    public static function createUpdate($data)
    {
//        try {

        $rules = self::$rules;

        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {

            return array('validationError', $validator);
        }

        $record = ReservationsEntity::findOrNew($data['id']);

        $record->passengers = $data['passengers'];
        $record->route_date_id = $data['route_date_id'];
        $record->room_type_id = $data['room_type_id'];
        $record->comment = $data['comment'];
        $record->save();
//        } catch (\Exception $ex) {
//
//            \Log::error($ex);
//
//            return array('runtimeError', $ex);
//        }

        return array('success', $record);
    }


    public static function destroyReservation($reservationId)
    {
        try {

            $record = ReservationsEntity::find($reservationId);

            ReservationAdditionalInfoEntity::where('reservation_id', '=', $reservationId)->delete();
            ClientEntity::where('reservation_id', '=', $reservationId)->delete();
            ReservationPacketEntity::where('reservation_id', '=', $reservationId)->delete();

            $record->delete();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', 'success');
    }
}
