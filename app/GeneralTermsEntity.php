<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class GeneralTermsEntity extends Model
{
    protected $table = 'general_terms';

    public $timestamps = false;


    public static $rules = array(

        'title_bg' => 'required',
        'text_bg' => 'required',

    );


    public static function updatePage($data)
    {
        try {

            $record = GeneralTermsEntity::first();
            $rules = self::$rules;

            $validator = \Validator::make($data, $rules);
            if ($validator->fails()) {
                return array('validationError', $validator);
            }
            
            $record->title_bg = $data['title_bg'];
            $record->text_bg = $data['text_bg'];

            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }
}
