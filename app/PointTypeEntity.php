<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class PointTypeEntity extends Model
{
    protected $table = 'point_types';

    public static $path = 'uploads/images/pointTypes/';

    public $timestamps = false;


    public static $rules = array(
        'picture' => 'image',
        'name_bg' => 'required',
    );

    public static function getTypeName($typeId)
    {
        return PointTypeEntity::find($typeId)->name_bg;
    }


    public static function createUpdate($data)
    {
        try {
            $rules = self::$rules;
            if ($data['point_type_id'] == 0) {
                $rules['picture'] = 'image|required';
            }

            $validator = \Validator::make($data, $rules);
            if ($validator->fails()) {
                return array('validationError', $validator);
            }


            $record = PointTypeEntity::findOrNew($data['point_type_id']);

            $record->name_bg = $data['name_bg'];


            if (isset($data['picture'])) {
                if (!empty($record->picture)) {
                    File::delete(self::$path . $record->picture);

                }
                $fileName = time() . str_random(10) . '.' . $data['picture']->getClientOriginalExtension();
                $image = Image::make($data['picture']->getRealPath());
                $image->save(self::$path . $fileName);
                $record->picture = $fileName;
            }

            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }

    public static function destroyPointType($pointTypeId)
    {
        if ($pointTypeId == 1) {
            return dump('Не може да бъде изтрит');
        }
        try {

            $listOfPointOnThisType = PointsEntity::where('point_type_id', '=', $pointTypeId)->get();
            foreach ($listOfPointOnThisType as $onePoint) {
                RoutePointEntity::where('point_id', '=', $onePoint->id)->delete();

                $stopPoint = PointsEntity::find($onePoint->id);
                File::delete(PointsEntity::$path . $stopPoint->picture);
                $stopPoint->delete();
            }

            $pointType = PointTypeEntity::find($pointTypeId);
            File::delete(self::$path . $pointType->picture);
            $pointType->delete();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');
    }
}
