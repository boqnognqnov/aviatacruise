<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateRoomOfferEntity extends Model
{
    protected $table = 'date_room_offers';


    public static function createUpdate($shipRooms, $dateId)
    {
        try {
            foreach ($shipRooms as $shipId => $rooms) {
                foreach ($rooms as $roomId => $oneRoomOnShip) {

                    DateRoomOfferEntity::where('room_id', '=', $roomId)->where('date_id', '=', $dateId)->delete();

                    $record = new DateRoomOfferEntity();

                    $record->room_id = $roomId;
                    $record->date_id = $dateId;
                    $record->price = $oneRoomOnShip['price'];

                    $record->room_discount = $oneRoomOnShip['room_discount'];
//                $record->discount = $oneRoomOnShip['discount'];

//                $record->date = $oneRoomOnShip['date'];
                    $record->save();
                }


            }


        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }


}
