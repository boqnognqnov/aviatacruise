<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class NewsGalleryEntity extends Model
{
    protected $table = 'news_gallery';
    public static $path = 'uploads/images/newsGallery/';


    public static function uploadImages($data)
    {

        $position = 1;

        foreach ($data['pictures'] as $picture) {

            $lastPos = NewsGalleryEntity::orderBy('position', 'DESC')->get()->first();

            if ($lastPos) {
                $position = ($lastPos->position) + 1;
            }


            $record = new NewsGalleryEntity();

            $fileName = time() . str_random(10) . '.' . $picture->getClientOriginalExtension();
            $image = Image::make($picture->getRealPath());
            $image->resize(690, 390);
            $image->save(self::$path . $fileName);
            $record->picture = $fileName;
            $record->position = $position;

            $record->news_id = $data['news_id'];

            $record->save();
        }
    }

    public static function destroyImg($galleryImgId)
    {
        try {
            $record = NewsGalleryEntity::find($galleryImgId);
            File::delete(NewsGalleryEntity::$path . $record->picture);
            $record->delete();
        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');
    }

    public static function setPictureSeoData($data)
    {
        try {
            $record = NewsGalleryEntity::find($data['pic_id']);
            $record->pic_title_bg = $data['pic_title_bg'];
            $record->pic_alt_bg = $data['pic_alt_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }

}
