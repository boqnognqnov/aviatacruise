<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('auth/login', function () {
    return view('auth.login');
});


Route::post('auth/login', 'AdministratorController@postAdminLogin');


//    Route::get('logout', 'Auth\AuthController@getLogout');
Route::get('logout', 'Auth\AuthController@logout');


//ADMIN
Route::group(['middleware' => 'auth'], function () {

    Route::get('/admin', function () {
        return view('admin.index');
    });

//ROUTES
    Route::get('/admin/routes', 'AdminRoutesController@listRoutes');
    Route::get('/admin/routes/create_update/{route_id?}', 'AdminRoutesController@showForm');
    Route::post('/admin/routes/create_update', 'AdminRoutesController@createUpdateRoutes');
    Route::post('/admin/routes/destroy', 'AdminRoutesController@destroyRoute');

    Route::get('/admin/routes/set_promo_packet_to_route/{route_id}/{packet_id}', 'AdminRoutesController@setPromoPackToRoute');

    Route::post('/admin/routes/seo/picture_data', 'AdminRoutesController@setSeoPictureData');

//Route::post('/admin/routes/setShipsOnThisRoute', 'AdminRoutesController@setShipsOnThisRoute');
//ROUTES

//ROUTE TYPES
    Route::get('/admin/routes-types', 'AdminRoutesTypesController@listRouteTypes');
    Route::get('/admin/routes-types/create_update/{route_type_id?}', 'AdminRoutesTypesController@showCreateUpdate');
    Route::post('/admin/routes-types/create_update', 'AdminRoutesTypesController@createUpdateRouteType');
    Route::post('/admin/routes-types/destroy', 'AdminRoutesTypesController@destroyRouteType');

    Route::post('/admin/routes-types/seo/picture_data', 'AdminRoutesTypesController@setSeoPictureData');

//ROUTE TYPES

//ROUTE POINTS
    Route::get('/admin/routes-points', 'AdminRoutePointsController@listPoints');
    Route::get('/admin/routes-points/create_update/{point_id?}', 'AdminRoutePointsController@showForm');
    Route::post('/admin/routes-points/create_update', 'AdminRoutePointsController@createUpdatePoint');
    Route::post('/admin/routes-points/destroy', 'AdminRoutePointsController@destroyPoint');

    Route::post('/api/getRoutePoints', 'Api\GetRoutePoints@getRoutePoints');

    Route::get('/admin/routes-points/set_point_form/{route_id}/{point_id?}/{route_point_id?}', 'AdminRoutePointsController@setPointToRouteForm');
    Route::post('/admin/routes-points/set-route-point', 'AdminRoutePointsController@setPointToRoute');
    Route::get('/admin/routes-points/destroy-point/{route_id}/{point_id?}', 'AdminRoutePointsController@destroyPointFromRoute');

    Route::post('/admin/routes-points/seo/picture_data', 'AdminRoutePointsController@setSeoPictureData');

//ROUTE POINTS


//POINT TYPES
    Route::get('/admin/point-types', 'AdminRoutePointsController@listPointTypes');
    Route::get('/admin/point-types/create_update/{point_type_id?}', 'AdminRoutePointsController@showPointTypeForm');
    Route::post('/admin/point-types/create_update', 'AdminRoutePointsController@createUpdatePointType');
    Route::post('/admin/point-types/destroy', 'AdminRoutePointsController@destroyPointType');
//POINT TYPES

    Route::get('/admin/routes-dates/list/{route_id}/{dateId?}', 'AdminRouteDatesController@listDates');
    Route::post('/admin/routes-dates/set_date', 'AdminRouteDatesController@setDateOnRoute');
    Route::get('/admin/routes-dates/set_activity/{date_id}', 'AdminRouteDatesController@setActivity');
    Route::post('/admin/routes-dates/destroy', 'AdminRouteDatesController@destroyEvent');


//ROUTE DATES

//ROUTE GALLERY
    Route::get('/admin/routes-gallery/{route_id}', 'AdminRoutesGalleryController@showGallery');
    Route::post('/admin/routes-gallery/upload_imgs', 'AdminRoutesGalleryController@uploadPictures');
    Route::post('/admin/routes-gallery/destroy', 'AdminRoutesGalleryController@destroyPicture');

    Route::get('/admin/routes-gallery/move/right/{route_id}/{image_id}', 'AdminRoutesGalleryController@moveImgRight');
    Route::get('/admin/routes-gallery/move/left/{route_id}/{image_id}', 'AdminRoutesGalleryController@moveImgLeft');

    Route::post('/admin/routes-gallery/gallery/picture_data', 'AdminRoutesGalleryController@setSeoPictureDataGallery');
//ROUTE GALLERY

//ROUTE DATES

//COMPANIES
    Route::get('/admin/companies', 'AdminCompaniesController@listCompanies');
    Route::get('/admin/companies/create_update/{company_id?}', 'AdminCompaniesController@showCreateUpdate');
    Route::post('/admin/companies/create_update', 'AdminCompaniesController@createUpdateCompany');
    Route::post('/admin/companies/destroy', 'AdminCompaniesController@destroyCompany');

    Route::post('/admin/companies/seo/picture_data', 'AdminCompaniesController@setSeoPictureData');
    Route::post('/admin/companies/seo/logo_data', 'AdminCompaniesController@setSeoLogoData');


//COMPANIES

//SHIPS
    Route::get('/admin/ships/list/{company_id?}', 'AdminShipsController@listShips');
    Route::get('/admin/ships/edit_form/{ship_id?}', 'AdminShipsController@showEditForm');
    Route::post('/admin/ships/create_update', 'AdminShipsController@createUpdateShip');
    Route::post('/admin/ships/destroy', 'AdminShipsController@destroyShip');

    Route::post('/admin/ships/seo/picture_data', 'AdminShipsController@setSeoPictureData');
//SHIPS

//SHIP GALLERY
    Route::get('/admin/ships/gallery/{ship_id}', 'AdminShipsController@showShipGallery');
    Route::post('/admin/ships/gallery/destroy', 'AdminShipsController@destroyGalleryImg');
    Route::post('/admin/ships/gallery/upload_imgs', 'AdminShipsController@uploadGalleryImg');

    Route::post('/admin/ships/gallery/picture_data', 'AdminShipsController@setSeoPictureDataGallery');
//SHIP GALLERY

//ROOMS
    Route::get('/admin/rooms/list/{ship_id}/{deck_id?}', 'AdminRoomTypesController@listRoomTypes');
    Route::get('/admin/rooms/create_update/{ship_id}/{roomType_id?}', 'AdminRoomTypesController@showCreateUpdate');
    Route::post('/admin/rooms/create_update', 'AdminRoomTypesController@createUpdateRoomType');
    Route::post('/admin/rooms/destroy', 'AdminRoomTypesController@destroyRoomType');


//ROOMS

//DECKS
    Route::get('/admin/decks/list/{ship_id}', 'AdminDecksController@listDecks');
    Route::get('/admin/decks/create_update/{ship_id}/{deck_id?}', 'AdminDecksController@showCreateUpdate');
    Route::post('/admin/decks/create_update', 'AdminDecksController@createUpdateDeck');
    Route::post('/admin/decks/destroy', 'AdminDecksController@destroyDeck');
//DECKS

//NEWS
    Route::get('/admin/news', 'AdminNewsController@listNews');
    Route::get('/admin/news/edit_form/{news_id?}', 'AdminNewsController@showEditForm');
    Route::post('/admin/news/update_create', 'AdminNewsController@updateCreateNews');
    Route::post('/admin/news/destroy', 'AdminNewsController@destroyNews');

    Route::post('/admin/news/seo/picture_data', 'AdminNewsController@setSeoPictureData');
    Route::post('/admin/news/gallery/picture_data', 'AdminNewsController@setSeoPictureDataGallery');

    Route::get('/admin/news/category/{category_id?}', 'AdminNewsController@listCategories');
    Route::post('/admin/news/category/create_update', 'AdminNewsController@addOrEditCategories');
    Route::post('/admin/news/category/destroy', 'AdminNewsController@destroyCategory');

    Route::get('/admin/news/gallery/{news_id}', 'AdminNewsController@showGallery');
    Route::post('/admin/news/gallery/destroyImg', 'AdminNewsController@destroyGalleryImg');
    Route::post('/admin/news/gallery/upload', 'AdminNewsController@uploadGalleryImgs');

    Route::get('/admin/news/gallery/move/right/{news_id}/{images_id}', 'AdminNewsController@moveImgRight');
    Route::get('/admin/news/gallery/move/left/{news_id}/{images_id}', 'AdminNewsController@moveImgLeft');
//NEWS


//DESTINATIONS
    Route::get('/admin/destinations', 'AdminDestinationsController@listDestinations');
    Route::get('/admin/destinations/update_create_form/{destination_id?}', 'AdminDestinationsController@showForm');
    Route::post('/admin/destinations/create_update', 'AdminDestinationsController@createUpdateDestinations');
    Route::post('/admin/destinations/destroy', 'AdminDestinationsController@destroyDestinatations');

    Route::post('/admin/destinations/seo/picture_data', 'AdminDestinationsController@setSeoPictureData');

//DESTINATIONS

//PROMO PACKETS
    Route::get('/admin/promo-packets', 'AdminPromoPacketsController@listPackets');
    Route::get('/admin/promo-packets/update_create_form/{packet_id?}', 'AdminPromoPacketsController@showEditForm');
    Route::post('/admin/promo-packets/create_update', 'AdminPromoPacketsController@updateCreatePackets');
    Route::post('/admin/promo-packets/destroy', 'AdminPromoPacketsController@destroyPacket');
//PROMO PACKETS

//ABOUT US
    Route::get('/admin/about-us', 'AdminPageAboutController@showAboutForm');
    Route::post('/admin/about-us', 'AdminPageAboutController@updateAboutPage');
    Route::get('/admin/about-us/other', 'AdminAboutTextController@showForm');
    Route::post('/admin/about-us/other/updateBenefits', 'AdminAboutTextController@updateBenefitsBox');
    Route::post('/admin/about-us/other', 'AdminAboutTextController@updateOthers');
    Route::post('/admin/about-us/other/destroy', 'AdminAboutTextController@destroyOtherField');

    Route::post('/admin/about-us/seo/slide_img/picture_data', 'AdminPageAboutController@setSeoSliderPictureData');
    Route::post('/admin/about-us/seo/about_page/picture_data', 'AdminPageAboutController@setAboutSeoPictureData');
    Route::post('/admin/about-us/seo/index_page/picture_data', 'AdminPageAboutController@setIndexSeoPictureData');
//ABOUT US


//ADDITIONAL INFO
    Route::get('/admin/additional-info/list', 'AdminAdditionalInfoController@listInfoTypes');
    Route::get('/admin/additional-info/edit/{infoTypeId?}', 'AdminAdditionalInfoController@editInfoTypes');
    Route::post('/admin/additional-info/store', 'AdminAdditionalInfoController@updateCreateType');
    Route::post('/admin/additional-info/destroyType', 'AdminAdditionalInfoController@destroyType');
//ADDITIONAL INFO

//INDEX PAGE
    Route::get('/admin/index-page/slider/list', 'AdminIndexController@listSliders');
    Route::get('/admin/index-page/slider/edit/{slider_id?}', 'AdminIndexController@editSlider');
    Route::post('/admin/index-page/slider/store', 'AdminIndexController@storeSlider');
    Route::post('/admin/index-page/slider/destroySlider', 'AdminIndexController@destroySlider');
    Route::get('/admin/index-page/slider/set_active/{slider_id}', 'AdminIndexController@setActivity');
//INDEX PAGE

//RESERVATIONS
    Route::get('/admin/reservations/list', 'AdminReservationController@listReservations');
    Route::get('/admin/reservations/reservation_form/{reservation_id?}', 'AdminReservationController@showReservationForm');
    Route::post('/admin/reservations/store', 'AdminReservationController@storeReservation');
    Route::post('/admin/reservations/destroy', 'AdminReservationController@destroyReservation');

//RESERVATIONS

//EMAIL SUBSCRIPTION
    Route::get('/admin/email_subscription/list', 'ACEmailToSubscriptionController@adminListEmails');
    Route::post('/admin/email_subscription/destroyEmail', 'ACEmailToSubscriptionController@adminDestroyEmail');

    Route::post('/admin/email_subscription/send_messagess', 'ACEmailToSubscriptionController@sendMessageToAllUsers');
//EMAIL SUBSCRIPTION

//CONTACTS
    Route::get('/admin/contacts/show', 'AdminContactsController@showForm');
    Route::post('/admin/contacts/store', 'AdminContactsController@storeChanges');
//CONTACTS

//QUESTIONS
    Route::get('/admin/questions/category/{category_id?}', 'AdminQuestionsController@listCategories');
    Route::post('/admin/questions/category/create_update', 'AdminQuestionsController@addOrEditCategories');
    Route::post('/admin/questions/category/destroy', 'AdminQuestionsController@destroyCategory');

    Route::get('/admin/questions/list', 'AdminQuestionsController@listQuests');
    Route::get('/admin/questions/create_edit/{quest_id?}', 'AdminQuestionsController@showEditForm');
    Route::post('/admin/questions/store', 'AdminQuestionsController@storeChanges');
    Route::post('/admin/questions/destroy', 'AdminQuestionsController@destroyQuestion');
//QUESTIONS
//GENERAL TERMS
    Route::get('/admin/general-terms/show', 'AdminGeneralTermsController@showForm');
    Route::post('/admin/general-terms/store', 'AdminGeneralTermsController@storeChanges');
//GENERAL TERMS

//SEO PAGE DATA
    Route::get('/admin/seo/static-pages/list', 'SeoController@listStaticPages');
    Route::get('/admin/seo/static-pages/show_form/{page_id}', 'SeoController@showPageSeoForm');
    Route::post('/admin/seo/static-pages/store', 'SeoController@setSeoDataOnPage');
//SEO PAGE DATA


});
//ADMIN


Route::get('/', 'IndexPageController@showPage');


Route::get('/company-list', 'CompaniesController@listCompanies');
Route::post('/company-list/js_listing', 'CompaniesController@listCompaniesJS');
Route::get('/inner-company/{company_id}', 'CompaniesController@showCompany');


Route::get('/ship-list', 'ShipsController@listShips');
Route::post('/ship-list/js_listing', 'ShipsController@listShipsJS');
Route::get('/ship/{slug}', 'ShipsController@showShip');


Route::get('/cruize-types', 'RouteController@listRouteTypes');
Route::post('/cruize-types/js_listing', 'RouteController@moreCruiseTypesJS');

Route::get('/inner-cruize-type/{route_type_slug}', 'RouteController@showRouteType');
Route::post('/inner-cruize-type/js_listing', 'RouteController@moreRoutesOnThisTypeJS');


Route::get('/promotions', 'RouteController@listPromotions');
Route::post('/promotions/js_listing', 'RouteController@listMorePromotionsJs');

Route::get('/top-cruises', 'TopRoutesController@listTopRoutes');
Route::post('/top-cruises/js_listing', 'TopRoutesController@listMoreTopRoutesJs');


Route::get('/offer/{route_slug}', 'OfferController@showOffer');
Route::get('/offer-details/choose_cabin_and_date', 'OfferController@chooseCabinAndDate');
Route::get('/offer-details/choose_cabin_and_packets', 'OfferController@chooseCabinAndPackets');
Route::get('/choose-cabbin', 'OfferController@chooseCabin');
Route::post('/choose-cabbin/calculateJs', 'OfferController@calculateTotalPriceJs');
Route::get('/success-reservation', 'OfferController@confirmReservation');

Route::post('/success-reservation/email_to_subscription', 'ACEmailToSubscriptionController@emailSubscription');


Route::get('/ports', 'PortsController@listPorts');

Route::get('/inner-port/{port_slug}', 'PortsController@showPort');


Route::get('/destinations', 'DestinationsController@listDestinations');
Route::post('/destinations/js_listing', 'DestinationsController@listMoreDestinationsJs');

Route::get('/inner-destinations/{destination_slug}', 'DestinationsController@showDestination');
Route::post('/inner-destinations/more_routes_js_listing', 'DestinationsController@listRoutesWithThisDestinationJs');


Route::get('/search-results', function () {
    return view('/search-results');
});


Route::get('/news/{criterion?}/{crtiterion_val?}', 'NewsController@listNews');
Route::post('/news/js_listing', 'NewsController@listNewsJS');
Route::get('/news-inner/{news_slug}', 'NewsController@showNewsInnerPage');


Route::get('/contacts', 'ContactsController@showContactPage');
Route::post('/contacts', 'ContactsController@sendMail');

Route::get('/about-us', 'AboutController@showPage');

Route::get('/faq', 'QuestionsController@listQuestsByCategory');

Route::get('/search', 'SearchController@search');
Route::post('/search/getSession', 'SearchController@getSearchInputsJs');
Route::post('/search/listInputs', 'SearchController@listInputs');


