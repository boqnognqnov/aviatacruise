<?php

namespace App\Http\Controllers;

use App\CategoryNewsEntity;
use App\DestinationEntity;
use App\Http\Requests;
use App\NewsEntity;
use App\NewsGalleryEntity;
use App\PointsEntity;
use App\PromoPacketEntity;
use App\PromoPacksToRouteEntity;
use App\RouteEntity;
use App\RouteShipEntity;
use App\RouteTypeEntity;
use App\ShipEntity;
use Illuminate\Http\Request;


class AdminRoutesController extends Controller
{


    public function listRoutes()
    {
        $routes = RouteEntity::all()->toArray();

        return view('admin.routes.list', ['list' => $routes]);
    }

    public function showForm($routeId = 0)
    {

        $selectedRoute['id'] = 0;
        $points = [];

        if ($routeId != 0) {
            $selectedRoute = RouteEntity::find($routeId)->toArray();

            $pointsRaw = PointsEntity::all();

            foreach ($pointsRaw as $onePoint) {
                $points[$onePoint->id] = $onePoint->name_bg;
            }
        }

        $routeTypesRaw = RouteTypeEntity::all();
        $routeTypes = [];
        foreach ($routeTypesRaw as $oneRouteType) {
            $routeTypes[$oneRouteType->id] = $oneRouteType->name_bg;
        }

        $destinationsRaw = DestinationEntity::all();
        $destinations = [];
        foreach ($destinationsRaw as $oneDest) {
            $destinations[$oneDest->id] = $oneDest->name_bg;
        }

        $shipsRaw = ShipEntity::all()->toArray();

        $ships = [];
        foreach ($shipsRaw as $oneShip) {
            $ships[$oneShip['id']] = $oneShip['name_bg'];
        }

        $promoPacks = $this->getPromoPacks($routeId);
//        return dump($promoPacks);


        return view('admin.routes.routeForm', [
            'selectedItem' => $selectedRoute,
            'routeTypes' => $routeTypes,
            'destinations' => $destinations,
            'points' => $points,
            'ships' => $ships,
            'promoPacks' => $promoPacks,
        ]);
    }

    private function getPromoPacks($routeId)
    {
//        $packsList = PromoPacketEntity::join('promo_packet_to_route', 'promo_packets.id', '=', 'promo_packet_to_route.packet_id')
//            ->where('promo_packet_to_route.route_id', '=', $routeId)
//            ->select('promo_packets.id', 'promo_packets.name_bg')
//            ->get()
//            ->toArray();

        $allPacks = PromoPacketEntity::all()->toArray();
        $packArrToReturn = [];
        foreach ($allPacks as $onePack) {
            $tempPack = PromoPacksToRouteEntity::where('packet_id', '=', $onePack['id'])
                ->where('route_id', '=', $routeId)
                ->first();
            $packArrToReturn[$onePack['id']]['name'] = $onePack['name_bg'];
            $packArrToReturn[$onePack['id']]['route_id'] = $routeId;

            $packArrToReturn[$onePack['id']]['checked'] = false;
            if ($tempPack) {
                $packArrToReturn[$onePack['id']]['checked'] = true;
            }
        }
        return $packArrToReturn;
    }

    public function setPromoPackToRoute($routeId, $packId)
    {
        $status = PromoPacksToRouteEntity::setThisPacket($routeId, $packId);
        return redirect()->back();
    }

    public function createUpdateRoutes(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = RouteEntity::createUpdate($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/routes/create_update/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyRoute(Request $request)
    {
        $routeId = $request->get('routeId');
//        return dump($routeId);
        $status = RouteEntity::destroyRoute($routeId);

        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/routes')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

//    public function setShipsOnThisRoute(Request $request)
//    {
//
//        try {
//            $data = $request->all();
//
//
//            RouteShipEntity::where('route_id', '=', $data['routeId'])->delete();
//
//            if (isset($data['check_boxes'])) {
//                foreach ($data['check_boxes'] as $oneShipId) {
//                    $record = new RouteShipEntity();
//                    $record->route_id = $data['routeId'];
//                    $record->ship_id = $oneShipId;
//
//                    $record->save();
//                }
//            }
//
//
//        } catch (\Exception $ex) {
//            return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
//
//        }
//        return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
//    }

    public function setSeoPictureData(Request $request)
    {
        $data = $request->all();
        $status = RouteEntity::setPictureSeoData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/routes/create_update/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

}