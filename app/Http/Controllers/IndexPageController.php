<?php

namespace App\Http\Controllers;

use App\AboutTextEntity;
use App\Classes\GlobalFunctions;
use App\CompanyEntity;
use App\Http\Requests;
use App\PageAboutEntity;
use App\PageIndexSliderEntity;
use App\RouteEntity;
use App\ShipEntity;
use Illuminate\Http\Request;


class IndexPageController extends Controller
{


    public function showPage()
    {
        $lang = \App::getLocale();

        $aboutData = PageAboutEntity::first()->toArray();
        $dataBenefits = AboutTextEntity::orderBy('id')->take(4)->get()->toArray();
        $slidersData = PageIndexSliderEntity::orderBy('id')->where('is_active', '=', true)->get()->toArray();

        $routesWithDiscount = GlobalFunctions::getRandRoutesWithDiscount(10);


        $numOfTopRoutes = GlobalFunctions::getGlobalArray('index_page_top_routes');

        $topRoutesRaw = RouteEntity::where('routes.is_front', '=', true)
            ->join('ships', 'ships.id', '=', 'routes.ship_id')
            ->join('route_dates', 'routes.id', '=', 'route_dates.route_id')
            ->join('date_room_offers', 'route_dates.id', '=', 'date_room_offers.date_id')
            ->join('room_types', 'room_types.id', '=', 'date_room_offers.room_id')
            ->join('route_points', 'routes.id', '=', 'route_points.route_id')
            ->join('points', 'route_points.point_id', '=', 'points.id')
            ->distinct()
            ->orderBy('routes.id')
            ->orderBy('date_room_offers.price')
            ->orderBy('route_dates.date')
            ->orderBy('route_points.sequence', 'ASC')
            ->select(
                'routes.id as route_id',
                'routes.route_title_' . $lang . ' as route_name',
                'routes.picture as route_picture',
                'routes.route_slug_' . $lang . ' as route_slug',
                'ships.id as ship_id',
                'ships.name_' . $lang . ' as ship_name',
                'date_room_offers.price as room_price',
                'route_dates.date as route_date',
                'room_types.id as room_id',
                'room_types.name_' . $lang . ' as room_name',
                'points.id as first_point_id',
                'points.name_' . $lang . ' as first_point_name',
                'points.country_' . $lang . ' as first_point_country',
                'routes.pic_alt_' . $lang . ' as pic_alt',
                'routes.pic_title_' . $lang . ' as pic_title'
            )
            ->get()
            ->toArray();

//        return dump($topRoutesRaw);

        $topRoutes = [];
        $currentRouteId = 0;
        foreach ($topRoutesRaw as $oneRoute) {
            if ($currentRouteId != $oneRoute['route_id']) {
                $currentRouteId = $oneRoute['route_id'];
                $topRoutes[$currentRouteId] = $oneRoute;
                $topRoutes[$currentRouteId]['route_date'] = GlobalFunctions::generateDateTimeToStr($oneRoute['route_date']);
            }
        }

        $topRoutes = array_slice($topRoutes, 0, $numOfTopRoutes - 1);
        $topRoutesGroups = GlobalFunctions::transformArrByGroups(2, $topRoutes);

//        return dump(GlobalFunctions::getFavoriteNews());
        return view('index', [
            'slidersData' => $slidersData,
            'aboutData' => $aboutData,
            'benefitsData' => $dataBenefits,
            'routesWithDiscount' => $routesWithDiscount,
            'topRoutesGroups' => $topRoutesGroups,
            'seoData' => GlobalFunctions::getStaticPageSeoData(1),
            'favNews' => GlobalFunctions::getFavoriteNews()]);
    }

}