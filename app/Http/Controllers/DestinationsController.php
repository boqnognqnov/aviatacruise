<?php

namespace App\Http\Controllers;

use App\Classes\GlobalFunctions;
use App\DestinationEntity;
use App\Http\Requests;
use App\PointsEntity;
use App\RouteEntity;
use Illuminate\Http\Request;


class DestinationsController extends Controller
{


    public function listDestinations()
    {
        $lang = \App::getLocale();

        $destinationsPreferredCount = GlobalFunctions::getGlobalArray('destinations_preferred_count');

        $results = $this->getDestinations(false, 0, $destinationsPreferredCount);

        if ($destinationsPreferredCount > $results['recordCounter']) {
            $destinationsPreferredCount = $results['recordCounter'];
        }


        $ports = PointsEntity::orderBy('id')
            ->where('point_type_id', '=', 1)
            ->take(10)
            ->select(
                'points.name_' . $lang . ' as port_name',
                'points.slug_' . $lang . ' as port_slug',

                'points.pic_title_' . $lang . ' as pic_title',
                'points.pic_alt_' . $lang . ' as pic_alt',

                'points.picture')
            ->get();


        return view('destinations', [
            'results' => $results['results'],
            'ports' => $ports,
            'destinationsCount' => $destinationsPreferredCount,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'seoData' => GlobalFunctions::getStaticPageSeoData(2),
            'breadcrumps' => GlobalFunctions::breadCrumbs('destinations')

        ]);
    }

    public function listMoreDestinationsJs(Request $request)
    {

        $data = $request->all();
        $moreDestinationsCount = GlobalFunctions::getGlobalArray('destinations_more_count');
        $currentDestinationsCount = $data['count'];

        $results = $this->getDestinations(true, $currentDestinationsCount, $moreDestinationsCount);


        return \Response::json(array(
            'status' => 'success',
            'items' => $results['results'],
            'destinationsCount' => $results['recordCounter'],

        ), 200);
    }


    private function getDestinations($isJs, $skip, $take)
    {
        $lang = \App::getLocale();

        $destinationsRaw = DestinationEntity::orderBy('id')
            ->skip($skip)
            ->take($take)
            ->select('destinations.name_' . $lang . ' as dest_name',
                'destinations.description_' . $lang . ' as dest_description',
                'destinations.slug_' . $lang . ' as dest_slug',
                'destinations.pic_title_' . $lang . ' as pic_title',
                'destinations.pic_alt_' . $lang . ' as pic_alt',
                'destinations.picture')
            ->get()
            ->toArray();

        $results = [];

        if ($isJs == false) {
            $counterByTree = 0;
            $counterRecords = 0;
            foreach ($destinationsRaw as $key => $oneResult) {
                if ($counterRecords == 2) {
                    $counterByTree++;
                    $counterRecords = 0;
                }
                $counterRecords++;
                $results[$counterByTree][$key] = $oneResult;
            }

        } else {
            $results = $destinationsRaw;
        }


        $data['recordCounter'] = sizeof($destinationsRaw);
        $data['results'] = $results;

        return $data;
    }

    public function showDestination($destinationSlug)
    {
        $lang = \App::getLocale();

        $destination = DestinationEntity::where('slug_' . $lang, 'LIKE', $destinationSlug)
            ->select('destinations.id as destination_id',
                'destinations.name_' . $lang . ' as title',
                'destinations.description_' . $lang . ' as description',
                'destinations.slug_' . $lang . ' as description_slug',
                'destinations.picture',
                'destinations.seo_title_' . $lang . ' as seo_title',
                'destinations.seo_description_' . $lang . ' as seo_description',

                'destinations.pic_title_' . $lang . ' as pic_title',
                'destinations.pic_alt_' . $lang . ' as pic_alt'

            )
            ->first()->toArray();
        $destination['picture'] = DestinationEntity::$path . $destination['picture'];


        $routesPreferredCount = GlobalFunctions::getGlobalArray('destination_routes_preferred_count');

        $routesWithThisDestinations = $this->getRoutesWithThisDestination(false, 0, $routesPreferredCount, $destination['destination_id']);

//        return dump($routesWithThisDestinations);
        if ($routesPreferredCount > $routesWithThisDestinations['recordCounter']) {
            $routesPreferredCount = $routesWithThisDestinations['recordCounter'];
        }

        $moreRoutesJsData['routesPreferredCount'] = $routesPreferredCount;
        $moreRoutesJsData['destinationId'] = $destination['destination_id'];

        $breadCrumpArr['url'] = 'inner-destinations/' . $destination['description_slug'];
        $breadCrumpArr['title'] = $destination['title'];

        return view('inner-destinations', [
            'destination' => $destination,
            'routesWithThisDestinationsGroups' => $routesWithThisDestinations['results'],
            'moreRoutesJsData' => $moreRoutesJsData,
            'seoData' => GlobalFunctions::getDynamicSeoDataFromModel($destination),
            'breadcrumps' => GlobalFunctions::breadCrumbs('destinations', $breadCrumpArr)
        ]);


    }

    public function listRoutesWithThisDestinationJs(Request $request)
    {

        $data = $request->all();
//        return dump($data);
        $moreRoutesCount = GlobalFunctions::getGlobalArray('destination_routes_more_count');
        $currentRoutesCount = $data['count'];
        $destinationId = $data['destinationId'];


        $routesWithThisDestinations = $this->getRoutesWithThisDestination(true, $currentRoutesCount, $moreRoutesCount, $destinationId);


        return \Response::json(array(
            'status' => 'success',
            'items' => $routesWithThisDestinations['results'],
            'routesCount' => $routesWithThisDestinations['recordCounter'],

        ), 200);
    }

    private function getRoutesWithThisDestination($isJs, $skip, $take, $destinationId)
    {
        $lang = \App::getLocale();

        $routesWithThisDestinationsRaw = RouteEntity::where('routes.destination_id', '=', $destinationId)
            ->join('ships', 'routes.ship_id', '=', 'ships.id')
            ->join('companies', 'ships.company_id', '=', 'companies.id')
            ->distinct()
            ->select('routes.id as route_id',
                'routes.route_title_' . $lang . ' as route_name',
                'routes.route_description_' . $lang . ' as route_description',
                'routes.picture',
                'companies.name_' . $lang . ' as company_name',
                'routes.route_slug_' . $lang . ' as route_slug',
                'ships.name_' . $lang . ' as ship_name',

                'routes.pic_title_' . $lang . ' as pic_title',
                'routes.pic_alt_' . $lang . ' as pic_alt'
            )
            ->skip($skip)
            ->take($take)
            ->get()
            ->toArray();
        $routesWithThisDestinationsRaw = GlobalFunctions::getLowerPricePerCabin($routesWithThisDestinationsRaw);
        $routesWithThisDestinationsRaw = GlobalFunctions::getFirstPoint($routesWithThisDestinationsRaw);


        if ($isJs == false) {
            $counter = 0;
            $counter2 = 0;
            $routesWithThisDestinations = [];
            foreach ($routesWithThisDestinationsRaw as $oneRoute) {

                if ($counter == 2) {
                    $counter = 0;
                    $counter2++;
                }
                $routesWithThisDestinations[$counter2][$counter] = $oneRoute;
                $counter++;
            }
        } else {
            $routesWithThisDestinations = $routesWithThisDestinationsRaw;
        }

        $data['recordCounter'] = sizeof($routesWithThisDestinationsRaw);
        $data['results'] = $routesWithThisDestinations;

        return $data;
    }


}