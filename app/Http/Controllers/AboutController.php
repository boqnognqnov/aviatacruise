<?php

namespace App\Http\Controllers;

use App\AboutTextEntity;
use App\Classes\GlobalFunctions;
use App\CompanyEntity;
use App\Http\Requests;
use App\PageAboutEntity;
use Illuminate\Http\Request;


class AboutController extends Controller
{


    public function showPage()
    {
        $data = PageAboutEntity::first()->toArray();
        $dataBenefits = AboutTextEntity::where('id', '<', 5)->get()->toArray();
        $dataOther = AboutTextEntity::where('id', '>', 4)->get()->toArray();

//        return dump( GlobalFunctions::getFavoriteNews());
        return view('aboutus', [
            'data' => $data,
            'benefitsData' => $dataBenefits,
            'otherData' => $dataOther,
            'seoData' => GlobalFunctions::getStaticPageSeoData(8),
            'favNews' => GlobalFunctions::getFavoriteNews(),
            'breadcrumps'=>GlobalFunctions::breadCrumbs('about-us')
        ]);
    }

}