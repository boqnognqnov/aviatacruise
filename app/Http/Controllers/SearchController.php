<?php

namespace App\Http\Controllers;

use App\Classes\GlobalFunctions;
use App\CompanyEntity;
use App\DateRoomOfferEntity;
use App\DestinationEntity;
use App\Http\Requests;
use App\PointsEntity;
use App\RoomTypeEntity;
use App\RouteDateEntity;
use App\RouteEntity;
use App\RouteShipEntity;
use App\RouteTypeEntity;
use App\ShipEntity;
use App\ShipGalleryEntity;
use Illuminate\Http\Request;


class SearchController extends Controller
{
    public function search(Request $request)
    {
        $lang = \App::getlocale();

        $data = $request->all();
        $this->storeSearchInputs($data);
//        \Session::forget('search_inputs');
//        return dump($data);
        $routes = null;
        try {
            $routes = RouteEntity::orderBy('routes.id');

            if ($data['route_type'] != 0) {
                $routes = $routes->where('routes.route_type_id', '=', $data['route_type']);
            }

            if ($data['portId'] != 0) {
                $routes = $routes->join('route_points', 'route_points.route_id', '=', 'routes.id')
                    ->join('points', 'route_points.point_id', '=', 'points.id')
                    ->where('points.id', '=', $data['portId']);
            }

            if ($data['destinationId'] != 0) {
                $routes = $routes->where('routes.destination_id', '=', $data['destinationId']);
            }

            if ($data['companyId'] != 0) {
                $routes = $routes->join('ships', 'ships.id', '=', 'routes.ship_id')
                    ->join('companies', 'ships.company_id', '=', 'companies.id')
                    ->where('companies.id', '=', $data['companyId']);
            }

            if ($data['shipId'] != 0) {
                $routes = $routes->where('routes.ship_id', '=', $data['shipId']);
            }


            if ($data['start_date'] != 0 || $data['end_date'] != 0) {

                $startDate = $data['start_date'];
                if ($startDate == 0) {
                    $startDate = (new \DateTime())->format('Y-m-d H:i:s');
                } else {
                    $startDate = (new \DateTime($startDate))->format('Y-m-d H:i:s');
                }


                $endDate = $data['end_date'];
                if ($endDate == 0) {
                    $endDateArr = explode('-', $startDate);
                    $endDate = ($endDateArr[0] + 1) . '-' . $endDateArr[1] . '-' . $endDateArr[2];
                } else {
                    $endDate = (new \DateTime($endDate))->format('Y-m-d H:i:s');
                }


                $routes = $routes->join('route_dates', 'routes.id', '=', 'route_dates.route_id')
                    ->where('route_dates.date', '>=', $startDate)
                    ->where('route_dates.end_date', '<', $endDate);
            }


            $routes = $routes->select(
                'routes.id as route_id',
                'routes.ship_id',
                'routes.picture as route_picture',
                'routes.route_title_' . $lang . ' as route_name',
                'routes.route_slug_' . $lang . ' as route_slug'

            )
                ->get()
                ->toArray();


            $routes = GlobalFunctions::getFirstPoint($routes);
            $routes = GlobalFunctions::getLowerPricePerCabin($routes);
            $routes = $this->getShipNames($routes);

//            return dump($routes);

        } catch (\Exception $ex) {

            \Log::error($ex);

//            return dump($ex);
        }

        $resultsCount = sizeof($routes);
        $routes = GlobalFunctions::transformArrByGroups(2, $routes);

        return view('search-results', [
            'routeGroups' => $routes,
            'numOfResults' => $resultsCount,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
        ]);


    }

    private function getShipNames($routes)
    {
        $lang = \App::getLocale();
        $dataToReturn = [];
        foreach ($routes as $key => $oneRoute) {
            $dataToReturn[$key] = $oneRoute;
            $tempShip = ShipEntity::find($oneRoute['ship_id'])->toArray();
            $dataToReturn[$key]['ship_name'] = $tempShip['name_' . $lang];
        }
        return $dataToReturn;
    }

    private function storeSearchInputs($requestData)
    {
        $inputs = [];
        if (isset($requestData['route_type'])) {
            $inputs['routeTypeId'] = $requestData['route_type'];
        }
        if (isset($requestData['portId'])) {
            $inputs['portId'] = $requestData['portId'];
        }
        if (isset($requestData['duration'])) {
            $inputs['duration'] = $requestData['duration'];
        }
        if (isset($requestData['destinationId'])) {
            $inputs['destinationId'] = $requestData['destinationId'];
        }
        if (isset($requestData['dateMonth'])) {
            $inputs['dateMonth'] = $requestData['dateMonth'];
        }
        if (isset($requestData['companyId'])) {
            $inputs['companyId'] = $requestData['companyId'];
        }
        if (isset($requestData['shipId'])) {
            $inputs['shipId'] = $requestData['shipId'];
        }

        if (isset($requestData['start_date'])) {
            $inputs['start_date'] = $requestData['start_date'];
        }
        if (isset($requestData['end_date'])) {
            $inputs['end_date'] = $requestData['end_date'];
        }
        \Session::put('search_inputs', $inputs);
    }

    public function getSearchInputsJs()
    {
        $inputsData = [];
        if (\Session::get('search_inputs')) {
            $inputsData = \Session::get('search_inputs');
        }

        return \Response::json(array(
            'status' => 'success',
            'sessionData' => $inputsData,

        ), 200);
    }

    public function listInputs(Request $request)
    {
        \Session::forget('search_inputs');
        $lang = \App::getLocale();

        $firstDropDownsValues = GlobalFunctions::getGlobalArray('firstDropDownSearch_' . $lang);
        $inputs = [];
        $data = $request->all();


        $routeTypesRaw = RouteTypeEntity::join('routes', 'routes.route_type_id', '=', 'routes_types.id')
            ->select('routes_types.*')
            ->get()
            ->toArray();

        $inputs['routeTypes'][0] = $firstDropDownsValues['route_types'];
        foreach ($routeTypesRaw as $oneRouteType) {
            $inputs['routeTypes'][$oneRouteType['id']] = $oneRouteType['name_' . $lang];
        }

        if ($data['routeTypeId'] > 0) {
            $durationsRaw = RouteEntity::where('routes.route_type_id', '=', $data['routeTypeId'])
                ->orderBy('duration_count')
                ->select('routes.duration_count')
                ->get()->toArray();
        } else {
            $durationsRaw = RouteEntity::orderBy('duration_count')
                ->select('routes.duration_count')
                ->get()->toArray();
        }

//        return dump($durationsRaw);

        $inputs['durations'][0] = $firstDropDownsValues['durations'];
        foreach ($durationsRaw as $oneDuration) {
            $inputs['durations'][$oneDuration['duration_count']] = $oneDuration['duration_count'];
        }


        if ($data['routeTypeId'] > 0) {
            $destinationsRaw = DestinationEntity::join('routes', 'routes.destination_id', '=', 'destinations.id')
                ->where('destinations.id', '=', $data['routeTypeId'])
                ->select('destinations.id', 'destinations.name_' . $lang . ' as destination_name')
                ->get()
                ->toArray();
        } else {
            $destinationsRaw = DestinationEntity::orderBy('id')
                ->select('destinations.id', 'destinations.name_' . $lang . ' as destination_name')
                ->get()
                ->toArray();

        }

        $inputs['destinations'][0] = $firstDropDownsValues['destinations'];
        foreach ($destinationsRaw as $oneDestination) {
            $inputs['destinations'][$oneDestination['id']] = $oneDestination['destination_name'];
        }

//        FOR PREPARE
        $datesRaw = RouteDateEntity::orderBy('route_dates.date')
            ->select('route_dates.id', 'route_dates.date')
            ->get()
            ->toArray();
        $month = 0;

        $inputs['dates'][0] = $firstDropDownsValues['dateToStart'];
        foreach ($datesRaw as $oneDate) {
            $temp = explode('-', $oneDate['date']);
            $tempMonth = $temp[1];
            if ($month != $tempMonth) {
                $month = $tempMonth;
                $inputs['dates'][$tempMonth] = GlobalFunctions::getGlobalArray('monthNamed_' . $lang)[$tempMonth];
            }

        }

        $portsRaw = PointsEntity::where('points.point_type_id', '=', 1)
            ->select('points.id', 'points.name_' . $lang . ' as port_name')
            ->get()
            ->toArray();

        $inputs['ports'][0] = $firstDropDownsValues['ports'];
        foreach ($portsRaw as $onePort) {
            $inputs['ports'][$onePort['id']] = $onePort['port_name'];
        }

        $companyRaw = CompanyEntity::orderBy('companies.id')
            ->select('companies.id', 'companies.name_' . $lang . ' as company_name')
            ->get()
            ->toArray();

        $inputs['companies'][0] = $firstDropDownsValues['companies'];
        foreach ($companyRaw as $oneCompany) {
            $inputs['companies'][$oneCompany['id']] = $oneCompany['company_name'];
        }

        $shipsRaw = [];
        if ($data['routeTypeId'] > 0) {
            $shipsRaw = ShipEntity::orderBy('ships.id')
                ->join('routes', 'routes.ship_id', '=', 'ships.id')
                ->where('routes.route_type_id', '=', $data['routeTypeId'])
                ->select('ships.id', 'ships.name_' . $lang . ' as ship_name')
                ->get()
                ->toArray();
        } else {
            $shipsRaw = ShipEntity::orderBy('ships.id')
                ->select('ships.id', 'ships.name_' . $lang . ' as ship_name')
                ->get()
                ->toArray();
        }


        $inputs['ships'][0] = $firstDropDownsValues['ships'];
        foreach ($shipsRaw as $oneShip) {
            $inputs['ships'][$oneShip['id']] = $oneShip['ship_name'];
        }


//        FOR PREPARE


        return \Response::json(array(
            'status' => 'success',
            'inputs' => $inputs,

        ), 200);
    }
}