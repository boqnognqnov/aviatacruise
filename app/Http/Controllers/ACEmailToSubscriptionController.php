<?php

namespace App\Http\Controllers;

use App\CategoryNewsEntity;
use App\Classes\GlobalFunctions;
use App\DestinationEntity;
use App\EmailSubscriptionEntity;
use App\Http\Requests;
use App\NewsEntity;
use App\NewsGalleryEntity;
use App\PointsEntity;
use App\RouteEntity;
use App\RouteGalleryEntity;
use App\RouteTypeEntity;
use Illuminate\Http\Request;


class ACEmailToSubscriptionController extends Controller
{


    public function emailSubscription(Request $request)
    {
        $email = $request->get('email_to_subscription');
//        "email_to_subscription" => "boqnognqnov@abv.bg"

        $status = EmailSubscriptionEntity::setEmail($email);

        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('/')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }

    public function adminListEmails()
    {
        $emails = EmailSubscriptionEntity::all()->toArray();
        return view('admin.emailSubcriptions.list', ['emails' => $emails]);
    }

    public function adminDestroyEmail(Request $request)
    {
        $emailId = $request->get('emailId');

        $status = EmailSubscriptionEntity::destroyEmail($emailId);
        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }


    }

    public function sendMessageToAllUsers(Request $request)
    {
        $text = $request->get('text');
        if (empty($text)) {
            return back()->withInput()->withErrors('Моля попълнете полето за съобщение !');
        }

        $allUsers = EmailSubscriptionEntity::all();
        $data['users'] = GlobalFunctions::getRecordPropertiesIfExist($allUsers, []);
        $data['text'] = $text;


        try {

            \Mail::send('emails.subscriptionMsg', $data, function ($message) use ($data) {
                $message->from('administrtor@abv.bg');
                foreach ($data['users'] as $key => $oneUser) {
                    if ($key == 0) {
                        $message->to($oneUser['email'], 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
                    } else {
                        $message->bcc($oneUser['email'], 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
                    }

                }
            });

        } catch (\Exception $ex) {

            return back()->withInput()->withErrors('Грешка при опит за изпращана на съобщение !');
        }
        return redirect()->back()->with('success', 'Вие успешно изпратихте съобщението');

    }


}