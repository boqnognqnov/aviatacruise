<?php

namespace App\Http\Controllers;

use App\CategoryNewsEntity;
use App\Classes\GlobalFunctions;
use App\DestinationEntity;
use App\Http\Requests;
use App\NewsEntity;
use App\NewsGalleryEntity;
use App\PointsEntity;
use App\RouteDateEntity;
use App\RouteEntity;
use App\RoutePointEntity;
use App\RouteShipEntity;
use App\RouteTypeEntity;
use App\ShipEntity;
use Illuminate\Http\Request;


class AdminRouteDatesController extends Controller
{


    public function listDates($routeId, $dateId = 0)
    {
        $dateRecord['id'] = 0;
        if ($dateId != 0) {
            $dateRecord = RouteDateEntity::find($dateId)->toArray();
            $dateRecord['date'] = GlobalFunctions::generateDateTimeToStr($dateRecord['date']);
            $dateRecord['end_date'] = GlobalFunctions::generateDateTimeToStr($dateRecord['end_date']);

        }

        $listOfDates = RouteDateEntity::where('route_id', '=', $routeId)->get()->toArray();

//        $listOfShips = ShipEntity::join('routes_ships', 'ships.id', '=', 'routes_ships.ship_id')
//            ->where('routes_ships.route_id', '=', $routeId)->select('ships.id', 'ships.name_bg')->get();

        $listOfShips = ShipEntity::join('routes', 'ships.id', '=', 'routes.ship_id')
            ->where('routes.id', '=', $routeId)->select('ships.id', 'ships.name_bg')->get();


        return view('admin.routes.listRouteDates', [
            'list' => $listOfDates,
            'selectedItem' => $dateRecord,
            'routeId' => $routeId,
            'ships' => $listOfShips
        ]);
    }

    public function setDateOnRoute(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = RouteDateEntity::setDate($data);
//        return dump($status);
        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/routes-dates/list/' . $status[1]->route_id . '/' . $status[1]->date_id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyEvent(Request $request)
    {
        $data = $request->get('dateRecId');
        $status = RouteDateEntity::destroyRouteEvent($data);
        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/routes-dates/list/' . $request->get('routeId'))->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setActivity($dateRecId)
    {

        $status = RouteDateEntity::changeStatus($dateRecId);
        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/routes-dates/list/' . $status[1]->route_id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


}