<?php

namespace App\Http\Controllers;

use App\AboutTextEntity;
use App\CompanyEntity;
use App\Http\Requests;
use App\PageAboutEntity;
use Illuminate\Http\Request;


class AdminPageAboutController extends Controller
{


    public function showAboutForm()
    {
        $selectedItem = PageAboutEntity::find(1);

        return view('admin.about-us.aboutForm', ['selectedItem' => $selectedItem]);
    }

    public function updateAboutPage(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = PageAboutEntity::updatePage($data);

        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.updatingError'));
            case 'success':
                return back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setSeoSliderPictureData(Request $request)
    {
        $data = $request->all();
        $status = PageAboutEntity::setSeoSliderPictureData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setAboutSeoPictureData(Request $request)
    {
        $data = $request->all();
        $status = PageAboutEntity::setAboutSeoPictureData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setIndexSeoPictureData(Request $request)
    {
        $data = $request->all();
        $status = PageAboutEntity::setIndexSeoPictureData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


}