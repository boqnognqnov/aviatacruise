<?php

namespace App\Http\Controllers;

use App\CategoryNewsEntity;
use App\DestinationEntity;
use App\Http\Requests;
use App\NewsEntity;
use App\NewsGalleryEntity;
use App\PointsEntity;
use App\RouteEntity;
use App\RouteGalleryEntity;
use App\RouteTypeEntity;
use Illuminate\Http\Request;


class AdminRoutesGalleryController extends Controller
{


    public function showGallery($routeId)
    {

//        $newsT = RouteGalleryEntity::all();
//        $counter = 1;
//        foreach ($newsT as $one) {
//            $one->position = $counter;
//            $one->save();
//            $counter++;
//        }


        $gallery = RouteGalleryEntity::where('route_id', '=', $routeId)->orderBy('position')->get()->toArray();
        return view('admin.routes.routeGallery', [
            'gallery' => $gallery,
            'routeId' => $routeId,
        ]);
    }

    public function uploadPictures(Request $request)
    {
        $data = $request->all();

        if ($data['pictures'][0] == null) {
            return back();
        }

        $status = RouteGalleryEntity::uploadImages($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/routes-gallery/' . $data['route_id'])->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyPicture(Request $request)
    {
        $imgId = $request->get('gallery_id');
//        $record = RouteGalleryEntity::find($imgId);
//        return dump($record);
        $status = RouteGalleryEntity::destroyImg($imgId);

        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function moveImgRight($routeId, $imageId)
    {
//        return dump($newsId);
        $max = RouteGalleryEntity::orderBy('position', 'DESC')->get()->first()->position;

        $currentRecord = RouteGalleryEntity::find($imageId);
        $currentPosition = $currentRecord->position;
        if ($max > $currentPosition) {

            $downRecord = RouteGalleryEntity::where('position', '>', $currentPosition)
                ->where('route_id', '=', $routeId)
                ->orderBy('position', 'ASC')->first();
            if ($downRecord) {
                $currentRecord->position = $downRecord->position;
                $currentRecord->save();
                $downRecord->position = $currentPosition;
                $downRecord->save();
            }
        }

        return redirect()->back();

    }

    public function moveImgLeft($routeId, $imageId)
    {
//        return dump($imageId);
        $min = RouteGalleryEntity::orderBy('position', 'ASC')->get()->first()->position;

        $currentRecord = RouteGalleryEntity::find($imageId);
        $currentPosition = $currentRecord->position;
        if ($min < $currentPosition) {

            $downRecord = RouteGalleryEntity::where('position', '<', $currentPosition)
                ->where('route_id', '=', $routeId)
                ->orderBy('position', 'DESC')->first();
            if ($downRecord) {
                $currentRecord->position = $downRecord->position;
                $currentRecord->save();
                $downRecord->position = $currentPosition;
                $downRecord->save();
            }

        }

        return redirect()->back();

    }

    public function setSeoPictureDataGallery(Request $request)
    {
        $data = $request->all();
        $status = RouteGalleryEntity::setPictureSeoData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


}