<?php

namespace App\Http\Controllers;

use App\AdditionalInfoEntity;
use App\Classes\GlobalFunctions;
use App\Classes\MakeReservation;
use App\CompanyEntity;
use App\DateRoomOfferEntity;
use App\Http\Requests;
use App\PageContactsEntity;
use App\PointsEntity;
use App\PointTypeEntity;
use App\PromoPacketEntity;
use App\RoomTypeEntity;
use App\RouteDateEntity;
use App\RouteEntity;
use App\RouteGalleryEntity;
use App\RouteShipEntity;
use App\RouteTypeEntity;
use App\ShipEntity;
use App\ShipGalleryEntity;
use Illuminate\Http\Request;


class ContactsController extends Controller
{


    public function showContactPage()
    {
        $record = PageContactsEntity::first()->toArray();

        $lang = \App::getLocale();
        $record['address'] = $record['address_' . $lang];
        $record['work_time'] = $record['work_time_' . $lang];

//        $cruiseTypesRaw = RouteTypeEntity::all()->toArray();
//        $cruiseTypes = [];
//        foreach ($cruiseTypesRaw as $oneType) {
//            $cruiseTypes[$oneType['id']] = $oneType['name_' . $lang];
//        }
        $coordinatesArr = explode(', ', $record['coordinates']);


        return view('contacts', [
            'record' => $record,
//            'routeTypes' => $cruiseTypes,
            'coordinatesArr' => $coordinatesArr,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'seoData' => GlobalFunctions::getStaticPageSeoData(9),
            'breadcrumps'=>GlobalFunctions::breadCrumbs('contacts')

        ]);
    }

    public static $rules = array(
        'name' => 'required',
        'email' => 'required',
//        'port' => 'numeric|required',
        'text' => 'required',

    );


    public function sendMail(Request $request)
    {

        $data = $request->all();


        $rules = self::$rules;

        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            return redirect('contacts#status_message')->withInput()->withErrors($validator);
        }


        $data['email'] = $request->get('email');
        $data['text'] = $request->get('text_message');
        $data['name'] = $request->get('name');

        if (empty($data['name']) || empty($data['email']) || empty($data['text'])) {
            return back()->withInput()->withErrors('error', trans('contacts.emptyInputs'));
        }
        try {
            if (!empty($data['email'])) {
                \Mail::send('emails.requestMessage', $data, function ($message) use ($data) {
                    $message->from($data['email'], $data['name']);
                    $message->to('boqnognqnov@gmail.com', 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
                    $message->bcc('bozhidarovboyan@gmail.com', 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
//                    $message->bcc('alarma7a@gmail.com', 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
                });
            }
        } catch (\Exception $ex) {
            return back()->withErrors(trans('contacts.errorException'));
        }
        return redirect()->back()->with('successMessage', trans('contacts.successMessage'));


    }


}