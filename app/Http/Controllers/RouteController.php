<?php

namespace App\Http\Controllers;

use App\Classes\GlobalFunctions;
use App\CompanyEntity;
use App\DateRoomOfferEntity;
use App\Http\Requests;
use App\PointsEntity;
use App\RoomTypeEntity;
use App\RouteEntity;
use App\RouteShipEntity;
use App\RouteTypeEntity;
use App\ShipEntity;
use App\ShipGalleryEntity;
use Illuminate\Http\Request;


class RouteController extends Controller
{


    public function listRouteTypes()
    {
        $cruiseTypesPreferredCount = GlobalFunctions::getGlobalArray('cruise_types_preferred_count');

        $routeTypesRaw = RouteTypeEntity::orderBy('id')->take($cruiseTypesPreferredCount)->get()->toArray();

        $routeTypes = GlobalFunctions::transformArrByGroups(2, $routeTypesRaw);


        if (sizeof($routeTypesRaw) < $cruiseTypesPreferredCount) {
            $cruiseTypesPreferredCount = sizeof($routeTypesRaw);
        }


        return view('cruise-type-list', [
            'routeTypes' => $routeTypes,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'routeTypesCount' => $cruiseTypesPreferredCount,
            'seoData' => GlobalFunctions::getStaticPageSeoData(3),
            'breadcrumps' => GlobalFunctions::breadCrumbs('cruize-types')

        ]);

    }

    public function moreCruiseTypesJS(Request $request)
    {
        $data = $request->all();
        $moreTypeCount = GlobalFunctions::getGlobalArray('cruise_types_more_count');
        $currentTypeCount = $data['count'];


        $typesRaw = RouteTypeEntity::orderBy('id')->skip($currentTypeCount)->take($moreTypeCount)->get()->toArray();
        $types = [];
        foreach ($typesRaw as $key => $oneItem) {
            $types[$key] = $oneItem;
            $types[$key]['name'] = $oneItem['name_' . \App::getLocale()];
            $types[$key]['text'] = $oneItem['text_' . \App::getLocale()];
            $types[$key]['slug'] = $oneItem['slug_' . \App::getLocale()];
        }

        return \Response::json(array(
            'status' => 'success',
            'items' => $types,
            'routeTypesCount' => $currentTypeCount,

        ), 200);
    }


    public function showRouteType($slug)
    {
        $lang = \App::getLocale();

        $routeType = RouteTypeEntity::where('slug_' . $lang, 'LIKE', $slug)->first()->toArray();

        $routesOnThisTypePreferredCount = GlobalFunctions::getGlobalArray('cruise_list_on_type_preferred_count');


        $routesOfThisTypeRaw = $this->getRoutesOfThisTypeRaw(0, $routesOnThisTypePreferredCount, $routeType['id']);

        $routesOfThisTypeRaw = GlobalFunctions::getLowerPricePerCabin($routesOfThisTypeRaw);
        $routesOfThisTypeRaw = GlobalFunctions::getFirstPoint($routesOfThisTypeRaw);

        $routesOfThisType = GlobalFunctions::transformArrByGroups(2, $routesOfThisTypeRaw);


        if (sizeof($routesOfThisTypeRaw) < $routesOnThisTypePreferredCount) {
            $routesOnThisTypePreferredCount = sizeof($routesOfThisTypeRaw);
        }
//        return dump($routesOfThisTypeRaw);

        $breadCrumpArr['url'] = 'inner-cruize-type/' . $routeType['slug_' . $lang];
        $breadCrumpArr['title'] = $routeType['name_' . $lang];

        return view('inner-cruize-type', [
            'routeType' => $routeType,
            'routesOfThisType' => $routesOfThisType,
            'routeTypeSlug' => $slug,
            'routesOnThisTypeCount' => $routesOnThisTypePreferredCount,
            'seoData' => GlobalFunctions::getDynamicSeoDataFromModel($routeType, true),
            'breadcrumps' => GlobalFunctions::breadCrumbs('cruize-types', $breadCrumpArr)
        ]);
    }


    public function moreRoutesOnThisTypeJS(Request $request)
    {
        $data = $request->all();
        $moreRouteOfTypeCount = GlobalFunctions::getGlobalArray('cruise_list_on_type_more_count');

        $currentRouteOfTypeCount = $data['count'];
        $currentSlug = $data['currentSlug'];

        $lang = \App::getLocale();

        $routeType = RouteTypeEntity::where('slug_' . $lang, 'LIKE', $currentSlug)->first()->toArray();

        $routesOfThisTypeRaw = $this->getRoutesOfThisTypeRaw($currentRouteOfTypeCount, $moreRouteOfTypeCount, $routeType['id']);

        $routesOfThisTypeRaw = GlobalFunctions::getLowerPricePerCabin($routesOfThisTypeRaw);
        $routesOfThisTypeRaw = GlobalFunctions::getFirstPoint($routesOfThisTypeRaw);


        $routesOfThisType = [];
        foreach ($routesOfThisTypeRaw as $key => $oneItem) {
            $routesOfThisType[$key] = $oneItem;
            $routesOfThisType[$key]['name'] = $oneItem['route_name'];
            $routesOfThisType[$key]['text'] = $oneItem['route_description'];
            $routesOfThisType[$key]['slug'] = $oneItem['route_slug'];
        }

        return \Response::json(array(
            'status' => 'success',
            'items' => $routesOfThisType,
            'routesOnThisTypeCount' => $currentRouteOfTypeCount,

        ), 200);
    }

    private function getRoutesOfThisTypeRaw($skip, $take, $routeTypeId)
    {

        $lang = \App::getLocale();

        $routesOfThisTypeRaw = RouteEntity::
        join('ships', 'routes.ship_id', '=', 'ships.id')
            ->join('companies', 'ships.company_id', '=', 'companies.id')
            ->where('route_type_id', '=', $routeTypeId)
            ->distinct()
            ->select('routes.id as route_id',
                'routes.route_title_' . $lang . ' as route_name',
                'routes.route_description_' . $lang . ' as route_description',
                'routes.picture', 'companies.name_' . $lang . ' as company_name',
                'routes.route_slug_' . $lang . ' as route_slug',

                'routes.pic_title_' . $lang . ' as pic_title',
                'routes.pic_alt_' . $lang . ' as pic_alt'

            )
            ->skip($skip)
            ->take($take)
            ->get()
            ->toArray();

        return $routesOfThisTypeRaw;
    }


    public function listPromotions()
    {

        $promoRoutesPreferredCount = GlobalFunctions::getGlobalArray('promo_routes_preferred_count');

        $results = $this->getPromoRoutes(false, 0, $promoRoutesPreferredCount);

        if ($promoRoutesPreferredCount > $results['recordCounter']) {
            $promoRoutesPreferredCount = $results['recordCounter'];
        }

        return view('promotions', [
            'results' => $results['results'],
            'promoRouteCount' => $promoRoutesPreferredCount,
            'seoData' => GlobalFunctions::getStaticPageSeoData(4),
            'favNews' => GlobalFunctions::getFavoriteNews(),
            'breadcrumps' => GlobalFunctions::breadCrumbs('promotions')
        ]);
    }

    public function listMorePromotionsJs(Request $request)
    {

        $data = $request->all();
        $moreTypeCount = GlobalFunctions::getGlobalArray('promo_routes_more_count');
        $currentTypeCount = $data['count'];


        $results = $this->getPromoRoutes(true, $currentTypeCount, $moreTypeCount);


        return \Response::json(array(
            'status' => 'success',
            'items' => $results['results'],
            'promoRouteCount' => $results['recordCounter'],

        ), 200);
    }


    private function getPromoRoutes($isJs, $skip, $take)
    {
        $lang = \App::getLocale();

        $routes = RouteEntity::join('destinations', 'routes.destination_id', '=', 'destinations.id')
            ->join('route_dates', 'routes.id', '=', 'route_dates.route_id')
            ->where('route_dates.discount', '>', 0)
            ->join('date_room_offers', 'route_dates.id', '=', 'date_room_offers.date_id')
            ->where('date_room_offers.price', '>', 0)
            ->orderBy('routes.id')
            ->orderBy('date_room_offers.price')
            ->orderBy('route_dates.date')
            ->distinct()
            ->select('routes.id as route_id',
                'routes.route_title_' . $lang . ' as route_name',
                'routes.route_slug_' . $lang . ' as route_slug',
                'date_room_offers.price as room_price',
                'route_dates.date as route_start_date',
                'destinations.name_' . $lang . ' as destination_name',
                'routes.picture as route_picture',
                'route_dates.discount as route_discount',

                'routes.pic_title_' . $lang . ' as pic_title',
                'routes.pic_alt_' . $lang . ' as pic_alt'
            )
            ->get()
            ->toArray();

        $routesWithMinPricePerRoom = [];
        $routeId = 0;

        foreach ($routes as $oneRoute) {
            if ($routeId != $oneRoute['route_id']) {
                $routeId = $oneRoute['route_id'];
                $routesWithMinPricePerRoom[$oneRoute['route_id']] = $oneRoute;
                $routesWithMinPricePerRoom[$oneRoute['route_id']]['route_start_date'] = GlobalFunctions::generateDateTimeToStr($oneRoute['route_start_date']);
            }
        }

        $allRoutesData = GlobalFunctions::getFirstPoint($routesWithMinPricePerRoom);
        $resultsRaw = array_slice($allRoutesData, $skip, $take);

        $results = [];

        if ($isJs == false) {
            $counterByTree = 0;
            $counterRecords = 0;
            foreach ($resultsRaw as $key => $oneResult) {
                if ($counterRecords == 3) {
                    $counterByTree++;
                    $counterRecords = 0;
                }
                $counterRecords++;
                $results[$counterByTree][$key] = $oneResult;
            }

        } else {
            $results = $resultsRaw;
        }


        $data['recordCounter'] = sizeof($resultsRaw);
        $data['results'] = $results;

        return $data;
    }


}