<?php

namespace App\Http\Controllers;

use App\CategoryNewsEntity;
use App\CategoryQuestionsEntity;
use App\Http\Requests;
use App\NewsEntity;
use App\NewsGalleryEntity;
use App\QuestionsEntity;
use Illuminate\Http\Request;


class AdminQuestionsController extends Controller
{


    public function listQuests()
    {

        $quests = QuestionsEntity::join('question_categories', 'questions.category_id', '=', 'question_categories.id')
            ->orderBy('questions.created_at', 'DESC')
            ->select('questions.id as question_id', 'questions.name_bg as question_name', 'questions.created_at as question_date', 'question_categories.name_bg as category_name')
            ->get()->toArray();

        return view('admin.questions.list', ['items' => $quests]);
    }

    public function showEditForm($questId = 0)
    {
        $record['id'] = 0;
        if ($questId > 0) {
            $record = QuestionsEntity::find($questId)->toArray();
        }
        $categoriesRaw = CategoryQuestionsEntity::all()->toArray();
        $categories = [];
        foreach ($categoriesRaw as $oneCategory) {
            $categories[$oneCategory['id']] = $oneCategory['name_bg'];
        }


        return view('admin.questions.questForm', ['selectedItem' => $record, 'categories' => $categories]);
    }

    public function storeChanges(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = QuestionsEntity::createUpdate($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/questions/create_edit/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }


    }

    public function destroyQuestion(Request $request)
    {
        $data = $request->all();
        $status = QuestionsEntity::destroyQuest($data['questId']);
        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/questions/list')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }


    public function listCategories($questId = 0)
    {

        $categories = CategoryQuestionsEntity::all()->toArray();

        if ($questId != 0) {
            $selectedCat = CategoryQuestionsEntity::find($questId)->toArray();


            return view('admin.questions.categoryList', [
                'categories' => $categories,
                'selectedCat' => $selectedCat
            ]);
        }

        return view('admin.questions.categoryList', ['categories' => $categories]);
    }

    public function addOrEditCategories(Request $request)
    {

        $data = $request->all();
//        return dump($data);
        $status = CategoryQuestionsEntity::createUpdate($data);
        return redirect()->back();
    }

    public function destroyCategory(Request $request)
    {

        $data = $request->all();
        $status = CategoryQuestionsEntity::destroyCategory($data['catId']);
        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/questions/category')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


}