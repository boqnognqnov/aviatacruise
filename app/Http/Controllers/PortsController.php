<?php

namespace App\Http\Controllers;

use App\CategoryNewsEntity;
use App\Classes\GlobalFunctions;
use App\Http\Requests;
use App\NewsEntity;
use App\NewsGalleryEntity;
use App\PointsEntity;
use App\RouteEntity;
use App\RoutePointEntity;
use Illuminate\Http\Request;


class PortsController extends Controller
{


    public function listPorts()
    {
        $ports = PointsEntity::where('point_type_id', '=', 1)->get()->toArray();

        return view('ports', [
            'ports' => $ports,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'seoData' => GlobalFunctions::getStaticPageSeoData(11)

        ]);
    }

    public function showPort($portSlug)
    {
        $lang = \App::getLocale();
        $port = PointsEntity::where('slug_' . $lang, 'LIKE', $portSlug)->first()->toArray();

        $routesWithThisPort = RouteEntity::join('ships', 'routes.ship_id', '=', 'ships.id')
            ->join('companies', 'ships.company_id', '=', 'companies.id')
            ->join('route_points', 'routes.id', '=', 'route_points.route_id')
            ->join('points', 'points.id', '=', 'route_points.point_id')
            ->where('points.id', '=', $port['id'])
            ->distinct()
            ->select('routes.id as route_id',
                'routes.route_title_' . $lang . ' as route_name',
                'routes.route_description_' . \App::getLocale() . ' as route_description',
                'routes.picture',
                'companies.name_' . $lang . ' as company_name',
                'routes.route_slug_' . $lang . ' as route_slug'
            )
            ->get()
            ->toArray();

        $routesWithThisPort = GlobalFunctions::getLowerPricePerCabin($routesWithThisPort);
        $routesWithThisPort = GlobalFunctions::getFirstPoint($routesWithThisPort);

        $coordinatesArr = explode(', ', $port['coordinates']);

        return view('inner-port', [
            'port' => $port,
            'routesWithThisPort' => $routesWithThisPort,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'coordinatesArr' => $coordinatesArr,
        ]);

    }


}