<?php

namespace App\Http\Controllers;

use App\Classes\GlobalFunctions;
use App\CompanyEntity;
use App\DateRoomOfferEntity;
use App\Http\Requests;
use App\PointsEntity;
use App\RoomTypeEntity;
use App\RouteEntity;
use App\RouteShipEntity;
use App\RouteTypeEntity;
use App\ShipEntity;
use App\ShipGalleryEntity;
use Illuminate\Http\Request;


class TopRoutesController extends Controller
{


    public function listTopRoutes()
    {

        $topRoutesPreferredCount = GlobalFunctions::getGlobalArray('top_routes_preferred_count');

        $results = $this->getTopRoutes(false, 0, $topRoutesPreferredCount);

        if ($topRoutesPreferredCount > $results['recordCounter']) {
            $topRoutesPreferredCount = $results['recordCounter'];
        }

        return view('top-routes', [
            'results' => $results['results'],
            'topRouteCount' => $topRoutesPreferredCount,
            'seoData' => GlobalFunctions::getStaticPageSeoData(12),
            'favNews' => GlobalFunctions::getFavoriteNews(),
            'breadcrumps' => GlobalFunctions::breadCrumbs('top-cruises')
        ]);
    }

    public function listMoreTopRoutesJs(Request $request)
    {

        $data = $request->all();
        $moreTypeCount = GlobalFunctions::getGlobalArray('top_routes_more_count');
        $currentTypeCount = $data['count'];


        $results = $this->getTopRoutes(true, $currentTypeCount, $moreTypeCount);


        return \Response::json(array(
            'status' => 'success',
            'items' => $results['results'],
            'topRouteCount' => $results['recordCounter'],

        ), 200);
    }


    private function getTopRoutes($isJs, $skip, $take)
    {
        $lang = \App::getLocale();

        $routes = RouteEntity::where('routes.is_front', '=', true)
            ->join('destinations', 'routes.destination_id', '=', 'destinations.id')
            ->join('route_dates', 'routes.id', '=', 'route_dates.route_id')
            ->join('date_room_offers', 'route_dates.id', '=', 'date_room_offers.date_id')
            ->where('date_room_offers.price', '>', 0)
            ->orderBy('routes.id')
            ->orderBy('date_room_offers.price')
            ->orderBy('route_dates.date')
            ->distinct()
            ->select('routes.id as route_id', 'routes.route_title_' . $lang . ' as route_name', 'routes.route_slug_' . $lang . ' as route_slug', 'date_room_offers.price as room_price', 'route_dates.date as route_start_date', 'destinations.name_' . $lang . ' as destination_name', 'routes.picture as route_picture', 'route_dates.discount as route_discount')
            ->get()
            ->toArray();

        $routesWithMinPricePerRoom = [];
        $routeId = 0;

        foreach ($routes as $oneRoute) {
            if ($routeId != $oneRoute['route_id']) {
                $routeId = $oneRoute['route_id'];
                $routesWithMinPricePerRoom[$oneRoute['route_id']] = $oneRoute;
                $routesWithMinPricePerRoom[$oneRoute['route_id']]['route_start_date'] = GlobalFunctions::generateDateTimeToStr($oneRoute['route_start_date']);
            }
        }

        $allRoutesData = GlobalFunctions::getFirstPoint($routesWithMinPricePerRoom);
        $resultsRaw = array_slice($allRoutesData, $skip, $take);


        if ($isJs == false) {
            $results = GlobalFunctions::transformArrByGroups(3, $resultsRaw);

        } else {
            $results = $resultsRaw;
        }


        $data['recordCounter'] = sizeof($resultsRaw);
        $data['results'] = $results;

        return $data;
    }


}