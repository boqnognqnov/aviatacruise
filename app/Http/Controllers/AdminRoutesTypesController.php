<?php

namespace App\Http\Controllers;

use App\CompanyEntity;
use App\Http\Requests;
use App\RouteTypeEntity;
use Illuminate\Http\Request;


class AdminRoutesTypesController extends Controller
{

    public function listRouteTypes()
    {

        $routeTypes = RouteTypeEntity::orderBy('created_at', 'DESC')->get()->toArray();

        return view('admin.routes.route-types-list', ['routeTypes' => $routeTypes]);
    }

    public function showCreateUpdate($routeType = 0)
    {
        $selectedRoutesTypes['id'] = 0;

        if ($routeType != 0) {

            $selectedRoutesTypes = RouteTypeEntity::find($routeType)->toArray();
        }
//        return dump($selectedRoutesTypes);

        return view('admin.routes.routeTypeForm', [
            'selectedRoutesTypes' => $selectedRoutesTypes,
        ]);
    }

    public function createUpdateRouteType(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = RouteTypeEntity::createUpdate($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('/admin/routes-types/create_update/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyRouteType(Request $request)
    {
        $routeTypeId = $request->get('routeTypeId');
//        return dump($routeTypeId);
        $status = RouteTypeEntity::destroyRouteТype($routeTypeId);

        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/routes-types')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setSeoPictureData(Request $request)
    {
        $data = $request->all();
        $status = RouteTypeEntity::setPictureSeoData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/routes-types/create_update/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }
}