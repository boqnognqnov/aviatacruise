<?php

namespace App\Http\Controllers;

use App\CategoryNewsEntity;
use App\Classes\GlobalFunctions;
use App\Http\Requests;
use App\NewsEntity;
use App\NewsGalleryEntity;
use App\SeoStaticPagesEntity;
use Illuminate\Http\Request;


class SeoController extends Controller
{

    public function listStaticPages()
    {
        $records = SeoStaticPagesEntity::all()->toArray();

        return view('admin.seo.list', ['records' => $records]);
    }

    public function showPageSeoForm($pageId)
    {
//        $model = $modelName->first()->toArray();
        $record = SeoStaticPagesEntity::find($pageId)->toArray();

//        return dump($record);

        return view('admin.seo.pageForm', ['record' => $record]);
    }

    public function setSeoDataOnPage(Request $request)
    {
        $data = $request->all();

        $status = SeoStaticPagesEntity::updatePageData($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/seo/static-pages/show_form/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }


}