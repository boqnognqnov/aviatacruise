<?php

namespace App\Http\Controllers;

use App\AboutTextEntity;
use App\AdditionalInfoEntity;
use App\Classes\GlobalFunctions;
use App\CompanyEntity;
use App\DateRoomOfferEntity;
use App\Http\Requests;
use App\PageAboutEntity;
use App\PromoPacketEntity;
use App\ReservationsEntity;
use Illuminate\Http\Request;


class AdminReservationController extends Controller
{


    public function listReservations()
    {
        $reservationInfos = $this->getReservationInfo();
//        return dump($reservationInfos);


        return view('admin.reservations.list', [
            'items' => $reservationInfos
        ]);
    }

    private function getReservationInfo($reservationId = 0)
    {
        $reservationInfos = ReservationsEntity::join('clients', 'reservations.id', '=', 'clients.reservation_id')
            ->where('clients.is_titular', '=', true);


        if ($reservationId > 0) {
            $reservationInfos = $reservationInfos->where('reservations.id', '=', $reservationId);
        }

        $reservationInfos = $reservationInfos->join('route_dates', 'reservations.route_date_id', '=', 'route_dates.id')
            ->join('room_types', 'reservations.room_type_id', '=', 'room_types.id')
            ->join('date_room_offers', 'route_dates.id', '=', 'date_room_offers.date_id')
            ->join('routes', 'route_dates.route_id', '=', 'routes.id')
            ->join('ships', 'routes.ship_id', '=', 'ships.id')
//
            ->distinct()
            ->select('reservations.id as reservation_id',
                'reservations.passengers as passengers',
                'clients.fname as titular_fname',
                'clients.sname as titular_sname',
                'clients.lname as titular_lname',
                'route_dates.date as start_route_date',
                'room_types.name_bg as cabin_name',
                'reservations.comment as reservation_comment',
                'route_dates.id as route_date_id',
                'room_types.id as room_id',
                'route_dates.discount as route_discount',
                'routes.route_title_bg as route_name',
                'ships.name_bg as ship_name',
                'clients.email as titular_email',
                'clients.phone as titular_phone'
            )
            ->get()
            ->toArray();


        $reservationInfos = $this->getPromoPacks($reservationInfos);

        $reservationInfos = $this->calculatePrices($reservationInfos);

        $reservationInfos = $this->getInfoSubscription($reservationInfos);

        return $reservationInfos;
    }

    private function calculatePrices($data)
    {
        $temp1 = [];
        $totalPrice = 0;

        foreach ($data as $key => $oneItem) {
            $temp1[$key] = $oneItem;
            $dateRoomOfferData = DateRoomOfferEntity::where('room_id', '=', $oneItem['room_id'])->where('date_id', '=', $oneItem['route_date_id'])->first()->toArray();
            $temp1[$key]['date_room_offer_id'] = $dateRoomOfferData['id'];
            $temp1[$key]['room_price_without_discount'] = $dateRoomOfferData['price'];
            $temp1[$key]['room_discount'] = $dateRoomOfferData['room_discount'];
            $temp1[$key]['room_discount_price_by_one'] = GlobalFunctions::calculatePriceByParams($dateRoomOfferData['price'], $dateRoomOfferData['room_discount'], $oneItem['route_discount'], 1);

            $totalPrice += ($temp1[$key]['room_discount_price_by_one'] * $oneItem['passengers']);

            if (isset($temp1[$key]['promoPackets'])) {
                foreach ($temp1[$key]['promoPackets'] as $onePromoPack) {
                    $totalPrice += $onePromoPack['packet_price'] * $oneItem['passengers'];
                }
            }

            $temp1[$key]['total_price'] = $totalPrice;
        }
        return $temp1;
    }

    private function getPromoPacks($data)
    {
        $temp1 = [];
        foreach ($data as $key => $oneItem) {
            $temp1[$key] = $oneItem;
            $packets = PromoPacketEntity::join('reservation_packets', 'reservation_packets.packet_id', '=', 'promo_packets.id')
                ->where('reservation_packets.reservation_id', '=', $oneItem['reservation_id'])
                ->select()
                ->get()
                ->toArray();
            foreach ($packets as $packKey => $onePack) {
                $temp1[$key]['promoPackets'][$packKey]['packet_name'] = $onePack['name_bg'];
                $temp1[$key]['promoPackets'][$packKey]['packet_price'] = $onePack['price'];
            }


        }
        return $temp1;
    }

    private function getInfoSubscription($data)
    {
        $temp1 = [];
        foreach ($data as $key => $oneItem) {
            $temp1[$key] = $oneItem;
            $packets = AdditionalInfoEntity::join('reservation_additional_info', 'reservation_additional_info.info_id', '=', 'additional_info.id')
                ->where('reservation_additional_info.reservation_id', '=', $oneItem['reservation_id'])
                ->select()
                ->get()
                ->toArray();
            foreach ($packets as $packKey => $onePack) {
                $temp1[$key]['reseravation_subscription'][$packKey]['subscription_name'] = $onePack['name_bg'];
//                $temp1[$key]['reseravation_subscription'][$packKey]['packet_price'] = $onePack['price'];
            }


        }
        return $temp1;
    }


    public function showReservationForm($reservationId = 0)
    {
        $selectedReservation['id'] = 0;
        if ($reservationId > 0) {
            $selectedReservation = $this->getReservationInfo($reservationId);
        }
        return view('admin.reservations.reservationForm', [
            'selectedItem' => $selectedReservation[0]
        ]);
    }

    public function destroyReservation(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = ReservationsEntity::destroyReservation($data['reservationId']);
        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/reservations/list')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

//Route::get('/admin/reservations/list', 'AdminReservationController@listReservations');
//Route::get('/admin/reservations/reservation_form/{reservation_id?}', 'AdminReservationController@editReservation');
//Route::post('/admin/reservations/store', 'AdminReservationController@storeReservation');
//Route::post('/admin/reservations/destroy', 'AdminReservationController@destroyReservation');


}