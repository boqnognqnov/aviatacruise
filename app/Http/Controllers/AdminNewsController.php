<?php

namespace App\Http\Controllers;

use App\CategoryNewsEntity;
use App\Classes\GlobalFunctions;
use App\Http\Requests;
use App\NewsEntity;
use App\NewsGalleryEntity;
use Illuminate\Http\Request;


class AdminNewsController extends Controller
{


    public function listNews()
    {


//        $newsT = NewsGalleryEntity::all();
//        $counter = 1;
//        foreach ($newsT as $one) {
//            $one->position = $counter;
//            $one->save();
//            $counter++;
//        }
        $news = NewsEntity::orderBy('created_at', 'DESC')->get()->toArray();
        return view('admin.news.list', ['news' => $news]);
    }

    public function showEditForm($newsId = 0)
    {
        $selectedNews['id'] = 0;

        if ($newsId != 0) {
            $selectedNews = NewsEntity::find($newsId)->toArray();
            $selectedNews['date'] = GlobalFunctions::generateDateTimeToStr($selectedNews['created_at']);
        }
        $newsCatListRaw = CategoryNewsEntity::all();
        $newsCatList = [];
        foreach ($newsCatListRaw as $oneCat) {
            $newsCatList[$oneCat->id] = $oneCat->name_bg;
        }
        return view('admin.news.newsForm', [
            'selectedNews' => $selectedNews,
            'newsCatList' => $newsCatList
        ]);
    }


    public function updateCreateNews(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = NewsEntity::createUpdate($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/news/edit_form/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyNews(Request $request)
    {
        $newsId = $request->get('newsId');
        $status = NewsEntity::destroyNews($newsId);
        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/news')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }


    public function listCategories($categoryId = 0)
    {


        $categories = CategoryNewsEntity::all()->toArray();

        if ($categoryId != 0) {
            $selectedCat = CategoryNewsEntity::find($categoryId)->toArray();


            return view('admin.news.categoryList', [
                'categories' => $categories,
                'selectedCat' => $selectedCat
            ]);
        }

        return view('admin.news.categoryList', ['categories' => $categories]);
    }

    public function addOrEditCategories(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = CategoryNewsEntity::createUpdate($data);
        return redirect()->back();


    }

    public function destroyCategory(Request $request)
    {
        $categoryId = $request->get('catId');
        $status = CategoryNewsEntity::destroyCategory($categoryId);
        return redirect()->back();

    }

    public function showGallery($newsId)
    {
        $gallery = NewsGalleryEntity::where('news_id', '=', $newsId)->orderBy('position', 'ASC')->get()->toArray();

        return view('admin.news.gallery', ['gallery' => $gallery, 'newsId' => $newsId]);

    }


    public function destroyGalleryImg(Request $request)
    {
        $imgId = $request->get('gallery_id');
        $status = NewsGalleryEntity::destroyImg($imgId);
        return redirect()->back();

    }

    public function uploadGalleryImgs(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        if ($data['pictures'][0] == null) {
            return back();
        }

        $status = NewsGalleryEntity::uploadImages($data);
        return redirect()->back();

    }

    public function moveImgRight($newsId, $imageId)
    {
//        return dump($newsId);
        $max = NewsGalleryEntity::orderBy('position', 'DESC')->get()->first()->position;

        $currentRecord = NewsGalleryEntity::find($imageId);
        $currentPosition = $currentRecord->position;
        if ($max > $currentPosition) {

            $downRecord = NewsGalleryEntity::where('position', '>', $currentPosition)
                ->where('news_id', '=', $newsId)
                ->orderBy('position', 'ASC')->first();
            if ($downRecord) {
                $currentRecord->position = $downRecord->position;
                $currentRecord->save();
                $downRecord->position = $currentPosition;
                $downRecord->save();
            }
        }

        return redirect()->back();

    }

    public function moveImgLeft($newsId, $imageId)
    {
//        return dump($imageId);
        $min = NewsGalleryEntity::orderBy('position', 'ASC')->get()->first()->position;

        $currentRecord = NewsGalleryEntity::find($imageId);
        $currentPosition = $currentRecord->position;
        if ($min < $currentPosition) {

            $downRecord = NewsGalleryEntity::where('position', '<', $currentPosition)
                ->where('news_id', '=', $newsId)
                ->orderBy('position', 'DESC')->first();
            if ($downRecord) {
                $currentRecord->position = $downRecord->position;
                $currentRecord->save();
                $downRecord->position = $currentPosition;
                $downRecord->save();
            }

        }

        return redirect()->back();

    }

    public function setSeoPictureDataGallery(Request $request)
    {
        $data = $request->all();
        $status = NewsGalleryEntity::setPictureSeoData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setSeoPictureData(Request $request)
    {
        $data = $request->all();
        $status = NewsEntity::setPictureSeoData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/news/edit_form/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


}