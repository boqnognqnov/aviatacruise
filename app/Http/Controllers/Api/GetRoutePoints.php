<?php namespace App\Http\Controllers\Api;

use App\Classes\GlobalFunctions;
use App\Complex;
use App\ComplexRooms;
use App\EarlyBooking;
use App\EventTagsEntity;
use App\Extras;
use App\NewsTagsEntity;
use App\PointsEntity;
use App\RoomExtras;
use App\RoomOccupancy;
use App\TagToEventsEntity;
use App\TagToNewsEntity;
use App\User;
use Illuminate\Http\Request;

use App\Reservations;
use App\ReservedRooms;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class GetRoutePoints extends Controller
{
    public function getRoutePoints(Request $request)
    {
        $pointName = $request->get('point_name');

//        return dump(GlobalFunctions::transLiterate('latin', $pointName));
        $pointList = [];
        if (!empty($pointName)) {
            $pointList = PointsEntity::where('name_bg', 'LIKE', '%' . $pointName . '%')
                ->orWhere('name_bg', 'LIKE', '%' . GlobalFunctions::transLiterate('latin', $pointName) . '%')
                ->get()->toArray();
        }

        return \Response::json(array(
            'status' => 'success',
            'data' => $pointList
        ), 200);
    }


}
