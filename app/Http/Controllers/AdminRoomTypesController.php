<?php

namespace App\Http\Controllers;

use App\CompanyEntity;
use App\DeckEntity;
use App\DeckRoomTypeEntity;
use App\Http\Requests;
use App\RoomTypeEntity;
use Illuminate\Http\Request;


class AdminRoomTypesController extends Controller
{

    public function listRoomTypes($shipId, $deckId = null)
    {
        if($deckId){

           $roomTypes = RoomTypeEntity::where('deck_id', $deckId)->get()->toArray();
        } else {

            $roomTypes = RoomTypeEntity::join('decks', 'room_types.deck_id', '=', 'decks.id')
                ->select('room_types.*', 'decks.name_bg as deck_name_bg')->where('decks.ship_id', $shipId)->get()->toArray();
        }

        return view('admin.rooms.list', ['shipId' => $shipId, 'roomTypes' => $roomTypes]);
    }

    public function showCreateUpdate($shipId, $roomTypeId = 0)
    {
        $selectedRoomType['id'] = 0;

        if ($roomTypeId != 0) {

            $selectedRoomType = RoomTypeEntity::find($roomTypeId)->toArray();
        }

        $decks = DeckEntity::where('ship_id', $shipId)->get()->toArray();
        $selectedDecks = array();
        $checked = 0;
        foreach ($decks as $key => $value) {

            $selectedDecks[$value['id']] = $value['name_bg'];

            if(isset($selectedRoomType['deck_id'])
            && $selectedRoomType['deck_id'] == $value['id']){
                $checked = $value['id'];
            }
        }

        return view('admin.rooms.roomTypeForm', [
            'shipId' => $shipId,
            'selectedItem' => $selectedRoomType,
            'selectedDecks' => $selectedDecks,
            'checked' => $checked,
        ]);
    }

    public function createUpdateRoomType(Request $request)
    {
        $data = $request->all();
        $status = RoomTypeEntity::createUpdate($data);

        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyRoomType(Request $request)
    {
        $roomTypeTd = $request->get('roomTypeId');
        $status = RoomTypeEntity::destroyRoomType($roomTypeTd);

        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.deletingError'));
            case 'success':
                return back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }
}