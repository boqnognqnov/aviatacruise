<?php

namespace App\Http\Controllers;

use App\AboutTextEntity;
use App\Http\Requests;
use App\PageAboutEntity;
use Illuminate\Http\Request;


class AdminAboutTextController extends Controller
{

    public function showForm()
    {
//        $firstFourRows= AboutTextEntity::where('id', '<', 5)->get()->toArray();
        $firstFourRows = AboutTextEntity::orderBy('id')->take(4)->get()->toArray();
        $otherRows = AboutTextEntity::where('id', '>', 4)->get()->toArray();
//        $otherRows = AboutTextEntity::orderBy('id')->skip(4)->get()->toArray();

//        return dump($otherRows);

        return view('admin.about-us.aboutOtherForm', ['firstFourRows' => $firstFourRows, 'otherRows' => $otherRows]);
    }

    public function updateBenefitsBox(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = AboutTextEntity::setPreDefinedRows($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.updatingError'));
            case 'success':
                return back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function updateOthers(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = AboutTextEntity::createUpdate($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.updatingError'));
            case 'success':
                return back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyOtherField(Request $request)
    {

        $field = $request->get('fieldId');
        $status = AboutTextEntity::destroyRow($field);

        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.deletingError'));
            case 'success':
                return back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }
}