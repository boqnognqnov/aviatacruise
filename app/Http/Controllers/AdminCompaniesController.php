<?php

namespace App\Http\Controllers;

use App\CompanyEntity;
use App\Http\Requests;
use Illuminate\Http\Request;


class AdminCompaniesController extends Controller
{


    public function listCompanies()
    {

        $companies = CompanyEntity::all()->toArray();

        return view('admin.companies.list', ['companies' => $companies]);
    }

    public function showCreateUpdate($companyId = 0)
    {
        $selectedCompany['id'] = 0;

        if ($companyId != 0) {

            $selectedCompany = CompanyEntity::find($companyId)->toArray();
        }

        return view('admin.companies.companyForm', [
            'selectedItem' => $selectedCompany,
        ]);
    }

    public function createUpdateCompany(Request $request)
    {
        $data = $request->all();
        $status = CompanyEntity::createUpdate($data);

        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/companies/create_update/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyCompany(Request $request)
    {
        $companyId = $request->get('companyId');
        $status = CompanyEntity::destroyCompany($companyId);

        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/companies')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setSeoPictureData(Request $request)
    {
        $data = $request->all();
        $status = CompanyEntity::setPictureSeoData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/companies/create_update/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setSeoLogoData(Request $request)
    {
        $data = $request->all();
        $status = CompanyEntity::setLogoSeoData($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/companies/create_update/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }
}