<?php

namespace App\Http\Controllers;

use App\Classes\GlobalFunctions;
use App\CompanyEntity;
use App\DateRoomOfferEntity;
use App\Http\Requests;
use App\PointsEntity;
use App\RoomTypeEntity;
use App\RouteEntity;
use App\RouteShipEntity;
use App\ShipEntity;
use App\ShipGalleryEntity;
use Illuminate\Http\Request;


class ShipsController extends Controller
{


    public function listShips()
    {

        $shipsPreferredCount = GlobalFunctions::getGlobalArray('ships_preferred_count');
        $shipsRaw = ShipEntity::orderBy('id')->take($shipsPreferredCount)->get()->toArray();

        $ships = GlobalFunctions::transformArrByGroups(2, $shipsRaw);

        if (sizeof($shipsRaw) < $shipsPreferredCount) {
            $shipsPreferredCount = sizeof($shipsRaw);
        }

//        return dump(GlobalFunctions::getRandRoutesWithDiscount());

        return view('ship-list', [
            'list' => $ships,
            'shipsCount' => $shipsPreferredCount,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'seoData' => GlobalFunctions::getStaticPageSeoData(6),
            'breadcrumps' => GlobalFunctions::breadCrumbs('ship-list')
        ]);
    }


    public function listShipsJS(Request $request)
    {
        $data = $request->all();
        $moreShipsCount = GlobalFunctions::getGlobalArray('ships_more_count');
        $currentShipsCount = $data['count'];
//        return dump('count = ' . $currentShipsCount . ' ' . 'more = ' . $moreShipsCount);

        $shipsRaw = ShipEntity::orderBy('id')->skip($currentShipsCount)->take($moreShipsCount)->get()->toArray();
        $ships = [];
        foreach ($shipsRaw as $key => $oneItem) {
            $ships[$key] = $oneItem;
            $ships[$key]['name'] = $oneItem['name_' . \App::getLocale()];
            $ships[$key]['text'] = $oneItem['description_text_' . \App::getLocale()];
            $ships[$key]['slug'] = $oneItem['slug_' . \App::getLocale()];
        }

        return \Response::json(array(
            'status' => 'success',
            'items' => $ships,
            'shipsCount' => $currentShipsCount,

        ), 200);

//        return dump($data);
    }

    public function showShip($slug)
    {
        $lang = \App::getLocale();
        $ship = ShipEntity::where('slug_' . $lang, 'LIKE', $slug)->first()->toArray();
        $galleryRaw = ShipGalleryEntity::where('ship_id', '=', $ship['id'])->get()->toArray();

        $gallery = [];
        foreach ($galleryRaw as $key => $onePic) {
            $gallery[$key]['picture'] = ShipGalleryEntity::$path . $onePic['picture'];
            $gallery[$key]['pic_title'] = $onePic['pic_title_' . $lang];
            $gallery[$key]['pic_alt'] = $onePic['pic_alt_' . $lang];
        }

        $company = CompanyEntity::find($ship['company_id'])->toArray();

        $cabins = GlobalFunctions::getCabinsByShipId($ship['id']);

        $decs = GlobalFunctions::getDecsByShipId($ship['id']);

//        return dump($decs);

        $routesWithThisShip = RouteEntity::join('ships', 'routes.ship_id', '=', 'ships.id')
            ->join('companies', 'ships.company_id', '=', 'companies.id')
            ->where('ships.id', '=', $ship['id'])
            ->distinct()
            ->select('routes.id as route_id',
                'routes.route_title_' . $lang . ' as route_name',
                'routes.route_description_' . \App::getLocale() . ' as route_description',
                'routes.picture',
                'companies.name_' . $lang . ' as company_name',
                'routes.route_slug_' . $lang . ' as route_slug',

                'routes.pic_title_' . $lang . ' as pic_title',
                'routes.pic_alt_' . $lang . ' as pic_alt'
            )
            ->get()
            ->toArray();

        $routesWithThisShip = GlobalFunctions::getLowerPricePerCabin($routesWithThisShip);
        $routesWithThisShip = GlobalFunctions::getFirstPoint($routesWithThisShip);

        $breadCrumpArr['url'] = 'ship/' . $ship['slug_' . $lang];
        $breadCrumpArr['title'] = $ship['name_' . $lang];

        return view('inner-ships', [
            'ship' => $ship,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'gallery' => $gallery,
            'cabins' => $cabins,
            'decs' => $decs,
            'routesWithThisShip' => $routesWithThisShip,
            'company' => $company,
            'seoData' => GlobalFunctions::getDynamicSeoDataFromModel($ship, true),
            'breadcrumps' => GlobalFunctions::breadCrumbs('ship-list', $breadCrumpArr)
        ]);

    }


}