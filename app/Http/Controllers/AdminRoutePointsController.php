<?php

namespace App\Http\Controllers;

use App\CategoryNewsEntity;
use App\Classes\GlobalFunctions;
use App\DestinationEntity;
use App\Http\Requests;
use App\NewsEntity;
use App\NewsGalleryEntity;
use App\PointsEntity;
use App\PointTypeEntity;
use App\RouteEntity;
use App\RoutePointEntity;
use App\RouteTypeEntity;
use Illuminate\Http\Request;


class AdminRoutePointsController extends Controller
{


    public function listPoints()
    {
        $points = PointsEntity::all()->toArray();

        return view('admin.routes.listPoints', ['list' => $points]);
    }

    public function showForm($pointId = 0)
    {
        $selectedItem['id'] = 0;
        $selectedItem['is_port'] = true;

        if ($pointId != 0) {
            $selectedItem = PointsEntity::find($pointId)->toArray();
        }

        $rawPointTypes = PointTypeEntity::all();
        $pointTypes = [];
        foreach ($rawPointTypes as $oneType) {
            $pointTypes[$oneType->id] = $oneType->name_bg;
        }

        return view('admin.routes.routePointForm', [
            'selectedItem' => $selectedItem,
            'pointTypes' => $pointTypes,
        ]);

    }

    public function createUpdatePoint(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = PointsEntity::createUpdate($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/routes-points/create_update/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyPoint(Request $request)
    {
        $pointId = $request->get('pointId');
//        return dump($routeId);
        $status = PointsEntity::destroyPoint($pointId);

        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/routes-points')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
//        pointId
    }

    public function setPointToRouteForm($routeId, $pointId = 0, $routePointId = 0)
    {
        $newPoint = null;
        if ($pointId != 0) {
            if ($routePointId == 0) {
                $newPoint = PointsEntity::find($pointId)->toArray();
            } else {
                $newPoint = RoutePointEntity::find($routePointId)->toArray();
                $routePointId = $newPoint['id'];
                $newPoint['id'] = $newPoint['point_id'];
                $newPoint['routePointId'] = $routePointId;
            }


//            Session::put('key', 'value');
        }
//
        $selectedPoints = GlobalFunctions::getRoutePointsByRouteId($routeId);

//        return dump($selectedPoints);
        return view('admin.routes.setRoutePoints', [
            'selectedPoints' => $selectedPoints,
            'newPoint' => $newPoint,
            'route_id' => $routeId,
            'route_point_id' => $routePointId
        ]);
    }


    public function setPointToRoute(Request $request)
    {

        $data = $request->all();
//        return dump($data);

        $record = null;

        if (empty($data['route_point_id'])) {
            $points = RoutePointEntity::where('route_id', '=', $data['route_id'])->get()->toArray();
            $pointCount = sizeof($points);
            $record = new RoutePointEntity();
            $record->route_id = $data['route_id'];
            $record->point_id = $data['point_id'];
            $record->sequence = $pointCount + 1;
        } else {
            $record = RoutePointEntity::find($data['route_point_id']);
        }


        $record->day = $data['day'];
        $record->time_arrival = $data['time_arrival'];
        $record->time_departure = $data['time_departure'];

        $record->save();
        return redirect()->back();
    }

    public function destroyPointFromRoute($routeId, $pointId)
    {

        $record = RoutePointEntity::where('route_id', '=', $routeId)->where('point_id', '=', $pointId)->first();
        $record->delete();
        return redirect()->back();
    }


    public function listPointTypes()
    {
        $list = PointTypeEntity::all()->toArray();
        return view('admin.routes.listPointTypes', ['list' => $list]);
    }

    public function showPointTypeForm($pointTypeId = 0)
    {
        $selectedItem['id'] = 0;
        if ($pointTypeId != 0) {
            $selectedItem = PointTypeEntity::find($pointTypeId)->toArray();
        }

        return view('admin.routes.pointTypeForm', ['selectedItem' => $selectedItem]);
    }

    public function createUpdatePointType(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = PointTypeEntity::createUpdate($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/point-types/create_update/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyPointType(Request $request)
    {
        $pointTypeId = $request->get('point_type_id');

        $status = PointTypeEntity::destroyPointType($pointTypeId);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }

    public function setSeoPictureData(Request $request)
    {
        $data = $request->all();
        $status = PointsEntity::setPictureSeoData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('/admin/routes-points/create_update/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


}