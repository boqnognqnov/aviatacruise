<?php

namespace App\Http\Controllers;

use App\AdditionalInfoEntity;
use App\Classes\GlobalFunctions;
use App\Classes\MakeReservation;
use App\CompanyEntity;
use App\DateRoomOfferEntity;
use App\Http\Requests;
use App\PointsEntity;
use App\PointTypeEntity;
use App\PromoPacketEntity;
use App\RoomTypeEntity;
use App\RouteDateEntity;
use App\RouteEntity;
use App\RouteGalleryEntity;
use App\RouteShipEntity;
use App\RouteTypeEntity;
use App\ShipEntity;
use App\ShipGalleryEntity;
use Illuminate\Http\Request;


class OfferController extends Controller
{


    public function showOffer($offer_slug)
    {
        $lang = \App::getLocale();
        $route = RouteEntity::where('route_slug_' . $lang, 'LIKE', $offer_slug)
            ->select('routes.id as route_id',
                'routes.ship_id',
                'routes.route_title_' . $lang . ' as route_name',
                'routes.route_slug_' . $lang . ' as route_slug',
                'routes.route_description_' . $lang . ' as route_description',
                'routes.picture',
                'routes.seo_title_' . $lang . ' as seo_title',
                'routes.seo_description_' . $lang . ' as seo_description'
            )
            ->first()->toArray();

        $route = GlobalFunctions::getFirstPoint([$route]);
        $route = GlobalFunctions::getLowerPricePerCabin($route);
        $route = $route[0];


        $gallery = [];
        $galleryRaw = RouteGalleryEntity::where('route_id', '=', $route['route_id'])->get();
        if ($galleryRaw) {
            $gallery = $galleryRaw->toArray();
        }

        $ship = ShipEntity::find($route['ship_id'])->toArray();
        $cabins = GlobalFunctions::getCabinsByShipId($ship['id']);
        $decs = GlobalFunctions::getDecsByShipId($ship['id']);
        $company = CompanyEntity::find($ship['company_id'])->toArray();

        $pointsRaw = GlobalFunctions::getRoutePointsByRouteId($route['route_id']);
        $points = [];
        foreach ($pointsRaw as $key => $onePoint) {
            $points[$key] = $onePoint;
            $points[$key]['name'] = $onePoint['name_' . $lang];
            $points[$key]['country'] = $onePoint['country_' . $lang];

            $coordinatesArr = explode(', ', $onePoint['coordinates']);
            $points[$key]['coordinate_x'] = $coordinatesArr[0];
            $points[$key]['coordinate_y'] = $coordinatesArr[1];

            $pointTypeData = PointTypeEntity::find($onePoint['point_type_id']);
            $points[$key]['point_type_name'] = $pointTypeData['name_' . $lang];
            $points[$key]['point_type_picture'] = $pointTypeData['picture'];
        }

        $reservationCases = $this->getReservationDatesAndCabins($route['route_id'], 1);
//        return dump($reservationCases);

        $sessionData['route_id'] = $route['route_id'];
        \Session::put('reservationData', $sessionData);


        return view('offer', [
            'gallery' => $gallery,
            'route' => $route,
            'ship' => $ship,
            'cabins' => $cabins,
            'decs' => $decs,
            'company' => $company,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'points' => $points,
            'reservationCases' => $reservationCases,
            'seoData' => GlobalFunctions::getDynamicSeoDataFromModel($route),
            'breadcrumps' => GlobalFunctions::breadCrumbs('offer')
        ]);
    }


    public function chooseCabinAndDate(Request $request)
    {
        $sessionData = \Session::get('reservationData');

        if (!$sessionData) {
            return redirect('/');
        }


        $data = $request->all();
        if (!isset($data['radio'])) {
            return redirect()->back()->withErrors(trans('modelStatusMessages.reservation_step_1_room_not_selected'));
        }
        $data = explode('_', $data['radio']);

        $sessionData['date_id'] = $data[0];
        $sessionData['room_id'] = $data[1];

        \Session::put('reservationData', $sessionData);

        $reservationCases = $this->getReservationDatesAndCabins($sessionData['route_id']);
        $availableCabinsByDateWithZero = $reservationCases[$sessionData['date_id']];


        $availableCabinsByDate['date'] = $availableCabinsByDateWithZero['date'];
        foreach ($availableCabinsByDateWithZero['data'] as $key => $oneCabinData) {
            if ($oneCabinData['total_price'] > 0) {
                $availableCabinsByDate['data'][$key] = $oneCabinData;
            }
        }

        $promoPackets = PromoPacketEntity::join('promo_packet_to_route', 'promo_packets.id', '=', 'promo_packet_to_route.packet_id')
            ->where('promo_packet_to_route.route_id', '=', $sessionData['route_id'])
            ->select('promo_packets.*')
            ->get()
            ->toArray();

        $additionalInfos = AdditionalInfoEntity::all()->toArray();


        return view('choose-cabbin', [
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'availableCabinsByDate' => $availableCabinsByDate,
            'selectedCabin' => $sessionData['room_id'],
            'promoPackets' => $promoPackets,
            'additionalInfos' => $additionalInfos,
            'breadcrumps' => GlobalFunctions::breadCrumbs('offer-details')
        ]);

    }

    public function chooseCabinAndPackets(Request $request)
    {
        $reservationData = \Session::get('reservationData');
        if (!$reservationData) {
            return redirect('/');
        }
        $data = $request->all();

        $lang = \App::getLocale();

        $reservationData['room_id'] = $data['cabinId'];

        if (isset($data['infoSubscription'])) {
            $reservationData['infoSubscription'] = $data['infoSubscription'];
        }

//        $reservationData['passengers'] = 1;
//        if (isset($data['passengers'])) {
//            $reservationData['passengers'] = 2;
//        }
        $reservationData['passengers'] = 2;

        $reservationData['promoPackets'] = [];
        if (isset($data['promoPackets'])) {
            $reservationData['promoPackets'] = $data['promoPackets'];
        }

        $dateId = $reservationData['date_id'];
        $reservationData['prices'] = $this->calculatePrices($dateId, $data['cabinId'], $reservationData['passengers'], $reservationData['promoPackets']);

        $reservationData['additional_data'] = [];
        $pointsRaw = GlobalFunctions::getRoutePointsByRouteId($reservationData['route_id']);

        $reservationData['additional_data']['route_points_arr'] = [];
        foreach ($pointsRaw as $key => $onePoint) {
            $reservationData['additional_data']['route_points_arr'][$key] = $onePoint['name_' . $lang];
        }

        $reservationData['additional_data']['shipAndCompanyData'] = CompanyEntity::join('ships', 'companies.id', '=', 'ships.company_id')
            ->join('decks', 'ships.id', '=', 'decks.ship_id')
            ->join('room_types', 'decks.id', '=', 'room_types.deck_id')
            ->where('room_types.id', '=', $data['cabinId'])
            ->distinct()
            ->select('room_types.name_' . $lang . ' as room_name',
                'companies.id as company_id', 'ships.name_' . $lang . ' as ship_name',
                'companies.name_' . $lang . ' as company_name')
            ->get()
            ->first()
            ->toArray();

        $reservationData['additional_data']['dates'] = RouteDateEntity::where('route_id', '=', $reservationData['route_id'])->first()->toArray();
        $reservationData['additional_data']['dates']['duration_count'] = RouteEntity::find($reservationData['route_id'])->duration_count;

        $reservationData['additional_data']['promo_packets_named'] = [];
        if ($reservationData['promoPackets']) {
            foreach ($reservationData['promoPackets'] as $onePromoPackId) {
                $reservationData['additional_data']['promo_packets_named'][$onePromoPackId] = PromoPacketEntity::find($onePromoPackId)->toArray();
            }
        }
        $roomType = RoomTypeEntity::find($reservationData['room_id'])->toArray();
        $reservationData['additional_data']['cabin_name'] = $roomType['name_' . $lang];


        \Session::put('reservationData', $reservationData);

        $dropDownsToReturn = GlobalFunctions::generateDateOfBirdsDropdown();


        return view('personal-data', [
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'reservationData' => $reservationData,
            'birthComboToReturn' => $dropDownsToReturn,
            'breadcrumps' => GlobalFunctions::breadCrumbs('personal-data')
        ]);
    }

    public function confirmReservation(Request $request)
    {
        $reservationData = \Session::get('reservationData');
        if (!$reservationData) {
            return redirect('/');
        }
        $data = $request->all();


        $dataForNewReservation = MakeReservation::treatmentPersonalData($data, $reservationData);
//        return dump($dataForNewReservation);

//        return dump($dataForNewReservation);

        $status = MakeReservation::validateReservationData($dataForNewReservation);
        if ($status[0] != 'success') {
            return back()->withInput()->withErrors($status[1]);
        }


        $status = MakeReservation::createNewReservation($dataForNewReservation['reservationData'],
            $dataForNewReservation['passengersData'],
            $dataForNewReservation['clearData'],
            $dataForNewReservation['session']);
        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.updatingError'));
            case 'success':
//                \Session::forget('reservationData');
                return view('success-reservation', [
                    'reservationData' => $reservationData,
                    'clientsInformation' => $dataForNewReservation,
                    'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
                    'breadcrumps' => GlobalFunctions::breadCrumbs('success-reservation')
                ]);
        }


    }

    private function calculatePrices($dateId, $roomId, $passengersCounter, $promoPacksArr)
    {
        $price_per_cabin_multiplied_count = GlobalFunctions::getGlobalArray('price_per_cabin_multiplied_count');
        $dataToReturn = [];
        $roomPricesAndDiscounts = DateRoomOfferEntity::join('route_dates', 'date_room_offers.date_id', '=', 'route_dates.id')
            ->where('date_room_offers.room_id', '=', $roomId)
            ->where('date_room_offers.date_id', '=', $dateId)
            ->select(
                'date_room_offers.price as room_price',
                'date_room_offers.room_discount as room_discount',
                'route_dates.discount as route_discount'
            )
            ->get()
            ->first()
            ->toArray();

        $totalPromoPackPrice = 0;

        foreach ($promoPacksArr as $onePromoPack) {
            $onePromoPrice = PromoPacketEntity::find($onePromoPack)->price;

            $totalPromoPackPrice += $onePromoPrice;
        }

        $dataToReturn['totalPromoPacksPrice'] = $totalPromoPackPrice * $passengersCounter;

        $dataToReturn['priceOnlyRoom'] = GlobalFunctions::calculatePriceByParams($roomPricesAndDiscounts['room_price'], $roomPricesAndDiscounts['room_discount'], $roomPricesAndDiscounts['route_discount'], $price_per_cabin_multiplied_count);
        $dataToReturn['totalRoomPrice'] = $dataToReturn['priceOnlyRoom'] + $dataToReturn['totalPromoPacksPrice'];

        return $dataToReturn;
    }

    public function calculateTotalPriceJs(Request $request)
    {
        $data = $request->all();
        $reservationData = \Session::get('reservationData');

        $dateId = $reservationData['date_id'];

        $roomId = $data['roomId'];
        $passengers = $data['passengersCount'];

        $promoPacks = [];
        if (isset($data['checkedPromoPackIds'])) {
            $promoPacks = $data['checkedPromoPackIds'];
        }

        $dataToReturn = $this->calculatePrices($dateId, $roomId, $passengers, $promoPacks);

        return \Response::json(array(
            'status' => 'success',
            'data' => $dataToReturn
        ), 200);


    }


    private function getReservationDatesAndCabins($routeId, $forOnePassenger = '')
    {

        $price_per_cabin_multiplied_count = GlobalFunctions::getGlobalArray('price_per_cabin_multiplied_count');
        if (is_numeric($forOnePassenger)) {
            $price_per_cabin_multiplied_count = $forOnePassenger;
        }

        $reservationCasesRaw = RouteDateEntity::join('date_room_offers', 'date_room_offers.date_id', '=', 'route_dates.id')
            ->join('room_types', 'date_room_offers.room_id', '=', 'room_types.id')
            ->where('route_dates.route_id', '=', $routeId)
            ->where('route_dates.is_disabled', '=', false)
            ->orderBy('route_dates.date')
            ->distinct()
            ->select('*', 'route_dates.id as date_id', 'room_types.id as cabin_id')
            ->get()->toArray();


        $tempDate = null;
        $reservationCases = [];
        $counterByDate = 0;


        foreach ($reservationCasesRaw as $oneCase) {

            if ($oneCase['price'] == null) {
                $oneCase['price'] = 0;
            }

            $counterByRoom = $oneCase['cabin_id'];
            if ($tempDate != $oneCase['date']) {
                $counterByDate = $oneCase['date_id'];
                $tempDate = $oneCase['date'];
            }

            $reservationCases[$counterByDate]['date'] = GlobalFunctions::getStampToStrFullMonth($oneCase['date']);

            $roomDiscount = $oneCase['room_discount'];
            if ($roomDiscount == null) {
                $roomDiscount = 0;
            }

            $routeDiscount = $oneCase['discount'];
            if ($routeDiscount == null) {
                $routeDiscount = 0;
            }

            $reservationCases[$counterByDate]['data'][$counterByRoom]['room_discount'] = $roomDiscount;
            $reservationCases[$counterByDate]['data'][$counterByRoom]['total_price'] = GlobalFunctions::calculatePriceByParams($oneCase['price'], $roomDiscount, $routeDiscount, $price_per_cabin_multiplied_count);
            $reservationCases[$counterByDate]['data'][$counterByRoom]['room_name'] = $oneCase['name_' . \App::getLocale()];


//            dump('$roomDiscount' + $roomDiscount);
//            dump('$routeDiscount' + $routeDiscount);
//            dump('$oneCase[\'price\']' + $oneCase['price']);

        }
        return $reservationCases;
    }


}