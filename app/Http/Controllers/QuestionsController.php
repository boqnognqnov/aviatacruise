<?php

namespace App\Http\Controllers;

use App\Classes\GlobalFunctions;
use App\CompanyEntity;
use App\DateRoomOfferEntity;
use App\Http\Requests;
use App\PointsEntity;
use App\QuestionsEntity;
use App\RoomTypeEntity;
use App\RouteEntity;
use App\RouteShipEntity;
use App\ShipEntity;
use App\ShipGalleryEntity;
use Illuminate\Http\Request;


class QuestionsController extends Controller
{


    public function listQuestsByCategory()
    {
        $lang = \App::getLocale();

        $recordsRaw = QuestionsEntity::join('question_categories', 'questions.category_id', '=', 'question_categories.id')
            ->select(
                'question_categories.id as cat_id',
                'question_categories.name_' . $lang . ' as category_name',
                'question_categories.slug_' . $lang . ' as category_slug',
                'questions.name_' . $lang . ' as question_name',
                'questions.text_' . $lang . ' as question_text',
                'questions.slug_' . $lang . ' as questiony_slug')
            ->get()
            ->toArray();

        $questRecords = [];

        foreach ($recordsRaw as $key => $oneRec) {
            $questRecords[$oneRec['category_name']][$key]['questTitle'] = $oneRec['question_name'];
            $questRecords[$oneRec['category_name']][$key]['questText'] = $oneRec['question_text'];
        }

//        return dump($questRecords);
        return view('faq', [
            'questRecords' => $questRecords,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'seoData' => GlobalFunctions::getStaticPageSeoData(7),
            'breadcrumps' => GlobalFunctions::breadCrumbs('faq')
        ]);
//        faq

    }


}