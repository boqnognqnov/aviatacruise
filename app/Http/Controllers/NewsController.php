<?php

namespace App\Http\Controllers;

use App\CategoryNewsEntity;
use App\Classes\GlobalFunctions;
use App\Http\Requests;
use App\NewsEntity;
use App\NewsGalleryEntity;
use Illuminate\Http\Request;


class NewsController extends Controller
{


    public function listNews($criterion = '', $criterionVal = '')
    {
        $newsPreferredCount = GlobalFunctions::getGlobalArray('news_preferred_count');

        $newsRaw = [];

        $jsParams['criterion'] = '';
        $jsParams['criterionVal'] = '';
        if (!empty($criterion)) {
            $jsParams['criterion'] = $criterion;
            $jsParams['criterionVal'] = $criterionVal;
        }

        if (empty($criterion)) {
            $newsRaw = NewsEntity::orderBy('created_at', 'DESC')->take($newsPreferredCount)->get()->toArray();
        } elseif ($criterion == 'category') {
            $newsRaw = NewsEntity::join('news_category', 'news_category.id', '=', 'news.category_id')
                ->where('news_category.slug_' . \App::getLocale(), 'LIKE', $criterionVal)
                ->orderBy('news.created_at', 'DESC')->take($newsPreferredCount)->get()->toArray();
        } elseif ($criterion == 'archive') {

            $dateFrom = new \DateTime($criterionVal);
            $dateToRaw = explode('-', $criterionVal);
            $dateTo = $dateToRaw[0] . '-' . ($dateToRaw[1] + 1) . '-' . '01';

            $newsRaw = NewsEntity::where('created_at', '>=', $dateFrom)
                ->where('created_at', '<', $dateTo)
                ->get()
                ->toArray();

        }

        $news = GlobalFunctions::transformArrByGroups(2, $newsRaw);

        $jsParams['count'] = $newsPreferredCount;
        if (sizeof($newsRaw) < $newsPreferredCount) {
            $jsParams['count'] = sizeof($newsRaw);
        }

        $sideBarData = $this->getSideBarData();

        return view('news', [
            'newsRaw' => $news,
            'sideBarData' => $sideBarData,
            'jsParams' => $jsParams,
            'seoData' => GlobalFunctions::getStaticPageSeoData(10),
            'favNews' => GlobalFunctions::getFavoriteNews(),
            'breadcrumps' => GlobalFunctions::breadCrumbs('news')
        ]);
    }

    private function getSideBarData()
    {
        $sideBarData['categories'] = $this->getFilledCategories();

        $newsDatesList = NewsEntity::orderBy('created_at', 'DESC')->get(['created_at']);
        $sideBarData['archive'] = $this->collectYears($newsDatesList, 'created_at');

        return $sideBarData;
    }

    public function showNewsInnerPage($news_slug)
    {
        $lang = \App::getLocale();

        $news = NewsEntity::where('slug_' . $lang, '=', $news_slug)->first()->toArray();
        $sideBarData = $this->getSideBarData();

        $galleryRaw = [];
        $galleryObj = NewsGalleryEntity::where('news_gallery.news_id', '=', $news['id'])->get();
        if ($galleryObj) {
            $galleryRaw = $galleryObj->toArray();
        }
        $gallery = [];
//        $gallery[0] = NewsEntity::$path . $news['picture'];
        foreach ($galleryRaw as $key => $oneImg) {
//            $gallery[$key + 1] = NewsGalleryEntity::$path . $oneImg['picture'];
            $gallery[$key]['picture'] = NewsGalleryEntity::$path . $oneImg['picture'];

            $gallery[$key]['pic_title'] = $oneImg['pic_title_' . $lang];
            $gallery[$key]['pic_alt'] = $oneImg['pic_alt_' . $lang];
        }

        $breadCrumpArr['url'] = 'news-inner/' . $news['slug_' . $lang];
        $breadCrumpArr['title'] = $news['title_' . $lang];

        return view('news-inner', [
            'news' => $news,
            'sideBarData' => $sideBarData,
            'gallery' => $gallery,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'seoData' => GlobalFunctions::getDynamicSeoDataFromModel($news, true),
            'breadcrumps' => GlobalFunctions::breadCrumbs('news', $breadCrumpArr)
        ]);
    }

    public function listNewsJS(Request $request)
    {
        $lang = \App::getLocale();

        $data = $request->all();
        $newsMoreCount = GlobalFunctions::getGlobalArray('news_more_count');

        $currentNewsCount = $data['count'];
        $criterion = $data['criterion'];
        $criterionVal = $data['criterionVal'];


        $newsRaw = [];
        if (empty($criterion)) {
            $newsRaw = NewsEntity::orderBy('created_at', 'DESC')->skip($currentNewsCount)->take($newsMoreCount)->get()->toArray();
        } elseif ($criterion == 'category') {
            $newsRaw = NewsEntity::join('news_category', 'news_category.id', '=', 'news.category_id')
                ->where('news_category.slug_' . $lang, 'LIKE', $criterionVal)
                ->orderBy('news.created_at', 'DESC')->skip($currentNewsCount)->take($newsMoreCount)->get()->toArray();
        } elseif ($criterion == 'archive') {

        }

        $news = [];
        foreach ($newsRaw as $key => $oneNews) {
            $news[$key] = $oneNews;
            $news[$key]['title'] = $oneNews['title_' . $lang];
            $news[$key]['text'] = $oneNews['text_' . $lang];
            $news[$key]['slug'] = $oneNews['slug_' . $lang];
        }

        return \Response::json(array(
            'status' => 'success',
            'news' => $news,
            'newsCount' => $currentNewsCount,

        ), 200);
    }

    private function getFilledCategories()
    {
        $categories = CategoryNewsEntity
            ::join('news', 'news.category_id', '=', 'news_category.id')
//            ->where('news.title_en', 'NOT LIKE', '')
            ->select(\DB::raw('DISTINCT news_category.*'))
            ->get()->toArray();

        return $categories;
    }

    private function collectYears($records, $stampColumn)
    {

        $dateList = [];

        $year = 0;
        $month = 0;
        foreach ($records as $oneRecord) {


            $tempYearRaw = $oneRecord[$stampColumn];

            $tempDate = explode('-', $tempYearRaw);
            $tempYear = $tempDate[0];
            $tempMonth = $tempDate[1];
//            $tempDay = explode(' ', $tempDate[2]);
//            $tempDay = $tempDay[0];
            if ($year != $tempYear) {
                $year = $tempYear;
                $dateList[$year] = [];
            }

            if ($month != $tempMonth) {
                $month = $tempMonth;
                $dateList[$year][$month] = GlobalFunctions::getGlobalArray('monthNamed_' . \App::getLocale())[$month];
            }

        }
        return $dateList;

    }


}