<?php

namespace App\Http\Controllers;

use App\AboutTextEntity;
use App\GeneralTermsEntity;
use App\Http\Requests;
use App\PageAboutEntity;
use App\PageContactsEntity;
use Illuminate\Http\Request;


class AdminGeneralTermsController extends Controller
{

    public
    function showForm()
    {

        $record = GeneralTermsEntity::first()->toArray();

        return view('admin.generalTerms.termsForm', ['record' => $record]);
    }

    public
    function storeChanges(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = GeneralTermsEntity::updatePage($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.updatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


}