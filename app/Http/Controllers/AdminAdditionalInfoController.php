<?php

namespace App\Http\Controllers;

use App\AdditionalInfoEntity;
use App\Classes\GlobalFunctions;
use App\CompanyEntity;
use App\DateRoomOfferEntity;
use App\Http\Requests;
use App\PointsEntity;
use App\PointTypeEntity;
use App\PromoPacketEntity;
use App\RoomTypeEntity;
use App\RouteDateEntity;
use App\RouteEntity;
use App\RouteGalleryEntity;
use App\RouteShipEntity;
use App\RouteTypeEntity;
use App\ShipEntity;
use App\ShipGalleryEntity;
use Illuminate\Http\Request;


class AdminAdditionalInfoController extends Controller
{


    public function listInfoTypes()
    {
        $infoTypes = AdditionalInfoEntity::all()->toArray();
        return view('admin.additionalInfo.infoTypes', ['infoTypes' => $infoTypes]);
    }

    public function editInfoTypes($infoTypeId = 0)
    {
        $selectedType['id'] = 0;
        if ($infoTypeId > 0) {
            $selectedType = AdditionalInfoEntity::find($infoTypeId)->toArray();
        }

        return view('admin.additionalInfo.formInfoType', [
            'selectedType' => $selectedType,
        ]);
    }


    public function updateCreateType(Request $request)
    {
        $data = $request->all();
        $status = AdditionalInfoEntity::createUpdate($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/additional-info/edit/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyType(Request $request)
    {
        $typeId = $request->get('infoTypeId');
        $status = AdditionalInfoEntity::destroy($typeId);

        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/additional-info/list')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


}