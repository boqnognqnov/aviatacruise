<?php

namespace App\Http\Controllers;

use App\Classes\GlobalFunctions;
use App\CompanyEntity;
use App\DateRoomOfferEntity;
use App\DestinationEntity;
use App\Http\Requests;
use App\PointsEntity;
use App\PromoPacketEntity;
use App\RoomTypeEntity;
use App\RouteEntity;
use App\RouteShipEntity;
use App\ShipEntity;
use App\ShipGalleryEntity;
use Illuminate\Http\Request;


class CompaniesController extends Controller
{


    public function listCompanies()
    {

        $companiesPreferredCount = GlobalFunctions::getGlobalArray('companies_preferred_count');
        $companiesRaw = CompanyEntity::orderBy('id')->take($companiesPreferredCount)->get()->toArray();

        $companies = GlobalFunctions::transformArrByGroups(2, $companiesRaw);

        if (sizeof($companiesRaw) < $companiesPreferredCount) {
            $companiesPreferredCount = sizeof($companiesRaw);
        }

//        return dump($companies);

        return view('company-list', [
            'list' => $companies,
            'companiesCount' => $companiesPreferredCount,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'seoData' => GlobalFunctions::getStaticPageSeoData(5),
            'breadcrumps' => GlobalFunctions::breadCrumbs('company-list')
        ]);
    }


    public function listCompaniesJS(Request $request)
    {
        $data = $request->all();
        $moreCompaniesCount = GlobalFunctions::getGlobalArray('companies_more_count');
        $currentCompanyCount = $data['count'];

        $companiesRaw = CompanyEntity::orderBy('id')->skip($currentCompanyCount)->take($moreCompaniesCount)->get()->toArray();
        $companies = [];
        foreach ($companiesRaw as $key => $oneItem) {
            $companies[$key] = $oneItem;
            $companies[$key]['name'] = $oneItem['name_' . \App::getLocale()];
            $companies[$key]['text'] = $oneItem['description_' . \App::getLocale()];
            $companies[$key]['slug'] = $oneItem['slug_' . \App::getLocale()];
        }

        return \Response::json(array(
            'status' => 'success',
            'items' => $companies,
            'companiesCount' => $currentCompanyCount,

        ), 200);

    }

    public function showCompany($slug)
    {
        $lang = \App::getLocale();
        $company = CompanyEntity::where('slug_' . $lang, 'LIKE', $slug)->first()->toArray();
        $ccSummaryComponentData['title'] = $company['name_' . $lang];
        $ccSummaryComponentData['picture'] = CompanyEntity::$pathLogo . $company['logo'];
        $ccSummaryComponentData['description'] = $company['description_' . $lang];

        $ccSummaryComponentData['pic_title'] = $company['pic_title_' . $lang];
        $ccSummaryComponentData['pic_alt'] = $company['pic_description_' . $lang];


        $shipsOnThisCompnay = ShipEntity::where('company_id', '=', $company['id'])->get()->toArray();

        $routesOnThisCompany = RouteEntity::join('ships', 'routes.ship_id', '=', 'ships.id')
            ->join('companies', 'ships.company_id', '=', 'companies.id')
            ->where('companies.id', '=', $company['id'])
            ->distinct()
            ->select('routes.id as route_id',
                'routes.route_title_' . $lang . ' as route_name',
                'routes.route_description_' . $lang . ' as route_description',
                'routes.picture',
                'companies.name_' . $lang . ' as company_name',
                'routes.route_slug_' . $lang . ' as route_slug',

                'routes.pic_title_' . $lang . ' as pic_title',
                'routes.pic_alt_' . $lang . ' as pic_alt'
            )
            ->get()
            ->toArray();

        $routesOnThisCompany = GlobalFunctions::getLowerPricePerCabin($routesOnThisCompany);
        $routesOnThisCompany = GlobalFunctions::getFirstPoint($routesOnThisCompany);

        $destinationsOnThisCompany = DestinationEntity::join('routes', 'destinations.id', '=', 'routes.destination_id')
            ->join('ships', 'routes.ship_id', '=', 'ships.id')
            ->join('companies', 'ships.company_id', '=', 'companies.id')
            ->where('companies.id', '=', $company['id'])
            ->select('destinations.name_' . $lang . ' as destination_name',
                'destinations.description_' . $lang . ' as destination_description',
                'destinations.slug_' . $lang . ' as destination_slug',
                'destinations.picture')
            ->get()
            ->toArray();


        $promoPacketsOnThisCompany = PromoPacketEntity::join('promo_packet_to_route', 'promo_packets.id', '=', 'promo_packet_to_route.packet_id')
            ->join('routes', 'routes.id', '=', 'promo_packet_to_route.route_id')
            ->join('ships', 'ships.id', '=', 'routes.ship_id')
            ->join('companies', 'companies.id', '=', 'ships.company_id')
            ->where('companies.id', '=', $company['id'])
            ->select('promo_packets.name_' . $lang . ' as packet_name',
                'promo_packets.description_' . $lang . ' as packet_description',
                'promo_packets.price')
            ->get()
            ->toArray();


//        $routes = CompanyEntity::find($ship['company_id'])->toArray();
//        $destinations = CompanyEntity::find($ship['company_id'])->toArray();

        $breadCrumpArr['url'] = 'inner-company/' . $company['slug_' . $lang];
        $breadCrumpArr['title'] = $company['name_' . $lang];

        return view('inner-company', [
            'company' => $company,
            'shipsOnThisCompnay' => $shipsOnThisCompnay,
            'ccSummaryComponentData' => $ccSummaryComponentData,
            'routesOnThisCompany' => $routesOnThisCompany,
            'destinationsOnThisCompany' => $destinationsOnThisCompany,
            'promoPacketsOnThisCompany' => $promoPacketsOnThisCompany,
            'discountRoutes' => GlobalFunctions::getRandRoutesWithDiscount(),
            'seoData' => GlobalFunctions::getDynamicSeoDataFromModel($company, true),
            'breadcrumps' => GlobalFunctions::breadCrumbs('company-list', $breadCrumpArr)
        ]);

    }


}