<?php

namespace App\Http\Controllers;

use App\CategoryNewsEntity;
use App\DestinationEntity;
use App\Http\Requests;
use App\NewsEntity;
use App\NewsGalleryEntity;
use Illuminate\Http\Request;


class AdminDestinationsController extends Controller
{


    public function listDestinations()
    {
        $destinations = DestinationEntity::all()->toArray();
        return view('admin.destinations.list', ['list' => $destinations]);
    }

    public function showForm($destinationId = 0)
    {
        $selectedItem['id'] = 0;

        if ($destinationId != 0) {
            $selectedItem = DestinationEntity::find($destinationId)->toArray();
        }
        return view('admin.destinations.destinationForm', ['selectedItem' => $selectedItem]);
    }

    public function createUpdateDestinations(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = DestinationEntity::createUpdate($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/destinations/update_create_form/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }

    public function destroyDestinatations(Request $request)
    {

        $destId = $request->get('destId');

        $status = DestinationEntity::destroyDestianation($destId);
        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/destinations')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setSeoPictureData(Request $request)
    {
        $data = $request->all();
        $status = DestinationEntity::setPictureSeoData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('/admin/destinations/update_create_form/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


}