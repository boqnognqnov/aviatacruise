<?php

namespace App\Http\Controllers;

use App\CompanyEntity;
use App\DeckEntity;
use App\Http\Requests;
use Illuminate\Http\Request;


class AdminDecksController extends Controller
{

    public function listDecks($shipId)
    {
        $decks = DeckEntity::where('ship_id', $shipId)->get()->toArray();
//        return dump($decks);

        return view('admin.decks.list', ['shipId' => $shipId, 'decks' => $decks]);
    }

    public function showCreateUpdate($shipId, $deckId = 0)
    {
        $selectedDeck['id'] = 0;

        if ($deckId != 0) {

            $selectedDeck = DeckEntity::find($deckId)->toArray();
        }

        return view('admin.decks.deckForm', [
            'shipId' => $shipId,
            'selectedItem' => $selectedDeck,
        ]);
    }

    public function createUpdateDeck(Request $request)
    {
        $data = $request->all();
        $status = DeckEntity::createUpdate($data);

        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyDeck(Request $request)
    {
        $deckId = $request->get('deckId');
        $status = DeckEntity::destroyDeck($deckId);

        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.deletingError'));
            case 'success':
                return redirect('admin/decks/list')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }
}