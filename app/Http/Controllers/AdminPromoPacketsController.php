<?php

namespace App\Http\Controllers;

use App\CategoryNewsEntity;
use App\Http\Requests;
use App\NewsEntity;
use App\NewsGalleryEntity;
use App\PromoPacketEntity;
use Illuminate\Http\Request;


class AdminPromoPacketsController extends Controller
{


    public function listPackets()
    {


        $packets = PromoPacketEntity::all()->toArray();
        return view('admin.promoPackets.list', ['list' => $packets]);
    }

    public function showEditForm($packetId = 0)
    {
        $selectedItem['id'] = 0;

        if ($packetId != 0) {
            $selectedItem = PromoPacketEntity::find($packetId)->toArray();
        }

        return view('admin.promoPackets.promoForm', [
            'selectedItem' => $selectedItem,

        ]);
    }


    public function updateCreatePackets(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = PromoPacketEntity::createUpdate($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/promo-packets/update_create_form/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyPacket(Request $request)
    {
        $packetId = $request->get('packetId');
      
        $status = PromoPacketEntity::destroyPromo($packetId);
        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/promo-packets')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }


}