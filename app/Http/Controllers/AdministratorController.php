<?php

namespace App\Http\Controllers;

use App\AboutPageEntity;
use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\TagToNewsEntity;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;
use App\Http\Controllers\Auth\AuthController;


class AdministratorController extends Controller
{


    public function postAdminLogin(Request $request)
    {
        $data = $request->all();
//        $password = bcrypt($data['password']);
        $password = $data['password'];
//        return dump($data);
        if (Auth::attempt(['email' => $data['email'], 'password' => $password])) {
            return redirect()->intended('admin');
        } else {
            return back();
        }
    }





}