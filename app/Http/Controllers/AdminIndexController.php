<?php

namespace App\Http\Controllers;

use App\AboutTextEntity;
use App\CompanyEntity;
use App\Http\Requests;
use App\PageAboutEntity;
use App\PageIndexSliderEntity;
use Illuminate\Http\Request;


class AdminIndexController extends Controller
{


    public function listSliders()
    {

        $slidersData = PageIndexSliderEntity::all()->toArray();
        return view('admin.page_index.listSlidersData', ['slidersData' => $slidersData]);
    }

    public function editSlider($sliderId = 0)
    {
        $selectedSlider['id'] = 0;
        if ($sliderId > 0) {
            $selectedSlider = PageIndexSliderEntity::find($sliderId)->toArray();
        }
        return view('admin.page_index.indexSliderForm', ['selectedItem' => $selectedSlider]);
    }

    public function storeSlider(Request $request)
    {
        $data = $request->all();
        $status = PageIndexSliderEntity::createUpdate($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/index-page/slider/edit/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }

    public function destroySlider(Request $request)
    {
        $data = $request->all();
        $status = PageIndexSliderEntity::destroySlider($data['id']);
        switch ($status[0]) {

            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/index-page/slider/list')->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setActivity($sliderId)
    {
        $record = PageIndexSliderEntity::find($sliderId);
        if ($record->is_active == true) {
            $record->is_active = false;
        } else {
            $record->is_active = true;
        }
        $record->save();
        return redirect()->back();
    }


}