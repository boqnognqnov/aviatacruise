<?php

namespace App\Http\Controllers;

use App\CompanyEntity;
use App\Http\Requests;
use App\RouteShipEntity;
use App\ShipEntity;
use App\ShipGalleryEntity;
use Illuminate\Http\Request;


class AdminShipsController extends Controller
{


    public function listShips($companyId = 0)
    {
        $companies = $this->getCompaniesList();

        if ($companyId != 0) {
            $ships = ShipEntity::where('company_id', '=', $companyId)->get()->toArray();
        } else {
            $ships = ShipEntity::all()->toArray();
        }

        return view('admin.ships.list', [
            'list' => $ships,
            'companies' => $companies
        ]);
    }

    public function showEditForm($shipId = 0)
    {

        $selectedItem['id'] = 0;

        if ($shipId != 0) {
            $selectedItem = ShipEntity::find($shipId)->toArray();
        }


        return view('admin.ships.shipForm', [
            'selectedItem' => $selectedItem,
            'companies' => $this->getCompaniesList(),
        ]);
    }

    private function getCompaniesList()
    {
        $companiesRawList = CompanyEntity::all()->toArray();

        $companies = [];
        foreach ($companiesRawList as $oneCompany) {
            $companies[$oneCompany['id']] = $oneCompany['name_bg'];
        }
        return $companies;
    }

    public function createUpdateShip(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = ShipEntity::createUpdate($data);
//        return dump($status);

        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/ships/edit_form/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyShip(Request $request)
    {
        $shipId = $request->get('shipId');
//        return dump($shipId);
        $status = ShipEntity::destroyShip($shipId);

        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function showShipGallery($shipId)
    {
        $gallery = ShipGalleryEntity::where('ship_id', '=', $shipId)->get()->toArray();

        return view('admin.ships.gallery', [
            'gallery' => $gallery,
            'shipId' => $shipId,
        ]);
    }

    public function destroyGalleryImg(Request $request)
    {
        $imgId = $request->get('img_id');
//        return dump($imgId);
        $status = ShipGalleryEntity::destroyImg($imgId);

        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function uploadGalleryImg(Request $request)
    {
        $data = $request->all();
        if ($data['pictures'][0] == null) {
            return back();
        }

        $status = ShipGalleryEntity::uploadImgs($data);
        switch ($status[0]) {
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setSeoPictureDataGallery(Request $request)
    {
        $data = $request->all();
        $status = ShipGalleryEntity::setPictureSeoData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect()->back()->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setSeoPictureData(Request $request)
    {
        $data = $request->all();
        $status = ShipEntity::setPictureSeoData($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'runtimeError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'success':
                return redirect('admin/ships/edit_form/' . $status[1]->id)->with('success', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }
}