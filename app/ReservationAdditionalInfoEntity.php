<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ReservationAdditionalInfoEntity extends Model
{
    protected $table = 'reservation_additional_info';

    public static $rules = array(
        'reservation_id' => 'required',
        'info_id' => 'required',
    );

    public static function createUpdate($data){
        try {

            $rules = self::$rules;

            $validator = \Validator::make($data, $rules);

            if ($validator->fails()) {

                return array('validationError', $validator);
            }

            $record = ReservationAdditionalInfoEntity::findOrNew($data['id']);

            $record->reservation_id = $data['reservation_id'];
            $record->info_id = $data['info_id'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }


}
