<?php

namespace App\Classes;

use App;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Log;
use Illuminate\Support\Facades\Input;


class MakeReservation
{

    public static function treatmentPersonalData($data, $session)
    {
        $numOfPassengers = $session['passengers'];

        $reservationData['id'] = 0;
        $reservationData['passengers'] = $numOfPassengers;
        $reservationData['route_date_id'] = $session['date_id'];
        $reservationData['room_type_id'] = $session['room_id'];
        $reservationData['comment'] = $data['titilar_comment'];


        $passengersData = [];
        for ($i = 0; $i < $numOfPassengers; $i++) {
            $tempDay = $data['additional_person_dayBirth'][$i];
            $tempMonth = $data['additional_person_monthBirth'][$i];
            $tempYear = $data['additional_person_yearBirth'][$i];
            $dateStr = $tempMonth . '/' . $tempDay . '/' . $tempYear;
            $passengersData[$i]['date_birth'] = GlobalFunctions::convertStringToTime($dateStr);

            $passengersData[$i]['id'] = 0;
            $passengersData[$i]['fname'] = $data['additional_person_fName'][$i];
            $passengersData[$i]['lname'] = $data['additional_person_lName'][$i];

            $passengersData[$i]['is_titular'] = false;
            $passengersData[$i]['id_number'] = '8611110984';

        }

        $clearData['titularData'] = [];
        $clearData['titularData']['id'] = 0;
        $clearData['titularData']['fname'] = $data['titilar_fName'];
        $clearData['titularData']['sname'] = $data['titilar_mName'];
        $clearData['titularData']['lname'] = $data['titilar_lName'];

        $clearData['titularData']['email'] = $data['email'];
        $clearData['titularData']['phone'] = $data['phone'];

        $tempTitularDateStr = $data['titilar_monthBirth'] . '/' . $data['titilar_dayBirth'] . '/' . $data['titilar_yearBirth'];

        $clearData['titularData']['date_birth'] = GlobalFunctions::convertStringToTime($tempTitularDateStr);
        $clearData['titularData']['is_titular'] = true;
        $clearData['titularData']['id_number'] = '8611110984';


        $dataToReturn['reservationData'] = $reservationData;
        $dataToReturn['passengersData'] = $passengersData;
        $dataToReturn['clearData'] = $clearData;
        $dataToReturn['session'] = $session;

        return $dataToReturn;
    }

    public static function validateReservationData($reservationData)
    {

        $status = App\ClientEntity::validateClients($reservationData['clearData']['titularData']);
        if ($status[0] != 'success') {
            return $status;
        }

        foreach ($reservationData['passengersData'] as $firstOrSecondPassenger => $onePassenger) {
            if ($firstOrSecondPassenger == 1) {
                if (empty($onePassenger['fname']) && empty($onePassenger['lname'])) {
                    \Session::put('isOnlyOne', true);
                    continue;
                } else {
                    \Session::put('isOnlyOne', false);
                }
            }
            $status = App\ClientEntity::validateClients($onePassenger);
            if ($status[0] != 'success') {
                return $status;
            }

        }

        return array('success', 'success');
    }


    public static function createNewReservation($reservationData, $passengersData, $clearData, $session)
    {


        \DB::beginTransaction();
        try {
            $status = App\ReservationsEntity::createUpdate($reservationData);
            if ($status[0] == 'success') {
//                SAVE TITULAR CLIENT
                $reservationId = $status[1]->id;
                $clearData['titularData']['reservation_id'] = $reservationId;
                App\ClientEntity::createUpdate($clearData['titularData']);
//                SAVE TITULAR CLIENT

//                SAVE OTHER PASSENGERS
                foreach ($passengersData as $firstOrSecondPassenger => $onePassenger) {
                    if ($firstOrSecondPassenger == 1) {
                        $isOne = \Session::get('isOnlyOne');
                        if ($isOne == true) {
                            continue;
                        }
                    }
                    $onePassenger['reservation_id'] = $reservationId;
                    App\ClientEntity::createUpdate($onePassenger);
                }
//                SAVE OTHER PASSENGERS

//                SAVE PROMO PACKETS
                $reservationPacketData['company_id'] = $session['additional_data']['shipAndCompanyData']['company_id'];
                $reservationPacketData['reservation_id'] = $reservationId;
                $reservationPacketData['id'] = 0;
                foreach ($session['promoPackets'] as $onePacket) {
                    $reservationPacketData['packet_id'] = $onePacket;
                    App\ReservationPacketEntity::createUpdate($reservationPacketData);
                }

//                SAVE PROMO PACKETS

                //                SAVE ADDITIONAL INFOS
                $reservationInfoData['id'] = 0;
                $reservationInfoData['reservation_id'] = $reservationId;
                if (isset($session['infoSubscription'])) {
                    foreach ($session['infoSubscription'] as $oneInfo) {
                        $reservationInfoData['info_id'] = $oneInfo;
                        App\ReservationAdditionalInfoEntity::createUpdate($reservationInfoData);
                    }
                }


//                SAVE ADDITIONAL INFOS


            }

        } catch (\Exception $e) {
            \DB::rollBack();

            throw $e;
            return array('runtimeError', 'runtimeError');
        }

        \DB::commit();

        return array('success', 'success');
    }

}