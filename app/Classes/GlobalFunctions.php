<?php

namespace App\Classes;

use App;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Log;
use Illuminate\Support\Facades\Input;


class GlobalFunctions
{

    public static function convertStringToTime($date)
    {
        $dateArr = explode('/', $date);
        $dateStr = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1];
        return new \DateTime($dateStr);
    }

    public static function generateDateTimeToStr($date)
    {
//    EXAMPLE:    2016-01-20 18:28:03 to 01/20/2016

//        if ($date == null) {
//        return $date;
//        }
        $dateArr = explode('-', $date);
        $day = explode(' ', $dateArr[2]);

        $day = $day[0];
        $date = $dateArr[1] . '/' . $day . '/' . $dateArr[0];

        return $date;
    }

    public static function getGlobalArray($key = '')
    {

        if (!empty($key)) {
            $data = Config::get('GlobalArrays.' . $key);
        } else {
            $data = Config::get('GlobalArrays');
        }
        return $data;
    }

    public static function transLiterate($cyrOrLatin, $text)
    {

        $transArrays = $newText = self::getGlobalArray('transLiterate');
        if ($cyrOrLatin == 'cyrillic') {
            $newText = str_replace($transArrays['cyr'], $transArrays['lat'], $text);
        } else {
            $newText = str_replace($transArrays['lat'], $transArrays['cyr'], $text);
        }
        return $newText;

    }


    public static function getRandRoutesWithDiscount($resultCount = 3)
    {
        $lang = \App::getLocale();
//

        $routesRaw = App\RouteEntity::join('ships', 'ships.id', '=', 'routes.ship_id')
            ->join('route_dates', 'routes.id', '=', 'route_dates.route_id')
            ->where('route_dates.discount', '>', 0)
            ->join('date_room_offers', 'route_dates.id', '=', 'date_room_offers.date_id')
            ->join('room_types', 'room_types.id', '=', 'date_room_offers.room_id')
            ->join('route_points', 'routes.id', '=', 'route_points.route_id')
            ->join('points', 'route_points.point_id', '=', 'points.id')
            ->distinct()
            ->orderBy('routes.id')
            ->orderBy('date_room_offers.price')
            ->orderBy('route_dates.date')
            ->orderBy('route_points.sequence', 'ASC')
            ->select(
                'routes.id as route_id',
                'routes.route_title_' . $lang . ' as route_name',
                'routes.picture as route_picture',
                'routes.route_slug_' . $lang . ' as route_slug',
                'ships.id as ship_id',
                'ships.name_' . $lang . ' as ship_name',
                'date_room_offers.price as room_price',
                'route_dates.date as route_date',
                'route_dates.discount as route_discount',
                'room_types.id as room_id',
                'room_types.name_' . $lang . ' as room_name',
                'points.id as first_point_id',
                'points.name_' . $lang . ' as first_point_name',
                'points.country_' . $lang . ' as first_point_country',
                'routes.pic_alt_' . $lang . ' as pic_alt',
                'routes.pic_title_' . $lang . ' as pic_title'
            )
            ->get()
            ->toArray();


        $routes = [];
        $currentRouteId = 0;
        foreach ($routesRaw as $oneRoute) {
            if ($currentRouteId != $oneRoute['route_id']) {
                $currentRouteId = $oneRoute['route_id'];
                $routes[$currentRouteId] = $oneRoute;
                $routes[$currentRouteId]['route_date'] = GlobalFunctions::generateDateTimeToStr($oneRoute['route_date']);
            }
        }

        shuffle($routes);

        $routes = array_slice($routes, 0, $resultCount);

        return $routes;
    }

    public static function getAllShips($routeId)
    {


        $shipsRaw = App\ShipEntity::all()->toArray();

        $ships = [];

        foreach ($shipsRaw as $key => $oneShip) {
            $tempSet = App\RouteShipEntity::where('ship_id', '=', $oneShip['id'])->where('route_id', '=', $routeId)->first();
            $ships[$key]['id'] = $oneShip['id'];
            $ships[$key]['name'] = $oneShip['name_bg'];

            $ships[$key]['checked'] = false;
            if ($tempSet) {
                $ships[$key]['checked'] = true;
            }
        }


        return $ships;
    }


    public static function getLowerPricePerCabin($routeData)
    {
        foreach ($routeData as $key => $oneRoute) {
            $routeData[$key]['room_price_and_date'] = [];

            $tempRec = App\DateRoomOfferEntity::join('route_dates', 'date_room_offers.date_id', '=', 'route_dates.id')
                ->where('route_dates.route_id', '=', $oneRoute['route_id'])
                ->orderBy('route_dates.date', 'ASC')
                ->orderBy('date_room_offers.price', 'ASC')
                ->select('date_room_offers.price', 'route_dates.date as route_date')
                ->distinct()
                ->first();
            if ($tempRec) {
                $routeData[$key]['room_price_and_date'] = $tempRec->toArray();
                $routeData[$key]['room_price_and_date']['route_date'] = GlobalFunctions::generateDateTimeToStr($routeData[$key]['room_price_and_date']['route_date']);
            }
        }


        return $routeData;
    }

    public static function getFirstPoint($routeData)
    {
        $lang = \App::getLocale();

        foreach ($routeData as $key => $oneRoute) {

            $routeData[$key]['first_point'] = [];
            $tempFirstPoint = App\PointsEntity::join('route_points', 'points.id', '=', 'route_points.point_id')
                ->where('route_points.route_id', '=', $oneRoute['route_id'])
                ->orderBy('route_points.sequence', 'ASC')
                ->select(
//                    'points.*',
                    'points.name_' . $lang . ' as point_name',
                    'points.country_' . $lang . ' as point_country',
                    'points.description_' . $lang . ' as point_description'

                )
                ->distinct()
                ->first();
            if ($tempFirstPoint) {
                $routeData[$key]['first_point'] = $tempFirstPoint->toArray();
            }
        }


        return $routeData;
    }

    public static function getDecsByShipId($shipId)
    {
//        $decs = App\RoomTypeEntity::join('decks', 'room_types.deck_id', '=', 'decks.id')
//            ->join('ships', 'decks.ship_id', '=', 'ships.id')
//            ->select('decks.*', 'decks.id as deck_id')->distinct()
//            ->where('ships.id', '=', $shipId)
//            ->get()->toArray();

        $decs = [];

        $decsRaw = App\DeckEntity::where('decks.ship_id', '=', $shipId)
            ->select('decks.*', 'decks.id as deck_id')->distinct()
            ->get();
        if ($decsRaw) {
            $decs = $decsRaw->toArray();
        }


        return $decs;
    }

    public static function getCabinsByShipId($shipId)
    {
        $cabins = App\RoomTypeEntity::join('decks', 'room_types.deck_id', '=', 'decks.id')
            ->join('ships', 'decks.ship_id', '=', 'ships.id')
            ->select('room_types.id as room_id', 'room_types.name_' . \App::getLocale() . ' as room_name', 'room_types.description_' . \App::getLocale() . ' as room_description', 'room_types.room_count as room_count', 'room_types.picture as room_picture')->distinct()
            ->where('ships.id', '=', $shipId)
            ->get()->toArray();

        return $cabins;
    }

    public static function getRoutePointsByRouteId($routeId)
    {
        $selectedPoints = App\PointsEntity::join('route_points', 'points.id', '=', 'route_points.point_id')
            ->where('route_points.route_id', '=', $routeId)->orderBy('sequence', 'ASC')->get()->toArray();

        return $selectedPoints;
    }

    public static function getStampToStrFullMonth($datetime)
    {
//    EXAMPLE:    2016-01-20 18:28:03 to 01/20/2016

        $dateArr = explode('-', $datetime);
        $day = explode(' ', $dateArr[2]);
        $day = $day[0];
        $monthNamedArr = Config::get('GlobalArrays.monthNamed_' . \App::getLocale());
//        $date = $monthNamedArr[$dateArr[1]] . ' ' . $day . ', ' . $dateArr[0];
        $date = $day . ' ' . $monthNamedArr[$dateArr[1]] . ' ' . $dateArr[0];

        return $date;
    }

    public static function generateDateOfBirdsDropdown()
    {

        $dropDownsToReturn['days'] = self::generateDaysOfMonthDropDown();
        $dropDownsToReturn['months'] = self::generateMonthDropDown();
        $dropDownsToReturn['years'] = self::generateYearsDropDown();

        return $dropDownsToReturn;
    }


    private static function generateMonthDropDown()
    {
        $monthArr = self::getGlobalArray('monthNamed_' . \App::getLocale());
        return $monthArr;
    }

    private static function generateDaysOfMonthDropDown()
    {

        $maxCount = 32;
        $daysArr = [];
        for ($i = 1; $i < $maxCount; $i++) {
            if (strlen($i) == 1) {
                $i = '0' . $i;
            }
            $daysArr[$i] = $i;
        }
        return $daysArr;
    }

    public static function generateYearsDropDown()
    {
        $upVal = self::getGlobalArray('years_dropdown_up_value');
        $downVal = self::getGlobalArray('years_dropdown_down_value');
        $yearArr = [];
        for ($i = $downVal; $i < $upVal; $i++) {
            $yearArr[$i] = $i;
        }

        return $yearArr;
    }

    public static function calculatePriceByParams($roomPrice, $roomDiscount, $routeDiscount, $numOfPassengers)
    {
        $roomPrice = ($roomPrice - ($roomPrice * (($roomDiscount + $routeDiscount) / 100))) * $numOfPassengers;
        return $roomPrice;
    }

    public static function transformArrByGroups($countGroup, $arrayRaw)
    {
        $counter = 0;
        $counter2 = 0;
        $newArr = [];
        foreach ($arrayRaw as $oneItem) {

            if ($counter == $countGroup) {
                $counter = 0;
                $counter2++;
            }
            $newArr[$counter2][$counter] = $oneItem;
            $counter++;
        }

        return $newArr;
    }


    public static function getStaticPageSeoData($pageId)
    {
        $lang = \App::getLocale();
        $dataToReturn = [];
        $data = App\SeoStaticPagesEntity::find($pageId)->toArray();
        $dataToReturn['title'] = $data['seo_title_' . $lang];
        $dataToReturn['description'] = $data['seo_description_' . $lang];

        return $dataToReturn;

    }

    public static function getDynamicSeoDataFromModel($model, $hasPrefix = false)
    {
        $lang = \App::getLocale();
        $dataToReturn = [];
        if ($hasPrefix) {
            $dataToReturn['title'] = $model['seo_title_' . $lang];
            $dataToReturn['description'] = $model['seo_description_' . $lang];
        } else {
            $dataToReturn['title'] = $model['seo_title'];
            $dataToReturn['description'] = $model['seo_description'];
        }

        return $dataToReturn;

    }

    public static function getPicSeoDataByRecordId($model, $itemId)
    {
        $lang = \App::getLocale();
        $dataToReturn = [];
        $data = $model::find($itemId)->toArray();
        $dataToReturn['pic_alt'] = $data['pic_alt_' . $lang];
        $dataToReturn['pic_title'] = $data['pic_title_' . $lang];

        return $dataToReturn;

    }

    public static function getRecordPropertiesIfExist($recordsRaw, $propertiesArr, $clear = false)
    {
        $lang = \App::getLocale();
        $records = [];


        if ($recordsRaw) {
            $recordsRawTemp = $recordsRaw->toArray();
            foreach ($recordsRawTemp as $key => $oneTemp) {
                $records[$key] = $oneTemp;
                foreach ($propertiesArr as $oneProp) {
                    $records[$key][str_replace('_', '', $oneProp)] = $oneTemp[$oneProp . $lang];

                    if ($clear == true) {
                        unset($records[$key][$oneProp . $lang]);
                    }
                }

            }
        }

        return $records;
    }

    public static function getFavoriteNews()
    {
        $news = App\NewsEntity::where('news.is_front', '=', true)->get();
        $news = self::getRecordPropertiesIfExist($news, ['title_', 'slug_', 'text_', 'sub_title_'], true);

        foreach ($news as $key => $oneNews) {
            $date = new \DateTime($oneNews['created_at']);
            if (\App::getLocale() != 'bg') {
                $news[$key]['created_at'] = $date->format('l, d F');
            } else {
                $monthNum = $date->format('m');
                $dayNum = $date->format('N');
                $dayDate = $date->format('d');

                $dayOfWeek = GlobalFunctions::getGlobalArray('day_of_week_bg')[$dayNum];
                $month = GlobalFunctions::getGlobalArray('monthNamedCapitalise_bg')[$monthNum];

                $news[$key]['date'] = $dayOfWeek . ', ' . $dayDate . ' ' . $month;

            }

        }
        return $news;
    }

    public static function breadCrumbs($pageName, $childDataArr = [])
    {
        $breadcrumpArr = [];
        $pageData = GlobalFunctions::getGlobalArray('bread_crumps')[$pageName];

        foreach ($pageData['parent_keys'] as $key => $oneParent) {
            $breadcrumpArr[$key]['url'] = GlobalFunctions::getGlobalArray('bread_crumps')[$oneParent]['url'];
            $breadcrumpArr[$key]['title'] = GlobalFunctions::getGlobalArray('bread_crumps')[$oneParent]['title_' . \App::getLocale()];
        }

        $count = sizeof($breadcrumpArr);

        $breadcrumpArr[$count]['url'] = $pageData['url'];
        $breadcrumpArr[$count]['title'] = $pageData['title_' . \App::getLocale()];


        if (sizeof($childDataArr) > 0) {
            $newCount = sizeof($breadcrumpArr);
            $breadcrumpArr[$newCount]['url'] = $childDataArr['url'];
            $breadcrumpArr[$newCount]['title'] = $childDataArr['title'];
        }


//        dump($breadcrumpArr);
        return $breadcrumpArr;
    }


}