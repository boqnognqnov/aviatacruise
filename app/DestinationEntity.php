<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class DestinationEntity extends Model
{
    protected $table = 'destinations';
    public static $path = 'uploads/images/destinations/';


    public static function rules()
    {
        return array(
            'picture' => 'image',
            'name_bg' => 'required',
            'description_bg' => 'required',
        );
    }

    public static function createUpdate($data)
    {
        try {
            $rules = self::rules();
            if ($data['destination_id'] == 0) {
                $rules['picture'] = 'image|required';
            }

            $validator = \Validator::make($data, $rules);
            if ($validator->fails()) {
                return array('validationError', $validator);
            }


            $record = DestinationEntity::findOrNew($data['destination_id']);

            $record->name_bg = $data['name_bg'];
            $record->slug_bg = str_slug($data['name_bg']);
            $record->description_bg = $data['description_bg'];

            if (isset($data['seo_title_bg'])) {
                $record->seo_title_bg = $data['seo_title_bg'];
            }

            if (isset($data['seo_description_bg'])) {
                $record->seo_description_bg = $data['seo_description_bg'];
            }


            if (isset($data['picture'])) {
                if (!empty($record->picture)) {
                    File::delete(self::$path . $record->picture);

                }
                $fileName = time() . str_random(10) . '.' . $data['picture']->getClientOriginalExtension();
                $image = Image::make($data['picture']->getRealPath());
                $image->resize(720, 450);
                $image->save(self::$path . $fileName);
                $record->picture = $fileName;
            }

            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }

    public static function destroyDestianation($destinationId)
    {
        try {

            $record = DestinationEntity::find($destinationId);
            File::delete(self::$path . $record->picture);
            $record->delete();

//            DELETE ROUTES !!!

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');
    }

    public static function setPictureSeoData($data)
    {
        try {
            $record = DestinationEntity::find($data['destination_id']);
            $record->pic_title_bg = $data['pic_title_bg'];
            $record->pic_alt_bg = $data['pic_alt_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }
}
