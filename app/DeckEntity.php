<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class DeckEntity extends Model
{
    protected $table = 'decks';

    public static $path = 'uploads/images/decks/';
    public static $rules = array(
        'name_bg' => 'required',
        'schema' => 'image',
    );

    public static function createUpdate($data)
    {
        try {

            $rules = self::$rules;

            if ($data['deck_id'] == 0) {

                $rules['schema'] = 'image|required';
            }

            $validator = \Validator::make($data, $rules);

            if ($validator->fails()) {

                return array('validationError', $validator);
            }

            $record = DeckEntity::findOrNew($data['deck_id']);

            $record->ship_id = $data['ship_id'];
            $record->name_bg = $data['name_bg'];

            if (isset($data['schema'])) {

                if (!empty($record->schema)) {

                    File::delete(self::$path . $record->schema);
                }

                $fileName = time() . str_random(10) . '.' . $data['schema']->getClientOriginalExtension();
                $image = Image::make($data['schema']->getRealPath());

                $image->save(self::$path . $fileName);
                $record->schema = $fileName;
            }

            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', $record);
    }

    public static function destroyDeck($id)
    {
//        try {

            $record = DeckEntity::find($id);

            File::delete(self::$path . $record->schema);

            $record->delete();
//        } catch (\Exception $ex) {
//
//            \Log::error($ex);
//
//            return array('runtimeError', $ex);
//        }

        return array('success', 'success');
    }
}
