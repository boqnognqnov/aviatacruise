<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class RouteGalleryEntity extends Model
{
    protected $table = 'routes_gallery';

    public static $path = 'uploads/images/routesGallery/';

    public static function uploadImages($data)
    {
        try {

            $position = 1;

            foreach ($data['pictures'] as $picture) {

                $record = new RouteGalleryEntity();

                $lastPos = RouteGalleryEntity::orderBy('position', 'DESC')->get()->first();
                if ($lastPos) {
                    $position = ($lastPos->position) + 1;
                }

                $fileName = time() . str_random(10) . '.' . $picture->getClientOriginalExtension();
                $image = Image::make($picture->getRealPath());

                $image->resize(1900, 500);

                $image->save(self::$path . $fileName);

                $record->position = $position;

                $record->picture = $fileName;
                $record->route_id = $data['route_id'];
                $record->save();
            }
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', 'success');
    }

    public static function destroyImg($galleryImgId)
    {
        try {
            $record = RouteGalleryEntity::find($galleryImgId);

            File::delete(RouteGalleryEntity::$path . $record->picture);

            $record->delete();
        } catch (\Exception $ex) {

            \Log::error($ex);

            return array('runtimeError', $ex);
        }

        return array('success', 'success');
    }

    public static function setPictureSeoData($data)
    {
        try {
            $record = RouteGalleryEntity::find($data['pic_id']);
            $record->pic_title_bg = $data['pic_title_bg'];
            $record->pic_alt_bg = $data['pic_alt_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }
}
