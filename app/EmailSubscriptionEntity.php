<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class EmailSubscriptionEntity extends Model
{
    protected $table = 'email_subscription';


    public static $rules = array(

        'email' => 'required|email',
    );

    public static function setEmail($email)
    {
        $data['email'] = $email;
        $validator = \Validator::make($data, self::$rules);
        if ($validator->fails()) {
            return;
        }
        try {
            $oldRec = EmailSubscriptionEntity::where('email', 'LIKE', $email)->first();
            if (!$oldRec) {
                $record = new EmailSubscriptionEntity();
                $record->email = $email;
                $record->save();
            }

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');

    }

    public static function destroyEmail($emailId)
    {
        try {
            $record = EmailSubscriptionEntity::find($emailId);
            $record->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');
    }
}
