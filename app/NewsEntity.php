<?php

namespace App;

use App\Classes\GlobalFunctions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class NewsEntity extends Model
{
    protected $table = 'news';
    public static $path = 'uploads/images/news/';


    public static function newsRules()
    {
        return array(
            'picture' => 'image',
            'title_bg' => 'required',
            'text_bg' => 'required',
        );
    }


    public static function createUpdate($data)
    {
        try {
            $rules = self::newsRules();
            if ($data['news_id'] == 0) {
                $rules['picture'] = 'image|required';
            }

            $validator = \Validator::make($data, $rules);
            if ($validator->fails()) {
                return array('validationError', $validator);
            }


            $record = NewsEntity::findOrNew($data['news_id']);

            $date = GlobalFunctions::convertStringToTime($data['date']);

            $record->created_at = $date;
            $record->category_id = $data['category_id'];
            $record->title_bg = $data['title_bg'];
            $record->sub_title_bg = $data['sub_title_bg'];
            $record->slug_bg = str_slug($data['title_bg']);
            $record->text_bg = $data['text_bg'];

            $record->is_front = false;
            if (isset($data['is_front'])) {
                $record->is_front = true;
            }

            if (isset($data['picture'])) {
                if (!empty($record->picture)) {
                    File::delete(self::$path . $record->picture);

                }
                $fileName = time() . str_random(10) . '.' . $data['picture']->getClientOriginalExtension();
                $image = Image::make($data['picture']->getRealPath());
                $image->resize(690, 510);
                $image->save(self::$path . $fileName);
                $record->picture = $fileName;
            }

            if (isset($data['seo_title_bg'])) {
                $record->seo_title_bg = $data['seo_title_bg'];
            }

            if (isset($data['seo_description_bg'])) {
                $record->seo_description_bg = $data['seo_description_bg'];
            }

            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }

    public static function destroyNews($newsId)
    {
        try {
            $gallery = NewsGalleryEntity::where('news_id', '=', $newsId)->get();
            foreach ($gallery as $oneImg) {
                File::delete(NewsGalleryEntity::$path . $oneImg->picture);
            }

            $record = NewsEntity::find($newsId);
            File::delete(self::$path . $record->picture);
            $record->delete();
        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');
    }

    public static function setPictureSeoData($data)
    {
        try {
            $record = NewsEntity::find($data['news_id']);
            $record->pic_title_bg = $data['pic_title_bg'];
            $record->pic_alt_bg = $data['pic_alt_bg'];
            $record->save();
        } catch (\Exception $ex) {

            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }
}
