<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class QuestionsEntity extends Model
{

    protected $table = 'questions';

    public static function newsRules()
    {
        return array(
//            'picture' => 'image',
            'name_bg' => 'required',
            'text_bg' => 'required',
        );
    }


    public static function createUpdate($data)
    {
        try {
            $rules = self::newsRules();


            $validator = \Validator::make($data, $rules);
            if ($validator->fails()) {
                return array('validationError', $validator);
            }


            $record = QuestionsEntity::findOrNew($data['quest_id']);

            $record->category_id = $data['category_id'];
            $record->name_bg = $data['name_bg'];
            $record->slug_bg = str_slug($data['name_bg']);
            $record->text_bg = $data['text_bg'];


            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }

    public static function destroyQuest($questId)
    {
        try {
            QuestionsEntity::destroy($questId);
        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');
    }
}
