<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class PromoPacketEntity extends Model
{
    protected $table = 'promo_packets';

    public static $rules = array(
        'description_bg' => 'required',
        'name_bg' => 'required',
        'price' => 'required'
    );

    public static function createUpdate($data)
    {
        $rules = self::$rules;
        $validator = \Validator::make($data, $rules);
        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        try {
            $record = PromoPacketEntity::findOrNew($data['promo_id']);

            $record->name_bg = $data['name_bg'];
            $record->description_bg = $data['description_bg'];
            $record->price = $data['price'];

            $record->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', $record);
    }

    public static function destroyPromo($promoId)
    {
        try {
            ReservationPacketEntity::where('packet_id', '=', $promoId)->delete();
            PromoPacketEntity::destroy($promoId);

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('runtimeError', $ex);
        }
        return array('success', 'success');
    }
}
