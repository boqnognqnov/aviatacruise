@extends('master')

@section('plugin-assets-css')
@endsection
@section('plugin-assets-js')

@endsection


@section('page-title', ' ')
@section('meta-description', ' ')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="inner-nav">
                    @foreach($breadcrumps as $onecrump)
                        <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="success-wrapper">
        <div class="container text-over-image">
            <div class="row">
                <div class="col-md-12 text-center">
                    <img title="" alt="" src="img/icons/success-top-logo.png">
                    <div class="dotted-wrapper">
                        <h3>УСПЕШНО ИЗПРАТИХТЕ ЗАЯВКА ЗА РЕЗЕРВАЦИЯ!</h3>
                    </div>
                    <p>Веднага щом бъде прегледанa ще се свържем с Вас, за да уточним всички детайли по заплащането.</p>
                    <p class="tanks-node">Благодарим Ви, че избрахте
                    <h1> АВИАТА ТРАВЕЛ!</h1></p>
                </div>
            </div>
        </div>

        @include('components.reservation-reveiw',['reservationData'=>$reservationData])
    </div>
    <div class="success-personal-data">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>лични данни:<span></span></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">

                    <h4>Лице за контакт /титуляр:</h4>
                    <?php $titularData = $clientsInformation['clearData']['titularData']; ?>
                    <ul>
                        <li>
                            <strong>Име:</strong>{!! $titularData['fname'].' '. $titularData['sname'].' '. $titularData['lname'] !!}
                        </li>
                        <li><strong>Дата на
                                раждане:</strong>
                            {!! \App\Classes\GlobalFunctions::generateDateTimeToStr($titularData['date_birth']->format('Y-m-d H:i:s')) !!}
                        </li>
                        <li><strong>E-mail:</strong> {!! $titularData['email'] !!}</li>
                        <li><strong>Телефон:</strong> {!! $titularData['phone'] !!}</li>
                        <li><strong>Коментар:</strong> {!! $clientsInformation['reservationData']['comment'] !!}</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h4>Лица пътуващи във вътрешна каюта: {!! $clientsInformation['reservationData']['passengers'] !!}
                        лица</h4>
                    <ul>
                        @foreach($clientsInformation['passengersData'] as $onePassenger)

                            <li><strong>Име:</strong>{!! $onePassenger['fname'].' '.$onePassenger['lname'] !!}</li>
                            <li><strong>Дата на
                                    раждане:</strong>
                                {!! \App\Classes\GlobalFunctions::generateDateTimeToStr($onePassenger['date_birth']->format('Y-m-d H:i:s')) !!}
                            </li>
                            <li>
                                <div class="dotted-border"></div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="container subscribe-mail">
            <div class="row">
                <div class="col-md-12">
                    <h5><span class="black">@</span>E-mail бюлетин:</h5>
                    <p>Запишете се в ежеседмичния ни бюлетин и получавайте актуална информация за най-изгодните ни
                        оферти!</p>
                    {!! Form::open(array('method'=>'post','class'=>'','action'=>'ACEmailToSubscriptionController@emailSubscription')) !!}
                    {{ Form::text('email_to_subscription', $titularData['email'] , [ "class"=>"email-input", "placeholder"=>"Въведете E-mail адрес..." ]) }}
                    <input class="submit" value="ЗАПИСВАНЕ" type="submit">
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
    @include('components.footer-promotions')


@endsection
@section('page-scripts')
    <script src="js/scripts.js"></script>
    <script>
        $(document).ready(function () {

            (function (window, location) {
                history.replaceState(null, document.title, location.pathname + "#!/history");
                history.pushState(null, document.title, location.pathname);

                window.addEventListener("popstate", function () {
                    if (location.hash === "#!/history") {
                        history.replaceState(null, document.title, location.pathname);
                        setTimeout(function () {
                            location.replace("../../../../offer-details/choose_cabin_and_packets");
                        }, 0);
                    }
                }, false);
            }(window, location));
        });

    </script>
@endsection
