@extends('master')

@section('plugin-assets-css')

@endsection
@section('plugin-assets-js')

@endsection

@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])


@section('fb-title', $ccSummaryComponentData['title'])
@section('fb-image', asset($ccSummaryComponentData['picture']))
@section('fb-description',  $ccSummaryComponentData['description'])

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="inner-nav">
                        @foreach($breadcrumps as $onecrump)
                            <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>

        @include('components.cc-summary',$ccSummaryComponentData)
        <div>
            <div class="container">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a title="" href="#ships" aria-controls="ships" role="tab"
                                                              data-toggle="tab">кораби</a></li>
                    <li role="presentation"><a title="" href="#cruises" aria-controls="cruises" role="tab"
                                               data-toggle="tab">Круизи</a>
                    </li>
                    <li role="presentation"><a title="" href="#destination" aria-controls="destination" role="tab"
                                               data-toggle="tab">дестинации</a></li>
                    <li role="presentation"><a title="" href="#aditional-sugests" aria-controls="aditional-sugests"
                                               role="tab"
                                               data-toggle="tab">допълнителни предложения</a></li>
                    <li role="presentation"><a title="" href="#pricing" aria-controls="pricing" role="tab"
                                               data-toggle="tab">общи
                            условия</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="ships">
                        @foreach($shipsOnThisCompnay as $oneShip)
                            <div class="single-ship">
                                <div class="row">
                                    <div class="col-md-6">
                                        <img title="" alt="" class="img-responsive"
                                             src="{!! asset(\App\ShipEntity::$path.$oneShip['picture']) !!}"/>
                                    </div>
                                    <div class="col-md-6">
                                        <h3>{!! $oneShip['name_'.\App::getLocale()] !!}</h3>
                                        <p>{!! $oneShip['description_text_'.\App::getLocale()] !!}</p>
                                        <a title="" href="{!! url('ship/'.$oneShip['slug_'.\App::getLocale()]) !!}"
                                           class="view-more">Виж повече<span></span></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                    <div role="tabpanel" class="tab-pane" id="cruises">
                        @include('components.tab-cruiz',['routes'=>$routesOnThisCompany])
                    </div>

                    <div role="tabpanel" class="tab-pane" id="destination">
                        @foreach($destinationsOnThisCompany as $oneDestination)
                            <div class="single-destination">
                                <div class="row">
                                    <div class="col-md-5">
                                        <img title="" alt="" class="img-responsive"
                                             src="{!! asset(\App\DestinationEntity::$path.$oneDestination['picture']) !!}"/>
                                    </div>
                                    <div class="col-md-7">
                                        <h3>{!! $oneDestination['destination_name'] !!}</h3>
                                        <p>{!! $oneDestination['destination_description'] !!}</p>
                                        <a title="" class="veiw-more"
                                           href="{!! url('inner-destinations/'.$oneDestination['destination_slug']) !!}">Виж
                                            Повече<span></span></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                    <div role="tabpanel" class="tab-pane" id="aditional-sugests">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Допълнителни предложения</h3>
                                <ul>
                                    @foreach($promoPacketsOnThisCompany as $onePacket)
                                        <li>
                                            <div class="single-packet">
                                                <h4>{!! $onePacket['packet_name'] !!}</h4>
                                                <p>{!! $onePacket['packet_description'] !!}</p>
                                                <p class="price">{!! $onePacket['price'] !!} EUR</p>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="pricing">
                        {{--@include('components.tab-price')--}}
                        @include('components.tab-terms',['company'=>$company])
                    </div>

                </div>
                @include('components.social-share')
            </div>
        </div>
    </section>
    @include('components.footer-promotions')


@endsection
@section('page-scripts')
    <script src="js/scripts.js"></script>
    <script>
        $(document).ready(function () {


        });
    </script>
@endsection
