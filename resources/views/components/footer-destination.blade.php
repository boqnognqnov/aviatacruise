<div class="footer-promotion">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="destination-footer-title">Дестинации<span></span></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				  <ul class="main-destination-footer-links">
					<li><a title=""  href="#">Аляска и Канада</a></li>
					<li><a title=""  href="#">Австралия и Нова Зеландия</a></li>
					<li><a title=""  href="#">Южна и Северна Америка</a></li>
					<li><a title=""  href="#">Кариби, Бахами</a></li>
					<li><a title=""  href="#">Куба</a></li>
				  </ul>
			</div>
			<div class="col-md-3">
				 <p class="sec-destination-footer-title">Северна Европа</p>
				 <ul class="sec-destination-footer-links">
					<li><a title=""  href="#">Аляска и Канада</a></li>
					<li><a title=""  href="#">Австралия и Нова Зеландия</a></li>
					<li><a title=""  href="#">Южна и Северна Америка</a></li>
					<li><a title=""  href="#">Кариби, Бахами</a></li>
					<li><a title=""  href="#">Куба</a></li>
				  </ul>
			</div>
			<div class="col-md-3">
				 <p class="sec-destination-footer-title">Южна Европа</p>
				 <ul class="sec-destination-footer-links">
					<li><a title=""  href="#">Западно Средиземноморие</a></li>
					<li><a title=""  href="#">Източно Средиземноморие</a></li>
					<li><a title=""  href="#">Гръцки острови</a></li>
					<li><a title=""  href="#">Канарски острови</a></li>
					<li><a title=""  href="#">Гърция/ Турция/ Черно море</a></li>
					<li><a title=""  href="#">Близкия изток и ОАЕ</a></li>
				  </ul>
			</div>
			<div class="col-md-3">
				 <ul class="main-destination-footer-links">
					<li><a title=""  href="#">Азия</a></li>
					<li><a title=""  href="#">Световни круизи</a></li>
					<li><a title=""  href="#">Трансатлантически и препозициониране</a></li>
				  </ul>
			</div>
		</div>
	</div>
</div>
