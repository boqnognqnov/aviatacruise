@foreach($routes as $oneCruise)

    <div class="single-cruise">
        <a title="" href="{!! url('offer/'.$oneCruise['route_slug']) !!}">
            <div class="row">
                <div class="col-md-3">
                    <img title="{!! $oneCruise['pic_title'] !!}" alt="{!! $oneCruise['pic_alt'] !!}"
                         class="img-responsive" src="{!! asset(\App\RouteEntity::$path.$oneCruise['picture']) !!}">
                </div>
                <div class="col-md-9">
                    <div class="inner-text-wrapper">
                        <div class="row">
                            <div class="col-md-9">
                                <h3>{!! @$oneCruise['route_name'] !!}</h3>
                                <ul>
                                    <li class="hotel-name"><span>Компания: </span>{!! @$oneCruise['company_name'] !!}
                                    </li>
                                    <li class="offer-location">
                                        <span></span>от {!! @$oneCruise['first_point']['name_'.\App::getLocale()] !!}
                                    </li>
                                    <li class="offer-dates">
                                        <span></span>{!! @$oneCruise['room_price_and_date']['route_date'] !!}
                                    </li>

                                </ul>
                            </div>
                            <div class="col-md-3">
                                <p class="offer-price">
                                    <span class="euro-sight"></span>
                                    {!! @$oneCruise['room_price_and_date']['price'] !!}
                                    <span class="per-person">
                            <span class="top-arrow"></span>
                            на човек
                        </span>
                                </p>
                            </div>
                        </div>
                        <p class="info">{!! @$oneCruise['route_description'] !!}</p>

                    </div>
                    <p class="view-more">Виж повече<span></span></p>
                </div>
            </div>
        </a>
    </div>

@endforeach
