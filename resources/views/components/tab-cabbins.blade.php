@foreach($cabins as $oneCabin)
    <div class="row">
        <div class="col-md-12">
            <h3>{!! $oneCabin['room_name'] !!}</h3>
            <div class="single-cabbin clearfix">
                <img title="" alt="" src="{!! asset(\App\RoomTypeEntity::$path.$oneCabin['room_picture']) !!}">
                <div class="text-wrapper">
                    <p>
                        {!! $oneCabin['room_description'] !!}
                    </p>

                </div>
            </div>
        </div>
    </div>
@endforeach
