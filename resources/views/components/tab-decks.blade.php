<div class="row">
    <div class="col-md-12">
        @foreach($decs as $oneDeck)
            <h3>{!! $oneDeck['name_'.\App::getLocale()] !!}</h3>
            <div class="zoom-img-wrapper">
                <div class="zoom-img" data-image="{!! asset(\App\DeckEntity::$path.$oneDeck['schema']) !!}"
                     data-image-zoom="{!! asset(\App\DeckEntity::$path.$oneDeck['schema']) !!}" data-size="150">
                </div>
            </div>
        @endforeach

    </div>
</div>
<script>
    $(document).ready(function () {
        $(".zoom-img").magnifierRentgen();
    });
</script>
