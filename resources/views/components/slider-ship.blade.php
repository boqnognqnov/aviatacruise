<link href="{!! asset('css/flexslider.min.css') !!}" rel="stylesheet">
<script src="{!! asset('js/jquery.flexslider.min.js') !!}"></script>
<div style="position:relative">
    <div class="container">
        <h2 class="slider-offer-title">
            {!! $ship['name_'.\App::getLocale()] !!}
        </h2>
    </div>
    <div id="slider-offer" class="flexslider">

        <div class="gradient-slider"></div>
        <ul class="slides">
            @foreach($gallery as $onePic)
                <li><img title="{!! $onePic['pic_title'] !!}" alt="{!! $onePic['pic_alt'] !!}"
                         src="{!! asset($onePic['picture']) !!}"></li>
            @endforeach

        </ul>
    </div>
    <div class="course-info-wrapper container">
        <div class="course-info">

            <ul>
                {{--<li><img title="" alt="" src="img/icons/icons-slider/pin.png"/>от Атина (Лаврион)</li>--}}
                {{--<li><img title="" alt="" src="img/icons/icons-slider/calendar.png"/>28 / 04 / 2016</li>--}}
            </ul>
            <ul>
                <li><img title="{!! $onePic['pic_title'] !!}" alt="{!! $onePic['pic_alt'] !!}"
                         src="{!! asset(\App\CompanyEntity::$pathLogo.$company['logo']) !!}"/>
                    / {!! $company['name_'.\App::getLocale()] !!}
                </li>
                <li><img title="" alt=""
                         src="img/icons/icons-slider/logo-2.png"/>{!! $ship['name_'.\App::getLocale()] !!}</li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row" style="position:relative">
            <div id="slider-offer-thumbnails" class="flexslider">
                <ul class="slides">
                    @foreach($gallery as $onePic)
                        <li><img title="" alt="" src="{!! asset ($onePic['picture']) !!}"/></li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#slider-offer-thumbnails').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            asNavFor: '#slider-offer',
            itemWidth: 120
        });

        $('#slider-offer').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#slider-offer-thumbnails"
        });

    });
</script>
