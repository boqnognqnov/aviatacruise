<div class="container reservation-review">
    <div class="row header">
        <div class="col-xs-6">
            <h3>преглед на резервация</h3>
        </div>
        <div class="col-xs-6">
            <p class="price">крайна цена:<span class="euro-sight"><i class="fa fa-eur"></i></span><span
                        class="value">{!! $reservationData['prices']['totalRoomPrice'] !!}</span>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <ul>
                <li><strong>Круизна
                        компания: </strong>{!! $reservationData['additional_data']['shipAndCompanyData']['company_name'] !!}
                </li>
                <li>
                    <strong>Кораб: </strong>{!! $reservationData['additional_data']['shipAndCompanyData']['ship_name'] !!}
                </li>

                <?php
                $routeList = '';
                foreach ($reservationData['additional_data']['route_points_arr'] as $onePointName) {
                    $routeList .= $onePointName . ', ';
                }
                $routeList = trim($routeList, " ,")
                ?>
                <li><strong>Маршрут: </strong>

                    {!! $routeList !!}
                </li>
                <li><strong>Начална
                        дата: </strong>{!! \App\Classes\GlobalFunctions::generateDateTimeToStr($reservationData['additional_data']['dates']['date']) !!}
                </li>
                <li><strong>Крайна
                        дата: </strong>{!! \App\Classes\GlobalFunctions::generateDateTimeToStr($reservationData['additional_data']['dates']['end_date']) !!}
                </li>
                <li><strong>Брой
                        нощувки: </strong>{!! $reservationData['additional_data']['dates']['duration_count'] !!}
                </li>
            </ul>
        </div>
        <div class="col-md-3 personal">
            <ul>
                <li><strong>Каюта: </strong>{!! $reservationData['additional_data']['cabin_name'] !!}</li>
                <li><strong>Брoй възрастни: </strong>{!! $reservationData['passengers'] !!}</li>
                @if(isset($reservationData['infoSubscription'][1] ))
                    <li><strong>Деца: </strong>да</li>
                @else
                    <li><strong>Деца: </strong>не</li>
                @endif
                @if(sizeof($reservationData['additional_data']['promo_packets_named'])>0)
                    <li><strong>Доплащания: </strong>
                        @foreach($reservationData['additional_data']['promo_packets_named'] as $onePromoPack)
                            {!! $onePromoPack['name_'.\App::getLocale()] !!}
                        @endforeach
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>
