<h2 class="main-tab-title">{!! $ship['name_'.\App::getLocale()] !!}</h2>
<div class="technical-specification">
    <h3>техническа спецификация<span></span></h3>
    <div class="row">
        <div class="col-md-4">
            <ul>
                <li><span>Брой пътници:</span> {!! $ship['people'] !!}</li>
                <li><span>Тонаж:</span> {!! $ship['tonnage'] !!} тона</li>
                <li><span>Брой екипаж:</span> {!! $ship['equipage_count'] !!}</li>
                <li><span>Ширина:</span> {!! $ship['width'] !!} метра</li>
                <li><span>Дължина: </span> {!! $ship['length'] !!} метра</li>
            </ul>
        </div>
        <div class="col-md-4">
            <ul>
                <li><span>Година на пускане на вода:</span> {!! $ship['first_start'] !!} година</li>
                <li><span>Скорост:</span> {!! $ship['speed'] !!} възела</li>
                <li><span>Флаг:</span> {!! $ship['flag'] !!}</li>
                <li><span>Електрическо напрежение:</span> {!! $ship['voltage'] !!}</li>
            </ul>
        </div>
    </div>
</div>
<div class="ship-description">
    <h3> {!! $ship['description_title_'.\App::getLocale()] !!}<span></span></h3>
    <p>{!! $ship['description_text_'.\App::getLocale()] !!}</p>
    <h3>{!! $ship['food_title_'.\App::getLocale()] !!}<span></span></h3>
    <p>{!! $ship['food_text_'.\App::getLocale()] !!}</p>

    <h3 style="margin-top:30px">{!! $ship['description_title_'.\App::getLocale()] !!}<span></span></h3>
    <div class="row">
        <div class="col-md-4">
            <p>{!! $ship['life_left_text_'.\App::getLocale()] !!}</p>

        </div>
        <div class="col-md-4">
            <p>{!! $ship['life_right_text_'.\App::getLocale()] !!}</p>
        </div>
    </div>
</div>
