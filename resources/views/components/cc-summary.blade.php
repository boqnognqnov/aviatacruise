<div class="cc-summary">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>{!! $title !!}</h2>
                <img title="{!! $pic_title !!}" alt="{!! $pic_alt !!}" class="cc-logo" src="{!! asset($picture) !!}"/>
                <p>{!! $description !!}</p>

            </div>
        </div>
    </div>
</div>
