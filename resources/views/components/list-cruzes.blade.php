<div id="cruises" class="background-white">
				<div class="row">
					<div class="col-md-12">
						<h2 class="main">Други круизи от това пристанище </h2>
					</div>
				</div>
				<div class="single-cruise">
					<div class="row">
						<div class="col-md-3">
							<img title="" alt="" class="img-responsive" src="img/gallery/best-2.jpg">
						</div>
						<div class="col-md-9">
							<div class="inner-text-wrapper">
								<div class="row">
									<div class="col-md-9">
										<h1>Великденски all inclusive круиз с транспорт до Атина</h1>
										<ul>
											<li class="hotel-name"><span>Компания: </span>Calestyal Olumpia *3</li>
											<li class="offer-location"><span></span>от Атина (Лаврион)</li>
											<li class="offer-dates"><span></span>28 / 04 / 2016</li>
										</ul>
									</div>
									<div class="col-md-3">
										<p class="offer-price">
											<span class="euro-sight"></span>
											349
										<span class="per-person">
											<span class="top-arrow"></span>
											на човек
										</span>
										</p>
									</div>
								</div>
								<p class="info">Круиз с организиран полет до началното пристанище Дубай и посещение на най-интересните дестинации в района на персийския и оманския заливи. На 25 март - полет София - Дубай и нощувка в хотел. Целият следващ ден ще е на ваше разположение и вечерта се качвате на борда на MSC Musica 4*. В петък - 1 април, морското пътешествие приключва и летите обратно за София</p>

							</div>
							<a title=""  href="#" class="view-more">Виж повече<span></span></a>
						</div>
					</div>
				</div>

				<div class="single-cruise">
					<div class="row">
						<div class="col-md-3">
							<img title="" alt="" class="img-responsive" src="img/gallery/best-2.jpg">
						</div>
						<div class="col-md-9">
							<div class="inner-text-wrapper">
								<div class="row">
									<div class="col-md-9">
										<h1>Великденски all inclusive круиз с транспорт до Атина</h1>
										<ul>
											<li class="hotel-name"><span>Компания: </span>Calestyal Olumpia *3</li>
											<li class="offer-location"><span></span>от Атина (Лаврион)</li>
											<li class="offer-dates"><span></span>28 / 04 / 2016</li>
										</ul>
									</div>
									<div class="col-md-3">
										<p class="offer-price">
											<span class="euro-sight"></span>
											349
										<span class="per-person">
											<span class="top-arrow"></span>
											на човек
										</span>
										</p>
									</div>
								</div>
								<p class="info">Круиз с организиран полет до началното пристанище Дубай и посещение на най-интересните дестинации в района на персийския и оманския заливи. На 25 март - полет София - Дубай и нощувка в хотел. Целият следващ ден ще е на ваше разположение и вечерта се качвате на борда на MSC Musica 4*. В петък - 1 април, морското пътешествие приключва и летите обратно за София</p>

							</div>
							<a title=""  href="#" class="view-more">Виж повече<span></span></a>
						</div>
					</div>
				</div>
				<div class="single-cruise">
					<div class="row">
						<div class="col-md-3">
							<img title="" alt="" class="img-responsive" src="img/gallery/best-2.jpg">
						</div>
						<div class="col-md-9">
							<div class="inner-text-wrapper">
								<div class="row">
									<div class="col-md-9">
										<h1>Великденски all inclusive круиз с транспорт до Атина</h1>
										<ul>
											<li class="hotel-name"><span>Компания: </span>Calestyal Olumpia *3</li>
											<li class="offer-location"><span></span>от Атина (Лаврион)</li>
											<li class="offer-dates"><span></span>28 / 04 / 2016</li>
										</ul>
									</div>
									<div class="col-md-3">
										<p class="offer-price">
											<span class="euro-sight"></span>
											349
										<span class="per-person">
											<span class="top-arrow"></span>
											на човек
										</span>
										</p>
									</div>
								</div>
								<p class="info">Круиз с организиран полет до началното пристанище Дубай и посещение на най-интересните дестинации в района на персийския и оманския заливи. На 25 март - полет София - Дубай и нощувка в хотел. Целият следващ ден ще е на ваше разположение и вечерта се качвате на борда на MSC Musica 4*. В петък - 1 април, морското пътешествие приключва и летите обратно за София</p>

							</div>
							<a title=""  href="#" class="view-more">Виж повече<span></span></a>
						</div>
					</div>
				</div>
			</div>
