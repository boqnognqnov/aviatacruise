<div id="news-homepage">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div id="homepage-news-slider">
                    <div class="swiper-wrapper">
                        @foreach($favNews as $oneNews)
                            <div class="swiper-slide clearfix">
                                <a title="" href="{!! url('news-inner/'.$oneNews['slug']) !!}">
                                    <h5>{!! $oneNews['title'] !!}</h5>
                                    <p class="post-summary">{!! $oneNews['date'] !!} / aviata cruise</p>
                                    <img title="" alt="" class="post-img"
                                         src="{!! asset(\App\NewsEntity::$path.$oneNews['picture']) !!}"/>
                                    <div class="post-content-wrapper">
                                        <div class="post-content">
                                            <div class="sub_title">
                                                <p>{!! $oneNews['subtitle'] !!}</p>
                                            </div>
                                            <p>{!! $oneNews['text'] !!}</p>
                                        </div>
                                        <span class="veiw-more">Прочети цялата статия<span></span></span>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="news-pagination"></div>
            </div>
            <div class="col-md-6">
                <div class="mail-news">
                    <h5><span></span>E-mail бюлетин</h5>
                    <p>
                        запишете се в ежеседмичния ни бюлетин
                        и получавайте актуална информация
                        <span>за най-изгодните ни оферти!</span>
                    </p>

                    {!! Form::open(array('method'=>'post','class'=>'','action'=>'ACEmailToSubscriptionController@emailSubscription')) !!}
                    {{ Form::text('email_to_subscription', null , [ "id"=>"enter-mail-addr", "placeholder"=>"Въведете E-mail адрес..." ]) }}
                    <input class="submit" id="add-to-mail-list" value="ЗАПИСВАНЕ" type="submit">
                    {!! Form::close() !!}

                    {{--{!! Form::open(array('method'=>'post','class'=>'')) !!}--}}
                    {{--{{ Form::text('name', '', [ "id"=>"enter-mail-addr", "placeholder"=>"Въведете E-mail адрес..." ]) }}--}}
                    {{--<input type="submit" id="add-to-mail-list" value="ЗАПИСВАНЕ">--}}
                    {{--</form>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var swiper3 = new Swiper('#homepage-news-slider', {
            pagination: '.news-pagination',
            slidesPerView: 1,
            paginationClickable: true,
            spaceBetween: 0,
            freeMode: false
        });
    });
</script>
