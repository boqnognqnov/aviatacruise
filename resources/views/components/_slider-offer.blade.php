<link href="{!! asset('css/flexslider.min.css') !!}" rel="stylesheet">
<script src="{!! asset('js/jquery.flexslider.min.js') !!}"></script>
<div style="position:relative">
    <div id="slider-offer" class="flexslider">
        <div class="course-info-wrapper container">
            <div class="course-info">
                <h1>
                    8 дни Обединени Арабски Емирства и Оман с полет до Дубай
                </h1>
                <ul>
                    <li><img title="" alt="" src="{!! asset('img/icons/icons-slider/pin.png') !!}"/>от Атина (Лаврион)</li>
                    <li><img title="" alt="" src="{!! asset('img/icons/icons-slider/calendar.png') !!}"/>28 / 04 / 2016</li>
                </ul>
                <ul>
                    <li><img title="" alt="" src="{!! asset('img/icons/icons-slider/logo.png') !!}"/>/ компания MSC Cruises /</li>
                    <li><img title="" alt="" src="{!! asset('img/icons/icons-slider/logo-2.png') !!}"/>MSC Musica - 4* / кораб /</li>
                </ul>
            </div>
        </div>
        <div class="gradient-slider"></div>
        <ul class="slides">
            @foreach($gallery as $onePic)
                <li style="background-image:url({!! asset($onePic) !!})"></li>
            @endforeach

        </ul>
    </div>
    <div class="container">
        <div class="row" style="position:relative">
            <div id="slider-offer-thumbnails" class="flexslider">
                <ul class="slides">
                    @foreach($gallery as $onePic)
                        <li><img title="" alt="" src="{!! asset ($onePic) !!}"/></li>
                    @endforeach

                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#slider-offer-thumbnails').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            asNavFor: '#slider-offer',
            itemWidth: 120
        });

        $('#slider-offer').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#slider-offer-thumbnails"
        });

    });
</script>
