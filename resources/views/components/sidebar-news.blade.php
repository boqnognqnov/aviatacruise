<div class="sidebar-news">
    <h4><span>//</span>Категории <span>//</span></h4>
    <ul class="news-cat">
        @foreach($categories as $oneCategory)
            <li>
                <a title=""  href="{!! url('/news/category/'.$oneCategory['slug_'.\App::getLocale()]) !!}"><span></span>{!! $oneCategory['name_'.\App::getLocale()] !!}
                </a></li>
        @endforeach

    </ul>
    {{--{!! url('/news/archive/12-25-2016') !!}--}}
    <h4><span>//</span>Архив<span>//</span></h4>
    <ul class="news-arhive">
        @foreach($archive as $year=>$oneYearArr)

            <li><span></span>{!! $year !!}
                <div>
                    <ul>
                        @foreach($oneYearArr as $monthNum=>$monthName)
                            <li>
                                <a title=""  href="{!! url('/news/archive/'.$year.'-'.$monthNum.'-01') !!}"><span></span>{!! $monthName !!}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </li>
        @endforeach

    </ul>
</div>
<script>
    $(document).ready(function () {

        $('.news-arhive>li').on('click', function (e) {
//            e.preventDefault();
            var $this = $(this).find('ul');
            $('.news-arhive>li ul').not($this).hide(200);
            $this.toggle(200);

        });
    });
</script>
