<div class="footer-promotion">
    <div class="container">
        <div class="row">
            @foreach($discountRoutes as $oneRoute)
                <div class="col-md-4">
                    <a title="" href="{!! url('offer/'.$oneRoute['route_slug']) !!}"
                       class="single-promotion-wrapper clearfix">
                        <div class="clearfix">
                            <h5>{!! $oneRoute['route_name'] !!}</h5>
                        </div>
                        <div class="img-wrapper">
                            <div class="top-label-img"><span>{!! $oneRoute['route_discount'] !!}% </span>
                                <span>отстъпка</span>
                            </div>
                            <div class="gradient"></div>
                            <div class="image">
                                <img class="img-responsive" alt="{!! $oneRoute['pic_alt'] !!}"
                                     title="{!! $oneRoute['pic_title'] !!}"
                                     src=" {!! asset(\App\RouteEntity::$path.$oneRoute['route_picture']) !!}">
                            </div>
                            <ul>
                                <li class="offer-dates">
                                    <span></span>{!! $oneRoute['route_date'] !!}
                                </li>
                                <li class="offer-location">
                                    <span></span>от {!! $oneRoute['first_point_name'].' ('.$oneRoute['first_point_country'].')' !!}
                                </li>
                                <li class="price">{!! $oneRoute['room_price'] !!}<span></span></li>
                            </ul>
                        </div>
                    </a>
                </div>

            @endforeach

        </div>
    </div>
</div>
