<div id="choose-vacation">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <p class="choose-vacation-label">Изберете круиз</p>
            </div>
        </div>
    </div>
    {!! Form::open(array('method'=>'get','action'=>'SearchController@search','class'=>'')) !!}
    <div class="container">
        <div class="select-wrapper">
            <div class="row">
                <div class="col-md-3">

                    <?php  $array = [0 => 'Тип круиз'];?>
                    {!!  Form::select('route_type' , $array, null, ['class'=>'selectpicker','id'=>'search_route_type' ]) !!}
                    <?php  $array = [0 => 'Пристанище'] ?>
                    {!!  Form::select('portId' , $array, null, ['class'=>'selectpicker','id'=>'search_route_ports' ]) !!}
                </div>
                <div class="col-md-3">
                    <?php  $array = [0 => 'Продължителност'] ?>
                    {!!  Form::select('duration' , $array, null, ['class'=>'selectpicker' ,'id'=>'search_route_durations']) !!}
                    <?php  $array = [0 => 'Компания'] ?>
                    {!!  Form::select('companyId' , $array, null, ['class'=>'selectpicker','id'=>'search_route_companies' ]) !!}
                </div>
                <div class="col-md-3">
                    <?php  $array = [0 => 'Дестинация'] ?>
                    {!!  Form::select('destinationId' , $array, null, ['class'=>'selectpicker' ,'id'=>'search_route_destinations']) !!}
                    <?php  $array = [0 => 'Кораб'] ?>
                    {!!  Form::select('shipId' , $array, null, ['class'=>'selectpicker','id'=>'search_route_ships']) !!}
                </div>
                <div class="col-md-3">
                    {{ Form::text('start_date', '', ["placeholder"=>"дата на тръгване от:",'class'=>'datepicker datepicker-start' ])  }}
                    {{ Form::text('end_date', '', ["placeholder"=>"дата на тръгване до:",'class'=>'datepicker datepicker-end' ])  }}
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <input type="submit" id="search" value="Tърси"/>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<script>

    $(document).ready(function () {
//        $('select option:first-child').attr("disabled", "disabled");
        $('.datepicker-start').datepicker({
            dateFormat: 'dd-mm-yy',
            firstDay: 1
        });
        $('.datepicker-end').datepicker({
            dateFormat: 'dd-mm-yy',
            firstDay: 1
        });
    });
</script>
<link href="{!! asset('css/juqery-ui.css') !!}" rel="stylesheet">
<link href="{!! asset('css/bootstrap-select.min.css') !!}" rel="stylesheet">
<script src="{!! asset('js/bootstrap-select.min.js') !!}"></script>
<script src="{!! asset('js/functions/searchOperations.js') !!}"></script>
