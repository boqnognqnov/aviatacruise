<div class="social-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="social-links">
                    <li>СПОДЕЛИ:</li>
                    <li><a title="" class="fb">
                            <div id="fb-root"></div>
                            <!-- Your share button code -->
                            <div class="fb-share-button"

                                 data-href="{!! Request::fullUrl() !!}"
                                 data-layout="button_count">
                            </div>
                        </a></li>
                        <li>
                          <a href="https://twitter.com/share"  data-text="aaaa"  data-url="{!! Request::fullUrl() !!}"  class="twitter customer share tw"></a></li>                
                    <li><a title="" href="#" class="gp">
                      <!-- Place this tag in your head or just before your close body tag. -->
                      <div class="g-plus" data-action="share" data-annotation="none" data-href="{!! Request::fullUrl() !!}"></div>
                        </a></li>
                    <li><a title=""  href="mailto:someone@example.com?Subject=Hello%20again" target="_top" class="mail"></a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <a title=""  href="javascript:window.print()" class="print"> Принтирай</a>
             </div>
        </div>
    </div>
</div>
