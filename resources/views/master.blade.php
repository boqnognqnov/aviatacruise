@extends('layouts.default')
@section('body')
    <header>
        <div id="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <ul class="social-top">
                            <li><a title="" target="_blank" class="fb" href="#"></a></li>
                            <li><a title="" target="_blank" class="tw" href="#"></a></li>
                            <li><a title="" target="_blank" class="gp" href="#"></a></li>
                            <li><a title="" target="_blank" class="yt" href="#"></a></li>
                            <li><a title="" target="_blank" class="p" href="#"></a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
            </div>
        </div>
        <div class="logo-wrapper">
            <div class="container">
                <div class="col-md-2" itemscope itemtype="http://schema.org/Person">
                    <a title="" href="/" title="" id="logo"><img title="" alt="" itemprop="name" src="{!! asset('img/logo.png') !!}"/></a>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <div class="call-us clearfix">
                        <h2>
                            <span class="phone-icon"></span>
                            <span class="phone">0888 803619</span>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar yamm navbar-default main-nav-wrapper" role="navigation">
            <div class="container">
                <div class="navbar-header">

                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse" id="navbar-main">
                    <ul class="nav navbar-nav">
                        <li><a title="" href="/">начало</a></li>
                        <li><a title="" href="{!! url('destinations') !!}">Дестинации</a></li>
                        <li class="dropdown">
                            <a title="" href="{!! url('cruize-types') !!}" id="themes">Тип Круиз</a>
                            {{--<ul class="dropdown-menu" aria-labelledby="themes">--}}
                            {{--<li><a title="" href="#">1</a></li>--}}
                            {{--<li><a title="" href="#">2</a></li>--}}
                            {{--</ul>--}}
                        </li>
                        <li><a title="" href="{!! url('promotions') !!}">Промоции</a></li>
                        <li><a title="" href="{!! url('company-list') !!}">Компании</a></li>
                        <li><a title="" href="{!! url('ship-list') !!}">Кораби</a></li>
                        <li><a title="" href="{!! url('faq') !!}">Въпроси</a></li>
                        <li><a title="" href="{!! url('about-us') !!}">За нас</a></li>
                        <li><a title="" href="{!! url('contacts') !!}">Контакти</a></li>
                        <li><a title="" href="{!! url('news') !!}">Блог</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    @yield('content')
    <footer>
        <div class="container">
            <div class="row"  itemscope itemtype="http://schema.org/LocalBusiness">
                <div itemscope itemtype="http://schema.org/Person" class="col-md-2">
                    <img title="" alt="" itemprop="name" src="{!! asset('img/logo-footer.png') !!}"/>
                </div>
                <div class="col-md-3 col-md-offset-1">
                    <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="addr">
                        <span></span>
                        Адрес: гр. София, <br>
                        ул. Бачо Киро № 25 А, партер
                    </p>
                    <p  itemprop="email" class="email"><span></span>info@aviata.bg</p>
                </div>
                <div class="col-md-2 col-md-offset-1">
                    <p itemprop="telephone" class="phone">
                        <span></span>
                        02 4219599<br>
                        +359 888 803619
                    </p>
                    <p itemprop="faxNumber" class="fax">
                        <span></span>
                        02 / 421 964
                    </p>
                </div>
                <div class="col-md-3">
                    <p itemprop="openingHours"  class="working-time">
                        <span class="icon"></span>
                        Делник:<span class="yellow"> 09:00-18:00</span><br/>
                        Събота:<span class="yellow"> 09:00-14:30</span>
                    </p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12 anchor-wrapper">
                    <img title="" alt="" src="{!! asset('img/footer-ancor.png') !!}"/>
                </div>
            </div>
        </div>
        <div class="companies-logos">
            <div class="container">
                <ul>
                    <li><a title="" href="#" class="celestial"></a></li>
                    <li><a title="" href="#" class="msc"></a></li>
                    <li><a title="" href="#" class="norwegian"></a></li>
                    <li><a title="" href="#" class="rowal-caribian"></a></li>
                    <li><a title="" href="#" class="celebrity"></a></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <ul class="footer-menu">
                        <li><a title="" href="{!! url('destinations') !!}">Маршрути</a></li>
                        <li><a title="" href="{!! url('grid-list') !!}">Кораби</a></li>
                        <li><a title="" href="{!! url('company-list') !!}">Компании</a></li>
                        <li><a title="" href="{!! url('ports') !!}">Пристанища</a></li>
                        <li><a title="" href="{!! url('promotions') !!}">Промоции</a></li>
                        <li><a title="" href="{!! url('news') !!}">Блог</a></li>
                        <li><a title="" href="{!! url('about-us') !!}">За Нас</a></li>
                        <li><a title="" href="{!! url('faq') !!}">Въпроси</a></li>
                        <li><a title="" href="{!! url('contacts') !!}">Контакти</a></li>
                        <li><a title="" href="#" data-toggle="modal" data-target="#terms-condition-footer">Общи Условия</a></li>
                    </ul>
                </div>
                <div class="col-md-12 text-center">
                    <p class="copyright">&copy; 2016 Aviata Всички права запазени.</p>
                    <ul class="social-bottom">
                        <li><a title="" target="_blank" class="fb" href="#"></a></li>
                        <li><a title="" target="_blank" class="tw" href="#"></a></li>
                        <li><a title="" target="_blank" class="gp" href="#"></a></li>
                        <li><a title="" target="_blank" class="yt" href="#"></a></li>
                        <li><a title="" target="_blank" class="p" href="#"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="terms-condition-footer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{!! @$generalTerms['title_'.\App::getLocale()] !!}</h4>
                </div>
                <div class="modal-body" data-spy="scroll">
                    <p>{!! @$generalTerms['text_'.\App::getLocale()] !!}
                    </p>


                </div>
            </div>
        </div>
    </div>

    @yield('page-scripts')
@endsection
