@extends('master')

@section('plugin-assets-css')
    <link href="{!! asset('css/flexslider.min.css') !!}" rel="stylesheet">
@endsection

@section('plugin-assets-js')
    <script src="{!! asset('js/jquery.flexslider.min.js') !!}"></script>
@endsection

@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])

@section('content')
    <section>
        <div id="slider-home-page" class="flexslider">
            <!-- Additional required wrapper -->
            <ul class="slides">
                @foreach($slidersData as $oneSlider)
                    <li style="background-image:url({!! \App\PageIndexSliderEntity::$path.$oneSlider['picture'] !!})">
                        <div class="container container">
                            <h3>{!! $oneSlider['title_'.\App::getLocale()] !!}</h3>
                            <div class="promo">
                                <p>{!! $oneSlider['text_'.\App::getLocale()] !!}</p>
                            </div>
                            <a title="" class="veiw-more"
                               href="{!! $oneSlider['button1_url'] !!}">{!! $oneSlider['button1_text_'.\App::getLocale()] !!}</a>
                            <a title="" class="contact-us" href="{!! url('contacts') !!}">връзка с нас</a>
                        </div>
                    </li>
                @endforeach

            </ul>
        </div>
        </div>
        <div class="background-white companies-logos">
            <div class="container">
                <ul class="slides">
                    <li><a title="" href="#" class="celestial"></a></li>
                    <li><a title="" href="#" class="msc"></a></li>
                    <li><a title="" href="#" class="norwegian"></a></li>
                    <li><a title="" href="#" class="rowal-caribian"></a></li>
                    <li><a title="" href="#" class="celebrity"></a></li>
                </ul>
            </div>
        </div>
        @include('components.search')

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="top-offers-slider">
                        <p class="main-spacial-title">Горещи оферти <span></span></p>
                        <h2 class="main-spacial-subtitle">Възползвайте се от промоционалните цени</h2>
                        <div class="arrows-wrapper">
                            <div class="top-offers-button-prev"></div>
                            <div class="top-offers-button-next"></div>
                        </div>
                        <div class="swiper-wrapper">
                            @foreach($routesWithDiscount as $oneRoute)
                                <div class="swiper-slide">
                                    <a title="" href="{!! url('offer/'.$oneRoute['route_slug']) !!}"
                                       class="single-promotion-offer clearfix">
                                        <div class="top-label-img"><span>{!! $oneRoute['route_discount'] !!}% </span>
                                            <span>отстъпка</span>
                                        </div>
                                        <img title="{!! $oneRoute['pic_title'] !!}" alt="{!! $oneRoute['pic_alt'] !!}"
                                             class="img-responsive"
                                             src="{!! asset(\App\RouteEntity::$path.$oneRoute['route_picture']) !!}"/>
                                        <div class="inner-text-wrapper">
                                            <h3>{!! $oneRoute['route_name'] !!}</h3>
                                            <p class="hotel-name">{!! $oneRoute['ship_name'] !!}</p>
                                            <ul>
                                                <li class="offer-location">
                                                    <span></span>от {!! $oneRoute['first_point_name'].' ('.$oneRoute['first_point_country'].')' !!}
                                                </li>
                                                <li class="offer-dates"><span></span>{!! $oneRoute['route_date'] !!}
                                                </li>
                                            </ul>
                                            <p class="offer-price">
                                                <span class="euro-sight"></span>
                                                {!! $oneRoute['room_price'] !!}
                                                <span class="per-person">
													<span class="top-arrow"></span>
													на човек
												</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div id="why-us"
             style="background-image: url({!! asset(\App\PageAboutEntity::$pathContentImg.$aboutData['content_img_front']) !!})">
            <div class="container">
                <div class="col-md-5">
                    <h4>{!! $aboutData['title2_bg'] !!}<a title="" href="{!! url('about-us') !!}">научи
                            повече<span></span></a></h4>
                </div>
                <div class="col-md-7">
                    @foreach($benefitsData as $oneData)

                        <div class="why-us-single">
                            <span style="background-image: url({!! asset(\App\AboutTextEntity::$pathPreDefinedRows . $oneData['picture_front']) !!});"></span>
                            <h5>{!! $oneData['text1_' . \App::getLocale()] !!}</h5>
                            <p>{!! $oneData['text2_' . \App::getLocale()] !!}</p>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

        <div id="newest-offer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="main-spacial-title">Горещи оферти</p>
                        <h2 class="main-spacial-subtitle">Възползвайте се от промоционалните цени</h2>
                    </div>
                </div>
                @foreach($topRoutesGroups as $oneGroup)
                    <div class="row">
                        @foreach($oneGroup as $oneRoute)
                            <div class="col-md-6">
                                <a title="" href="{!! url('offer/'.$oneRoute['route_slug']) !!}"
                                   class="single-offer-grid clearfix">

                                    <img title="{!! $oneRoute['pic_title'] !!}" alt="{!! $oneRoute['pic_alt'] !!}"
                                         class="img-responsive"
                                         src="{!! asset(\App\RouteEntity::$path.$oneRoute['route_picture']) !!}"/>
                                    <div class="inner-text-wrapper">
                                        <h3>{!! $oneRoute['route_name'] !!}</h3>
                                        <p class="hotel-name">{!! $oneRoute['ship_name'] !!}</p>
                                        <ul>
                                            <li class="offer-location">
                                                <span></span>от {!! $oneRoute['first_point_name'].' ('.$oneRoute['first_point_country'].')' !!}
                                            </li>
                                            <li class="offer-dates"><span></span>{!! $oneRoute['route_date'] !!}</li>
                                        </ul>
                                        <p class="offer-price">
                                            <span class="euro-sight"></span>
                                            {!! $oneRoute['room_price'] !!}
                                            <span class="per-person">
											<span class="top-arrow"></span>
											на човек
										</span>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach

            </div>

        </div>
        <a title="" href="{!! url('top-cruises') !!}" class="load-more">виж всички<span></span></a>
        </div>

        @include('components.news-subscribe')

    </section>
@endsection
@section('page-scripts')
    <script src="js/scripts.js"></script>
    <script>
        $(document).ready(function () {
            $('#slider-home-page').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#slider-home-page-carousel"
            });

            var swiper2 = new Swiper('#top-offers-slider', {
                slidesPerView: 3,
                paginationClickable: true,
                spaceBetween: 30,
                nextButton: '.top-offers-button-prev',
                prevButton: '.top-offers-button-next',
                breakpoints: {
                    767: {
                        slidesPerView: 1,
                    }
                }
            });

            var swiper3 = new Swiper('#homepage-news-slider', {
                pagination: '.news-pagination',
                slidesPerView: 1,
                paginationClickable: true,
                spaceBetween: 0,
                freeMode: false
            });

        });
    </script>
@endsection
