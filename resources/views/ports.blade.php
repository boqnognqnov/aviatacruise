@extends('master')

@section('plugin-assets-css')
@endsection
@section('plugin-assets-js')
@endsection

@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="inner-nav">
                        <li><a title="" href="#">Начало</a></li>
                        <li>Пристанища</li>
                    </ul>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>


        <div class="container">
            <div id="ports">
                @foreach($ports as $onePort)
                    <div class="row">
                        <div class="col-md-12">

                            <h2>{!! $onePort['name_'.\App::getLocale()] !!}</h2>
                            <a title="" href="{!! url('/inner-port/'.$onePort['slug_'.\App::getLocale()]) !!}">
                                <div class="single-port clearfix">

                                    <img title="" alt="" src="{!! asset(\App\PointsEntity::$path.$onePort['picture']) !!}">

                                    <div class="text-wrapper">
                                        <p>
                                            {!! $onePort['description_'.\App::getLocale()] !!}
                                        </p>
                                        <p class="veiw-more">виж повече</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>

    </section>
    {{-- @include('components.social-share') --}}
    @include('components.footer-promotions')


@endsection
@section('page-scripts')
    <script src="js/scripts.js"></script>
    <script>
        $(document).ready(function () {


        });
    </script>
@endsection
