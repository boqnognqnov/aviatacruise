@extends('master')

@section('plugin-assets-css')
@endsection
@section('plugin-assets-js')

@endsection

@section('page-title', ' ')
@section('meta-description', ' ')


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="inner-nav">
                    @foreach($breadcrumps as $onecrump)
                        <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="background-white">
        <div class="container">
            <h2 class="choose-cabbin-title">изберете спецификации за каюта<span></span></h2>
            {!! Form::open(array('method'=>'get','action'=>'OfferController@chooseCabinAndPackets','id'=>'choose-cabbin-form')) !!}

            <div id="accordion-choose-cabbin">

                @foreach($availableCabinsByDate['data'] as $roomId=>$oneCabin)

                    <h3>
                        <div class="row oneCabinTitle{!! $roomId !!}">
                            <div class="col-xs-6">
                                <div class="radio selectRoomRadio radio-danger">
                                    @if($roomId==$selectedCabin)
                                        {!! Form::radio('cabinId',  $roomId, true, [ "id"=>"cabbin".$roomId ]) !!}
                                    @else
                                        {!! Form::radio('cabinId',  $roomId, null, [ "id"=>"cabbin".$roomId]) !!}
                                    @endif
                                    <label for='{!!"cabbin".$roomId !!}'>
                                        <h4> {!! $oneCabin['room_name'] !!}</h4>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <p class="price">обща цена:<span
                                            class="value"
                                            id="cabinTotalPrice{!! $roomId !!}">{!! $oneCabin['total_price'] !!}</span><span
                                            class="euro-sight"><i
                                                class="fa fa-eur"></i></span></p>
                            </div>
                        </div>
                    </h3>
                    <div>
                        <div class="row oneCabinRow" id="oneCabin{!! $roomId !!}_id">
                            <div class="col-md-4">
                                <div class="checkbox passengersRadio">
                                    {!! Form::checkbox('passengers', 'true', true, [ "id"=>"passengers".$roomId,'data_room_id'=>$roomId,'disabled'=>'disabled'  ]) !!}
                                    <label for="passengers{!! $roomId !!}">
                                        <p class="price-inner">2-ма възрастни<span
                                                    class="value onlyRoomClass">  {!! $oneCabin['total_price'] !!}</span><span
                                                    class="euro-sight"><i class="fa fa-eur"></i>за каюта</span></p>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                @foreach($additionalInfos as $oneInfoType)
                                    <div class="infoTypesClass checkbox">
                                        {!! Form::checkbox('infoSubscription['.$oneInfoType['id'].']', $oneInfoType['id'], null, [ "id"=>'oneInfoType'.$roomId.'_'.$oneInfoType['id'],'data_room_id'=>$roomId ]) !!}
                                        <label for="{!!'oneInfoType'.$roomId.'_'.$oneInfoType['id'] !!}">{!! $oneInfoType['name_'.\App::getLocale()] !!}</label>
                                    </div>
                                @endforeach

                            </div>
                            <div class="col-md-4">
                                @foreach($promoPackets as $onePacket)
                                    <div class="promoPackTypesClass checkbox">
                                        {{ Form::checkbox('promoPackets['.$onePacket['id'].']', $onePacket['id'], null, [ "id"=>'onePacket'.$roomId.'_'.$onePacket['id'],'data_room_id'=>$roomId]) }}
                                        <label for="{!!'onePacket'.$roomId.'_'.$onePacket['id'] !!}">
                                            <p class="price-inner">{!! $onePacket['name_'.\App::getLocale()] !!}
                                                <span class="value">+{!! $onePacket['price'] !!}</span>
                                                <span class="euro-sight"><i class="fa fa-eur"></i></span>
                                            </p>
                                        </label>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <div class="row">
                <div class="col-md-9">
                    <p class="choose-disclimer">
                        * Посочените цени не гарантират наличието на свободни места.
                        Вашата резервация се счита за валидна само след изпратено писмено потвърждение от наша
                        страна на посочения от вас email адрес.
                    </p>
                </div>
                <div class="col-md-3">
                    <a title="" class="submit-wrapper">
                        <div class="border"></div>
                        <input type="submit" class="submit"/>
                    </a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <div id="pricing" class="container">
            <div class="row">
                @include('components.tab-price')
            </div>
        </div>
        {{-- @include('components.social-share') --}}
        @include('components.footer-promotions')

        @endsection
        @section('page-scripts')
            <script src="{!! asset('js/scripts.js') !!}"></script>
            <script src="{!! asset('js/functions/reservationStep2.js') !!}"></script>

@endsection
