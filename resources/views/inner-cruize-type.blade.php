@extends('master')

@section('plugin-assets-css')
@endsection
@section('plugin-assets-js')
@endsection

@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="inner-nav">
                        @foreach($breadcrumps as $onecrump)
                            <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>

        @include('components.cc-summary',['title'=>$routeType['name_'.\App::getLocale()],'pic_title'=>$routeType['pic_title_'.\App::getLocale()],'pic_alt'=>$routeType['pic_alt_'.\App::getLocale()],'description'=>$routeType['text_'.\App::getLocale()],'picture'=>\App\RouteTypeEntity::$path.$routeType['picture']])
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="main-spacial-title">Дестинации</p>
                    <h2 class="main-spacial-subtitle">оферти за аляска и канада</h2>
                </div>
            </div>
        </div>
        <div class="container single-destination-grid-wrapper" id="containerWithRoutes">
            @foreach($routesOfThisType as $oneRouteGroup)
                <div class="row">
                    @foreach($oneRouteGroup as $oneRoute)
                        <div class="col-md-6">

                            <div class="single-offer-grid clearfix">
                                <a title="" href="{!! url('offer/'.$oneRoute['route_slug']) !!}">
                                    <img title="{!! $oneRoute['pic_title'] !!}" alt="{!! $oneRoute['pic_alt'] !!}"
                                         class="img-responsive"
                                         src="{!! asset(\App\RouteEntity::$path.$oneRoute['picture']) !!}">
                                    <div class="inner-text-wrapper">
                                        <h3>{!! $oneRoute['route_name'] !!}</h3>
                                        <p class="hotel-name">{!! $oneRoute['company_name'] !!}</p>
                                        <ul>
                                            <li class="offer-location">
                                                <span></span>от {!! @$oneRoute['first_point']['point_name'].' ('.@$oneRoute['first_point']['point_country'].')' !!}
                                            </li>
                                            <li class="offer-dates">
                                                {{--<span></span>{!! \App\Classes\GlobalFunctions::generateDateTimeToStr($oneRoute['room_price_and_date']['route_date']) !!}--}}
                                                <span></span>{!! $oneRoute['room_price_and_date']['route_date'] !!}
                                            </li>
                                            <li>
                                                <span class="veiw-more">Виж повече<span></span></span></li>
                                        </ul>
                                        <p class="offer-price">
                                            <span class="euro-sight"></span>
                                            {!! $oneRoute['room_price_and_date']['price'] !!}
                                            <span class="per-person">
										<span class="top-arrow"></span>
										на човек
									</span>
                                        </p>
                                    </div>
                            </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
        <input type="hidden" id="routeOnTypeCountData" data_routes_in_this_type_count="{!! $routesOnThisTypeCount !!}"
               data_route_type_slug="{!! $routeTypeSlug !!}"
               value="criterion_data">

        <a title="" href="#" class="load-more" id="loadMoreOnThisType">зареди още<span></span></a>
    </section>
    @include('components.footer-destination')


@endsection
@section('page-scripts')
    <script src="{!! asset('js/scripts.js') !!}"></script>
    <script src="{!! asset('js/functions/routeOnThisTypeOperations.js') !!}"></script>

@endsection
