<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Article">
<head>
    <meta charset="UTF-8">
    <meta property="og:title" content="@yield('fb-title')"/>
    <meta property="og:image" content="@yield('fb-image')"/>
    <meta property="og:description" content="@yield('fb-description')"/>

  <meta itemprop="name" content="@yield('fb-title')"/>
  <meta itemprop="description" content="@yield('fb-description')"/>
  <meta itemprop="image" content="@yield('fb-image')"/>


    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>@yield('page-title')</title>
    <meta name = "format-detection" content = "telephone=no"/>
    <meta name="description" content="@yield('meta-description')"/>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" >
    <link href="{!! asset('css/bootstrap.css') !!}" rel='stylesheet' type='text/css'>
    <link href="{!! asset('css/font-awesome/css/font-awesome.css') !!}" rel='stylesheet' type='text/css'>
    <script src="https://apis.google.com/js/platform.js" async defer>
      {lang: 'bg'}
    </script>
    <link href='https://fonts.googleapis.com/css?family=Yeseva+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic,latin-ext,cyrillic-ext,vietnamese,greek-ext,greek'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,100italic,300italic,400italic,500,500italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic'
          rel='stylesheet' type='text/css'>

    <link href="{!! asset('css/swiper.min.css') !!}" rel='stylesheet' type='text/css'>
    @yield('plugin-assets-css')


    <link href="{!! asset('css/jQuery.MagnifierRentgen.min.css') !!}" rel='stylesheet' type='text/css'>

    <script src="https://apis.google.com/js/platform.js" async defer></script>


    <script src="{!! asset('js/jquery.js') !!}"></script>
    <script src="{!! asset('js/jquery-ui.min.js') !!}"></script>
    <script src="{!! asset('js/bootstrap.js') !!}"></script>
    <script src="{!! asset('js/swiper.jquery.min.js') !!}"></script>

    @yield('plugin-assets-js')


    <script src="{!! asset('js/jQuery.MagnifierRentgen.min.js') !!}"></script>

    <input type="hidden" name="jsToken" value="{{ csrf_token() }}">


</head>
<body>
<div id="fb-root"></div>
<script>
(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    $( document ).ready(function() {
    $('.tw').click(function(event) {
       var width  = 575,
       height = 400,
       left   = ($(window).width()  - width)  / 2,
       top    = ($(window).height() - height) / 2,
       url    = this.href,
       opts   = 'status=1' +
                ',width='  + width  +
                ',height=' + height +
                ',top='    + top    +
                ',left='   + left;
       window.open(url, 'twitter', opts);
       return false;
   });
   });
    </script>

@yield('body')
</body>
<script src="{!! asset('js/scripts.js') !!}"></script>
<link href="{!! asset('css/custom.css') !!}" rel='stylesheet' type='text/css'>
</html>
