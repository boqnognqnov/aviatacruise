@extends('master')

@section('plugin-assets-css')
    <link href="{!! asset('css/swiper.min.css') !!}" rel='stylesheet' type='text/css'>
    <link href="{!! asset('css/bootstrap-select.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/flexslider.min.css') !!}" rel="stylesheet">
@endsection
@section('plugin-assets-js')
    <script src="{!! asset('js/bootstrap-select.min.js') !!}"></script>
    <script src="{!! asset('js/swiper.jquery.min.js') !!}"></script>
    <script src="{!! asset('js/jquery-ui.min.js') !!}"></script>
    <script src="{!! asset('js/jquery.flexslider.min.js') !!}"></script>

@endsection

@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])

@section('fb-title', $news['title_'.\App::getLocale()])
{{--@section('fb-image', asset(@$gallery[0]))--}}
@section('fb-image', asset(\App\NewsEntity::$path.@$news['picture']))
@section('fb-description',  $news['text_'.\App::getLocale()] )

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="inner-nav">
                    @foreach($breadcrumps as $onecrump)
                        <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6">

            </div>
        </div>
    </div>
    <div id="news-inner">
        <div class="background-white">
            <div class="container">
                <ul class="news-summary">
                    <li class="date"><span></span>18.02.2016</li>
                    <li class="category"><span></span>Пътуване</li>
                    <li class="company"><span></span>Авиата Травел</li>
                </ul>
                <h2 class="main-spacial-subtitle">{!! $news['title_'.\App::getLocale()] !!}</h2>
            </div>
        </div>
        <div class="container">
            <div class="col-md-9">
                <div id="slider-inner-news" class="flexslider">
                    <ul class="slides">
                        @foreach($gallery as $oneImg)
                            <li><img title="{!! $oneImg['pic_title'] !!}" alt="{!! $oneImg['pic_alt'] !!}"
                                     src="{!! asset($oneImg['picture']) !!}"></li>

                        @endforeach

                    </ul>
                </div>
                <div class="background-white">
                    <div id="carousel-inner-news" class="flexslider">
                        <ul class="slides">
                            @foreach($gallery as $oneImg)
                                <li>
                                    <div class="darker"></div>
                                    <img title="{!! $oneImg['pic_title'] !!}" alt="{!! $oneImg['pic_alt'] !!}"
                                         src="{!! asset($oneImg['picture']) !!}">
                                </li>
                            @endforeach

                        </ul>
                    </div>

                    <div class="news-inner-text">
                        <p>
                        {!! $news['sub_title_'.\App::getLocale()] !!}
                        <p>
                        <p>
                        {!! $news['text_'.\App::getLocale()] !!}
                        <p>
                        <div class="social-section">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="social-links">
                                        <li>СПОДЕЛИ:</li>
                                        <li><a title="" class="fb">
                                                <div id="fb-root"></div>
                                                <!-- Your share button code -->
                                                <div class="fb-share-button"

                                                     data-href="{!! Request::fullUrl() !!}"
                                                     data-layout="button_count">
                                                </div>
                                            </a></li>
                                        <li>
                                            <a href="https://twitter.com/share" data-text="aaaa"
                                               data-url="{!! Request::fullUrl() !!}"
                                               class="twitter customer share tw"></a></li>
                                        <li><a title="" href="#" class="gp">
                                                <!-- Place this tag in your head or just before your close body tag. -->
                                                <div class="g-plus" data-action="share" data-annotation="none"
                                                     data-href="{!! Request::fullUrl() !!}"></div>
                                            </a></li>
                                        <li><a title="" href="mailto:someone@example.com?Subject=Hello%20again"
                                               target="_top"
                                               class="mail"></a></li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <a title="" href="javascript:window.print()" class="print"> Принтирай</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                @include('components.sidebar-news',$sideBarData)
            </div>
        </div>
    </div>
    @include('components.footer-promotions')


@endsection
@section('page-scripts')

    <script>
        $(document).ready(function () {
            $('#carousel-inner-news').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 170,
                itemMargin: 20,
                asNavFor: '#slider-inner-news'
            });

            $('#slider-inner-news').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#carousel-inner-news"
            });
        });
    </script>
@endsection
