@extends('master')

@section('plugin-assets-css')
    <link href="{!! asset('css/bootstrap-select.min.css') !!}" rel="stylesheet">
@endsection
@section('plugin-assets-js')
    <script src="{!! asset('js/bootstrap-select.min.js') !!}"></script>
@endsection

@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])

@section('content')
    <section>

        <div id="googleMap" style="height:500px;"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="inner-nav">
                        @foreach($breadcrumps as $onecrump)
                            <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-6">

                </div>
            </div>
        </div>
        <div id="contact-us-page" class="background-white">
            <div class="container">
                <div class="row text-center">
                    <h1>aviata travel ltd.</h1>
                    <h3>Свържете се с нас</h3>
                    <img title="" alt="" class="ancor" src="{!! asset('img/icons/contact-us-ancor.png') !!}">
                </div>
                <div class="row" itemscope itemtype="http://schema.org/Person">
                    <div class="col-md-4">
                        <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="addr">
                            <span></span>
                            {{--Адрес: гр. София, <br>--}}
                            {{--ул. Бачо Киро № 25 А, партер--}}
                            Адрес: {!! $record['address'] !!}
                        </p>
                        <p itemprop="email" class="email"><span></span>
                            {!! $record['mails'] !!}
                        </p>
                    </div>
                    <div class="col-md-2 col-md-offset-1">
                        <p itemprop="telephone" class="phone">
                            <span></span>
                            {!! $record['phones'] !!}
                        </p>
                        <p itemprop="faxNumber" class="fax">
                            <span></span>
                            {!! $record['fax'] !!}
                        </p>
                    </div>
                    <div class="col-md-3 col-md-offset-2">
                        <p itemprop="openingHours" class="working-time">
                            <span class="icon"></span>
                            {{--Делник:<span class="yellow"> 09:00-18:00</span><br>--}}
                            {{--Събота:<span class="yellow"> 09:00-14:30</span>--}}
                            {!! $record['work_time'] !!}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7 col-md-offset-2">
                        <div class="row" id="status_message">
                            <div class="col-md-12">

                                @if(Session::has('success'))
                                    <div class="alert alert-success">
                                        {!! Session::get('success') !!}
                                    </div>
                                @endif

                                @if($errors -> any() )
                                    <div class="alert alert-danger" style="width: 100%;">
                                        @foreach ($errors->all() as $error)
                                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                        <h4>Имате въпроси за круиз, чиито отговор не намерихте?</h4>
                        {!! Form::open(array('method'=>'post','id'=>'contact-us-form','action'=>'ContactsController@sendMail')) !!}
                        {{ Form::text('name', '', ["placeholder"=>"Вашето Име:" ])  }}
                        {{ Form::email('email', '', ["placeholder"=>"Вашият E-mail:" ]) }}
                        {{ Form::textarea('text', '', [ "id"=>"text-comment", "placeholder"=>"Текст на Вашето Съобщение..." ]) }}
                        <div class="capcha">
                            <input type="submit" value="изпрати" class="submit"/>
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
        @include('components.footer-promotions')
    </section>




@endsection
@section('page-scripts')
    <input type="hidden" name="coord_x" value="{!! @$coordinatesArr[0] !!}">
    <input type="hidden" name="coord_y" value="{!! @$coordinatesArr[1] !!}">
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
        function initialize() {

            var coordX = parseFloat($('input[name="coord_x"]').val());
            var coordY = parseFloat($('input[name="coord_y"]').val());

            var myLatLng = {lat: coordX, lng: coordY};
            var mapProp = {
                center: myLatLng,
                zoom: 18,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);


            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
        $(document).ready(function () {


        });
    </script>
@endsection
