@extends('master')

@section('plugin-assets-css')
@endsection
@section('plugin-assets-js')
@endsection


@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="inner-nav">
                        @foreach($breadcrumps as $onecrump)
                            <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                        @endforeach

                    </ul>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
        <div class="background-white main-destination-wrapper">
            <div class="container">
                <p class="main-spacial-title">Дестинации</p>
                <h2 class="main-spacial-subtitle">Богат избор от круизни дестинации </h2>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 destGroups">
                        @foreach($results as $oneResultGroup)
                            <div class="row">
                                @foreach($oneResultGroup as $oneDestination)
                                    <a title="" href="{!! url('/inner-destinations/'.$oneDestination['dest_slug']) !!}">
                                        <div class="col-md-6 oneDestRec">
                                            <div class="single-destination-main">
                                                <img title="{!! $oneDestination['pic_title'] !!}"
                                                     alt="{!! $oneDestination['pic_alt'] !!}" class="img-responsive"
                                                     src="{!! asset(\App\DestinationEntity::$path.$oneDestination['picture']) !!}">

                                                <h4>{!! $oneDestination['dest_name'] !!}</h4>
                                                <p>{!! $oneDestination['dest_description'] !!} </p>
                                                <p class="view-more">Виж повече<span></span></p>
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                        @endforeach

                    </div>
                    <div class="col-md-3 col-md-offset-1">
                        <h3 class="pouplar-ports-title">Най-популярни пристанища</h3>
                        <ul class="popular-ports">
                            @foreach($ports as $onePort)
                                <li><a title="" class="clearfix"
                                       href="{!! url('/inner-port/'.$onePort['port_slug']) !!}"><img
                                                title="{!! $onePort['pic_title'] !!}" alt="{!! $onePort['pic_alt'] !!}"
                                                src="{!! asset(\App\PointsEntity::$path.$onePort['picture']) !!}">
                                        <h4>{!! $onePort['port_name'] !!}</h4></a></li>
                            @endforeach
                        </ul>
                        <a title="" href="{!! url('ports') !!}" class="load-more-sidebar">виж всички<span></span></a>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="destinationsCountData" data_destinations_count="{!! $destinationsCount !!}"
               value="criterion_data">

        <div class="container">
            <a title="" href="#" class="load-more" id="load_more_destinations">зареди още<span></span></a>
        </div>
    </section>
    @include('components.footer-promotions')

@endsection
@section('page-scripts')
    <script src="js/scripts.js"></script>
    <script src="{!! asset('js/functions/moreDestinationsOperations.js') !!}"></script>
@endsection
