@extends('master')

@section('plugin-assets-css')
	<link href="{!! asset('css/swiper.min.css') !!}" rel='stylesheet' type='text/css'>
	<link href="{!! asset('css/bootstrap-select.min.css') !!}" rel="stylesheet">
	<link href="{!! asset('css/flexslider.min.css') !!}"  rel="stylesheet">
@endsection
@section('plugin-assets-js')
	<script src="{!! asset('js/bootstrap-select.min.js') !!}"></script>
	<script src="{!! asset('js/swiper.jquery.min.js') !!}" ></script>
	<script src="{!! asset('js/jquery-ui.min.js') !!}"></script>
	<script src="{!! asset('js/jquery.flexslider.min.js') !!}"></script>

@endsection



@section('page-title', 'Home')

@section('content')


@include('components.news-subscribe')


@endsection
@section('page-scripts')
	<script src="js/scripts.js"></script>
    <script>
        $(document).ready(function () {


        });
    </script>
@endsection
