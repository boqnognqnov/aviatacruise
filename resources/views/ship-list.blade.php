@extends('master')

@section('plugin-assets-css')
@endsection
@section('plugin-assets-js')
@endsection


@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="inner-nav">
                    @foreach($breadcrumps as $onecrump)
                        <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="ships-list-grid">
        <div class="background-white">
            <div class="container">
                <p class="main-spacial-title">Типове Круизи</p>
                <h2 class="main-spacial-subtitle">Възползвайте се от различните варианти за круизи
                    <div class="swicher-wrapper">
                        <button class="swich-to-grid"><i class="fa fa-th"></i></button>
                        <button class="swich-to-list active"><i class="fa fa-list-ul"></i></button>

                    </div>
                </h2>

            </div>
            <div class="ship-list">
                @foreach($list as $oneGroup)
                    <div class="container ships_couple">
                        @foreach($oneGroup as $oneShip)
                            <a title="" href="{!! url('ship/'.$oneShip['slug_'.\App::getLocale()]) !!}">
                                <div class="single-ship">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <img title="{!! $oneShip['pic_title_'.\App::getLocale()] !!}"
                                                 alt="{!! $oneShip['pic_alt_'.\App::getLocale()] !!}"
                                                 class="img-responsive"
                                                 src="{!! asset(\App\ShipEntity::$path.$oneShip['picture']) !!}"/>
                                        </div>
                                        <div class="col-md-6">
                                            <h3>{!! $oneShip['name_'.\App::getLocale()] !!}</h3>
                                            <p>
                                                {!! $oneShip['description_text_'.\App::getLocale()] !!}
                                            </p>
                                            <p class="view-more">Виж повече<span></span></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
            </div>
            @endforeach
        </div>

    </div>

    <input type="hidden" id="shipCountData" data_ships_count="{!! $shipsCount !!}" value="criterion_data">

    <div class="container">
        <a title="" href="#" class="load-more" id="moreShipsLink">зареди още<span></span></a>
    </div>
    @include('components.footer-promotions',['discountRoutes'=>$discountRoutes])
@endsection
@section('page-scripts')

    <script src="{!! asset('js/functions/shipListOperations.js') !!}"></script>
@endsection
