@extends('master')

@section('plugin-assets-css')
@endsection

@section('plugin-assets-js')

@endsection


@section('page-title', @$seoData['title'])
@section('meta-description', @$seoData['description'])
{{--  --}}
@section('fb-title', $ship['name_'.\App::getLocale()])
@section('fb-image',  asset(\App\ShipEntity::$path.$ship['picture']))
@section('fb-description', $ship['description_title_'.\App::getLocale()].' '.$ship['description_text_'.\App::getLocale()] )

@section('content')

    @include('components.slider-ship',['gallery'=>$gallery,'ship'=>$ship,'company'=>$company])
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="inner-nav">
                        @foreach($breadcrumps as $onecrump)
                            <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>


        <div class="container">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs special" role="tablist">
                <li role="presentation" class="active"><a title="" href="#ship" aria-controls="ship" role="tab"
                                                          data-toggle="tab"
                                                          class="ship"><span></span>кораб</a></li>
                <li role="presentation"><a title="" href="#cabins" aria-controls="cabins" role="tab" data-toggle="tab"
                                           class="cabins"><span></span>каюти</a></li>
                <li role="presentation"><a title="" href="#deck" aria-controls="deck" role="tab" data-toggle="tab"
                                           class="deck"><span></span>палуби</a></li>
                <li role="presentation"><a title="" href="#cruises" aria-controls="cruises" role="tab" data-toggle="tab"
                                           class="cruises"><span></span>Круизи</a></li>
                <li role="presentation"><a title="" href="#terms-condition" aria-controls="terms-condition" role="tab"
                                           data-toggle="tab" class="terms-condition"><span></span>общи условия</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="ship">
                    @include('components.tab-ship',['ship'=>$ship])
                </div>

                <div role="tabpanel" class="tab-pane" id="cabins">
                    @include('components.tab-cabbins',['cabins'=>$cabins])
                </div>

                <div role="tabpanel" class="tab-pane" id="deck">
                    @include('components.tab-decks',[['decs'=>$decs]])
                </div>

                <div role="tabpanel" class="tab-pane" id="cruises">
                    @include('components.tab-cruiz',['routes'=>$routesWithThisShip])
                </div>


                <div role="tabpanel" class="tab-pane" id="terms-condition">
                    @include('components.tab-terms',['company'=>$company])
                </div>
            </div>
        </div>

    </section>
    @include('components.social-share')
    @include('components.footer-promotions',['discountRoutes'=>$discountRoutes])


@endsection
@section('page-scripts')
    <script src="js/scripts.js"></script>
    <script>
        $(document).ready(function () {


        });
    </script>
@endsection
