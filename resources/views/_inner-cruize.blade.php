@extends('master')

@section('plugin-assets-css')

@endsection
@section('plugin-assets-js')

@endsection



@section('page-title', 'Home')
@section('fb-title', $route['route_name'])
@section('fb-title', asset(\App\RouteEntity::$path.$route['picture']))

@section('content')
<section>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<ul class="inner-nav">
					<li><a href="#">Начало</a></li>
					<li><a href="companies.html">Компании</a></li><li><a href="ports.html">Пристанища</a></li>
					<li><a href="#">Cruises</a></li>
				</ul>
			</div>
			<div class="col-md-6">
			</div>
		</div>
	</div>

    @include('components.cc-summary')
	<div class="container hide-for-print">
		<div id="cruises" class="background-white">
			<div class="row">
				<div class="col-md-12">
					<h2 class="main">Други круизи от този тип </h2>
				</div>
			</div>
			@include('components.tab-cruiz')
		</div>
	</div>
		@include('components.social-share')
</section>
@include('components.footer-promotions')


@endsection
@section('page-scripts')
	<script src="js/scripts.js"></script>
    <script>
        $(document).ready(function () {


        });
    </script>
@endsection
