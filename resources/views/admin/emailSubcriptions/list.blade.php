@extends( 'admin.master' )
@section( 'content' )

    <!-- DataTables CSS -->

    <link rel="stylesheet" type="text/css"
          href="{!! asset('panel/plugins/dataTables/css/jquery.dataTables.min.css') !!}">

    <script type="text/javascript" charset="utf8"
            src="{!! asset('panel/plugins/dataTables/js/jquery.dataTables.min.js') !!}"></script>

    <script type="text/javascript" charset="utf8"
            src="{!! asset('panel/plugins/dataTables/js/dataTables.bootstrap.min.js') !!}"></script>

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {!! Session::get('success') !!}
                        </div>
                    @endif

                    @if($errors -> any() )
                        <div class="alert alert-danger" style="width: 100%;">
                            @foreach ($errors->all() as $error)
                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">


                            <div class="box-tools">
                                {{--<a href="{!! url('admin/news') !!}" class="btn btn-primary"--}}
                                {{--style="display: inline-block">Back to news page</a>--}}
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <div>
                                {!! Form::open(array('action'=>'ACEmailToSubscriptionController@sendMessageToAllUsers','method'=>'post','files'=>true)) !!}
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3 class="box-title">Ново съобщение:</h3>
                                        {!! Form::textarea( 'text',null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-6 text-right">
                                        {!! Form::submit('Изпрати',['class'=>'btn btn-success']) !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="box-title">Абонирани клиенти по имейл</h3>
                                    <div id="example_wrapper" class="dataTables_wrapper">
                                        <table id="example" class="display pageResize" cellspacing="0" width="100%">

                                            <thead>
                                            <th>ID</th>
                                            <th>Email</th>
                                            <th>Дата на абонамента</th>

                                            <th colspan="1"></th>

                                            </thead>
                                            <tbody>
                                            @foreach($emails as $email)
                                                <tr>
                                                    <td>{!! $email['id'] !!}</td>
                                                    <td>{!! $email['email'] !!}</td>

                                                    <td>
                                                        {!! $email['created_at']  !!}
                                                    </td>

                                                    <td>
                                                        <button class="btn btn-danger delEmail"
                                                                data-email-id="{!! $email['id'] !!}">Изтриване
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


    <div style="display: none">
        {!! Form::open(array('action'=>'ACEmailToSubscriptionController@adminDestroyEmail','method'=>'post','id'=>'formEmailDel')) !!}
        {!! Form::hidden('emailId') !!}
        {!! Form::close() !!}
    </div>


    <script>
        $('#example').DataTable(
                {
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                    "bFilter": true,
                    "bLengthChange": true
                }
        );
        $('#example').on('draw.dt', function () {
            defineDeleteFunction();

        });

        function defineDeleteFunction() {
            $('.delEmail').on('click', function (event) {

                var id = $(this).attr('data-email-id');
//            alert(id);

                $('#formEmailDel input[name="emailId"]').val(id);

                var r = confirm("Do you really want to delete this email?");
                if (r == true) {
                    $('#formEmailDel').submit();
                }
            });
        }

        $(document).ready(function () {
            defineDeleteFunction();

        });


    </script>


@endsection