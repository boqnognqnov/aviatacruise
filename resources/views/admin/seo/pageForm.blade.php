@extends( 'admin.master' )
@section( 'content' )

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script>
        $(function () {
//            $(".datepicker").datepicker();
            $("#datepicker, #datepicker2").datepicker();
        });


    </script>

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Редактиране SEO информация за страницата</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/seo/static-pages/list') !!}" class="btn btn-primary">Списък
                                    статични страници</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {!! Session::get('success') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('method'=>'post','action'=>'SeoController@setSeoDataOnPage','files'=>true)) !!}
                            {!! Form::hidden('page_id',@$record['id']) !!}

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 col-md-2 control-label">SEO заглавие и
                                            описание</label>

                                        <div class="col-sm-8 col-md-10">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#news_area_bg"
                                                                                          aria-controls="news_area_bg"
                                                                                          role="tab"
                                                                                          data-toggle="tab">BG</a>
                                                </li>


                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                                    {!! Form::text( 'seo_title_bg',@$record['seo_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {{--{!! Form::textarea( 'seo_description_bg',@$record['seo_description_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}--}}
                                                    {!! Form::textarea( 'seo_description_bg',@$record['seo_description_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-sm-6 text-right">
                                        {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}
                            <hr>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection