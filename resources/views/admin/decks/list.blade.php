@extends( 'admin.master' )
@section( 'content' )

        <!-- DataTables CSS -->

<link rel="stylesheet" type="text/css"
      href="{!! asset('panel/plugins/dataTables/css/jquery.dataTables.min.css') !!}">

<script type="text/javascript" charset="utf8"
        src="{!! asset('panel/plugins/dataTables/js/jquery.dataTables.min.js') !!}"></script>

<script type="text/javascript" charset="utf8"
        src="{!! asset('panel/plugins/dataTables/js/dataTables.bootstrap.min.js') !!}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {!! Session::get('success') !!}
                    </div>
                @endif

                @if($errors -> any() )
                    <div class="alert alert-danger" style="width: 100%;">
                        @foreach ($errors->all() as $error)
                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">Палуби</h3>

                        <div class="box-tools">
                            {{--<a href="{!! url('admin/news') !!}" class="btn btn-primary"--}}
                            {{--style="display: inline-block">Back to news page</a>--}}
                            {{--<a href="{!! url('/admin/news/category') !!}"--}}
                            {{--class="btn btn-bitbucket" style="display: inline-block">Categories</a>--}}
                            <a href="{!! url('admin/decks/create_update/' . $shipId . '/') !!}"
                               class="btn btn-warning" style="display: inline-block">Нова палуба</a>
                            {{--<a href="{!! url('admin/news/tagList') !!}"--}}
                            {{--class="btn btn-success" style="display: inline-block">Edit tag list</a>--}}
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="example_wrapper" class="dataTables_wrapper">
                            <table id="example" class="display pageResize" cellspacing="0" width="100%">

                                <thead>
                                <th>ID</th>
                                <th>Име</th>
                                <th>Схема</th>
                                <th>Каюти</th>
                                <th colspan="2"></th>

                                </thead>
                                <tbody>
                                @foreach($decks as $deck)
                                    <tr>
                                        <td>{!! $deck['id'] !!}</td>
                                        <td>{!! $deck['name_bg'] !!}</td>
                                        <td><img src="{!! asset(\App\DeckEntity::$path . $deck['schema']) !!}"
                                                 width="250px">
                                        </td>
                                        <td>
                                            <a href="{!! url('admin/rooms/list/' . $shipId . '/' . $deck['id']) !!}"
                                               class="btn btn-success">Каюти</a>
                                        </td>
                                        <td>
                                            <a href="{!! url('admin/decks/create_update/' . $shipId . '/' . $deck['id']) !!}"
                                               class="btn btn-warning">Промяна</a>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger delDeck"
                                                    data-deck-id="{!! $deck['id'] !!}">Изтриване
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<div style="display: none">
    {!! Form::open(array('action'=>'AdminDecksController@destroyDeck','method'=>'post','id'=>'formDeckDel')) !!}
    {!! Form::hidden('deckId') !!}
    {!! Form::close() !!}
</div>


<input type="hidden" name="temp_news_id" value="{!! Session::get('successSetTags') !!}">


<script>
//    $('#example').DataTable(
//            {
//                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
//                "bFilter": true,
//                "bLengthChange": true
//            }
//    );
//    $('#example').on('draw.dt', function () {
//        defineDeleteFunction();
//
//    });

    function defineDeleteFunction() {
        $('.delDeck').on('click', function (event) {

            var id = $(this).attr('data-deck-id');
//            alert(id);

            $('#formDeckDel input[name="deckId"]').val(id);

            var r = confirm("Do you really want to delete this deck?");
            if (r == true) {
                $('#formDeckDel').submit();
            }
        });
    }

    $(document).ready(function () {
        defineDeleteFunction();

    });


</script>


@endsection