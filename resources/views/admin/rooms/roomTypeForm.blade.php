@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Създаване/Редактиране на тип каюта</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/rooms/list/' . $shipId) !!}" class="btn btn-primary">Списък с
                                    тип каюти</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {!! Session::get('success') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'AdminRoomTypesController@createUpdateRoomType','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('room_id', @$selectedItem['id']) !!}
                            {!! Form::hidden('ship_id', @$shipId) !!}

                            <div class="form-group clearfix">
                                <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img
                                            src="{!! asset(\App\RoomTypeEntity::$path.@$selectedItem['picture']) !!}"
                                            class="img-responsive"
                                            style="display: block;">


                                </label>

                                <div class="col-sm-8 col-md-10">
                                <p class="btn-info img-size">моля прикачете изображение с размер <strong>400px на 235px</strong></p>
                                {!! Form::file('picture',['placeholder'=>'picture','class'=>'form-control','id'=>'headerIMG']) !!}
                                    @if($errors -> first('picture') != '')
                                        <span class="help-block">{!! $errors -> first('picture') !!}</span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="" class="col-sm-4 col-md-2 control-label">Име</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#news_area_bg"
                                                                                  aria-controls="news_area_bg"
                                                                                  role="tab"
                                                                                  data-toggle="tab">BG</a></li>

                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                            {{ Form::text( 'name_bg', @$selectedItem['name_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) }}
                                        </div>
                                    </div>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                            {{ Form::textarea( 'description_bg', @$selectedItem['description_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) }}
                                        </div>
                                    </div>
                                </div>
                                {{--<label for="" class="col-sm-4 col-md-2 control-label">Цена</label>--}}
                                {{--{{ Form::text( 'price', @$selectedItem['price'], array( 'class' => 'form-control', 'rows' => '6' ) ) }}--}}

                                <label for="" class="col-sm-4 col-md-2 control-label">Брой каюти от този тип</label>
                                {{ Form::text( 'room_count', @$selectedItem['room_count'], array( 'class' => 'form-control', 'rows' => '6' ) ) }}

                                <label for="" class="col-sm-4 col-md-2 control-label">Палуби</label>
                                {{ Form::select('deck_id', $selectedDecks, $checked) }}

                            </div>
                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}
                            <hr>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
