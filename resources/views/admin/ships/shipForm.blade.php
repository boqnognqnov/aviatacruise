@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Създаване/Редактиране на кораб</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/ships/list') !!}" class="btn btn-primary">Списък с кораби</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {!! Session::get('success') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}


                            {!! Form::open(array('action'=>'AdminShipsController@createUpdateShip','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('ship_id', @$selectedItem['id']) !!}

                            {!! Form::label( 'Компания:' ) !!}
                            {!! Form::select('company_id', $companies, @$selectedItem['company_id'],['class'=>'form-control']) !!}

                            {{--{!! Form::label( 'Дестинация:' ) !!}--}}
                            {{--{!! Form::select('destination_id', $destinations,@$selectedItem['destination_id'],['class'=>'form-control']) !!}--}}
                            <br>
                            <hr>


                            <div class="form-group clearfix">
                                <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img
                                            src="{!! asset(\App\ShipEntity::$path . @$selectedItem['picture']) !!}"
                                            class="img-responsive"
                                            style="display: block;">
                                    @if(!empty($selectedItem['picture']))
                                        <button data-toggle="modal" data-target="#imgModal"
                                                class="btn btn-xs btn-success btn-block seoButton">Задай SEO
                                        </button>
                                    @endif


                                </label>

                                <div class="col-sm-8 col-md-10">
                                    <p class="btn-info img-size">моля прикачете изображение с размер <strong>720px на 450px</strong></p>
                                    {!! Form::file('picture',['placeholder'=>'picture','class'=>'form-control','id'=>'headerIMG']) !!}
                                    @if($errors -> first('picture') != '')
                                        <span class="help-block">{!! $errors -> first('picture') !!}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">
                                        <label for="" class="col-sm-4 col-md-2 control-label">Маршрут: заглавие и
                                            текст</label>

                                        <div class="col-sm-8 col-md-10">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#news_area_bg"
                                                                                          aria-controls="news_area_bg"
                                                                                          role="tab"
                                                                                          data-toggle="tab">BG</a></li>


                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="news_area_bg">

                                                    {!! Form::text( 'name_bg', @$selectedItem['name_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}


                                                    <div class="col-md-1">
                                                        <label>Брой пътници:</label>
                                                    </div>
                                                    <div class="col-md-1">
                                                        {!! Form::text( 'people', @$selectedItem['people'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Брой екипаж:</label>
                                                    </div>
                                                    <div class="col-md-1">
                                                        {!! Form::text( 'equipage_count', @$selectedItem['equipage_count'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Флаг:</label>
                                                    </div>
                                                    <div class="col-md-1">
                                                        {!! Form::text( 'flag', @$selectedItem['flag'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Ширина:</label>
                                                    </div>
                                                    <div class="col-md-1">
                                                        {!! Form::text( 'width', @$selectedItem['width'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Дължина:</label>
                                                    </div>
                                                    <div class="col-md-1">
                                                        {!! Form::text( 'length', @$selectedItem['length'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    {{--дсадса--}}
                                                    <div class="col-md-1">
                                                        <label>Тонаж:</label>
                                                    </div>
                                                    <div class="col-md-1">
                                                        {!! Form::text( 'tonnage', @$selectedItem['tonnage'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Скорост:</label>
                                                    </div>
                                                    <div class="col-md-1">
                                                        {!! Form::text( 'speed', @$selectedItem['speed'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Волтаж:</label>
                                                    </div>
                                                    <div class="col-md-1">
                                                        {!! Form::text( 'voltage', @$selectedItem['voltage'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label>Първо плаване дата:</label>
                                                    </div>
                                                    <div class="col-md-1">
                                                        {!! Form::text( 'first_start', @$selectedItem['first_start'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Описание за кораба заглавие:</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::text( 'description_title_bg', @$selectedItem['description_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Описание за кораба текст:</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::textarea( 'description_text_bg', @$selectedItem['description_text_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Заглавие графа- храна на кораба:</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::text( 'food_title_bg', @$selectedItem['food_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Текст графа- храна на кораба:</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::textarea( 'food_text_bg', @$selectedItem['food_text_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    {{--АДАСДСА--}}
                                                    <div class="col-md-6">
                                                        <label>Заглавие графа- нощен живот:</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::text( 'life_title_bg', @$selectedItem['life_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Текст графа- нощен живот ляво:</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::textarea( 'life_left_text_bg', @$selectedItem['life_left_text_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Текст графа- нощен живот дясно:</label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! Form::textarea( 'life_right_text_bg', @$selectedItem['life_right_text_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 col-md-2 control-label">SEO заглавие и
                                            описание</label>

                                        <div class="col-sm-8 col-md-10">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#news_area_bg"
                                                                                          aria-controls="news_area_bg"
                                                                                          role="tab"
                                                                                          data-toggle="tab">BG</a></li>


                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                                    {!! Form::text( 'seo_title_bg',@$selectedItem['seo_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'seo_description_bg',@$selectedItem['seo_description_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 text-right">
                                    {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                                    <a href="{!! url('admin/ships/list') !!}" class="btn btn-primary">Назад</a>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            {{--MODALS--}}
                            <script>
                                $(document).ready(function () {
                                    $(".seoButton").on("click", function (event) {
                                        event.preventDefault();
                                    });
                                });
                            </script>

                            <div class="modal fade" id="imgModal" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Задаване на SEO title и Alt</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!! Form::open(array('action'=>'AdminShipsController@setSeoPictureData','method'=>'post','files'=>true)) !!}
                                            {!! Form::hidden('ship_id', @$selectedItem['id']) !!}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 col-md-2 control-label">SEO
                                                            tiltle и Alt</label>

                                                        <div class="col-sm-8 col-md-10">
                                                            <ul class="nav nav-tabs" role="tablist">
                                                                <li role="presentation" class="active"><a
                                                                            href="#news_area_bg"
                                                                            aria-controls="news_area_bg"
                                                                            role="tab"
                                                                            data-toggle="tab">BG</a></li>


                                                            </ul>

                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane active"
                                                                     id="news_area_bg">
                                                                    {!! Form::text( 'pic_title_bg',@$selectedItem['pic_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                                    {!! Form::text( 'pic_alt_bg',@$selectedItem['pic_alt_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            {{--MODALS--}}



                            {{--END OF DATA--}}
                            <hr>

                            <br>
                        </div>


                    </div>
                </div>
            </div>
        </section>
    </div>



@endsection
