@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Редактиране на станица "За нас"</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/index-page/slider/list') !!}" class="btn btn-primary">Назад</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {!! Session::get('success') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'AdminIndexController@storeSlider','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('id', @$selectedItem['id']) !!}

                            <div class="col-md-12">
                                <div class="form-group clearfix">


                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group clearfix">
                                    <label for="slidePicture" class="col-sm-4 col-md-2 control-label">Слайд картинка
                                        @if(@$selectedItem['picture'])
                                            <img
                                                    src="{!! asset(\App\PageIndexSliderEntity::$path . @$selectedItem['picture']) !!}"
                                                    class="img-responsive"
                                                    style="display: block;">
                                        @endif
                                    </label>

                                    <div class="col-sm-8 col-md-10">
                                        {!! Form::file('picture',['placeholder'=>'picture','class'=>'btn btn-warning btn-block center-block','id'=>'slidePicture']) !!}
                                        @if($errors -> first('picture') != '')
                                            <span class="help-block">{!! $errors -> first('picture') !!}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-4 col-md-2 control-label">Слайд заглавие и текст</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#about_area_bg"
                                                                                  aria-controls="about_area_bg"
                                                                                  role="tab"
                                                                                  data-toggle="tab">BG</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                            {!! Form::text( 'title_bg', @$selectedItem['title_bg'], array( 'class' => 'form-control' ) ) !!}
                                            {!! Form::textarea( 'text_bg', @$selectedItem['text_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            <div class="col-md-6">
                                                <label>Бутон текст</label>
                                                {!! Form::text( 'button1_text_bg', @$selectedItem['button1_text_bg'], array( 'class' => 'form-control' ) ) !!}
                                            </div>
                                            <div class="col-md-6">
                                                <label>Бутон url</label>
                                                {!! Form::text( 'button1_url', @$selectedItem['button1_url'], array( 'class' => 'form-control' ) ) !!}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>

                            <div class="col-sm-12 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success btn-block center-block']) !!}
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}
                            <hr>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection