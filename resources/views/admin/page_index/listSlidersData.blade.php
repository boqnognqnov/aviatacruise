@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">Всички типове конструкции</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/index-page/slider/edit') !!}"
                                   class="btn btn-block btn-warning btn-sm">Добавяне на нов тип</a>
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table width="100%">
                                <thead>
                                <th>ID</th>
                                <th>Снимка</th>
                                <th>Заглавие</th>
                                <th>Заглавие бутон</th>
                                <th>Линк бутон</th>

                                <th>Вкл/Изкл</th>
                                <th>Промяна</th>
                                <th>Изтриване</th>

                                </thead>
                                <tbody>
                                @foreach($slidersData as $oneSlider)
                                    <tr>
                                        <td>{!! $oneSlider['id'] !!}</td>
                                        <td>
                                            <img src="{!! asset(\App\PageIndexSliderEntity::$path.$oneSlider['picture']) !!}"
                                                 width="160px">
                                        </td>
                                        <td>{!! $oneSlider['title_bg'] !!}</td>
                                        <td>{!! $oneSlider['button1_text_bg'] !!}</td>
                                        <td><a href="{!! $oneSlider['button1_url'] !!}" target="_blank">Линк</a></td>

                                        <td>
                                            @if($oneSlider['is_active']==false)
                                                <a href="{!! url('admin/index-page/slider/set_active/'.$oneSlider['id']) !!}"
                                                   class="activation btn btn-danger"
                                                   style="width: 150px">Неактивен</a>
                                            @else
                                                <a href="{!! url('admin/index-page/slider/set_active/'.$oneSlider['id']) !!}"
                                                   class="activation btn btn-success"
                                                   style="width: 150px">Активен</a>
                                            @endif

                                        </td>

                                        <td>
                                            <a href="{!! url('admin/index-page/slider/edit/'.$oneSlider['id']) !!}"
                                               class="btn btn-primary">Промяна</a>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger delSlider"
                                                    data_slider_id="{!! $oneSlider['id'] !!}">Изтриване
                                            </button>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div style="display: none">
        {!! Form::open(array('action'=>'AdminIndexController@destroySlider','method'=>'post','id'=>'formSliderDel')) !!}
        {!! Form::hidden('id') !!}
        {!! Form::close() !!}
    </div>

    <script>
        function defineDeleteFunction() {
            $('.delSlider').on('click', function (event) {

                var id = $(this).attr('data_slider_id');

                $('#formSliderDel input[name="id"]').val(id);
                var r = confirm("Do you really want to delete this slider data ?");
                if (r == true) {
                    $('#formSliderDel').submit();
                }
            });
        }

        $(document).ready(function () {
            defineDeleteFunction();

        });


    </script>
    <script>

        $('.activation.btn-danger').hover(function () {
            $(this).attr('class', 'activation btn btn-success');
            $(this).text('Активиране');
        }, function () {
            $(this).attr('class', 'activation btn btn-danger');
            $(this).text('Неактивен');
        });

        $('.activation.btn-success').hover(function () {
            $(this).attr('class', 'activation btn btn-danger');
            $(this).text('Деактивиране');
        }, function () {
            $(this).attr('class', 'activation btn btn-success');
            $(this).text('Активен');
        });
    </script>


@endsection