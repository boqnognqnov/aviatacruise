@extends( 'admin.master' )
@section( 'content' )

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script>
        $(function () {
//            $(".datepicker").datepicker();
            $("#datepicker, #datepicker2").datepicker();
        });


    </script>

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Create News</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/news') !!}" class="btn btn-primary">News List</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {!! Session::get('success') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'AdminNewsController@updateCreateNews','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('news_id',@$selectedNews['id']) !!}


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 col-md-2 control-label">Категория:</label>
                                            <div class="col-sm-8 col-md-10">
                                                {!! Form::select('category_id',$newsCatList,@$selectedNews['category_id'],['class'=>'form-control']) !!}
                                                @if($errors -> first('category_id') != '')
                                                    <span class="help-block">{!! $errors -> first('category_id') !!}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 col-md-2 control-label">Задаване дата:</label>
                                            <div class="col-sm-8 col-md-10">
                                                {!! Form::text('date',@$selectedNews['date'],['placeholder'=>'Дата','class'=>'form-control','id'=>'datepicker']) !!}
                                                @if($errors -> first('date') != '')
                                                    <span class="help-block">{!! $errors -> first('date') !!}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="" class="col-sm-4 col-md-2 control-label">Задай във
                                                фаворити:</label>
                                            <div class="col-sm-8 col-md-10">
                                                {{--{!! Form::text('date',@$selectedNews['date'],['placeholder'=>'Дата','class'=>'form-control','id'=>'datepicker']) !!}--}}
                                                {!! Form::checkbox('is_front',true,@$selectedNews['is_front']) !!}
                                                @if($errors -> first('date') != '')
                                                    <span class="help-block">{!! $errors -> first('date') !!}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group clearfix">
                                <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img
                                            src="{!! asset(\App\NewsEntity::$path.@$selectedNews['picture']) !!}"
                                            class="img-responsive"
                                            style="display: block;">

                                    @if(!empty($selectedNews['picture']))
                                        <button data-toggle="modal" data-target="#imgModal"
                                                class="btn btn-xs btn-success btn-block seoButton">Задай SEO
                                        </button>
                                    @endif


                                </label>

                                <div class="col-sm-8 col-md-10">
                                    <p class="btn-info img-size">моля прикачете изображение с размер <strong>690px на
                                            510px</strong></p>
                                    {!! Form::file('picture',['placeholder'=>'Парола','class'=>'form-control','id'=>'headerIMG']) !!}
                                    @if($errors -> first('picture') != '')
                                        <span class="help-block">{!! $errors -> first('picture') !!}</span>
                                    @endif
                                </div>
                            </div>



                            <div class="row">

                                <div class="col-md-12">

                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-sm-8 col-md-10 col-sm-push-4 col-md-push-2">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#our_activity_bg"
                                                                                              aria-controls="our_activity_bg"
                                                                                              role="tab"
                                                                                              data-toggle="tab">БГ</a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="our_activity_bg">

                                            <div class="form-group">
                                                <label for="" class="col-sm-4 col-md-2 control-label">Заглавие</label>

                                                <div class="col-sm-8 col-md-10">

                                                    {!! Form::text( 'title_bg',@$selectedNews['title_bg'], array( 'class' => 'form-control' ) ) !!}
                                                </div>

                                                <label for="" class="col-sm-4 col-md-2 control-label">Под-
                                                    заглавие</label>

                                                <div class="col-sm-8 col-md-10">

                                                    {!! Form::text( 'sub_title_bg',@$selectedNews['sub_title_bg'], array( 'class' => 'form-control' ) ) !!}
                                                </div>

                                                <label for="" class="col-sm-4 col-md-2 control-label">Текст</label>
                                                <div class="col-sm-8 col-md-10">
                                                    {!! Form::textarea( 'text_bg',@$selectedNews['text_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>

                            {{--<div class="row">--}}
                            {{--<div class="col-md-12">--}}
                            {{--<div class="form-group">--}}
                            {{--<label for="" class="col-sm-4 col-md-2 control-label">News title and--}}
                            {{--text</label>--}}

                            {{--<div class="col-sm-8 col-md-10">--}}
                            {{--<ul class="nav nav-tabs" role="tablist">--}}
                            {{--<li role="presentation" class="active"><a href="#news_area_bg"--}}
                            {{--aria-controls="news_area_bg"--}}
                            {{--role="tab"--}}
                            {{--data-toggle="tab">BG</a></li>--}}


                            {{--</ul>--}}

                            {{--<div class="tab-content">--}}
                            {{--<div role="tabpanel" class="tab-pane active" id="news_area_bg">--}}
                            {{--{!! Form::text( 'title_bg',@$selectedNews['title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}--}}
                            {{--{!! Form::textarea( 'text_bg',@$selectedNews['text_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}--}}
                            {{--</div>--}}


                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 col-md-2 control-label">SEO заглавие и
                                            описание</label>

                                        <div class="col-sm-8 col-md-10">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#news_area_bg"
                                                                                          aria-controls="news_area_bg"
                                                                                          role="tab"
                                                                                          data-toggle="tab">BG</a></li>


                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                                    {!! Form::text( 'seo_title_bg',@$selectedNews['seo_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'seo_description_bg',@$selectedNews['seo_description_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-sm-6 text-right">
                                        {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}

                            {{--MODALS--}}
                            <script>
                                $(document).ready(function () {
                                    $(".seoButton").on("click", function (event) {
                                        event.preventDefault();
                                    });
                                });
                            </script>

                            <div class="modal fade" id="imgModal" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Задаване на SEO title и Alt</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!! Form::open(array('action'=>'AdminNewsController@setSeoPictureData','method'=>'post','files'=>true)) !!}
                                            {!! Form::hidden('news_id', @$selectedNews['id']) !!}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 col-md-2 control-label">SEO
                                                            tiltle и Alt</label>

                                                        <div class="col-sm-8 col-md-10">
                                                            <ul class="nav nav-tabs" role="tablist">
                                                                <li role="presentation" class="active"><a
                                                                            href="#news_area_bg"
                                                                            aria-controls="news_area_bg"
                                                                            role="tab"
                                                                            data-toggle="tab">BG</a></li>


                                                            </ul>

                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane active"
                                                                     id="news_area_bg">
                                                                    {!! Form::text( 'pic_title_bg',@$selectedNews['pic_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                                    {!! Form::text( 'pic_alt_bg',@$selectedNews['pic_alt_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            {{--MODALS--}}

                            <hr>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
