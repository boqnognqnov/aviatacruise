@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Галерия</h3>
                </div>
                <p class="btn-info img-size">моля прикачете изображение с размер <strong>690px на 390px</strong></p>
                <div class="box-body no-padding">
                    <div>
                        {!! Form::open(array('action' => 'AdminNewsController@uploadGalleryImgs','class'=>'form-horizontal','files'=>true)) !!}
                        {!! Form::hidden('news_id',$newsId) !!}
                        {!! Form::file('pictures[]',['multiple'=>'true']) !!}
                        {!! Form::submit('Upload',['class'=>'btn btn-success']) !!}
                        {!! Form::close() !!}
                    </div>

                    <div class="box-body no-padding">
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {!! Session::get('success') !!}
                            </div>
                        @endif

                        @if($errors -> any() )
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger" style="width: 100%;">
                                    <button type="button" class="close" data-dismiss="alert">?</button>
                                    <p>{{ $error }}</p>
                                </div>
                            @endforeach
                        @endif
                        <br>

                        <div class="row">
                            @foreach( $gallery as $oneImg )

                                <div class="col-md-4 portfolio-item">

                                    <button data-toggle="modal" data-target="#imgModal"
                                            class="btn btn-xs btn-success btn-block seoButton"
                                            data_img_id="{!! $oneImg['id'] !!}"
                                            data_img_title_bg="{!! $oneImg['pic_title_bg'] !!}"
                                            data_img_alt_bg="{!! $oneImg['pic_alt_bg'] !!}"
                                    >
                                        Задай SEO
                                    </button>
                                    <img class="img-responsive"
                                         src="{!! asset(\App\NewsGalleryEntity::$path.$oneImg['picture']) !!}"
                                         alt="">
                                    <br>
                                    <a href="{!! url('admin/news/gallery/move/left/'.$newsId.'/'.$oneImg['id']) !!}"
                                       class="glyphicon glyphicon-hand-left"></a>

                                    <button class="glyphicon glyphicon-remove-circle imageDel"
                                            data_image_id="{!! $oneImg['id']!!}">
                                    </button>

                                    <a href="{!! url('admin/news/gallery/move/right/'.$newsId.'/'.$oneImg['id']) !!}"
                                       class="glyphicon glyphicon-hand-right"></a>
                                    </br>

                                </div>
                            @endforeach
                        </div>
                    </div>

                    <br>


                    <div class="text-center">
                        {{--<a href="{!! url('admin/propertys') !!}"--}}
                        {{--class="btn btn-primary">Назад</a>--}}
                    </div>
                </div>
            </div>

        </section>
    </div>

    <div style="display: none">
        {!! Form::open(array('action' => 'AdminNewsController@destroyGalleryImg', 'id' => 'galleryFormDel')) !!}
        {!! Form::submit('Изтриване', ['class'=>'btn btn-danger']) !!}
        {!! Form::hidden( 'gallery_id' ) !!}
        {!! Form::close() !!}
    </div>

    {{--MODALS--}}
    <script>
        $(document).ready(function () {
            $(".seoButton").on("click", function (event) {
                event.preventDefault();

                $('.inputsInBody input[name="pic_id"]').val($(this).attr('data_img_id'));
                $('.inputsInBody input[name="pic_title_bg"]').val($(this).attr('data_img_title_bg'));
                $('.inputsInBody input[name="pic_alt_bg"]').val($(this).attr('data_img_alt_bg'));
            });
        });
    </script>

    <div class="modal fade" id="imgModal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Задаване на SEO title и Alt</h4>
                </div>
                <div class="modal-body inputsInBody">
                    {!! Form::open(array('action'=>'AdminNewsController@setSeoPictureDataGallery','method'=>'post','files'=>true)) !!}
                    {!! Form::hidden('pic_id', null) !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="" class="col-sm-4 col-md-2 control-label">SEO
                                    tiltle и Alt</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a
                                                    href="#news_area_bg"
                                                    aria-controls="news_area_bg"
                                                    role="tab"
                                                    data-toggle="tab">BG</a></li>


                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active"
                                             id="news_area_bg">
                                            {!! Form::text( 'pic_title_bg',@$selectedItem['pic_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            {!! Form::text( 'pic_alt_bg',@$selectedItem['pic_alt_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--MODALS--}}


    <script>

        $(document).ready(function () {

            $('.imageDel').on('click', function (event) {
                var $this = $(this);
                var id = $this.attr('data_image_id');
                $('input[name="gallery_id"]').val(id);
                var r = confirm("Наистина ли желаете да изтриете изображението ?");
                if (r == true) {
                    $('#galleryFormDel').submit();
                }
            });

        });
    </script>



@endsection
