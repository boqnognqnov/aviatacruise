@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Редактиране на станица "За нас"</h3>

                            <div class="box-tools">
                                {{--<a href="{!! url('admin/about-us/other') !!}" class="btn btn-default">SEO на страница--}}
                                {{--"За нас"</a>--}}
                                <a href="{!! url('admin/about-us/other') !!}" class="btn btn-primary">Други</a>
                                <a href="{!! url('admin/index-page/slider/list') !!}" class="btn btn-success">Слайдер
                                    страница
                                    "Начало"</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {!! Session::get('success') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'AdminPageAboutController@updateAboutPage','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('id', @$selectedItem['id']) !!}

                            <div class="col-md-12">
                                <div class="form-group clearfix">


                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group clearfix">
                                    <label for="slidePicture" class="col-sm-4 col-md-2 control-label">Слайд картинка
                                        @if(@$selectedItem['slide_img'])
                                            <img
                                                    src="{!! asset(\App\PageAboutEntity::$pathSlideImg . @$selectedItem['slide_img']) !!}"
                                                    class="img-responsive"
                                                    style="display: block;">


                                        @endif
                                    </label>

                                    <div class="col-sm-8 col-md-10">
                                        {!! Form::file('slide_img',['placeholder'=>'picture','class'=>'btn btn-warning btn-block center-block','id'=>'slidePicture']) !!}
                                        @if($errors -> first('slide_img') != '')
                                            <span class="help-block">{!! $errors -> first('slide_img') !!}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group clearfix">
                                    <label for="contentPicture" class="col-sm-4 col-md-2 control-label">Картинка за
                                        съдържанието на блок
                                        "{!! @$selectedItem['title2_bg'] !!}" на страница "За нас"
                                        @if(@$selectedItem['content_img'])
                                            <img
                                                    src="{!! asset(\App\PageAboutEntity::$pathContentImg . @$selectedItem['content_img']) !!}"
                                                    class="img-responsive"
                                                    style="display: block;">

                                        @endif
                                    </label>

                                    <div class="col-sm-8 col-md-10">
                                        {!! Form::file('content_img',['placeholder'=>'picture','class'=>'btn btn-warning btn-block center-block','id'=>'contentPicture']) !!}
                                        @if($errors -> first('content_img') != '')
                                            <span class="help-block">{!! $errors -> first('content_img') !!}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group clearfix">
                                    <label for="contentPictureFront" class="col-sm-4 col-md-2 control-label">Картинка за
                                        съдържанието на блок
                                        "{!! @$selectedItem['title2_bg'] !!}" на страница "Начало"
                                        @if(@$selectedItem['content_img_front'])
                                            <img
                                                    src="{!! asset(\App\PageAboutEntity::$pathContentImg . @$selectedItem['content_img_front']) !!}"
                                                    class="img-responsive"
                                                    style="display: block;">
                                        @endif
                                    </label>

                                    <div class="col-sm-8 col-md-10">
                                        {!! Form::file('content_img_front',['placeholder'=>'picture','class'=>'btn btn-warning btn-block center-block','id'=>'contentPictureFront']) !!}
                                        @if($errors -> first('content_img') != '')
                                            <span class="help-block">{!! $errors -> first('content_img_front') !!}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 col-md-2 control-label">Слайд текст</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#about_area_bg"
                                                                                  aria-controls="about_area_bg"
                                                                                  role="tab"
                                                                                  data-toggle="tab">BG</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                            {!! Form::textarea( 'slide_text_bg', @$selectedItem['slide_text_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="text_first_left_bg" class="col-sm-4 col-md-2 control-label">Заглавие и текст
                                    "За
                                    нас"</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#about_area_bg"
                                                                                  aria-controls="about_area_bg"
                                                                                  role="tab"
                                                                                  data-toggle="tab">BG</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        {!! Form::text( 'title1_bg', @$selectedItem['title1_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                        <div role="tabpanel" class="tab-pane active" id="about_area_bg">
                                            {!! Form::textarea( 'text_first_left_bg', @$selectedItem['text_first_left_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title2_bg" class="col-sm-4 col-md-2 control-label">Заглавие на 4-те
                                    реда</label>
                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#about_area_bg"
                                                                                  aria-controls="about_area_bg"
                                                                                  role="tab"
                                                                                  data-toggle="tab">BG</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                            {!! Form::text( 'title2_bg', @$selectedItem['title2_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="text_deduction_bg" class="col-sm-4 col-md-2 control-label">Заглавие и текст
                                    заключение</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#about_area_bg"
                                                                                  aria-controls="about_area_bg"
                                                                                  role="tab"
                                                                                  data-toggle="tab">BG</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                            {!! Form::text( 'title3_bg', @$selectedItem['title3_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            {!! Form::textarea( 'text_deduction_bg', @$selectedItem['text_deduction_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success btn-block center-block']) !!}
                            </div>
                            {!! Form::close() !!}


                            <hr>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection