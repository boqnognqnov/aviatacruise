@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Редактиране на станица "За нас"</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/about-us') !!}" class="btn btn-primary">Назад</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {!! Session::get('success') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'AdminAboutTextController@updateBenefitsBox','method'=>'post','files'=>true)) !!}
                            {{--{!! Form::hidden('id', @$selectedItem['id']) !!}--}}

                            @foreach($firstFourRows as $row)
                                <div class="panel-group">
                                    <div class="panel panel-danger">
                                        <div class="panel-heading">Коммпонент за страница "Начало" и страница "За нас"
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <label for="picture_front" class="col-sm-4 col-md-2 control-label">Картинка
                                                        за
                                                        страница "Начало"
                                                        @if(@$row['picture_front'])
                                                            <img
                                                                    src="{!! asset(\App\AboutTextEntity::$pathPreDefinedRows . @$row['picture_front']) !!}"
                                                                    class="img-responsive"
                                                                    style="display: block;">
                                                        @endif
                                                    </label>
                                                    <div class="col-sm-8 col-md-10">
                                                        {!! Form::file('rows['. $row['id'] .'][picture_front]',['placeholder'=>'picture','class'=>'btn btn-warning btn-block center-block','id'=>'picture']) !!}
                                                        @if($errors -> first('picture') != '')
                                                            <span class="help-block">{!! $errors -> first('picture') !!}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <label for="picture" class="col-sm-4 col-md-2 control-label">Картинка
                                                        за страница "За нас"
                                                        @if(@$row['picture'])
                                                            <img
                                                                    src="{!! asset(\App\AboutTextEntity::$pathPreDefinedRows . @$row['picture']) !!}"
                                                                    class="img-responsive"
                                                                    style="display: block;">
                                                        @endif
                                                    </label>

                                                    <div class="col-sm-8 col-md-10">
                                                        {!! Form::file('rows['. $row['id'] .'][picture]',['placeholder'=>'picture','class'=>'btn btn-warning btn-block center-block','id'=>'picture']) !!}
                                                        @if($errors -> first('picture') != '')
                                                            <span class="help-block">{!! $errors -> first('picture') !!}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group clearfix">
                                                    <label for="text1_bg"
                                                           class="col-sm-4 col-md-2 control-label">Заглавие</label>
                                                    {!! Form::text( 'rows['. $row['id'] .'][text1_bg]', @$row['text1_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    <label for="text2_bg"
                                                           class="col-sm-4 col-md-2 control-label">Долен текст</label>
                                                    {!! Form::text( 'rows['. $row['id'] .'][text2_bg]', @$row['text2_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="col-sm-12 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success btn-block center-block']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="box-body">
                            {{--{!! dump($lastItems) !!}--}}
                            {!! Form::open(array('action'=>'AdminAboutTextController@updateOthers','method'=>'post')) !!}
                            {!! Form::hidden('id', @$lastItems['id']) !!}
                            <div id="fields">
                                @foreach($otherRows as $key => $row)
                                    <div class="form-group clearfix">
                                        <div class="col-md-10">
                                            <label for="otherRows[{!! $row['id'] !!}]"
                                                   class="col-sm-4 col-md-2 control-label">Текст</label>
                                            {!! Form::text( 'otherRows['. $row['id'] .']', @$row['text1_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                        </div>

                                        <div class="col-md-2">
                                            <label class="control-label">&nbsp;</label>
                                            <button class="btn btn-danger btn-block delOtherField"
                                                    data-field-id="{!! $row['id'] !!}">Изтриване
                                            </button>
                                        </div>
                                    </div>
                                @endforeach
                            </div>


                            <div class="col-sm-12 text-right">
                                <div class="form-group">
                                    <button class="btn btn-warning btn-block center-block" id="addField">Добавяне на
                                        поле
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-12 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success btn-block center-block']) !!}
                            </div>
                            {!! Form::close() !!}
                            {{--END OF DATA--}}
                            <hr>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div style="display: none">
        {!! Form::open(array('action'=>'AdminAboutTextController@destroyOtherField','method'=>'post','id'=>'formFieldDel')) !!}
        {!! Form::hidden('fieldId') !!}
        {!! Form::close() !!}
    </div>

    <script>
        $(document).ready(function () {
            defineDeleteFunction();
            $('#addField').on('click', function (event) {
                event.preventDefault();
                generateField();
            });
        });

        function generateField() {

            var fieldSet = '';

            fieldSet += '<div class="form-group clearfix">' +
                    '<div class="col-md-10" id="fields">' +
                    '<label for="otherRows" class="col-sm-4 col-md-2 control-label">Текст</label>' +
                    '<input class="form-control" name="otherRows[]" type="text">' +
                    '</div>' +
                    '<div class="col-md-2">' +
                    '<label class="control-label">&nbsp;</label>' +
                    '<button class="btn btn-danger btn-block delOtherField" data-field-id="' + {!! $row['id'] !!} +'">Изтриване </button>' +
                    '</div>' +
                    '</div>';

            $("#fields").append(fieldSet);
        }

        function defineDeleteFunction() {
            $('.delOtherField').on('click', function (event) {
                event.preventDefault();
                var id = $(this).attr('data-field-id');

                $('#formFieldDel input[name="fieldId"]').val(id);

                var r = confirm("Сигурни ли сте, че искате да изтриете това поле?");
                if (r == true) {
                    $('#formFieldDel').submit();
                }
            });
        }
    </script>
@endsection