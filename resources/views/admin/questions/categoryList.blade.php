@extends( 'admin.master' )
@section( 'content' )



    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    @if(Session::has('successMessage'))
                        <div class="alert alert-success">
                            {!! Session::get('successMessage') !!}
                        </div>
                    @endif

                    @if($errors -> any() )
                        <div class="alert alert-danger" style="width: 100%;">
                            @foreach ($errors->all() as $error)
                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">Categories</h3>

                            <div class="box-tools">
                                <a href="{!! url('/admin/questions/list') !!}" class="btn btn-primary"
                                   style="display: inline-block">Обратно към списък с въпроси</a>
                            </div>
                        </div>
                        {{--<div class="box-body table-responsive no-padding">--}}
                        @foreach($categories as $oneCat)
                            <a href="{!! url('admin/questions/category/'.$oneCat['id']) !!}"
                               class="btn btn-primary">{!! $oneCat['name_bg'] !!}</a>

                            <button class="btn btn-danger delQuestCat"
                                    data-quest_category-id="{!! $oneCat['id'] !!}"><span
                                        class="glyphicon glyphicon-trash"></span>
                            </button>
                        @endforeach
                        <hr>


                        {!! Form::open(array('action'=>'AdminQuestionsController@addOrEditCategories','method'=>'post')) !!}
                        @if(!isset($selectedCat['id']))
                            {!! Form::hidden('id',0) !!}
                        @else
                            {!! Form::hidden('id',$selectedCat['id']) !!}
                        @endif

                        <div class="form-group">
                            {!! Form::label('Title En','Title BG',['class'=>'control-label col-sm-2']) !!}
                            <div class="col-sm-10">
                                {!! Form::text('name_bg',@$selectedCat['name_bg'],['class'=>'form-control','id'=>'title_en']) !!}
                            </div>
                        </div>

                        <div class="col-sm-6 text-right">
                            {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            <a href="{!! url('/admin/questions/category') !!}" class="btn btn-primary">Clear form</a>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>

        </section>
    </div>

    <div style="display: none">
        {!! Form::open(array('action'=>'AdminQuestionsController@destroyCategory','method'=>'post','id'=>'formCatDel')) !!}
        {!! Form::hidden('catId') !!}
        {!! Form::close() !!}
    </div>


    <script>

        $(document).ready(function () {

            $('.delQuestCat').on('click', function (event) {


                var id = $(this).attr('data-quest_category-id');

                $('#formCatDel input[name="catId"]').val(id);
                var r = confirm("Do you really want to delete this category with all news ?");
                if (r == true) {
                    $('#formCatDel').submit();
                }
            });
        });
    </script>



@endsection