@extends('admin.layouts.default')
@section('body')

    <div class="wrapper">

        <header class="main-header">
            <a href="{!! url('admin/sliders') !!}" class="logo">
                <span class="logo-mini"><b>A</b>C</span>
                <span class="logo-lg"><b>Aviata</b>Cruise</span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
            </nav>
        </header>

        <aside class="main-sidebar">

            <section class="sidebar">

                <ul class="sidebar-menu">
                    <li class="header">NAVIGATION</li>

                    <li class=""><a href="{!! url('admin/companies') !!}"><i class="fa fa-link"></i>
                            <span>Компании</span></a></li>
                    <li class=""><a href="{!! url('admin/ships/list') !!}"><i class="fa fa-link"></i>
                            <span>Кораби</span></a></li>

                    <li class=""><a href="{!! url('admin/routes-types') !!}"><i class="fa fa-link"></i>
                            <span>Типове круизи</span></a></li>

                    <li class=""><a href="{!! url('admin/routes-points') !!}"><i class="fa fa-link"></i>
                            <span>Междинни точки</span></a></li>
                    <li class=""><a href="{!! url('admin/destinations') !!}"><i class="fa fa-link"></i>
                            <span>Дестинации</span></a></li>

                    <li class=""><a href="{!! url('admin/routes') !!}"><i class="fa fa-link"></i>
                            <span>Круизи</span></a></li>

                    <li class=""><a href="{!! url('admin/reservations/list') !!}"><i class="fa fa-link"></i>
                            <span>Резервации</span></a></li>

                    <li class=""><a href="{!! url('admin/promo-packets') !!}"><i class="fa fa-link"></i>
                            <span>Промо пакети</span></a></li>
                    <li class=""><a href="{!! url('admin/additional-info/list') !!}"><i class="fa fa-link"></i>
                            <span>Абонаменти за <br>&nbsp;&nbsp;&nbsp; информация</span></a></li>

                    <li class=""><a href="{!! url('admin/news') !!}"><i class="fa fa-link"></i>
                            <span>Новини</span></a></li>


                    <li class=""><a href="{!! url('admin/about-us') !!}"><i class="fa fa-link"></i>
                            <span>Страница "Начало" <br>&nbsp;&nbsp;&nbsp; и "За нас"</span></a></li>


                    <li class=""><a href="{!! url('admin/contacts/show') !!}"><i class="fa fa-link"></i>
                            <span>Контакти</span></a></li>

                    <li class=""><a href="{!! url('admin/questions/list') !!}"><i class="fa fa-link"></i>
                            <span>Въпроси</span></a></li>
                    <li class=""><a href="{!! url('admin/email_subscription/list') !!}"><i class="fa fa-link"></i>
                            <span>Абонаменти по имейл</span></a></li>

                    <li class=""><a href="{!! url('admin/seo/static-pages/list') !!}"><i class="fa fa-link"></i>
                            <span>SEO</span></a></li>

                    <li class=""><a href="{!! url('admin/general-terms/show') !!}"><i class="fa fa-link"></i>
                            <span>Общи условия</span></a></li>


                    <li class=""><a href="{!! url('logout') !!}"><i class="fa fa-link"></i> <span>Изход</span></a></li>


                </ul>
            </section>
        </aside>

        @yield('content')


        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                Aviata Administration
            </div>
            <strong>Copyright &copy; 2016 <a href="#">Aviata Cruise</a>.</strong> All rights reserved.
        </footer>
    </div>

@endsection