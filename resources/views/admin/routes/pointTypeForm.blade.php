@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Редактиране/Нов маршрут</h3>

                            <div class="box-tools">
                                <a href="{!! url('/admin/point-types') !!}" class="btn btn-primary">Списък типове
                                    точки</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {!! Session::get('success') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}


                            {!! Form::open(array('action'=>'AdminRoutePointsController@createUpdatePointType','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('point_type_id', @$selectedItem['id']) !!}


                            <div class="form-group clearfix">
                                <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img
                                            src="{!! asset(\App\PointTypeEntity::$path . @$selectedItem['picture']) !!}"
                                            class="img-responsive"
                                            style="display: block;">


                                </label>

                                <div class="col-sm-8 col-md-10">
                                
                                    {!! Form::file('picture',['placeholder'=>'picture','class'=>'form-control','id'=>'headerIMG']) !!}
                                    @if($errors -> first('picture') != '')
                                        <span class="help-block">{!! $errors -> first('picture') !!}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-4 col-md-2 control-label">Тип точка: име</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#news_area_bg"
                                                                                  aria-controls="news_area_bg"
                                                                                  role="tab"
                                                                                  data-toggle="tab">BG</a></li>


                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                            {!! Form::text( 'name_bg', @$selectedItem['name_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}

                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 text-right">
                                    {!! Form::submit('Save',['class'=>'btn btn-success']) !!}

                                </div>
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}


                            <hr>

                            <br>
                        </div>


                    </div>
                </div>
            </div>
        </section>
    </div>



@endsection
