@extends( 'admin.master' )
@section( 'content' )

        <!-- DataTables CSS -->

<link rel="stylesheet" type="text/css"
      href="{!! asset('panel/plugins/dataTables/css/jquery.dataTables.min.css') !!}">

<script type="text/javascript" charset="utf8"
        src="{!! asset('panel/plugins/dataTables/js/jquery.dataTables.min.js') !!}"></script>

<script type="text/javascript" charset="utf8"
        src="{!! asset('panel/plugins/dataTables/js/dataTables.bootstrap.min.js') !!}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {!! Session::get('success') !!}
                    </div>
                @endif

                @if($errors -> any() )
                    <div class="alert alert-danger" style="width: 100%;">
                        @foreach ($errors->all() as $error)
                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">Списък с типове круизи</h3>

                        <div class="box-tools">
                            {{--<a href="{!! url('admin/news') !!}" class="btn btn-primary"--}}
                            {{--style="display: inline-block">Back to news page</a>--}}
                            <a href="{!! url('admin/routes-types/create_update') !!}"
                               class="btn btn-warning" style="display: inline-block">Нов тип круиз</a>
                            {{--<a href="{!! url('admin/news/tagList') !!}"--}}
                            {{--class="btn btn-success" style="display: inline-block">Edit tag list</a>--}}
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="example_wrapper" class="dataTables_wrapper">
                            <table id="example" class="display pageResize" cellspacing="0" width="100%">

                                <thead>
                                <th>ID</th>
                                <th>Снимка</th>
                                <th>Заглавие</th>
                                <th>Описание</th>
                                <th>Дата на създаване</th>
                                <th>Промяна</th>
                                <th>Изтриване</th>
                                </thead>
                                <tbody>
                                @foreach($routeTypes as $routeType)
                                    <tr>
                                        <td>{!! $routeType['id'] !!}</td>
                                        <td><img src="{!! asset(\App\RouteTypeEntity::$path . $routeType['picture']) !!}"
                                                 width="100px">
                                        </td>
                                        <td>{!! $routeType['name_bg'] !!}</td>
                                        <td>{!! $routeType['text_bg'] !!}</td>
                                        <td>{!! \App\Classes\GlobalFunctions::generateDateTimeToStr($routeType['created_at']) !!}</td>

                                        <td>
                                            <a href="{!! url('admin/routes-types/create_update/'.$routeType['id']) !!}"
                                               class="btn btn-warning">Промяна</a>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger delRouteType"
                                                    data-routetype-id="{!! $routeType['id'] !!}">Изтриване
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<div style="display: none">
    {!! Form::open(array('action'=>'AdminRoutesTypesController@destroyRouteType','method'=>'post','id'=>'formRouteTypeDel')) !!}
    {!! Form::hidden('routeTypeId') !!}
    {!! Form::close() !!}
</div>

<script>
    $('#example').DataTable(
            {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bFilter": true,
                "bLengthChange": true
            }
    );
    $('#example').on('draw.dt', function () {

        defineDeleteFunction();
    });

    function defineDeleteFunction() {
        $('.delRouteType').on('click', function (event) {

            var id = $(this).attr('data-routetype-id');

            $('#formRouteTypeDel input[name="routeTypeId"]').val(id);

            var r = confirm("Наистина ли искате да изтриете този тип круизи и всички вече съществуващи круизи от този тип?");

            if (r == true) {

                $('#formRouteTypeDel').submit();
            }
        });
    }

    $(document).ready(function () {
        defineDeleteFunction();
    });


</script>


@endsection