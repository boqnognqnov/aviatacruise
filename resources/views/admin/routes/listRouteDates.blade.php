@extends( 'admin.master' )
@section( 'content' )

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script>
        $(function () {
//            $(".datepicker").datepicker();
            $("#datepicker, #datepicker2").datepicker();
        });


    </script>

    <!-- DataTables CSS -->

    <link rel="stylesheet" type="text/css"
          href="{!! asset('panel/plugins/dataTables/css/jquery.dataTables.min.css') !!}">

    <script type="text/javascript" charset="utf8"
            src="{!! asset('panel/plugins/dataTables/js/jquery.dataTables.min.js') !!}"></script>

    <script type="text/javascript" charset="utf8"
            src="{!! asset('panel/plugins/dataTables/js/dataTables.bootstrap.min.js') !!}"></script>

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {!! Session::get('success') !!}
                        </div>
                    @endif

                    @if($errors -> any() )
                        <div class="alert alert-danger" style="width: 100%;">
                            @foreach ($errors->all() as $error)
                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">Списък дати:</h3>

                            <div class="box-tools">

                            </div>
                        </div>
                        <div class="col-md-6">

                            <label>Задаване на дата на круиза:</label>

                            {!! Form::open(array('action'=>'AdminRouteDatesController@setDateOnRoute','method'=>'post','files'=>true,'class'=>'form-horizontal')) !!}

                            {!! Form::hidden('route_id',$routeId) !!}
                            {!! Form::hidden('id',@$selectedItem['id']) !!}

                            <div class="form-group">
                                <label for="editPassOld" class="col-sm-4 control-label">Задаване дата на
                                    тръгване:</label>

                                <div class="col-sm-8">
                                    {!! Form::text('date',@$selectedItem['date'],['placeholder'=>'Дата','class'=>'form-control','id'=>'datepicker']) !!}
                                    @if($errors -> first('date') != '')
                                        <span class="help-block">{!! $errors -> first('date') !!}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="editPassOld" class="col-sm-4 control-label">Задаване дата на
                                    пристигане:</label>

                                <div class="col-sm-8">
                                    {!! Form::text('end_date',@$selectedItem['end_date'],['placeholder'=>'Дата','class'=>'form-control datepicker','id'=>'datepicker2']) !!}
                                    @if($errors -> first('end_date') != '')
                                        <span class="help-block">{!! $errors -> first('end_date') !!}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="editPassOld" class="col-sm-4 control-label">Задаване на намаление за целия
                                    круиз:</label>

                                <div class="col-sm-8">
                                    {!! Form::text('discount',@$selectedItem['discount'],['placeholder'=>'Намаление','class'=>'form-control','id'=>'editPassOld']) !!}
                                    @if($errors -> first('discount') != '')
                                        <span class="help-block">{!! $errors -> first('discount') !!}</span>
                                    @endif
                                </div>
                            </div>

                            @foreach($ships as $oneShip)

                                <label>Кораб: {!! $oneShip->name_bg !!}</label>
                                @foreach(\App\RoomTypeEntity::getRoomsOnThisShip($oneShip->id) as $oneRoom)

                                    <?php $roomData = \App\RoomTypeEntity::getRoomPrice($oneRoom->id, $selectedItem['id']); ?>
                                    <div class="well">
                                        <div class="form-group">
                                            <label for="editPassOld" class="col-sm-4 control-label">Задаване на цена за
                                                каюта: {!! $oneRoom->name_bg !!}</label>

                                            <div class="col-sm-8">
                                                {!! Form::text('shipRooms['.$oneShip->id.']['.$oneRoom->id.'][price]',@$roomData->price,['placeholder'=>'Цена','class'=>'form-control','id'=>'editPassOld']) !!}
                                                @if($errors -> first('shipRooms['.$oneShip->id.']['.$oneRoom->id.'][price]') != '')
                                                    <span class="help-block">{!! $errors -> first('shipRooms['.$oneShip->id.']['.$oneRoom->id.'][price]') !!}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="editPassOld" class="col-sm-4 control-label">Задаване на
                                                намаление за
                                                каюта: {!! $oneRoom->name_bg !!}</label>

                                            <div class="col-sm-8">
                                                {!! Form::text('shipRooms['.$oneShip->id.']['.$oneRoom->id.'][room_discount]',@$roomData->room_discount,['placeholder'=>'намаление за каюта','class'=>'form-control','id'=>'editPassOld']) !!}
                                                @if($errors -> first('shipRooms['.$oneShip->id.']['.$oneRoom->id.'][room_discount]') != '')
                                                    <span class="help-block">{!! $errors -> first('shipRooms['.$oneShip->id.']['.$oneRoom->id.'][room_discount]') !!}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>


                                @endforeach


                            @endforeach


                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Задай датата',['class'=>'btn btn-success']) !!}
                                <a href="{!! url('admin/routes-dates/list/'.$routeId) !!}"
                                   class="btn btn-warning">Изчисти</a>
                            </div>
                            {!! Form::close() !!}
                        </div>

                        <div class="col-md-6">

                            <div class="box-body table-responsive no-padding">
                                <div id="example_wrapper" class="dataTables_wrapper">
                                    <table id="example" class="display pageResize" cellspacing="0" width="100%">

                                        <thead>
                                        <th>ID</th>
                                        <th>Дата на събитието</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        </thead>
                                        <tbody>
                                        @foreach($list as $oneItem)
                                            <tr>
                                                <td>{!! $oneItem['id'] !!}</td>


                                                <td>{!! \App\Classes\GlobalFunctions::generateDateTimeToStr($oneItem['date']) !!}</td>

                                                <td>
                                                    <a href="{!! url('admin/routes-dates/list/'.$oneItem['route_id'].'/'.$oneItem['id']) !!}"
                                                       class="btn btn-warning">Промяна</a>
                                                </td>


                                                <td>
                                                    @if($oneItem['is_disabled']==false)
                                                        <a href="{!! url('admin/routes-dates/set_activity/'.$oneItem['id']) !!}"
                                                           class="activation btn btn-success"
                                                           style="width: 150px">Активен</a>
                                                    @else
                                                        <a href="{!! url('admin/routes-dates/set_activity/'.$oneItem['id']) !!}"
                                                           class="activation btn btn-danger"
                                                           style="width: 150px">Неактивен</a>
                                                    @endif

                                                </td>
                                                <td>
                                                    <button class="btn btn-danger delEvent"
                                                            data-date-id="{!! $oneItem['id'] !!}">Изтриване
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            {{--<div class="row">--}}
            {{--<div class="col-xs-12">--}}
            {{--<div class="box box-warning">--}}
            {{--<div class="box-header">--}}
            {{--<h3 class="box-title">Списък дати:</h3>--}}

            {{--<div class="box-tools">--}}

            {{--</div>--}}
            {{--</div>--}}
            {{----}}

            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
        </section>
    </div>


    <div style="display: none">
        {!! Form::open(array('action'=>'AdminRouteDatesController@destroyEvent','method'=>'post','id'=>'formEventDel')) !!}
        {!! Form::hidden('dateRecId') !!}
        {!! Form::hidden('routeId',$routeId) !!}
        {!! Form::close() !!}
    </div>


    <input type="hidden" name="temp_news_id" value="{!! Session::get('successSetTags') !!}">


    <script>
        //        $('#example').DataTable(
        //                {
        //                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        //                    "bFilter": true,
        //                    "bLengthChange": true
        //                }
        //        );
        //        $('#example').on('draw.dt', function () {
        //            defineDeleteFunction();
        //
        //        });

        function defineDeleteFunction() {
            $('.delEvent').on('click', function (event) {

                var id = $(this).attr('data-date-id');

                $('#formEventDel input[name="dateRecId"]').val(id);
                var r = confirm("Do you really want to delete this event ?");
                if (r == true) {
                    $('#formEventDel').submit();
                }
            });
        }

        $(document).ready(function () {
            defineDeleteFunction();

        });


    </script>
    <script>

        $('.activation.btn-danger').hover(function () {
            $(this).attr('class', 'activation btn btn-success');
            $(this).text('Активиране');
        }, function () {
            $(this).attr('class', 'activation btn btn-danger');
            $(this).text('Неактивен');
        });

        $('.activation.btn-success').hover(function () {
            $(this).attr('class', 'activation btn btn-danger');
            $(this).text('Деактивиране');
        }, function () {
            $(this).attr('class', 'activation btn btn-success');
            $(this).text('Активен');
        });
    </script>


@endsection