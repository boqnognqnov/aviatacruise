@extends( 'admin.master' )
@section( 'content' )

        <!-- DataTables CSS -->

<link rel="stylesheet" type="text/css"
      href="{!! asset('panel/plugins/dataTables/css/jquery.dataTables.min.css') !!}">

<script type="text/javascript" charset="utf8"
        src="{!! asset('panel/plugins/dataTables/js/jquery.dataTables.min.js') !!}"></script>

<script type="text/javascript" charset="utf8"
        src="{!! asset('panel/plugins/dataTables/js/dataTables.bootstrap.min.js') !!}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {!! Session::get('success') !!}
                    </div>
                @endif

                @if($errors -> any() )
                    <div class="alert alert-danger" style="width: 100%;">
                        @foreach ($errors->all() as $error)
                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">Списък круизи</h3>

                        <div class="box-tools">
                            {{--<a href="{!! url('admin/news') !!}" class="btn btn-primary"--}}
                            {{--style="display: inline-block">Back to news page</a>--}}
                            {{--<a href="{!! url('/admin/news/category') !!}"--}}
                            {{--class="btn btn-bitbucket" style="display: inline-block">Categories</a>--}}
                            <a href="{!! url('admin/routes/create_update') !!}"
                               class="btn btn-warning" style="display: inline-block">Нов маршрут</a>
                            {{--<a href="{!! url('admin/news/tagList') !!}"--}}
                            {{--class="btn btn-success" style="display: inline-block">Edit tag list</a>--}}
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="example_wrapper" class="dataTables_wrapper">
                            <table id="example" class="display pageResize" cellspacing="0" width="100%">

                                <thead>
                                <th>ID</th>
                                <th>Снимка</th>
                                <th>Заглавие</th>
                                <th>Дата на създаване</th>
                                <th>Дати</th>
                                <th></th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($list as $oneItem)
                                    <tr>
                                        <td>{!! $oneItem['id'] !!}</td>
                                        <td><img src="{!! asset(\App\RouteEntity::$path.$oneItem['picture']) !!}"
                                                 width="100px">
                                        </td>
                                        <td>{!! $oneItem['route_title_bg'] !!}</td>
                                        <td>{!! \App\Classes\GlobalFunctions::generateDateTimeToStr($oneItem['created_at']) !!}</td>
                                        <td>
                                            <a href="{!! url('admin/routes-dates/list/'.$oneItem['id']) !!}"
                                               class="btn btn-primary">Задаване на дати</a>
                                        </td>
                                        <td>
                                            <a href="{!! url('admin/routes-gallery/'.$oneItem['id']) !!}"
                                               class="btn btn-success">Галерия</a>
                                        </td>
                                        <td>
                                            <a href="{!! url('admin/routes/create_update/'.$oneItem['id']) !!}"
                                               class="btn btn-warning">Промяна</a>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger delRoute"
                                                    data-route-id="{!! $oneItem['id'] !!}">Изтриване
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>


<div style="display: none">
    {!! Form::open(array('action'=>'AdminRoutesController@destroyRoute','method'=>'post','id'=>'formRouteDel')) !!}
    {!! Form::hidden('routeId') !!}
    {!! Form::close() !!}
</div>


<input type="hidden" name="temp_news_id" value="{!! Session::get('successSetTags') !!}">


<script>
    $('#example').DataTable(
            {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bFilter": true,
                "bLengthChange": true
            }
    );
    $('#example').on('draw.dt', function () {
        defineDeleteFunction();

    });

    function defineDeleteFunction() {
        $('.delRoute').on('click', function (event) {

            var id = $(this).attr('data-route-id');

            $('#formRouteDel input[name="routeId"]').val(id);
            var r = confirm("Do you really want to delete this route ?");
            if (r == true) {
                $('#formRouteDel').submit();
            }
        });
    }

    $(document).ready(function () {
        defineDeleteFunction();

    });


</script>


@endsection