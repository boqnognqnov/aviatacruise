@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Създаване/Променяне на тип круиз</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/routes-types') !!}" class="btn btn-primary">Типове круизи</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {!! Session::get('success') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'AdminRoutesTypesController@createUpdateRouteType','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('route_type_id', @$selectedRoutesTypes['id']) !!}

                            <div class="form-group clearfix">

                                @if(isset($selectedRoutesTypes['picture']))
                                    <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Изображение<img
                                                src="{!! asset(\App\RouteTypeEntity::$path . $selectedRoutesTypes['picture']) !!}"
                                                class="img-responsive"
                                                style="display: block;">
                                        @if(!empty($selectedRoutesTypes['picture']))
                                            <button data-toggle="modal" data-target="#imgModal"
                                                    class="btn btn-xs btn-danger btn-block seoButton">Задай SEO
                                            </button>
                                        @endif
                                    </label>
                                @endif
                                <div class="col-sm-10 col-md-offset-2">
                                    <p class="btn-info img-size">моля прикачете изображение с размер <strong>720px на 450px</strong></p>
                                    {{ Form::file('picture',['placeholder'=>'picture','class'=>'btn btn-warning btn-block center-block','id'=>'headerIMG']) }}
                                    @if($errors -> first('picture') != '')
                                        <span class="help-block">{!! $errors -> first('picture') !!}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 col-md-2 control-label">Заглавие и
                                            описание</label>

                                        <div class="col-sm-8 col-md-10">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#news_area_bg"
                                                                                          aria-controls="news_area_bg"
                                                                                          role="tab"
                                                                                          data-toggle="tab">BG</a></li>

                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                                    {{ Form::text('name_bg', @$selectedRoutesTypes['name_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) }}
                                                    {{ Form::textarea('text_bg', @$selectedRoutesTypes['text_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 col-md-2 control-label">SEO заглавие и
                                            описание</label>

                                        <div class="col-sm-8 col-md-10">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#news_area_bg"
                                                                                          aria-controls="news_area_bg"
                                                                                          role="tab"
                                                                                          data-toggle="tab">BG</a></li>


                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                                    {!! Form::text( 'seo_title_bg',@$selectedRoutesTypes['seo_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'seo_description_bg',@$selectedRoutesTypes['seo_description_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-sm-6 text-right">
                                        {{ Form::submit('Save',['class'=>'btn btn-success']) }}
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}
                            {{--MODALS--}}
                            <script>
                                $(document).ready(function () {
                                    $(".seoButton").on("click", function (event) {
                                        event.preventDefault();
                                    });
                                });
                            </script>

                            <div class="modal fade" id="imgModal" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Задаване на SEO title и Alt</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!! Form::open(array('action'=>'AdminRoutesTypesController@setSeoPictureData','method'=>'post','files'=>true)) !!}
                                            {!! Form::hidden('route_type_id', @$selectedRoutesTypes['id']) !!}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 col-md-2 control-label">SEO
                                                            tiltle и Alt</label>

                                                        <div class="col-sm-8 col-md-10">
                                                            <ul class="nav nav-tabs" role="tablist">
                                                                <li role="presentation" class="active"><a
                                                                            href="#news_area_bg"
                                                                            aria-controls="news_area_bg"
                                                                            role="tab"
                                                                            data-toggle="tab">BG</a></li>


                                                            </ul>

                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane active"
                                                                     id="news_area_bg">
                                                                    {!! Form::text( 'pic_title_bg',@$selectedRoutesTypes['pic_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                                    {!! Form::text( 'pic_alt_bg',@$selectedRoutesTypes['pic_alt_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            {{--MODALS--}}
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
