@foreach($promoPacks as $packetId=>$onePack)

    @if($onePack['checked']==true)
        <a href="{!! url('/admin/routes/set_promo_packet_to_route/'.$onePack['route_id'].'/'.$packetId) !!}"
           class="btn btn-success"
           style="display: inline-block">  {!! $onePack['name'] !!}</a>
    @else
        <a href="{!! url('/admin/routes/set_promo_packet_to_route/'.$onePack['route_id'].'/'.$packetId) !!}"
           class="btn btn-danger" style="display: inline-block">  {!! $onePack['name'] !!}</a>
    @endif
@endforeach