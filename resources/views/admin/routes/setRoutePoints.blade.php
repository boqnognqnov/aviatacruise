@extends( 'admin.master' )
@section( 'content' )

    {{--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">--}}
    {{--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>--}}
    {{--<link rel="stylesheet" href="/resources/demos/style.css">--}}
    {{--<script>--}}
        {{--$(function () {--}}
            {{--$(".datepicker").datepicker();--}}
        {{--});--}}

    {{--</script>--}}

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Добавяне на точки</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/routes') !!}" class="btn btn-primary">Списък маршрути</a>

                                <a href="{!! url('admin/routes') !!}" class="btn btn-warning">Задаване на точки</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {!! Session::get('success') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::hidden('route_id', @$route_id) !!}

                            <div class="clearfix">
                                <div class="form-group">
                                    <div class="col-md-2">
                                        <label>Търсене на точка по име:</label>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::text( 'point_name', null, array( 'class' => 'form-control' ) ) !!}
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="form-group">
                                    <div class="pointListClass">

                                    </div>
                                </div>
                            </div>

                            <hr>
                            @if($newPoint!=null)

                                <label>Задаване на информация на междинната точка:</label>

                                {!! Form::open(array('action'=>'AdminRoutePointsController@setPointToRoute','method'=>'post','files'=>true,'class'=>'form-horizontal')) !!}

                                {!! Form::hidden('route_id',$route_id) !!}
                                {!! Form::hidden('point_id',$newPoint['id']) !!}
                                {!! Form::hidden('route_point_id',@$newPoint['routePointId']) !!}

                                <div class="form-group">
                                    <label for="editPassOld" class="col-sm-4 control-label">Ден от пътуването:</label>

                                    <div class="col-sm-8">
                                        {!! Form::text('day',@$newPoint['day'],['placeholder'=>'','class'=>'form-control','id'=>'editPassOld']) !!}
                                        @if($errors -> first('day') != '')
                                            <span class="help-block">{!! $errors -> first('day') !!}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="editPassOld" class="col-sm-4 control-label">Време на пристигане:</label>

                                    <div class="col-sm-8">
                                        {!! Form::text('time_arrival',@$newPoint['time_arrival'],['placeholder'=>'','class'=>'form-control','id'=>'editPassOld']) !!}
                                        @if($errors -> first('time_arrival') != '')
                                            <span class="help-block">{!! $errors -> first('time_arrival') !!}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="editPassOld" class="col-sm-4 control-label">Време на тръгване:</label>

                                    <div class="col-sm-8">
                                        {!! Form::text('time_departure',@$newPoint['time_departure'],['placeholder'=>'','class'=>'form-control','id'=>'editPassOld']) !!}
                                        @if($errors -> first('time_departure') != '')
                                            <span class="help-block">{!! $errors -> first('time_departure') !!}</span>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-sm-6 text-right">
                                    {!! Form::submit('Задай точката',['class'=>'btn btn-success']) !!}
                                    <a href="{!! url('admin/routes-points/set_point_form/'.$route_id) !!}"
                                       class="btn btn-warning">Изчисти</a>
                                </div>
                                {!! Form::close() !!}
                            @endif
                            <hr>

                            <br>

                            <hr>
                            <label>Избрани точки</label>
                            <div class="box-body table-responsive no-padding">
                                <table>
                                    <thead>
                                    <th width="10%">#</th>
                                    <th width="10%">Име:</th>
                                    <th width="10%">Снимка</th>
                                    <th width="10%">Ден:</th>
                                    <th width="10%">Време пристигане:</th>
                                    <th width="10%">Време тръгване:</th>
                                    <th colspan="2" width="10%"></th>

                                    </thead>

                                    <tbody>
                                    @foreach($selectedPoints as $oneSelected)
                                        <tr>
                                            <td>{!! $oneSelected['sequence'] !!}</td>
                                            <td>{!! $oneSelected['name_bg'] !!}</td>
                                            <td>
                                                <img src="{!! asset(\App\PointsEntity::$path.$oneSelected['picture']) !!}"
                                                     width="150px">
                                            </td>
                                            <td>{!! $oneSelected['day'] !!}</td>
                                            <td>{!! $oneSelected['time_arrival'] !!}</td>
                                            <td>{!! $oneSelected['time_departure'] !!}</td>

                                            <td>
                                                <a class="btn btn-success"
                                                   href="{!! url('admin/routes-points/set_point_form/'.$route_id.'/'.$oneSelected['point_id'].'/'.$oneSelected['id']) !!}">Промяна</a>
                                            </td>
                                            <td>
                                                <a class="btn btn-danger"
                                                   href="{!! url('admin/routes-points/destroy-point/'.$route_id.'/'.$oneSelected['point_id']) !!}"><span
                                                            class="glyphicon glyphicon-trash"></span>
                                                </a>
                                            </td>
                                        </tr>





                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <script src="{!! asset('js/functions/getRoutePointsByCriterion.js') !!}"></script>


                            {{--DATA--}}

                            <hr>

                            <br>
                        </div>


                    </div>
                </div>
            </div>
        </section>
    </div>



@endsection