@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Създаване/Редактиране на компания</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/companies') !!}" class="btn btn-primary">Списък с компании</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {!! Session::get('success') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'AdminCompaniesController@createUpdateCompany','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('company_id', @$selectedItem['id']) !!}

                            <div class="col-md-6">
                                <div class="form-group clearfix">
                                    <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Снимка на компанията
                                        <img
                                                src="{!! asset(\App\CompanyEntity::$pathImg . @$selectedItem['picture']) !!}"
                                                class="img-responsive"
                                                style="display: block;">
                                        @if(!empty($selectedItem['picture']))
                                            <button data-toggle="modal" data-target="#imgModal"
                                                    class="btn btn-xs btn-success btn-block seoButton">Задай SEO
                                            </button>
                                        @endif
                                    </label>

                                    <div class="col-sm-8 col-md-10">
                                        <p class="btn-info img-size">моля прикачете изображение с размер <strong>720px на 450px</strong></p>
                                        {!! Form::file('picture',['placeholder'=>'picture','class'=>'form-control','id'=>'headerIMG']) !!}
                                        @if($errors -> first('picture') != '')
                                            <span class="help-block">{!! $errors -> first('picture') !!}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group clearfix">
                                    <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Лого на
                                        компанията<img
                                                src="{!! asset(\App\CompanyEntity::$pathLogo . @$selectedItem['logo']) !!}"
                                                class="img-responsive"
                                                style="display: block;">
                                        @if(!empty($selectedItem['logo']))
                                            <button data-toggle="modal" data-target="#imgModal2"
                                                    class="btn btn-xs btn-success btn-block seoButton">Задай SEO
                                            </button>
                                        @endif
                                    </label>

                                    <div class="col-sm-8 col-md-10">
                                        <p class="btn-info img-size">моля прикачете изображение с размер <strong>370px на 135px</strong></p>
                                        {!! Form::file('logo',['placeholder'=>'logo','class'=>'form-control','id'=>'headerIMG']) !!}
                                        @if($errors -> first('logo') != '')
                                            <span class="help-block">{!! $errors -> first('logo') !!}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 col-md-2 control-label">Компания: Име, описание и
                                            общи
                                            условия</label>

                                        <div class="col-sm-8 col-md-10">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#news_area_bg"
                                                                                          aria-controls="news_area_bg"
                                                                                          role="tab"
                                                                                          data-toggle="tab">BG</a></li>

                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                                    {!! Form::text( 'name_bg', @$selectedItem['name_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'description_bg', @$selectedItem['description_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'additional_info_bg', @$selectedItem['additional_info_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="" class="col-sm-4 col-md-2 control-label">SEO заглавие и
                                            описание</label>

                                        <div class="col-sm-8 col-md-10">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#news_area_bg"
                                                                                          aria-controls="news_area_bg"
                                                                                          role="tab"
                                                                                          data-toggle="tab">BG</a></li>


                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                                    {!! Form::text( 'seo_title_bg',@$selectedItem['seo_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'seo_description_bg',@$selectedItem['seo_description_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-sm-6 text-right">
                                        {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}

                            {{--MODALS--}}
                            <script>
                                $(document).ready(function () {
                                    $(".seoButton").on("click", function (event) {
                                        event.preventDefault();
                                    });
                                });
                            </script>

                            <div class="modal fade" id="imgModal" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Задаване на SEO title и Alt</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!! Form::open(array('action'=>'AdminCompaniesController@setSeoPictureData','method'=>'post','files'=>true)) !!}
                                            {!! Form::hidden('company_id', @$selectedItem['id']) !!}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 col-md-2 control-label">SEO
                                                            tiltle и Alt</label>

                                                        <div class="col-sm-8 col-md-10">
                                                            <ul class="nav nav-tabs" role="tablist">
                                                                <li role="presentation" class="active"><a
                                                                            href="#news_area_bg"
                                                                            aria-controls="news_area_bg"
                                                                            role="tab"
                                                                            data-toggle="tab">BG</a></li>


                                                            </ul>

                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane active"
                                                                     id="news_area_bg">
                                                                    {!! Form::text( 'pic_title_bg',@$selectedItem['pic_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                                    {!! Form::text( 'pic_description_bg',@$selectedItem['pic_description_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>

                            {{--dasdjasldjlkasjd--}}
                            <div class="modal fade" id="imgModal2" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Задаване на SEO title и Alt </h4>
                                        </div>
                                        <div class="modal-body">
                                            {!! Form::open(array('action'=>'AdminCompaniesController@setSeoLogoData','method'=>'post','files'=>true)) !!}
                                            {!! Form::hidden('company_id', @$selectedItem['id']) !!}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="" class="col-sm-4 col-md-2 control-label">SEO
                                                            tiltle и Alt</label>

                                                        <div class="col-sm-8 col-md-10">
                                                            <ul class="nav nav-tabs" role="tablist">
                                                                <li role="presentation" class="active"><a
                                                                            href="#news_area_bg"
                                                                            aria-controls="news_area_bg"
                                                                            role="tab"
                                                                            data-toggle="tab">BG</a></li>


                                                            </ul>

                                                            <div class="tab-content">
                                                                <div role="tabpanel" class="tab-pane active"
                                                                     id="news_area_bg">
                                                                    {!! Form::text( 'logo_title_bg',@$selectedItem['logo_title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                                    {!! Form::text( 'logo_description_bg',@$selectedItem['logo_description_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            {{--MODALS--}}
                            <hr>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
