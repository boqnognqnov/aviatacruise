@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Преглед на резервация</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/reservations/list') !!}" class="btn btn-primary">Списък
                                    резервации</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {!! Session::get('success') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        @if(isset($selectedItem['promoPackets']))
                                            <h2>Промо- пакети</h2>
                                            @foreach($selectedItem['promoPackets'] as $onePacket)
                                                <div class="panel panel-danger">

                                                    <div class="panel-body">{!! $onePacket['packet_name'] !!} &nbsp;&nbsp;&nbsp;&nbsp;<span
                                                                style="color:red"> {!!$onePacket['packet_price'] .' EUR' !!}</span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        @if(isset($selectedItem['reseravation_subscription']))
                                            <h2>Абонаменти за информация</h2>
                                            @foreach($selectedItem['reseravation_subscription'] as $oneInfo)
                                                <div class="panel panel-info">
                                                    <div class="panel-body">{!! $oneInfo['subscription_name'] !!}</div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Обща информация</div>
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        Имена на титуляра:
                                                        &nbsp;&nbsp;&nbsp;&nbsp; <span style="color:blue">
                                                    {!! $selectedItem['titular_fname'].'  '.$selectedItem['titular_sname'].'  '.$selectedItem['titular_lname'] !!}</span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        Телефон:
                                                        &nbsp;&nbsp;&nbsp;&nbsp; <span style="color:blue">
                                                    {!! $selectedItem['titular_phone'] !!}</span>
                                                    </div>
                                                    <div class="col-md-4">
                                                        Email:
                                                        &nbsp;&nbsp;&nbsp;&nbsp; <span style="color:blue">
                                                    {!! $selectedItem['titular_email'] !!}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <hr style="background-color: red">
                                                Име на кораба:
                                                &nbsp;&nbsp;&nbsp;&nbsp; <span style="color:blue">
                                                    {!! $selectedItem['ship_name'] !!}</span>

                                                <hr style="background-color: red">
                                            </div>
                                            <div class="col-md-6">

                                                <p>
                                                    Име на круиза: <span
                                                            style="color:blue">{!! $selectedItem['route_name'] !!}</span>
                                                </p>
                                                <p>
                                                    Тип каюта:
                                                    <span style="color:blue">{!! $selectedItem['cabin_name'] !!}</span>
                                                </p>


                                            </div>
                                            <div class="col-md-6">
                                                <p>
                                                    Намаление на круиза в този
                                                    период: <span
                                                            style="color:blue">{!! $selectedItem['route_discount'].' %' !!}</span>
                                                </p>
                                                <p>
                                                    Намаление на каютата в този
                                                    период:
                                                    <span style="color:blue">{!! $selectedItem['room_discount'].' %' !!}</span>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                Брой пасажери: <span
                                                        style="color:blue">{!! $selectedItem['passengers'] !!}</span>
                                            </div>
                                            <div class="col-md-6">
                                                Цена на стая за един пасажер: <span
                                                        style="color:red">{!! $selectedItem['room_discount_price_by_one'].' EUR' !!}</span>
                                            </div>

                                            <div class="col-md-12">
                                                <hr style="background-color: red">
                                                Крайна цена:<span
                                                        style="color:red"> {!! $selectedItem['total_price'].' EUR' !!}</span>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(!empty($selectedItem['reservation_comment']))
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">Коментар:</div>
                                            <div class="panel-body">
                                                <div class="col-md-12">
                                                    {!! $selectedItem['reservation_comment'] !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            {!! Form::open(array('action'=>'AdminNewsController@updateCreateNews','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('reservation_id',@$selectedItem['id']) !!}


                            <div class="form-group">
                                <label for="" class="col-sm-4 col-md-2 control-label">Съобщение</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#news_area_bg"
                                                                                  aria-controls="news_area_bg"
                                                                                  role="tab"
                                                                                  data-toggle="tab">BG</a>
                                        </li>


                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="news_area_bg">
                                            {!! Form::text( 'title_bg',@$selectedItem['title_bg'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            {!! Form::textarea( 'text_bg',@$selectedItem['text_bg'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Изпрати',['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}
                            <hr>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection