@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">Типове абонаменти за информация</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/additional-info/edit') !!}"
                                   class="btn btn-block btn-warning btn-sm">Добавяне на нов тип</a>
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table width="100%">
                                <thead>
                                <th>ID</th>
                                <th>Име</th>
                                <th>Промяна</th>
                                <th>Изтриване</th>
                                </thead>
                                <tbody>
                                @foreach($infoTypes as $infoType)
                                    <tr>
                                        <td>{!! $infoType['id'] !!}</td>
                                        <td>{!! $infoType['name_bg'] !!}</td>
                                        <td><a href="{!! url('admin/additional-info/edit/'.$infoType['id'] ) !!}"
                                               class="btn btn-primary">Промяна</a></td>
                                        <td>
                                            @if($infoType['id'] <4)
                                                <button class="btn btn-warning">Не може да бъде
                                                    изтрит
                                                </button>
                                            @else
                                                <button class="btn btn-danger delInfoType"
                                                        data_info_type_id="{!! $infoType['id'] !!}">Изтриване
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>

                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none">
                {!! Form::open(array('action'=>'AdminAdditionalInfoController@destroyType','method'=>'post','id'=>'formInfoTypeDel')) !!}
                {!! Form::hidden('infoTypeId') !!}
                {!! Form::close() !!}
            </div>

            <script>
                function defineDeleteFunction() {
                    $('.delInfoType').on('click', function (event) {

                        var id = $(this).attr('data_info_type_id');
//

                        $('#formInfoTypeDel input[name="infoTypeId"]').val(id);

                        var r = confirm("Do you really want to delete this company?");
                        if (r == true) {
                            $('#formInfoTypeDel').submit();
                        }
                    });
                }

                $(document).ready(function () {
                    defineDeleteFunction();

                });
            </script>

        </section>
    </div>


@endsection