@extends('master')

@section('plugin-assets-css')
@endsection
@section('plugin-assets-js')

@endsection


@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="inner-nav">
                    @foreach($breadcrumps as $onecrump)
                        <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="faq-accordion">
                    @foreach($questRecords as $categoryTitle=>$oneCategory)
                        <h3 class="title">{!! $categoryTitle !!}</h3>
                        <div class="inner-wrapper">
                            @foreach($oneCategory as $oneQuestTitle)
                                <h4>{!! $oneQuestTitle['questTitle'] !!}</h4>
                                <p>{!! $oneQuestTitle['questText'] !!}</p>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>

        </div>

    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
    </div>
    @include('components.footer-promotions')


@endsection
@section('page-scripts')
    <script src="js/scripts.js"></script>
    <script>
        $(document).ready(function () {
            $("#faq-accordion").accordion({
                active: 0
            });
        });
    </script>

@endsection
