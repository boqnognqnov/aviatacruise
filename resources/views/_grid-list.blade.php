@extends('master')

@section('plugin-assets-css')
@endsection
@section('plugin-assets-js')
@endsection



@section('page-title', 'Home')

@section('content')
@include('components.search')
<div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="inner-nav">
                        <li><a href="#">Начало</a></li>
                        <li><a href="#">Типове Круизи</a></li>
                    </ul>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
        <div class="ships-list-grid">
            <div class="background-white">
                <div class="container">
                    <h1 class="main-spacial-title">Типове Круизи</h1>
                    <h1 class="main-spacial-subtitle">Възползвайте се от различните варианти за круизи
                        <div class="swicher-wrapper">
                            <button class="swich-to-grid"><i class="fa fa-th"></i></button>
                            <button class="swich-to-list active"><i class="fa fa-list-ul"></i></button>

                        </div>
                    </h1>

                </div>
                <div class="container">
                    <div class="single-ship">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="img-responsive" src="{!! asset('img/cruises-1.jpg') !!}"/>
                            </div>
                            <div class="col-md-6">
                                <h1>Морски круизи</h1>
                                <p>
                                Lorem Ipsum е елементарен примерен текст, използван в печатарската и типографската индустрия.
                                <br><br>
                                Lorem Ipsum е индустриален стандарт от около 1500 година, когато неизвестен печатар взема няколко печатарски букви и ги разбърква, за да напечата с тях книга с примерни шрифтове. </p>
                                <a href="cruises.html" class="view-more">Виж повече<span></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="single-ship">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="img-responsive" src="{!! asset('img/cruises-2.jpg') !!}"/>
                            </div>
                            <div class="col-md-6">
                                <h1>През-океански круизи</h1>
                                <p>Lorem Ipsum е елементарен примерен текст, използван в печатарската и типографската индустрия.
                                <br><br>
                                 Lorem Ipsum е индустриален стандарт от около 1500 година, когато неизвестен печатар взема няколко печатарски букви и ги разбърква, за да напечата с тях книга с примерни шрифтове.
                                 </p>
                                <p>В днешно време NCL (произнася се Ен Си Ел) са най-известни ...</p>
                                <a href="cruises.html" class="view-more">Виж повече<span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="single-ship">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="img-responsive" src="{!! asset('img/cruises-3.jpg') !!}"/>
                        </div>
                        <div class="col-md-6">
                            <h1>Речни круизи</h1>
                            <p>Lorem Ipsum е елементарен примерен текст, използван в печатарската и типографската индустрия.
                                <br><br>
                                Lorem Ipsum е индустриален стандарт от около 1500 година, когато неизвестен печатар взема няколко печатарски букви и ги разбърква, за да напечата с тях книга с примерни шрифтове.
                             </p>
                            <a href="cruises.html" class="view-more">Виж повече<span></span></a>
                        </div>
                    </div>
                </div>
                <div class="single-ship">
                    <div class="row">
                        <div class="col-md-6">
                            <img class="img-responsive" src="{!! asset('img/cruises-4.jpg') !!}"/>
                        </div>
                        <div class="col-md-6">
                            <h1>Речни круизи</h1>
                            <p>Lorem Ipsum е елементарен примерен текст, използван в печатарската и типографската индустрия.
                                <br><br>
                                Lorem Ipsum е индустриален стандарт от около 1500 година, когато неизвестен печатар взема няколко печатарски букви и ги разбърква, за да напечата с тях книга с примерни шрифтове.
                            </p>
                            <a href="cruises.html" class="view-more">Виж повече<span></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="background-white">
                <div class="container">
                    <div class="single-ship">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="img-responsive" src="{!! asset('img/cruises-5.jpg') !!}"/>
                            </div>
                            <div class="col-md-6">
                                <h1>Гурме круизи</h1>
                                <p>Lorem Ipsum е елементарен примерен текст, използван в печатарската и типографската индустрия.
                                    <br><br>
                                    Lorem Ipsum е индустриален стандарт от около 1500 година, когато неизвестен печатар взема няколко печатарски букви и ги разбърква, за да напечата с тях книга с примерни шрифтове.
                                </p>
                                <a href="cruises.html" class="view-more">Виж повече<span></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="single-ship">
                        <div class="row">
                            <div class="col-md-6">
                                <img class="img-responsive" src="{!! asset('img/cruises-6.jpg') !!}"/>
                            </div>
                            <div class="col-md-6">
                                <h1>Приключенски круизи</h1>
                                <p>Lorem Ipsum е елементарен примерен текст, използван в печатарската и типографската индустрия.
                                    <br><br>
                                    Lorem Ipsum е индустриален стандарт от около 1500 година, когато неизвестен печатар взема няколко печатарски букви и ги разбърква, за да напечата с тях книга с примерни шрифтове.
                                </p>
                                <a href="cruises.html" class="view-more">Виж повече<span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <a href="#" class="load-more">зареди още<span></span></a>
        </div>
        @include('components.footer-promotions')
@endsection
@section('page-scripts')
    <script>
        $(document).ready(function () {
            if ($(window).width() > 1024) {
                $('button').on('click',function(e) {
                    if ($(this).hasClass('swich-to-grid')) {
                        $('.single-ship').removeClass('list').addClass('grid');
                        $('.single-news').removeClass('list').addClass('grid');
                        $('button').removeClass('active')
                        $(this).addClass('active');
                    }
                    else if($(this).hasClass('swich-to-list')) {
                        $('.single-ship').removeClass('grid').addClass('list');
                        $('.single-news').removeClass('grid').addClass('list');
                        $('button').removeClass('active')
                        $(this).addClass('active');
                    }
                });
            }
        });
    </script>
@endsection
