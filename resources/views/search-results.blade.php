@extends('master')

@section('plugin-assets-css')
@endsection
@section('plugin-assets-js')
@endsection


@section('page-title', ' ')
@section('meta-description', ' ')

@section('content')

    <section>
        @include('components.search')
        <div class="background-white search-results">
            <div class="container">
                <p class="main-spacial-title">Оферти</p>
                <h2 class="main-spacial-subtitle">Резултати от търсене:<span class="search-reslt-count">Намерени резултати: {!! $numOfResults !!}</span>
                </h2>
            </div>

            <div class="container">
                @foreach($routeGroups as $oneGroup)
                    <div class="row">
                        @foreach($oneGroup as $oneRoute)
                            <div class="col-md-6">
                                <div class="single-offer-grid clearfix">

                                    <img title="" alt="" class="img-responsive"
                                         src="{!! asset(\App\RouteEntity::$path.$oneRoute['route_picture']) !!}">
                                    <div class="inner-text-wrapper">
                                        <h3>{!! $oneRoute['route_name'] !!}</h3>
                                        <p class="hotel-name">{!! @$oneRoute['ship_name'] !!}</p>
                                        <ul>
                                            <li class="offer-location">
                                                <span></span>от {!! @$oneRoute['first_point']['point_name'] !!}
                                                ({!! @$oneRoute['first_point']['point_country'] !!})
                                            </li>
                                            <li class="offer-dates">
                                                <span></span>{!! @$oneRoute['room_price_and_date']['route_date'] !!}</li>
                                            <li><a title="" href="{!! url('offer/'.$oneRoute['route_slug']) !!}"
                                                   class="veiw-more">Виж
                                                    повече</a></li>
                                        </ul>
                                        <p class="offer-price">
                                            <span class="euro-sight"></span>
                                            {!! @$oneRoute['room_price_and_date']['price'] !!}
                                            <span class="per-person">
										<span class="top-arrow"></span>
										на човек
									</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
        <div class="container">
            <a title="" href="#" class="load-more">зареди още<span></span></a>
        </div>
    </section>
    @include('components.footer-promotions')


@endsection
@section('page-scripts')
    <script src="js/scripts.js"></script>
@endsection
