@extends('master')

@section('plugin-assets-css')
@endsection
@section('plugin-assets-js')

@endsection


@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="inner-nav">
                    @foreach($breadcrumps as $onecrump)
                        <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div id="news-list-grid">
        <div class="background-white">
            <div class="container">
                <p class="main-spacial-title">Блог</p>
                <h2 class="main-spacial-subtitle">полезни съвети и интересни статии
                    <div class="swicher-wrapper">
                        <button class="swich-to-grid"><i class="fa fa-th"></i></button>
                        <button class="swich-to-list active"><i class="fa fa-list-ul"></i></button>
                    </div>
                </h2>
            </div>
        </div>
        <div class="container">

            <div class="col-md-9 news_list">

                @foreach($newsRaw as $oneGroup)
                    <div class="clearfix news_couple">
                        @foreach($oneGroup as $news)
                            <a title="" href="{!! url('news-inner/'.$news['slug_'.\App::getLocale()]) !!}">
                                <div class="single-news">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="img-wrapper">
                                                <img title="{!! $news['pic_title_'.\App::getLocale()] !!}"
                                                     alt="{!! $news['pic_alt_'.\App::getLocale()] !!}"
                                                     class="img-responsive"
                                                     src="{!! asset(\App\NewsEntity::$path.$news['picture']) !!}"/>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h3>{!! $news['title_'.\App::getLocale()] !!}</h3>
                                            <p>
                                                {!! $news['text_'.\App::getLocale()] !!}
                                            </p>
                                            <p class="view-more">Виж повече<span></span></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endforeach

                    </div>
                @endforeach

            </div>

            <div class="col-md-3">
                @include('components.sidebar-news',$sideBarData)
            </div>
        </div>
    </div>
    <a title="" href="" class="load-more" id="moreNewsLink">зареди още<span></span></a>


    <input type="hidden" id="newsListingData" data_news_criterion="{!! $jsParams['criterion'] !!}"
           data_news_criterion_val="{!! $jsParams['criterionVal'] !!}" data_news_count="{!! $jsParams['count'] !!}"
           value="criterion_data">


    @include('components.news-subscribe')


@endsection
@section('page-scripts')

    <script src="{!! asset('js/functions/newsOperations.js') !!}"></script>


@endsection
