@extends('master')

@section('plugin-assets-css')
@endsection
@section('plugin-assets-js')
@endsection


@section('page-title', ' ')
@section('meta-description', ' ')



@section('fb-title', $port['name_'.\App::getLocale()])
@section('fb-image', asset(\App\PointsEntity::$path.'1464339109Kfn1UuclxM.jpg'))
@section('fb-description',  $port['description_'.\App::getLocale()])

@section('content')

    <section>
        <div id="inner-port" class="hide-for-print" style="background-image: url({!! asset(\App\PointsEntity::$path.$port['picture']) !!})">
            <div class="gradient-slider"></div>
            <div class="container">
                <h3>
                    <strong>{!! $port['name_'.\App::getLocale()] !!}</strong> ({!! $port['country_'.\App::getLocale()] !!})
                </h3>
            </div>
        </div>
        <div class="light-blue">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <ul class="inner-nav">
                            <li><a title="" href="#">Начало</a></li>
                            <li><a title="" href="#">Круизни кораби</a></li>
                            <li>Преглед на кораб</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="coord_x" value="{!! @$coordinatesArr[0] !!}">
        <input type="hidden" name="coord_y" value="{!! @$coordinatesArr[1] !!}">

        <div class="container inner-port-text">
            <div class="row">
                <div class="col-md-12">
                    <div id="googleMap" style="width:600px;height:415px;float:left; margin:0 20px 20px 0"></div>
                    <script src="http://maps.googleapis.com/maps/api/js"></script>


                    <script>
                        function initialize() {

                            var coordX = parseFloat($('input[name="coord_x"]').val());
                            var coordY = parseFloat($('input[name="coord_y"]').val());

                            var myLatLng = {lat: coordX, lng: coordY};
                            var mapProp = {
                                center: myLatLng,
                                zoom: 15,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };
                            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);


                            var marker = new google.maps.Marker({
                                position: myLatLng,
                                map: map
                            });
                        }
                        google.maps.event.addDomListener(window, 'load', initialize);
                    </script>
                    <p>{!! $port['description_inner_'.\App::getLocale()] !!}</p>
                </div>
            </div>
        </div>
        <div class="container hide-for-print">
            <div id="cruises" class="background-white">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="main">Други круизи от това пристанище </h2>
                    </div>
                </div>
                @include('components.tab-cruiz',['routes'=>$routesWithThisPort])
            </div>
        </div>
    </section>
    @include('components.social-share')
    @include('components.footer-promotions')


@endsection
@section('page-scripts')
    <script src="js/scripts.js"></script>
@endsection
