@extends('master')

@section('plugin-assets-css')
    <link href="{!! asset('css/bootstrap-select.min.css') !!}" rel="stylesheet">
@endsection
@section('plugin-assets-js')
    <script src="{!! asset('js/bootstrap-select.min.js') !!}"></script>
    <script src="{!! asset('js/jquery.validate.min.js') !!}"></script>

@endsection


@section('page-title', ' ')
@section('meta-description', ' ')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="inner-nav">
                    @foreach($breadcrumps as $onecrump)
                        <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {!! Session::get('success') !!}
                    </div>
                @endif

                @if($errors -> any() )
                    <div class="alert alert-danger" style="width: 100%;">
                        @foreach ($errors->all() as $error)
                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
    {{--{!! dump($reservationData) !!}--}}
    <div class="background-white personal-data">
        @include('components.reservation-reveiw',['reservationData'=>$reservationData])
        <div class="reservation-form-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3>данни за резервация<span></span></h3>
                        <p class="red">Лице за контакт /титуляр:</p>
                        <p><em>* Моля, попълнете полетата на кирилица</em></p>
                    </div>
                </div>
            </div>
            {!! Form::open(array('method'=>'get','action'=>'OfferController@confirmReservation', 'class' => 'formPersonal')) !!}
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        {{ Form::text('titilar_fName', null, [ "placeholder"=>"Име:", "id"=>"titilar_fName"  ]) }}

                    </div>
                    <div class="col-md-4">
                        {{ Form::text('titilar_mName',null, [ "placeholder"=>"Презиме:", "id"=>"titilar_mName"  ]) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::text('titilar_lName', null, [ "placeholder"=>"Фамилия:", "id"=>"titilar_lName"  ]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        {{ Form::email('email',null, [ "placeholder"=>"E-mail:", "id"=>"email" ]) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::tel('phone', null, [ "placeholder"=>"Телефон:", "id"=>"phone" ]) }}
                    </div>
                    <div class="col-md-4">
                        <div class="date-picker-wrapper">
                            Дата на раждане:

                            {{ Form::select('titilar_dayBirth' , $birthComboToReturn['days'], null, ['class'=>'selectpicker' ]) }}

                            {{  Form::select('titilar_monthBirth' , $birthComboToReturn['months'], null, ['class'=>'selectpicker' ]) }}

                            {{ Form::select('titilar_yearBirth' , $birthComboToReturn['years'], null, ['class'=>'selectpicker' ]) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::textarea('titilar_comment', null, [ "id"=>"", "placeholder"=>"Въпроси и коментари:" ]) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-top: 30px;" class="red">Лица пътуващи във вътрешна каюта: </p>
                        <ul class="nubmers-of-person">
                            <li><span></span>{!! $reservationData['passengers'] !!} възрастен/и</li>
                        </ul>
                    </div>
                </div>
                @for($i=0;$i<$reservationData['passengers'];$i++)
                    <div class="row">
                        <div class="col-md-4">
                            {{--<input type="text" placeholder="Име:"/>--}}
                            {{ Form::text('additional_person_fName['.$i.']',null,['placeholder'=>'Име:']) }}
                        </div>
                        <div class="col-md-4">
                            {{--<input type="text" placeholder="Фамилия:"/>--}}
                            {{ Form::text('additional_person_lName['.$i.']',null,['placeholder'=>'Фамилия:']) }}
                        </div>
                        <div class="col-md-4">
                            <div class="date-picker-wrapper">
                                Дата на раждане:
                                {{  Form::select('additional_person_dayBirth['.$i.']' , $birthComboToReturn['days'], null, ['class'=>'selectpicker' ]) }}

                                {{ Form::select('additional_person_monthBirth['.$i.']' , $birthComboToReturn['months'], null, ['class'=>'selectpicker' ]) }}

                                {{ Form::select('additional_person_yearBirth['.$i.']' , $birthComboToReturn['years'], null, ['class'=>'selectpicker' ]) }}
                            </div>
                        </div>
                    </div>
                @endfor
                <div class="row">
                    <div class="col-md-12">
                        <div style="border-top:1px solid #d9dfe4;margin-top:30px;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox">
                            <input id="termsCheckbox" checked type="checkbox">
                            <label></label>
                        </div>
                        <a href="#" data-toggle="modal" class="terms" data-target="#terms-condition-footer">Съгласен съм и приемам <strong>Общите Условия!</strong></a>
                    </div>
                    <div class="col-md-6">
                        <a title="" class="submit-wrapper">
                            <div class="border"></div>
                            <input type="submit" class="submit" value="Резервирай">
                        </a>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
{{--    @include('components.social-share')--}}
    @include('components.footer-promotions')


@endsection
@section('page-scripts')
    <script src="{!! asset('js/scripts.js') !!}"></script>
    <script src="{!! asset('js/functions/personalDataValidator.js') !!}"></script>
    <script>
        $(document).ready(function () {
            $('.selectpicker').selectpicker({});
            disableSubmitButton();
        });

        function disableSubmitButton() {

            /*$('input[type="checkbox"]').change(function () {
                if ($(this).val != '') {
                    $('input[type="submit"]').removeAttr('disabled');
                }
            });*/


            // $('input[type="submit"]').attr('disabled', 'disabled');
            // $('.reservation-form-wrapper .submit-wrapper .border').addClass('dray-border');
            // $('.reservation-form-wrapper .submit').addClass('dray-submit');

            // $('#termsCheckbox').click(function () {
            //     if ($(this).is(':checked')) {
            //         $('.submit').removeAttr('disabled'); //enable input
            //         $('.reservation-form-wrapper .submit-wrapper .border').removeClass('dray-border');
            //         $('.reservation-form-wrapper .submit').removeClass('dray-submit');
            //     } else {
            //         $('.submit').attr('disabled', true); //disable input
            //         $('.reservation-form-wrapper .submit-wrapper .border').addClass('dray-border');
            //         $('.reservation-form-wrapper .submit').addClass('dray-submit');
            //     }
            // });





        }
    </script>
@endsection
