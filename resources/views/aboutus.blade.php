@extends('master')

@section('plugin-assets-css')
    <link href="{!! asset('css/bootstrap-select.min.css') !!}" rel="stylesheet">
@endsection
@section('plugin-assets-js')
    <script src="{!! asset('js/bootstrap-select.min.js') !!}"></script>
@endsection

@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])

@section('fb-title', $data['title1_bg'])
@section('fb-image', asset(\App\PageAboutEntity::$pathContentImg . $data['content_img']))
@section('fb-description',  $data['text_first_left_' . \App::getLocale()] )

@section('content')
    <div class="why-us-main-banner"
         style="background-image: url({!! asset(\App\PageAboutEntity::$pathSlideImg . $data['slide_img']) !!});">
        <div class="container">
            <div class="col-md-12">
                {!! $data['slide_text_' . \App::getLocale()] !!}
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="inner-nav">
                    @foreach($breadcrumps as $onecrump)
                        <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    </div>
    <div class="why-us-promo"
         style="background-image: url({!! asset(\App\PageAboutEntity::$pathContentImg . $data['content_img']) !!});">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <p class="title">За Нас</p>
                    <h2>{!! $data['title1_bg'] !!}</h2>
                    {!! $data['text_first_left_' . \App::getLocale()] !!}
                </div>
                <div class="col-md-5 col-md-offset-2">
                    <p class="title">Предимства</p>
                    <h2>{!! $data['title2_' . \App::getLocale()] !!}</h2>
                    @foreach($benefitsData as $oneData)
                        <div class="why-us-single">
                            <span style="background-image: url({!! asset(\App\AboutTextEntity::$pathPreDefinedRows . $oneData['picture']) !!});"></span>
                            <h3>{!! $oneData['text1_' . \App::getLocale()] !!}</h3>
                            <p>{!! $oneData['text2_' . \App::getLocale()] !!}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="container why-us-additional">
        <div class="row">
            <div class="col-md-12">
                <h4>{!! $data['title3_' . \App::getLocale()] !!}</h4>
                <ul>
                    @foreach($otherData as $oneData)
                        <li><span></span>{!! $oneData['text1_' . \App::getLocale()] !!}</li>
                    @endforeach
                    {!! $data['text_deduction_' . \App::getLocale()] !!}
                </ul>

            </div>
        </div>
    </div>

    @include('components.social-share')
    @include('components.news-subscribe')

@endsection
@section('page-scripts')
    <script src="http://maps.googleapis.com/maps/api/js"></script>

@endsection
