@extends('master')

@section('plugin-assets-css')
@endsection
@section('plugin-assets-js')
@endsection


@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])

@section('content')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="inner-nav">
                        @foreach($breadcrumps as $onecrump)
                            <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                        @endforeach

                    </ul>
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
        <div class="background-white promotion-main-wrapper">
            <div class="container">
                <h2 class="main-spacial-subtitle">Промоции:</h2>
            </div>

            <div class="container promotion_list">
                @foreach($results as $oneGroup)
                    <div class="row">

                        @foreach($oneGroup as $oneRoute)

                            <div class="col-md-4 one_promo_record">
                                <a title="" href="{!! 'offer/'.$oneRoute['route_slug'] !!}"
                                   class="single-promotion-wrapper clearfix">
                                    <div class="clearfix">
                                        <h3>{!! $oneRoute['route_name'] !!}</h3>
                                    </div>
                                    <div class="img-wrapper">
                                        <div class="top-label-img"><span>{!! $oneRoute['route_discount'] !!}% </span>
                                            <span>отстъпка</span></div>
                                        <div class="gradient"></div>
                                        <img title="" alt="" class="img-responsive"
                                             src="{!! asset(\App\RouteEntity::$path.$oneRoute['route_picture']) !!}">
                                        <ul>
                                            <li class="offer-dates"><span></span>{!! $oneRoute['route_start_date'] !!}
                                            </li>
                                            <li class="offer-location">
                                                <span></span>от {!! @$oneRoute['first_point']['name_'.\App::getLocale()].' ('.@$oneRoute['first_point']['country_'.\App::getLocale()].')' !!}
                                            </li>
                                            <li class="price"><span></span>{!! $oneRoute['room_price'] !!}</li>
                                        </ul>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                    </div>
                @endforeach

            </div>
        </div>
        <input type="hidden" id="topCruiseCountData" data_top_cruise_count="{!! $topRouteCount !!}"
               data_lang="{!! \App::getLocale() !!}" value="criterion_data">

        <div class="container">
            <a title="" href="#" class="load-more" id="load_more_top_routes">зареди още<span></span></a>
        </div>


    </section>
    @include('components.news-subscribe')


@endsection
@section('page-scripts')
    <script src="js/scripts.js"></script>
    <script src="{!! asset('js/functions/topRoutesOperations.js') !!}"></script>

@endsection
