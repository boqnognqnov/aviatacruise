@extends('master')

@section('plugin-assets-css')


@endsection

@section('plugin-assets-js')

@endsection


@section('page-title', $seoData['title'])
@section('meta-description', $seoData['description'])
{{--{!! dump($route) !!}--}}

{{--{!! dump(asset(\App\RouteEntity::$path.$route['picture'])) !!}--}}
@section('fb-title', $route['route_name'])
@section('fb-image', asset(\App\RouteEntity::$path.$route['picture']))



@section('content')


    @include('components.slide-offer',['gallery'=>$gallery,'route'=>$route,'ship'=>$ship,'company'=>$company])


    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="inner-nav">
                        @foreach($breadcrumps as $onecrump)
                            <li><a title="" href="{!! url($onecrump['url']) !!}">{!! $onecrump['title'] !!}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-6">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {!! Session::get('success') !!}
                        </div>
                    @endif

                    @if($errors -> any() )
                        <div class="alert alert-danger" style="width: 100%;">
                            @foreach ($errors->all() as $error)
                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="background-white map-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div id="map" style="height:320px"></div>
                        <div class="row map-tooltips">
                            <div class="col-md-4">
                                <a title="" class="map-aditional-info">
                                    <span data-toggle="tooltip" data-placement="top" class="inshurance"
                                          title="Предлагаме Ви качественo застраховане на едни от най-добрите цени за бранша"></span>застраховане
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a title="" class="map-aditional-info">
                                    <span data-toggle="tooltip" data-placement="top" class="transportation"
                                          title="Предлагаме Ви качественo застраховане на едни от най-добрите цени за бранша"></span>транспортиране
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a title="" class="map-aditional-info">
                                    <span data-toggle="tooltip" data-placement="top" class="settle"
                                          title="Предлагаме Ви качественo застраховане на едни от най-добрите цени за бранша"></span>настаняване
                                    на място
                                </a>
                            </div>
                        </div>
                        <div class="social-section">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="social-links">
                                        <li>СПОДЕЛИ:</li>

                                        <li><a title="" class="fb">
                                                <div id="fb-root"></div>
                                                <!-- Your share button code -->
                                                <div class="fb-share-button"

                                                     data-href="{!! Request::fullUrl() !!}"
                                                     data-layout="button_count">
                                                </div>
                                            </a></li>
                                            <li>
                                              <a href="https://twitter.com/share"  data-text="aaaa"  data-url="{!! Request::fullUrl() !!}"  class="twitter customer share tw"></a></li>
                                        <li><a title="" href="#" class="gp">
                                          <!-- Place this tag in your head or just before your close body tag. -->
                                          <div class="g-plus" data-action="share" data-annotation="none" data-href="{!! Request::fullUrl() !!}"></div>
                                            </a></li>
                                        <li><a title="" href="mailto:someone@example.com?Subject=Hello%20again"
                                               target="_top"
                                               class="mail"></a></li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <a title="" href="javascript:window.print()" class="print"> Принтирай</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p>{!! $route['route_description'] !!} </p>
                    </div>
                </div>

                <div class="offer-form-wrapper">
                    <h3>изберете дата на тръгване и вид каюта<span></span></h3>

                    {!! Form::open(array('method'=>'get','action'=>'OfferController@chooseCabinAndDate')) !!}
                    @foreach($reservationCases as $dateID=>$oneGroup)
                        <div class="row">
                            <div class="col-custom-1">
                                <div class="date-go">{!! $oneGroup['date'] !!}</div>
                            </div>
                            <div class="clearfix exrta-right">
                                @foreach($oneGroup['data'] as $roomID=>$route)

                                    <div class="col-custom-3">
                                        @if($route['total_price']!=0)
                                            <div class="radio clearfix">
                                                <div class="radio-btn-inner-wrapper">
                                                    {!! Form::radio('radio', $dateID.'_'.$roomID, null, [ "id"=>$dateID.'_'.$roomID ]) !!}
                                                    <label for="{!! $dateID.'_'.$roomID !!}">
                                                        <p>{!! $route['room_name'] !!}</p>
                                                        <span>{!! $route['room_discount'] !!}% отстъпка</span></label>
                                                </div>
                                                <div class="offer-price-inner">
                                                    <span class="euro-sight"></span>{!! $route['total_price'] !!}<span
                                                            class="per-person"><span
                                                                class="top-arrow"></span>на човек</span>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                @endforeach
                            </div>
                        </div>
                    @endforeach


                    <div class="row no-background">
                        <div class="col-md-7">
                            <p class="choose-disclimer">
                                * Посочените цени са стартови на човек, настанен в избран вид двойна каюта.
                            </p>
                        </div>
                        <div class="col-md-5">
                            <a title="" class="submit-wrapper">
                                <div class="border"></div>
                                <input type="submit" disabled="disabled" class="submit" value="Резервирай">
                            </a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="container">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs offer-nav-tabs" role="tablist">
                <li role="presentation" class="active"><a title="" href="#destination" aria-controls="destination"
                                                          role="tab"
                                                          data-toggle="tab" class="destination"><span></span>Маршрут</a>
                </li>
                <li role="presentation"><a title="" href="#ship" aria-controls="ship" role="tab" data-toggle="tab"
                                           class="ship"><span></span>кораб</a></li>
                <li role="presentation"><a title="" href="#cabins" aria-controls="cabins" role="tab" data-toggle="tab"
                                           class="cabins"><span></span>каюти</a></li>
                <li role="presentation"><a title="" href="#deck" aria-controls="deck" role="tab" data-toggle="tab"
                                           class="deck"><span></span>палуби</a></li>
                <li role="presentation"><a title="" href="#terms-condition" aria-controls="terms-condition" role="tab"
                                           data-toggle="tab" class="terms-condition"><span></span>общи условия</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="destination">
                    <table id="destination-table">
                        <tr>
                            <th><p></p></th>
                            <th><p> Ден</p></th>
                            <th><p>Точка</p></th>
                            <th><p>Държава</p></th>
                            <th><p>Пристигане</p></th>
                            <th><p>Тръгване</p></th>
                        </tr>
                        {{--<td><p><img title="" alt="" src="{!! asset('img/icons/plane-icon-table.png') !!}"/></p></td>--}}
                        @foreach($points as $onePoint)
                            <tr>
                                <td>
                                    <img title="" alt="" src="{!! asset(\App\PointTypeEntity::$path.$onePoint['point_type_picture']) !!}"/>
                                </td>
                                <td><p>{!! $onePoint['day'] !!}</p></td>
                                <td><p>{!! $onePoint['name'] !!}</p></td>
                                <td><p>{!! $onePoint['country'] !!}</p></td>
                                <td><p>{!! $onePoint['time_arrival'] !!}</p></td>
                                <td><p>{!! $onePoint['time_departure'] !!}</p></td>
                            </tr>
                        @endforeach

                    </table>
                    <div id="destination-table-mobile">
                        @foreach($points as $onePoint)
                            <h3>
                                <div class="row">
                                    <div class="col-xs-4 text-right">
                                        Ден:
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        {!! $onePoint['day'] !!}
                                    </div>
                                    <div class="col-xs-4 text-left">
                                        <img title="" alt="" src="{!! asset(\App\PointTypeEntity::$path.$onePoint['point_type_picture']) !!}"/>
                                    </div>
                                </div>
                            </h3>
                            <div>
                                <div class="row">
                                    <div class="col-xs-6 text-center">
                                        Точка
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        {!! $onePoint['name'] !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 text-center">
                                        Държава
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        {!! $onePoint['country'] !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 text-center">
                                        Пристигане
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        {!! $onePoint['time_arrival'] !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 text-center">
                                        Тръгване
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        {!! $onePoint['time_departure'] !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="ship">
                    @include('components.tab-ship',['ship'=>$ship])
                </div>
                <div role="tabpanel" class="tab-pane" id="cabins">
                    @include('components.tab-cabbins',['cabins'=>$cabins])
                </div>
                <div role="tabpanel" class="tab-pane" id="deck">
                    @include('components.tab-decks',['decs'=>$decs])
                </div>
                <div role="tabpanel" class="tab-pane" id="terms-condition">
                    @include('components.tab-terms',['company'=>$company])
                </div>
            </div>
        </div>
    </section>
    @include('components.footer-promotions')

    @foreach($points as $onePoint)
        <input type="hidden" data_coord_x="{!! $onePoint['coordinate_x'] !!}"
               data_coord_y="{!! $onePoint['coordinate_y'] !!}"
               class="points_data">
    @endforeach


@endsection
@section('page-scripts')
    <script src="{!! asset('js/scripts.js') !!}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCMhsT9JBWJLRLmewKHcHU3flSl09K6obY">
    </script>
    <script type="text/javascript" src="{!! asset('js/markerwithlabel.js') !!}"></script>
    <style>
        .labels {
            color: white;
            font-family: "Lucida Grande", "Arial", sans-serif;
            font-size: 10px;
            text-align: center;
            width: 18px;
            height: 18px;
            line-height: 19px;
            background: #F34B43;
            border-radius: 50%;
            white-space: nowrap;
        }
    </style>
    <script>
        function initMap() {

//            var allMarkers = [
//                {lat: 43.208852, lng: 27.909276},
//                {lat: 24.108789, lng: 54.522845},
//                {lat: 26.847073, lng: 54.798350},
//                {lat: 25.092417, lng: 55.392635},
//                {lat: 29.092417, lng: 58.392635}
//            ];


            var allMarkers = [];

            $(".points_data").each(function () {
                var tempObj = {
                    lat: parseFloat($(this).attr('data_coord_x')),
                    lng: parseFloat($(this).attr('data_coord_y'))
                };

                allMarkers.push(tempObj);
            });

            var isDraggable = $(document).width() > 480 ? true : false;
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 3,
                draggable: isDraggable,
                scrollwheel: false,
                center: {lat: 32, lng: 35},
                mapTypeId: google.maps.MapTypeId.TERRAIN
            });


            var road = [];
            for (i = 0; allMarkers.length > i; i++) {
                var marker = new MarkerWithLabel({
                    position: allMarkers[i],
                    map: map,
                    title: 'Hello World!',
                    labelContent: i + 1,
                    labelAnchor: new google.maps.Point(9, 38),
                    labelClass: "labels", // the CSS class for the label
                    labelInBackground: false
                });

                road.push(allMarkers[i]);

            }
            var flightPlanCoordinates = road;

            var flightPath = new google.maps.Polyline({
                path: flightPlanCoordinates,
                geodesic: true,
                strokeColor: '#57A4DA',
                strokeOpacity: 1.0,
                strokeWeight: 2
            });

            flightPath.setMap(map);
        }
        google.maps.event.addDomListener(window, 'load', initMap);
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $("input[type='radio']").change(function () {
                $("input[type='submit']").prop("disabled", false);
                if ($("input[type='submit']").prop("disabled", false)) {
                    $('.offer-form-wrapper .submit-wrapper .border').removeClass('dray-border');
                    $('.offer-form-wrapper .submit').removeClass('dray-submit');
                }

            });
            if ($("input[type='submit']").prop("disabled", true)) {
                $('.offer-form-wrapper .submit-wrapper .border').addClass('dray-border');
                $('.offer-form-wrapper .submit').addClass('dray-submit');
            }
        });

    </script>
@endsection
