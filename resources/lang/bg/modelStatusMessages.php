<?php

//trans('modelStatusMessages.default')

return [

//  ADMIN CONTROLLERS STATUS MESSAGES
    'creatingError' => 'Грешка в записа/ редакцията на елемента',
    'imageFailed' => 'Системна грешка. Моля свържете се със наш сътрудник',
    'successMessage' => 'Успешна промяна на записа ',
    'default' => 'Системна грешка. Моля свържете се със наш сътрудник',

//  CLIENT CONTROLLERS STATUS MESSAGES
    'reservation_step_1_room_not_selected' => 'Моля изберете каюта в определена дата',


];
