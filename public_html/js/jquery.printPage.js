<div class="social-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul class="social-links">
                    <li>СПОДЕЛИ:</li>
                    <li><a href="#" class="fb"><div class="fb-share-button" data-href="#" data-layout="icon" data-mobile-iframe="true"></div></a></li>
                    <li><a href="https://twitter.com/intent/tweet?text=[tweettext]&url=[currenturl]&via=[twitterusername]" class="tw"></a></li>
                    <li><a href="#" class="gp"><div class="g-plusone"></div></a></li>
                    <li><a href="mailto:someone@example.com?Subject=Hello%20again" target="_top" class="mail"></a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <!--<a href="javascript:window.print()" class="print"> Принтирай</a>-->
                <a class="print" >Принтирай</a>
            </div>
        </div>
    </div>
</div>
<!--<iframe id="ifmcontentstoprint" style="height: 0px; width: 0px; position: absolute"></iframe>

<script src="{!! asset('js/html2canvas.min.js') !!}"></script>
<script src="{!! asset('js/html2canvas.svg.min.js') !!}"></script>
<script>
 $(document).ready(function() {

    $('.print').on('click', function(){

        html2canvas(document.body, {
          onrendered: function(canvas) {
            document.body.appendChild(canvas);
          }
        });

    })

 });
 function putInIframe(){
    var content = document.getElementsByTagName("canvas")[0];
    var pri = document.getElementById("ifmcontentstoprint").contentWindow.document;
    pri.open();
    console.log(content);
    pri.writeln(content);
    pri.close();
    pri.focus();
    pri.print();
}
</script>-!>
