/**
 * Created by B_OCS on 5/18/2016.
 */



$(document).ready(function () {


    $("#loadMoreOnThisType").on("click", function (event) {
        event.preventDefault();
        getMoreOnThisType();
    });


});

function getMoreOnThisType() {

    var count = $('#routeOnTypeCountData').attr('data_routes_in_this_type_count');
    var currentSlug = $('#routeOnTypeCountData').attr('data_route_type_slug');

    count = parseInt(count);

    var data = {
        count: count,
        currentSlug: currentSlug
    };


    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="jsToken"]').val()}});
    $.ajax({
        url: "/inner-cruize-type/js_listing",
        type: "POST",
        dataType: "json",
        data: data
    }).done(function (result) {
        // console.log(JSON.stringify(result));
        // alert('success');
        var itemList = result['items'];
        console.log(JSON.stringify(itemList));


        var countTemp = parseInt($('#routeOnTypeCountData').attr('data_routes_in_this_type_count'));

        var htmlElement = null;


        $.each(itemList, function (index, oneItem) {
            countTemp++;
            var itemHtml = '';

            if ((parseInt(countTemp)) % 2 != 0) {
                $('#containerWithRoutes').append($('<div>')
                    .addClass('row'));
            }

            htmlElement = $('#containerWithRoutes').find('.row').last();
            // console.log(htmlElement);
            // htmlElement.append($('<div>')
            //     .addClass('col-md-6')
            //     .append($('<div>')
            //         .addClass('single-offer-grid clearfix')
            //         .append($('<img>')
            //             .addClass('img-responsive')
            //             .attr('src', "../../../../../uploads/images/routes/" + oneItem['picture'])
            //         ).append($('<div>')
            //             .addClass('inner-text-wrapper')
            //             .append($('<h1>')
            //                 .text(oneItem['name']))
            //             .append($('<p>')
            //                 .addClass('hotel-name')
            //                 .text(oneItem['company_name']))
            //             .append($('<ul>')
            //                 .append($('<li>')
            //                     .addClass('offer-location')
            //                     .text(oneItem['first_point']['name_bg']))
            //             ))));

            itemHtml += '<div class="col-md-6">';
            itemHtml += '<div class="single-offer-grid clearfix">';
            itemHtml += '<a href="../../../../offer/' + oneItem['slug'] +'">' ;
            itemHtml += '<img alt="" title="" class="img-responsive" src="../../../../../uploads/images/routes/' + oneItem['picture'] + '"/>';

            itemHtml += '<div class="inner-text-wrapper">';
            itemHtml += '<h3>' + oneItem['name'];
            itemHtml += '</h3>';
            itemHtml += '<p class="hotel-name">' + oneItem['company_name'];
            itemHtml += '</p>';

            itemHtml += '<ul>';

            itemHtml += '<li class="offer-location"><span></span>' + oneItem['first_point']['name_bg'];
            itemHtml += '</li>';

            itemHtml += '<li class="offer-dates"><span></span>' + oneItem['room_price_and_date']['route_date'];
            itemHtml += '</li>';

            itemHtml += '<li><span href="../../../../offer/' + oneItem['slug'] + '" class="veiw-more">' + 'Виж повече' + '<span></span></span>';
            itemHtml += '</li>';
            itemHtml += '</ul>';

            itemHtml += '<p class="offer-price">';
            itemHtml += '<span class="euro-sight"></span>';
            itemHtml += oneItem['room_price_and_date']['price'];
            itemHtml += '<span class="per-person">';
            itemHtml += '<span class="top-arrow"></span>';
            itemHtml += 'на човек';
            itemHtml += '</span>';
            itemHtml += '</p>';

            itemHtml += '</div>';
            itemHtml += '</a>';
            itemHtml += '</div>';
            itemHtml += '</div>';

            htmlElement.append(itemHtml);
        });

        count = count + parseInt(itemList.length);

        $('#routeOnTypeCountData').attr('data_routes_in_this_type_count', count);


    }).fail(function (result) {

    })
}
