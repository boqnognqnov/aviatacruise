/**
 * Created by B_OCS on 5/18/2016.
 */



$(document).ready(function () {


    $("#load_more_top_routes").on("click", function (event) {
        event.preventDefault();
        getMoreOnThisType();
    });


});

function getMoreOnThisType() {

    var count = $('#topCruiseCountData').attr('data_top_cruise_count');


    count = parseInt(count);

    var data = {
        count: count
    };


    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="jsToken"]').val()}});
    $.ajax({
        url: "/top-cruises/js_listing",
        type: "POST",
        dataType: "json",
        data: data
    }).done(function (result) {

        var itemList = result['items'];
        console.log(JSON.stringify(itemList));


        var htmlToAppend = '';

        $.each(itemList, function (index, oneItem) {

            var lastPromoGroup = $('.promotion_list').find('.row').last();
            var elementsInLastRow = lastPromoGroup.find('.one_promo_record').length;

            var htmlToAppend = '';

            if (elementsInLastRow == 2) {
                $('.promotion_list').append($('<div>')
                    .addClass('row'));
            }


            htmlToAppend += '<div class="col-md-4 one_promo_record">';
            htmlToAppend += '<a title="" href="../../../../../../offer/' + oneItem['route_slug'] + '" class="single-promotion-wrapper clearfix">';
            htmlToAppend += '<div class="clearfix"> <h3>' + oneItem['route_name'] + '</h3> </div>';
            htmlToAppend += '<div class="img-wrapper">';
            htmlToAppend += '<div class="top-label-img"><span>' + oneItem['route_discount'] + '% </span> <span>отстъпка</span></div> <div class="gradient"></div>';
            htmlToAppend += '<img alt="" title="" class="img-responsive" src="../../../../uploads/images/routes/' + oneItem['route_picture'] + '">';
            htmlToAppend += '<ul>';
            htmlToAppend += '<li class="offer-dates"><span></span>' + oneItem['route_start_date'] + '</li>';
            htmlToAppend += '<li class="offer-location"> <span></span>от ' + oneItem['first_point']['name_' + $('#topCruiseCountData').attr('data_lang')] + ' (' + oneItem['first_point']['country_' + $('#topCruiseCountData').attr('data_lang')] + ') </li>';
            htmlToAppend += '<li class="price"><span></span>' + oneItem['room_price'] + '</li>';
            htmlToAppend += '</ul>';
            htmlToAppend += '</div>';
            htmlToAppend += '</a>';
            htmlToAppend += '</div>';

            lastPromoGroup.append(htmlToAppend);
        });

        count = count + parseInt(itemList.length);

        $('#topCruiseCountData').attr('data_top_cruise_count', count);

    }).fail(function (result) {

    })
}
