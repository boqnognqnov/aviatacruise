var routeTypeId = 0;
var portId = 0;
var duration = 0;
var destinationId = 0;
var dateMonth = 0;
var companyId = 0;
var shipId = 0;

var start_date = 0;
var end_date = 0;


$(document).ready(function () {
    getSearchSessionInputs();
    routeTypeClickEvent();
});
function getSearchSessionInputs() {
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="jsToken"]').val()}});
    $.ajax({
        url: "/search/getSession",
        type: "POST",
        dataType: "json"
    }).done(function (result) {

        var resultData = result['sessionData'];

        if (resultData['routeTypeId'] != undefined) {
            routeTypeId = resultData['routeTypeId'];
        }
        if (resultData['portId'] != undefined) {
            portId = resultData['portId'];
        }
        if (resultData['duration'] != undefined) {
            duration = resultData['duration'];
        }

        if (resultData['destinationId'] != undefined) {
            destinationId = resultData['destinationId'];
        }

        if (resultData['dateMonth'] != undefined) {
            dateMonth = resultData['dateMonth'];
        }

        if (resultData['companyId'] != undefined) {
            companyId = resultData['companyId'];
        }

        if (resultData['shipId'] != undefined) {
            shipId = resultData['shipId'];
        }
        if (resultData['start_date'] != undefined) {
            start_date = resultData['start_date'];
        }
        if (resultData['end_date'] != undefined) {
            end_date = resultData['end_date'];
        }
         console.log(start_date);
         console.log(end_date);
        listInputs();

    }).fail(function (result) {

    })
}

function listInputs() {
    var data = {
        routeTypeId: routeTypeId,
        portId: portId
    };
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="jsToken"]').val()}});
    $.ajax({
        url: "/search/listInputs",
        type: "POST",
        dataType: "json",
        data: data
    }).done(function (result) {
        var resultData = result['inputs'];
        // $('.selectpicker').empty();

        var routeTypes = resultData['routeTypes'];
        $('#search_route_type').empty();
        //$("[data-id='search_route_type']").find('.filter-option').empty();
        $.each(routeTypes, function (key, value) {
            var selected = false;
            if (key == routeTypeId) {
                selected = true;
            }
            $('#search_route_type')
                .append($("<option></option>")
                    .attr("value", key)
                    .prop("selected", selected)
                    .text(value));
            //    $("[data-id='search_route_type']").find('.filter-option').text(value);
            console.log('value' + value)
        });


        var routeDurations = resultData['durations'];
        console.log(resultData['durations']);
        console.log(routeTypeId);

        $('#search_route_durations').empty();
        var durationStr = '';
        $.each(routeDurations, function (key, value) {
            // console.log(value);
            var selected = false;
            if (key == duration) {
                selected = true;
            }
            // durationStr += '<option value="' + key + '" "selected"=' + selected + '>' + value + '</option>';
            $('#search_route_durations')
                .append($("<option></option>")
                    .attr("value", key)
                    .prop("selected", selected)
                    .text(value));
        });
        // $('#search_route_durations').append(durationStr);


        var destinations = resultData['destinations'];
        $('#search_route_destinations').empty();
        $.each(destinations, function (key, value) {
            var selected = false;
            if (key == destinationId) {
                selected = true;
            }
            $('#search_route_destinations')
                .append($("<option></option>")
                    .attr("value", key)
                    .prop("selected", selected)
                    .text(value));
        });

        // dsadsadasd
        var dates = resultData['dates'];
        $('#search_route_date').empty();
        $.each(dates, function (key, value) {
            var selected = false;
            if (key == dateMonth) {
                selected = true;
            }
            $('#search_route_date')
                .append($("<option></option>")
                    .attr("value", key)
                    .prop("selected", selected)
                    .text(value));
        });
        if(start_date!==0){
            $('.datepicker-start').attr('value', start_date);
        }
        if(end_date!==0){
            $('.datepicker-end').attr('value', end_date);
        }
        var ports = resultData['ports'];
        $('#search_route_ports').empty();
        $.each(ports, function (key, value) {
            var selected = false;
            if (key == portId) {
                selected = true;
            }
            $('#search_route_ports')
                .append($("<option></option>")
                    .attr("value", key)
                    .prop("selected", selected)
                    .text(value));
        });


        var companies = resultData['companies'];
        $('#search_route_companies').empty();
        $.each(companies, function (key, value) {
            var selected = false;
            if (key == companyId) {
                selected = true;
            }
            $('#search_route_companies')
                .append($("<option></option>")
                    .attr("value", key)
                    .prop("selected", selected)
                    .text(value));
        });

        var ships = resultData['ships'];
        $('#search_route_ships').empty();
        $.each(ships, function (key, value) {
            var selected = false;
            if (key == shipId) {
                selected = true;
            }
            $('#search_route_ships')
                .append($("<option></option>")
                    .attr("value", key)
                    .prop("selected", selected)
                    .text(value));
        });
        $('.selectpicker').selectpicker('refresh');
    }).fail(function (result) {

    })
}

function routeTypeClickEvent() {
    $("#search_route_type").on("click change", function (event) {
        // $('.selectpicker').empty();
        routeTypeId = $(this).val();
        listInputs();
    });
}
