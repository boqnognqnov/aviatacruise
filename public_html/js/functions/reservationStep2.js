$(document).ready(function () {
    var selectedRoom = $('.selectRoomRadio input:radio');
    console.log(selectedRoom);

    var counter = 0;
    var selectedRadio = 0;
    selectedRoom.each(function () {

        if ($(this).is(':checked')) {
            selectedRadio = counter;
        }
        counter++;

    });


    $("#accordion-choose-cabbin").accordion({
        collapsible: true,
        active: selectedRadio
    });
    $('#accordion-choose-cabbin h3').click(function (e) {
        $('#accordion-choose-cabbin').find('input:radio').prop('checked', false);
        $(this).find('input:radio').prop('checked', true);
        defineChangeRoom();
    });

    definePassengersChange();
    definePromoPacksChange();


});

function definePassengersChange() {
    $('#choose-cabbin-form .passengersRadio :radio').click(function () {
        var roomId = $(this).attr('data_room_id');
        collectPriceInfo(roomId);

    });
}

function definePromoPacksChange() {
    $('#choose-cabbin-form .promoPackTypesClass :checkbox').click(function () {
        var roomId = $(this).attr('data_room_id');
        collectPriceInfo(roomId);
    });
}

function defineChangeRoom() {
    // $('.passengersRadio input:radio').removeAttr('checked');
    $('.oneCabinRow .infoTypesClass input:checkbox').removeAttr('checked');
    $('.oneCabinRow .promoPackTypesClass input:checkbox').removeAttr('checked');
}

function collectPriceInfo(roomId) {
    // alert(roomId);
    var passengersCount = 1;
    if ($('#oneCabin' + roomId + '_id .passengersRadio :checkbox').is(':checked')) {
        passengersCount = 2;
    }

    var checkedPromoPacks = [];
    $('#oneCabin' + roomId + '_id .promoPackTypesClass input:checked').each(function () {
        console.log($(this).val());
        checkedPromoPacks.push($(this).val());

    });
    console.log(JSON.stringify(checkedPromoPacks));

    var data = {
        roomId: roomId,
        passengersCount: passengersCount,
        checkedPromoPackIds: checkedPromoPacks
    };
    calculateAndPrintPrice(data);
}

function calculateAndPrintPrice(dataToReturn) {
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="jsToken"]').val()}});
    $.ajax({
        url: "/choose-cabbin/calculateJs",
        type: "POST",
        dataType: "json",
        data: dataToReturn
    }).done(function (result) {
        console.log(JSON.stringify(result));
        var data = result['data'];

        $('#cabinTotalPrice' + dataToReturn['roomId']).text(data['totalRoomPrice']);
        $('#oneCabin' + dataToReturn['roomId'] + '_id .onlyRoomClass').text(data['priceOnlyRoom']);
    }).fail(function (result) {

    });
}

