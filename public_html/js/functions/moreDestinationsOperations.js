/**
 * Created by B_OCS on 5/18/2016.
 */



$(document).ready(function () {


    $("#load_more_destinations").on("click", function (event) {
        event.preventDefault();
        getMoreOnThisType();
    });


});

function getMoreOnThisType() {

    var count = $('#destinationsCountData').attr('data_destinations_count');


    count = parseInt(count);

    var data = {
        count: count
    };


    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="jsToken"]').val()}});
    $.ajax({
        url: "/destinations/js_listing",
        type: "POST",
        dataType: "json",
        data: data
    }).done(function (result) {

        var itemList = result['items'];
        console.log(JSON.stringify(itemList));


        var htmlToAppend = '';

        $.each(itemList, function (index, oneItem) {

            var lastPromoGroup = $('.destGroups').find('.row').last();
            var elementsInLastRow = lastPromoGroup.find('.oneDestRec').length;

            var htmlToAppend = '';

            if (elementsInLastRow == 1) {
                $('.destGroups').append($('<div>')
                    .addClass('row'));
            }

            htmlToAppend += '<a title="" href="../../../../inner-destinations/' + oneItem['dest_slug'] + '">';
            htmlToAppend += '<div class="col-md-6 oneDestRec">';
            htmlToAppend += ' <div class="single-destination-main">';
            htmlToAppend += '<img alt="" title="" class="img-responsive" src="../../../../uploads/images/destinations/' + oneItem['picture'] + '" >';
            htmlToAppend += '<h4>' + oneItem['dest_name'] + '</h4>';
            htmlToAppend += ' <p>' + oneItem['dest_description'] + '</p>';
            htmlToAppend += ' <p class="view-more">Виж повече<span></span></p>';

            htmlToAppend += '</div>';
            htmlToAppend += '</div>';
            htmlToAppend += '</a>';

            lastPromoGroup.append(htmlToAppend);
        });

        count = count + parseInt(itemList.length);

        $('#destinationsCountData').attr('data_destinations_count', count);

    }).fail(function (result) {

    })
}
