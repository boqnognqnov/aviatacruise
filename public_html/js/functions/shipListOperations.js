/**
 * Created by B_OCS on 5/18/2016.
 */



$(document).ready(function () {

    defineSortShips();
    $("#moreShipsLink").on("click", function (event) {
        event.preventDefault();
        getMoreShips();
    });


});

function getMoreShips() {

    var count = $('#shipCountData').attr('data_ships_count');

    count = parseInt(count);

    var data = {
        count: count
    };


    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="jsToken"]').val()}});
    $.ajax({
        url: "/ship-list/js_listing",
        type: "POST",
        dataType: "json",
        data: data
    }).done(function (result) {
        console.log(JSON.stringify(result));
        // alert('success');
        var itemList = result['items'];
        var lastCoupleDiv = null;

        var newsRecordStr = '';


        var countTemp = parseInt($('#shipCountData').attr('data_ships_count'));

        $.each(itemList, function (index, oneItem) {

            countTemp++;

            newsRecordStr = '<a title="" href="../../../../ship/' + oneItem['slug'] + '">';

            newsRecordStr += '<div class="single-ship">';
            newsRecordStr += '<div class="row">';
            newsRecordStr += '<div class="col-md-6">';
            newsRecordStr += '<div class="img-wrapper">';
            newsRecordStr += '<img alt="" title="" class="img-responsive" src="uploads/images/ships/' + oneItem['picture'] + '"/>';
            newsRecordStr += '</div>';
            newsRecordStr += '</div>';

            newsRecordStr += '<div class="col-md-6">';
            newsRecordStr += '<h3>' + oneItem['name'] + '</h3>';

            newsRecordStr += '<p>' + oneItem['text'] + '</p>';

            // newsRecordStr += '<a title="" href="../../../../news-inner/' + oneNews['slug'] + '" class="view-more">Виж повече<span></span></a>';
            newsRecordStr += '<p class="view-more">Виж повече<span></span></p>';

            newsRecordStr += '</div>';
            newsRecordStr += '</div>';
            newsRecordStr += '</div>';
            newsRecordStr += '</a>';


            if ((parseInt(countTemp)) % 2 == 0) {
                lastCoupleDiv = $('.ship-list').find('.ships_couple').last();
                lastCoupleDiv.append(newsRecordStr);
            } else {
                newsRecordStr = '<div class="container ships_couple">' +
                    newsRecordStr +
                    '</div>';
                $('.ship-list').append(newsRecordStr);
            }


        });

        count = count + parseInt(itemList.length);
        $('#shipCountData').attr('data_ships_count', count);

        defineSortShips();

    }).fail(function (result) {

    })
}

function defineSortShips() {
    if ($(window).width() > 1024) {
        $('button').on('click', function (e) {
            if ($(this).hasClass('swich-to-grid')) {
                $('.single-ship').removeClass('list').addClass('grid');
                $('.single-news').removeClass('list').addClass('grid');
                $('button').removeClass('active')
                $(this).addClass('active');
            }
            else if ($(this).hasClass('swich-to-list')) {
                $('.single-ship').removeClass('grid').addClass('list');
                $('.single-news').removeClass('grid').addClass('list');
                $('button').removeClass('active')
                $(this).addClass('active');
            }
        });
    }
}
