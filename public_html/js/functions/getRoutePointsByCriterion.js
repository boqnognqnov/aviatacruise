/**
 * Created by B_OCS on 5/13/2016.
 */


$(document).ready(function () {
    getRoutePoints();
});

$('input[name="point_name"]').on('click change keyup', function (event) {
    getRoutePoints();
});

function getRoutePoints(data) {

    var pointName = $('input[name="point_name"]').val();
    var routeId = $('input[name="route_id"]').val();

    var data = {
        point_name: pointName
    }
    var token = $('input[name="token_global"]').val();

    $.ajaxSetup({headers: {'X-CSRF-TOKEN': token}});
    $.ajax({
        url: "/api/getRoutePoints",
        type: "POST",
        dataType: "json",
        data: data
    }).done(function (result) {
        var pointList = result['data'];
        console.log(JSON.stringify(pointList));

        $('.pointListClass').empty();
        var pointsToAppend = '';
        $.each(pointList, function (index, value) {
            pointsToAppend += '<a href="../../../../../../admin/routes-points/set_point_form/' + routeId + '/' + value['id'] + '" class="btn btn-primary">' + value["name_bg"] + '</a>'
        });

        $('.pointListClass').html(pointsToAppend);
    }).fail(function (result) {
//            alert('error');
    })
}
