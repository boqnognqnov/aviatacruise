/**
 * Created by B_OCS on 5/18/2016.
 */



$(document).ready(function () {

    defineSortNews();
    $("#moreNewsLink").on("click", function (event) {
        event.preventDefault();
        getMoreNews();
    });


});

function getMoreNews() {

    var criterion = $('#newsListingData').attr('data_news_criterion');
    var criterionVal = $('#newsListingData').attr('data_news_criterion_val');
    var count = $('#newsListingData').attr('data_news_count');
    count = parseInt(count);

    var data = {
        criterion: criterion,
        criterionVal: criterionVal,
        count: count
    };


    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="jsToken"]').val()}});
    $.ajax({
        url: "/news/js_listing",
        type: "POST",
        dataType: "json",
        data: data
    }).done(function (result) {
        console.log(JSON.stringify(result));
        // alert('success');
        var newsList = result['news'];
        var lastCoupleDiv = null;

        var newsRecordStr = '';


        var countTemp = parseInt($('#newsListingData').attr('data_news_count'));

        $.each(newsList, function (index, oneNews) {

            countTemp++;

            newsRecordStr = '<a title="" href="../../../../news-inner/' + oneNews['slug'] + '">';

            newsRecordStr += '<div class="single-news">';
            newsRecordStr += '<div class="row">';
            newsRecordStr += '<div class="col-md-6">';
            newsRecordStr += '<div class="img-wrapper">';
            newsRecordStr += '<img alt="" title="" class="img-responsive" src="uploads/images/news/' + oneNews['picture'] + '"/>';
            newsRecordStr += '</div>';
            newsRecordStr += '</div>';

            newsRecordStr += '<div class="col-md-6">';
            newsRecordStr += '<h3>' + oneNews['title'] + '</h3>';

            newsRecordStr += '<p>' + oneNews['text'] + '</p>';

            newsRecordStr += '<p class="view-more">Виж повече<span></span></p>';

            newsRecordStr += '</div>';
            newsRecordStr += '</div>';
            newsRecordStr += '</div>';
            newsRecordStr += '</a>';


            if ((parseInt(countTemp)) % 2 == 0) {
                lastCoupleDiv = $('.news_list').find('.news_couple').last();
                lastCoupleDiv.append(newsRecordStr);
            } else {
                newsRecordStr = '<div class="clearfix news_couple">' +
                    newsRecordStr +
                    '</div>';
                $('.news_list').append(newsRecordStr);
            }


        });

        count = count + parseInt(newsList.length);
        $('#newsListingData').attr('data_news_count', count);

        defineSortNews();

    }).fail(function (result) {

    })
}

function defineSortNews() {
    if ($(window).width() > 1024) {
        $('button').on('click', function (e) {
            if ($(this).hasClass('swich-to-grid')) {
                $('.single-ship').removeClass('list').addClass('grid');
                $('.single-news').removeClass('list').addClass('grid');
                $('button').removeClass('active')
                $(this).addClass('active');
            }
            else if ($(this).hasClass('swich-to-list')) {
                $('.single-ship').removeClass('grid').addClass('list');
                $('.single-news').removeClass('grid').addClass('list');
                $('button').removeClass('active')
                $(this).addClass('active');
            }
        });
    }
}
