/**
 * Created by B_OCS on 5/18/2016.
 */



$(document).ready(function () {


    $("#load_more_routes").on("click", function (event) {
        event.preventDefault();
        getMoreOnThisType();
    });


});

function getMoreOnThisType() {

    var count = $('#routesCountData').attr('data_routes_count');
    var destinationId = $('#routesCountData').attr('data_destination_id');


    count = parseInt(count);

    var data = {
        count: count,
        destinationId: destinationId
    };


    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="jsToken"]').val()}});
    $.ajax({
        url: "/inner-destinations/more_routes_js_listing",
        type: "POST",
        dataType: "json",
        data: data
    }).done(function (result) {

        var itemList = result['items'];
        console.log(JSON.stringify(itemList));


        var htmlToAppend = '';

        $.each(itemList, function (index, oneItem) {

            var lastPromoGroup = $('.routeGroups').find('.row').last();
            var elementsInLastRow = lastPromoGroup.find('.oneRouteRec').length;

            var htmlToAppend = '';

            if (elementsInLastRow == 1) {
                $('.routeGroups').append($('<div>')
                    .addClass('row'));
            }


            htmlToAppend += '<div class="col-md-6 oneRouteRec">';
            htmlToAppend += '<div class="single-offer-grid clearfix">';
            htmlToAppend += '<a title="" href="../../../../../offer/' + oneItem['route_slug'] + '">'
            htmlToAppend += '<img alt="" title="" class="img-responsive" src="../../../../uploads/images/routes/' + oneItem['picture'] + '">';
            htmlToAppend += '<div class="inner-text-wrapper">';
            htmlToAppend += '<h3>' + oneItem['route_name'] + '</h3>';
            htmlToAppend += '<p class="hotel-name">' + oneItem['ship_name'] + '</p>';
            htmlToAppend += '<ul>';
            htmlToAppend += '<li class="offer-location">';
            htmlToAppend += '<span></span>от ' + oneItem['first_point']['point_name'];
            htmlToAppend += '</li>';

            htmlToAppend += '<li class="offer-dates">';
            htmlToAppend += '<span></span>от ' + oneItem['room_price_and_date']['route_date'];

            htmlToAppend += '</li>';
            htmlToAppend += '<p class="veiw-more">Виж повече<span></span></p>';

            htmlToAppend += '<li>';
            htmlToAppend += '</li>';


            htmlToAppend += '</ul>';

            htmlToAppend += '</div>';
            htmlToAppend += '</a>';
            htmlToAppend += '</div>';
            htmlToAppend += '</div>';

            lastPromoGroup.append(htmlToAppend);
        });

        count = count + parseInt(itemList.length);

        $('#routesCountData').attr('data_routes_count', count);

    }).fail(function (result) {

    })
}
